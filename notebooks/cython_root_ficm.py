# -*- coding: utf-8 -*-
"""
jupyter file
http://github-pages.ucl.ac.uk/rsd-engineeringcourse/ch08performance/040cython.html
"""
import numpy as np
from scipy.optimize import root

%load_ext Cython 

#######################
# OBJECTIVE FUNCTIONS AND JACOBIANS

# pure python with array argument passing 
def f(u,x,y,dim_x,dim_y,directed,L):
	""" The individual probabilities are:
		p_ij = z xi yj / ( 1+ z xi yj)
		The equation that must be solved is :	
		\sum_i \sum_j p_ij = L
	:param u: candidate solution of the equation	
	:type u: float
	:param x: fitnesses
	:type x: numpy.array
	:param y: fitnesses
	:type y: numpy.array
	:param dim_x: size of array x
	:type dim_x: integer
	:param dim_y: size of array y
	:type dim_y: integer
	:param directed: 1 for a directed network
	:type directed: integer
	:param L: number of links
	:type L: float or integer
	"""
	L=float(L)
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
				if (directed==True) and (i==j):
						pass
				else:
					xi=x[i]
					yj=y[j]		
					r+=  xi*yj/ (1.+u* xi*yj)
	return r*u-L

# cython: array are not typed
%%cython
cimport numpy as np 
cpdef f_cython_notype(double u, x, y,int dim_x,int dim_y, int directed, double L):
	""" The individual probabilities are:
		p_ij = z xi yj / ( 1+ z xi yj)
		The equation that must be solved is :	
		\sum_i \sum_j p_ij = L
	:param u: candidate solution of the equation	
	:type u: float
	:param x: fitnesses
	:type x: numpy.array
	:param y: fitnesses
	:type y: numpy.array
	:param dim_x: size of array x
	:type dim_x: integer
	:param dim_y: size of array y
	:type dim_y: integer
	:param directed: 1 for a directed network
	:type directed: integer
	:param L: number of links
	:type L: float or integer
	:returns: objective function
	"""
	cdef double r
	cdef double xi
	cdef double yj
	cdef int i 
	cdef int j
	
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
				if (directed==True) and (i==j):
						pass
				else:
					xi=x[i]
					yj=y[j]		
					r+=  xi*yj/ (1.+u* xi*yj)
	return r*u-L
	
# cython: array are typed
%%cython
cimport numpy as np
cpdef f_cython(double u, np.ndarray[double, ndim=1] x, np.ndarray[double, ndim=1] y,int dim_x,int dim_y, int directed, double L):
	cdef double r
	cdef double xi
	cdef double yj
	cdef int i 
	cdef int j
	
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
				if (directed==True) and (i==j):
						pass
				else:
					xi=x[i]
					yj=y[j]		
					r+=  xi*yj/ (1.+u* xi*yj)
	return r*u-L
# _func_dij
def _func_dij(u, dij, dim_x,dim_y, directed, L):
	"""  individual probabilities are:
		p_ij = z dij/ ( 1+ z dij)
		The equation to solve is :	
		\sum_i \sum_j p_ij = L	
		
	:param u: candidate solution of the equation
	:type u: float
	"""
	
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
				if (directed==True) and (i==j):
						pass
				else:		
					r+=  dij[i][j]/ (1.+u*dij[i][j])	
	return r*u-float(L)
# _func_dij_cython
%%cython
cimport numpy as np
cpdef _func_dij_cython(double u, np.ndarray[double, ndim=2] dij, int dim_x,int dim_y, int directed, double L):
	"""  individual probabilities are:
		p_ij = z dij/ ( 1+ z dij)
		The equation to solve is :	
		\sum_i \sum_j p_ij = L	
		
	:param u: candidate solution of the equation
	:type u: float
	"""
	cdef double r
	cdef int i 
	cdef int j
	cdef double d
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
				if (directed==True) and (i==j):
						pass
				else:	
					d = dij[i][j]
					r+=  d/ (1.+u*d)	
	return r*u-L
	

# jacobian, pure python
def _jac(u,x,y,dim_x,dim_y,directed,L):
	"""Jacobian of _func corresponds to :
		p_ij = z x_i y_j / ( 1+ z x_i y_j)
	:param u: Jacobian of the candidate solution of the equation
	:type u: float	
	:rtype: float
	:returns: jacobian
	"""
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
			if (directed==True) and (i==j):
					pass
			else:		
				r+=  x[i]*y[j]/ (1.+u* x[i]*y[j] )**2								
	return r
	
# cython: array are typed
%%cython	
import numpy as np
cimport numpy as np
cpdef _jac_cython(double u, np.ndarray[double, ndim=1] x, np.ndarray[double, ndim=1] y,int dim_x,int dim_y, int directed, double L):
	"""Jacobian of _func corresponds to :
		p_ij = z x_i y_j / ( 1+ z x_i y_j)
	:param u: Jacobian of the candidate solution of the equation
	:type u: float	
	:rtype: float
	:returns: jacobian
	"""
	cdef double r
	cdef double xi
	cdef double yj
	cdef int i 
	cdef int j	
	cdef np.ndarray rr = np.zeros(1,dtype=np.double)
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
			if (directed==True) and (i==j):
					pass
			else:		
				r+=  x[i]*y[j]/ (1.+u* x[i]*y[j] )**2											
	rr[0]=r			
	return rr	
	
# _jac_dij	
def _jac_dij(u, dij, dim_x,dim_y, directed, L):
	"""Jacobian of _func corresponds to :
		p_ij = z dij / ( 1+ z dij)
	:param u: Jacobian of the candidate solution of the equation
	:type u: float
	:returns: jacobian
	:rtype: float
	"""
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
			if (directed==True) and (i==j):
					pass
			else:
				d = dij[i][j]
				r+= d	/ (1.+u*d)**2					
	return r	

# _jac_dij_cython
%%cython	
import numpy as np
cimport numpy as np
cpdef _jac_dij_cython(double u, np.ndarray[double, ndim=2] dij, int dim_x,int dim_y, int directed, double L):
	"""Jacobian of _func corresponds to :
		p_ij = z dij / ( 1+ z dij)
	:param u: Jacobian of the candidate solution of the equation
	:type u: float
	:returns: jacobian
	:rtype: float
	"""
	cdef double r
	cdef double d
	cdef int i 
	cdef int j	
	cdef np.ndarray rr = np.zeros(1,dtype=np.double)
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
			if (directed==True) and (i==j):
					pass
			else:
				d = dij[i][j]
				r+= d	/ (1.+u*d)**2				
	rr[0]=r			
	return rr				
		
################
# COMPARISON OF SIMPLE FUNCTION CALLS
u = 0.5
dim_x = 1000
dim_y = dim_x
x = np.random.rand(dim_x)
y = np.random.rand(dim_y)
dim_z=dim_x*2; dij = np.random.rand(dim_x*dim_z).reshape((dim_x,dim_z))
directed = 0
L = 3*dim_x

# simple objective function calls
%timeit f(u,x,y,dim_x,dim_y,directed,L) # => 1 loop, best of 3: 1.09 s per loop
%timeit f_cython_notype(u,x,y,dim_x,dim_y,directed,L) #10 loops, best of 3: 122 ms per loop
%timeit f_cython(u,x,y,dim_x,dim_y,directed,L) #100 loops, best of 3: 9.66 ms per loop

%timeit _func_dij(u,dij,dim_x,dim_z,directed,L)#1 loop, best of 3: 2.74 s per loop
%timeit _func_dij_cython(u,dij,dim_x,dim_z,directed,L)#1 loop, best of 3: 487 ms per loop  !!!!!!!!!! BAD
#%timeit [ f(u,x,y,dim_x,dim_y,directed,L)  for u in np.linspace(0,1,10)]#1 loop, best of 3: 13.4 s per loop
#%timeit [ f(u,x,y,dim_x,dim_y,directed,L)  for u in np.linspace(0,1,10)]

# jacobian
%timeit _jac(u,x,y,dim_x,dim_y,directed,L) # 1 loop, best of 3: 1.79 s per loop
%timeit _jac_cython(u,x,y,dim_x,dim_y,directed,L) #100 loops, best of 3: 9.6 ms per loop

%timeit _jac_dij(u,dij,dim_x,dim_z,directed,L)# 1 loop, best of 3: 3.14 s per loop
%timeit _jac_dij_cython(u,dij,dim_x,dim_z,directed,L)#1 loop, best of 3: 485 ms per loop
###############################################################
# COMPARISON OF OPTIMIZE.ROOT CALLS
# https://docs.scipy.org/doc/scipy-0.19.0/reference/generated/scipy.optimize.root.html

###########
# f_cython
u = 0.5
dim_x = 300
dim_y = dim_x
x = np.random.rand(dim_x)
y = np.random.rand(dim_y)
directed = 0
L = 3*dim_x

# optimize.root + f_cython (objective function only=
x0 = 0.5
%timeit sol  = root(f, x0, args = (x,y,dim_x,dim_y,directed,L))#1 loop, best of 3: 8.9 s per loop
%timeit sol  = root(f_cython, x0, args = (x,y,dim_x,dim_y,directed,L))#9.66 ms per loop

# optimize.root + f_cython + jacobian
x0 = 0.5 
%timeit sol  = root(f, x0, args = (x,y,dim_x,dim_y,directed,L),	jac=_jac)#1 loop, best of 3: 9.65 s per loop
%timeit sol  = root(f_cython, x0, args = (x,y,dim_x,dim_y,directed,L), \
				jac=_jac_cython)# 100 loops, best of 3: 10.5 ms per loop
###########
# func_dij_cython
u = 0.5
dim_x = 300
dim_z=dim_x*2; dij = np.random.rand(dim_x*dim_z).reshape((dim_x,dim_z))
directed = 0
L = 3*dim_x
# optimize.root + f_cython (objective function only=
x0 = 0.5
%timeit sol  = root(_func_dij, x0, args = (dij,dim_x,dim_z,directed,L))#1 loop, best of 3: 16 s per loop
%timeit sol  = root(_func_dij_cython, x0, args = (dij,dim_x,dim_z,directed,L))#1 loop, best of 3: 482 ms per loop
# optimize.root + f_cython + jacobian
%timeit sol  = root(_func_dij, x0, args = (dij,dim_x,dim_z,directed,L),jac=_jac_dij)#1 loop, best of 3: 23.6 s per loop
%timeit sol  = root(_func_dij_cython, x0, args = (dij,dim_x,dim_z,directed,L),jac=_jac_dij_cython)#1 loop, best of 3: 674 ms per loop
