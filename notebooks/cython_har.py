# -*- coding: utf-8 -*-
"""
jupyter file
http://github-pages.ucl.ac.uk/rsd-engineeringcourse/ch08performance/040cython.html
"""
import numpy as np
import sys; sys.path.append('../experimental')
import har as cython_har
from scipy.stats import pareto
import powerlaw
import matplotlib.pylab as plt
#%load_ext Cython 

dim= 100		 
nsamp= 5000
seed=None
thin= 1	
accel=1
log=1
# choose pdf, make sure that E(X) = M/dim 
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
# pareto.pdf(x, b) = b / x**(b+1)
b=2. # NB: b must be > 1 so that E[x] exists.
k=1.0
rv = pareto(b)
M = dim * rv.mean()
x0= M * np.ones(dim)/dim
#M = s# SHOULD BE : dim * rv.mean()
f= rv.pdf


# functions
u=10.0
%timeit rv.pdf(u)
%timeit cython_har.pdf_pareto(u)
%timeit cython_har.pdf_pareto_cython(u,b,k)


# sample
#1 loop, best of 3: 2.94 s per loop
%timeit x,reject_rate = cython_har. har_cd_native(x0, dim, M,nsamp, f, seed, thin,	accel, log)
#1 loop, best of 3: 2.85 s per loop
%timeit x,reject_rate = cython_har.har_cd_native_inplace(x0, dim, M,nsamp, f, seed, thin,accel, log)
# 10 loops, best of 3: 61.9 ms per loop								
#%timeit x,reject_rate = cython_har.har_cd_native_inplace(x0, dim, M,nsamp, cython_har.pdf_pareto, seed, thin,accel, log)
%timeit x,reject_rate = cython_har.har_cd_native_inplace(x0, dim, M,nsamp, cython_har.pdf_pareto_cython, seed, thin,accel, log,b,k)
									
									
# check pdf, fit powerlaw
# fit
b=1. # NB: b must be > 1 so that E[x] exists.
dim= 10000		 
nsamp= 1E6 #dim**2
M = dim
x0= M * np.ones(dim)/dim
x,reject_rate = cython_har.har_cd_native_inplace(x0, dim, M,nsamp, cython_har.pdf_pareto_cython, seed, thin,accel, log,b,k)
x= np.array(x)


####
#powerlaw fit: Identifying the Scaling Range
# https://pypi.python.org/pypi/powerlaw
# http://nbviewer.jupyter.org/github/jeffalstott/powerlaw/blob/master/manuscript/Manuscript_Code.ipynb
# http://www.josephshaheen.com/fitting-power-law-comparing-degree-distribution-synthetic-network-collected-one/2506
# https://github.com/jothemachine/fitting-a-powerlaw/blob/master/Network_Fitting.ipynb

from powerlaw_plot import *
%matplotlib

fit = powerlaw.Fit(data, discrete=False, xmin = 0,xmax=sum(x0))
print "alpha=",fit.alpha
FigCCDFmax = fit.plot_ccdf(color='b', label=r"Empirical, no $x_{max}$")
fit.power_law.plot_ccdf(color='b', linestyle='--', ax=FigCCDFmax, label=r"Fit, no $x_{max}$")
#fit = powerlaw.Fit(data, discrete=True, xmax=1000)
#fit.plot_ccdf(color='r', label=r"Empirical, $x_{max}=1000$")
#fit.power_law.plot_ccdf(color='r', linestyle='--', ax=FigCCDFmax, label=r"Fit, $x_{max}=1000$")
fit.lognormal.plot_ccdf(ax=FigCCDFmax, color='g', linestyle='--', label='Lognormal Fit') 
fit.exponential.plot_ccdf(ax=FigCCDFmax, color='y', linestyle='--', label='Exponential Fit') 
#fit.truncated_power_law.plot_ccdf(ax=FigCCDFmax, color='m', linestyle='--', label='truncated_power_law')
plt.legend(loc="lower left")
