# -*- coding: utf-8 -*-
#  network_weights_test.py
#  
#  Copyright 2018 aurelien <aurelien@aurelien-300E4A-300E5A-300E7A-3430EA-3530EA>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sys
sys.path.append('../');sys.path.append('../cython')

import numpy as np
from transmat import *

import network,network_weights
from scipy.sparse import diags
from scipy.sparse.linalg import inv
import matplotlib.pyplot as plt

class TestClass_network_weights:
	"""
	To run all tests of the class:
	$nosetests network_weights_test.py:TestClass_network_weights
	$nosetests network_weights_test.py:TestClass_network_weights.test_mult
	$nosetests network_weights_test.py:TestClass_network_weights.test_make_dataset_sfc_block_bayesian
	"""
	def setUp(self):
		"""
		fname='../data/eurostat_network.h5'
		self.params_num = [1.0, 0.1 ,0.75 , 0.1,0.1,0.03] #k,al0,al1,al2,delta,r
		n=network.NaioNetwork()
		p={'network':{'year':'2010','geo':'CZ','nb':3,'nf':50,'nh':500,
					  'nlink_per_firm_cons': 2,  
					'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20},#3 is better for plotting
				'mc':{'niter':10}}
		nb = p['network']['nb']
		nf = p['network']['nf']
		nh = p['network']['nh']
		year = p['network']['year']
		geo = p['network']['geo']
		#L_invest = int(np.floor(p['network']['nlink_per_firm'] * nf)) # [Atalay11]		
		L_invest= int(p['network']['nlink_per_firm_invest'] *nf)
		L_cons_firm=int(p['network']['nlink_per_firm_cons']*nf)
		L_cons_hh = int(p['network']['nlink_per_hh_cons'] *nh)
		n=network.init_network_block(year=year,geo=geo,nb=nb,nf=nf,nh=nh,fname=fname,L_invest=L_invest, L_cons_firm=L_cons_firm , L_cons_hh = L_cons_hh,proxy_invest_conso=True	)
		self.A,self.B,self.C,self.D,self.E,self.F = n.sample_full_network(nb,nf,nh)
		self.n = n	
		self.nb,self.nf,self.nh = nb,nf,nh
		self.F,self.g,self.lb,self.ub = CSP_sfc_FiCM_noMLK(self.nb,self.nf,self.nh,
										self.A,self.B,self.C,self.D,self.E,
											self.params_num)
		"""
		r = network.example_init_network_block()
		p=r['p']
		nb = p['network']['nb']
		nf = p['network']['nf']
		nh = p['network']['nh']
		self.A,self.B,self.C,self.D,self.E=r['A'],r['B'],r['C'],r['D'],r['E']
		self.F,	self.g,self.lb,self.ub=	r['F'],r['g'],r['lb'],r['ub']
		#nr,nc = self.F.shape			
		nc = self.F.shape[1]
		self.mu_prior = np.ones(nc)	# another prior is in r['prior']	
		self.sig_prior = 0.1*self.mu_prior
	def setUp_rndfitness(self):
		"""
		NOT USED
		
		fname='../data/eurostat_network.h5'
		self.params_num = [1.0, 0.1 ,0.75 , 0.1,0.1,0.03] #k,al0,al1,al2,delta,r
		n=network.NaioNetwork()
		p={'network':{'year':'2010','geo':'CZ','nb':3,'nf':50,'nh':300,
					  'nlink_per_firm_cons': 2,  
					'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20,#3 is better for plotting
					'nlink_per_hh_wage':1},
				'mc':{'niter':10}}
		nb = p['network']['nb']
		nf = p['network']['nf']
		nh = p['network']['nh']
		year = p['network']['year']
		geo = p['network']['geo']
		L_invest= int(p['network']['nlink_per_firm_invest'] *nf)
		L_cons_firm=int(p['network']['nlink_per_firm_cons']*nf)
		L_cons_hh = int(p['network']['nlink_per_hh_cons'] *nh)
		L_wage = int(p['network']['nlink_per_hh_wage']*nh)
		# pareto.pdf(x, b) = b / x**(b+1) for x >= 1, b > 0.
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		b=1.5
		rv = pareto(b)
		fitness = rv.rvs(nf)
		n=network.init_network_rndfitness(year=year,geo=geo,nb=nb,nf=nf,nh=nh,
								fname=fname,
								L_invest=L_invest, L_cons_firm= L_cons_firm,
								L_cons_hh = L_cons_hh,
								L_wage= L_wage,
								fitness=fitness,proxy_invest_conso=True)
		self.A,self.B,self.C,self.D,self.E,self.F = n.sample_full_network(nb,nf,nh)
		self.n = n	
		self.nb,self.nf,self.nh = nb,nf,nh
		self.F,self.g,self.lb,self.ub = CSP_sfc_FiCM_noMLK(self.nb,self.nf,self.nh,
										self.A,self.B,self.C,self.D,self.E,
											self.params_num)
		nr,nc = self.F.shape	
		
		n = self.F.shape[1]
		self.mu_prior = np.ones(n,dtype=float)
		self.sig_prior = self.mu_prior
		"""
	def tearDown(self):
		pass
	def test_setup(self):	
		pass
	def test_mult(self):
		sig_prior_diag = diags(self.sig_prior)
		sig_A_T= sig_prior_diag.dot(self.F.transpose())	
	def test_inv(self):
		sig_prior_diag = diags(self.sig_prior)
		sig_A_T= sig_prior_diag.dot(self.F.transpose())	
		inv( self.F.dot(sig_A_T))  		
	def test_solve_weights_bayesian(self):
		mean_post,cov_post = network_weights.solve_weights_bayesian(self.F,self.g,
													self.mu_prior,
													self.sig_prior)		
	def test_solve_weights_bayesian_simple(self):
		mean_post,cov_post = network_weights.solve_weights_bayesian_simple(self.F,self.g,
													self.mu_prior)										
	def test_solve_weights_bayesian_sparse(self):
		thr=0.0001
		mean_post=network_weights.solve_weights_bayesian_sparse(self.F,self.g,threshold=thr)

	def test_system_solution_simple(self):
		mean_post,cov_post = network_weights.solve_weights_bayesian_simple(self.F,self.g,
													self.mu_prior)			
		res = self.F.dot(mean_post)	-self.g	
		plt.subplot(2,1,1)
		plt.plot(mean_post)
		plt.xlabel('posterior')
		plt.ylabel('residual')
		plt.subplot(2,1,2)
		plt.plot(res)
		plt.xlabel('equations')
		plt.ylabel('residual')
		plt.show()
																												
	def test_make_dataset_sfc_block_bayesian(self):	
		path_fname_in = '../data/eurostat_network.h5'
		path_fname_out = '../data/topo_vs_flow_sfc.h5'
		network_weights.make_dataset_sfc_block_bayesian(path_fname_in=path_fname_in,
														path_fname_out = path_fname_out)	
	def test_make_dataset_sfc_rndunif_bayesian(self):	
		path_fname_in = '../data/eurostat_network.h5'
		path_fname_out = '../data/topo_vs_flow_sfc.h5'		
		network_weights.make_dataset_sfc_rndunif_bayesian(path_fname_in=path_fname_in,
										path_fname_out = path_fname_out)	

	def test_make_dataset_sfc_rndunif_sparse(self):	
		path_fname_in = '../data/eurostat_network.h5'
		path_fname_out = '../data/topo_vs_flow_sfc.h5'		
		network_weights.make_dataset_sfc_rndunif_sparse(path_fname_in=path_fname_in,
										path_fname_out = path_fname_out)		
	def test_make_dataset_sfc_rndunif_leastnorm_pos(self):	
		path_fname_in = '../data/eurostat_network.h5'
		path_fname_out = '../data/topo_vs_flow_sfc.h5'		
		network_weights.make_dataset_sfc_rndunif_leastnorm_pos(path_fname_in=path_fname_in,
										path_fname_out = path_fname_out)
	def test_make_dataset_sfc_rndunif_nnls(self):	
		path_fname_in = '../data/eurostat_network.h5'
		path_fname_out = '../data/topo_vs_flow_sfc.h5'		
		network_weights.make_dataset_sfc_rndunif_nnls(path_fname_in=path_fname_in,
										path_fname_out = path_fname_out)
										


