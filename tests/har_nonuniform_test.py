# -*- coding: utf-8
#  har_nonuniform_test.py
#  
#  Copyright 2017 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
To run just a single test:
	$nosetests har_nonuniform_test.py:TestClass_har_nonuniform.test_init
	
https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.genpareto.html#scipy.stats.genpareto
"""

"""
TODO:
"""

import sys
sys.path.append('../');sys.path.append('../experimental')
import numpy as np
from transmat import example_CSP_problems, get_bmw_labels, bmw_sym_1F1BnW
from har_nonuniform import *
from scipy.stats import genpareto,expon,lognorm,gamma
import time
from gpareto import histogram_from_tabulation,income_tabulation, fit_pareto_tail, piecewise_pareto_vec,get_piecewise_pareto_with_parameters
from utils import fig_with_function_name


class TestClass_gpareto:
	"""	
	To run just a single test:	
	$nosetests har_nonuniform_test.py:TestClass_gpareto
	"""
	def setUp(self):
		"""
		"""
		pass		
	def tearDown(self):
		pass

	def test_get_piecewise_pareto_with_parameters(self):
		"""
		check type of output
		"""		
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		# Attention: x0 is reshaped as a row vector, to respect the convention in har.py:hitandrun
		x0 = x0.reshape(( 1,x0.shape[0] ))				
		pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False,idx = idx_WBs)
		assert(isinstance(pdf(x0),float ))

	def test_get_piecewise_pareto_with_parameters_rescale(self):
		"""
		test rescaling
		"""		
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		# Attention: x0 is reshaped as a row vector, to respect the convention in har.py:hitandrun
		x0 = x0.reshape(( 1,x0.shape[0] ))		
		# rescale
		#r= 0.1
		r = 2.507933048219346e-05
		pdf_r,x_max_r,yav_r,total_income_r = get_piecewise_pareto_with_parameters(onedim=True,rescale_factor=r)		
		pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=True)		
		# plot
		n=1000
		xtest = np.linspace(0,x_max+100,n)
		y = np.zeros((n,2))
		for i in range(xtest.shape[0]):		
			y[i,0] = pdf( [[ xtest[i] ]] )
			y[i,1] = pdf_r( [[ xtest[i]*r ]] )
			print xtest[i], xtest[i] *r, pdf( [[ xtest[i] ]] ) , pdf_r( [[ xtest[i] *r ]]  )
			#assert( np.allclose( pdf([[ xtest[i] ]])  , pdf_r([[ xtest[i] *r ]]) , atol=1E-4 )  ) 
		plt.plot(r*xtest,y[:,0],r*xtest,y[:,1])	
		plt.show()
		

class TestClass_har_nonuniform:
	"""	
	To run just a single test:
	$nosetests har_nonuniform_test.py:TestClass_har_nonuniform.test_init
	"""
	def setUp(self):
		"""
		"""
		self.tol = 1e-4
		self.nsamp = 1000
	def tearDown(self):
		pass

	def test_init(self):
		"""
		"""
		C,b,F,g,x0=example_CSP_problems(3)
		h=har(C,b,F,g,x0)
		h.init()
		assert(True)

	def test_basis(self):
		"""
		check that vectors D* y' , whith y' randomly chosen, is such that FDy'=0
		"""
		C,b,F,g,x0=example_CSP_problems(3)
		h=har(C,b,F,g,x0)	
		h._createBasis()
		D = h.basis
		y=np.random.uniform(size=D.shape[1])
		w0= np.dot(D, y.reshape((len(y),1)) )
		delta = np.dot(F,w0)
		assert(np.all( abs(delta)<self.tol ))		
	def test_w_equality(self):
		"""		
		check that w=Finv*g + D y' , whith y' randomly chosen, is such that Fw=g
		"""
		C,b,F,g,x0=example_CSP_problems(3)		
		h=har(C,b,F,g,x0)	
		h._createBasis()
		D = h.basis
		y=np.random.uniform(size=D.shape[1])
		w= np.dot(h.F_pinv,g) + np.dot(D, y.reshape((len(y),1)) )
		delta = -g+ np.dot(F,w) 
		#if(np.any(delta>0)):
		#		raise ValueError('equ Fw-g=0 is not respected')
		assert(np.all( abs(delta)<self.tol ))			
	def test_unif_inequality_native(self):
		"""
		test inequality in space ??
		"""		
		n=20; nsamp = n**4
		basis = np.eye(n)
		translation = np.zeros(n).reshape((n,1))
		debug=True
		uniform=True
		# no init!!		
		A = np.vstack((-np.eye(n), np.eye(n)))
		b=10*np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])	
		x0 = 1./n*np.ones(n).reshape([n,1])
		# sample only the ineq: Ay<=b
		x,reject_rate = cython_har.hitandrun(A,b,x0,nsamp,None,uniform,basis,translation)		
		# plot	
		h=har(0,0,0,0,0,uniform=False)
		h.reject_rate=reject_rate
		h.plot_metrics(x)
	def test_pareto_inequality_native(self):
		"""
		test inequality in space ??
		"""		
		n=20; nsamp =n**4
		basis = np.eye(n)
		translation = np.zeros(n).reshape((n,1))
		debug=True
		uniform=False
		pdf = cython_har.pdf_pareto_all
		# no init!!		
		A = np.vstack((-np.eye(n), np.eye(n)))
		b=10*np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])	
		x0 = 1./n*np.ones(n).reshape([n,1])
		# sample only the ineq: Ay<=b
		x,reject_rate = cython_har.hitandrun(A,b,x0,nsamp,pdf,uniform,basis,translation)		
		# plot	
		h=har(0,0,0,0,0,uniform=False)
		h.reject_rate=reject_rate
		h.plot_metrics(x)
	def test_piecewise_1D_inequality_native(self):
		"""
		test inequality only, with piecewise constant pdf 
		"""				
		n=20; nsamp = n**4			
		basis = np.eye(n)
		translation = np.zeros(n).reshape((n,1))
		debug=True
		uniform=False
		# get histogram
		thr,perc, b, yav,total_income,nb_household=income_tabulation()	
		xmax  = np.max(thr)+1000
		h,width= histogram_from_tabulation(thr,perc,xmax)
		Mtot = xmax
		# prepare piecewise function from histogram
		eps = 1E-20
		data = np.vstack((h,thr)).T
		data=np.vstack((np.array([eps,0]), data, np.array([np.nan,xmax])))
		def pdf(x):			
			# only 1 variable is pdf constrained
			return cython_har.piecewise(x[0][0],data)
		# no init!!				
		A = np.vstack((-np.eye(n), np.eye(n)))
		b=Mtot*np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])	# scale to max wage threshold
		# x0 needs not to check any equality condition		
		x0 = 20000*np.ones(n).reshape([n,1])
		# sample only the ineq: Ay<=b
		x,reject_rate = cython_har.hitandrun(A,b,x0,nsamp,pdf,uniform,basis,translation)				
		# plot	
		h=har(0,0,0,0,0,uniform=False)
		h.reject_rate=reject_rate
		h.plot_metrics(x)
	def test_piecewise_pareto_1D_inequality_native(self):
		"""
		test inequality only, with piecewise constant pdf 
		pdf constraint on DIM=1 ONLY
		"""				
		n=20; nsamp = n**4			
		basis = np.eye(n)
		translation = np.zeros(n).reshape((n,1))
		debug=True
		uniform=False
		# get histogram
		thr,perc, b, yav,total_income,nb_household=income_tabulation()	
		xmax  = np.max(thr)+1000
		h,width= histogram_from_tabulation(thr,perc,xmax)
		Mtot = xmax
		# prepare piecewise function from histogram
		eps = 1E-20
		data = np.vstack((h,thr)).T
		data=np.vstack((np.array([eps,0]), data, np.array([np.nan,xmax])))
		# fit pareto params 
		j=-2
		a,k=fit_pareto_tail(thr,perc,j)
		def pdf(x):			
			# only 1 variable is pdf constrained
			return cython_har.piecewise_pareto(x[0][0],data,thr[j], k , a)
		# no init!!				
		A = np.vstack((-np.eye(n), np.eye(n)))
		b=Mtot*np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])	# scale to max wage threshold
		# x0 needs not to check any equality condition		
		x0 = 20000*np.ones(n).reshape([n,1])
		# sample only the ineq: Ay<=b
		x,reject_rate = cython_har.hitandrun(A,b,x0,nsamp,pdf,uniform,basis,translation)				
		# plot	
		h=har(0,0,0,0,0,uniform=False)
		h.reject_rate=reject_rate
		h.plot_metrics(x)			
	def test_piecewise_pareto_nD_inequality_native(self):
		"""
		test inequality only, with piecewise constant pdf +pareto tail
		pdf constraint on ALL dimensions
		"""				
		n=10; nsamp = 10*n**4 
		basis = np.eye(n)
		translation = np.zeros(n).reshape((n,1))
		debug=True
		uniform=False
		# get histogram
		thr,perc, b, yav,total_income,nb_household=income_tabulation()	
		xmax  = np.max(thr)+1000
		h,width= histogram_from_tabulation(thr,perc,xmax)
		Mtot = xmax
		# prepare piecewise function from histogram
		eps = 1E-20
		data = np.vstack((h,thr)).T
		data=np.vstack((np.array([eps,0]), data, np.array([np.nan,xmax])))
		# fit pareto params 
		j=-2
		a,k=fit_pareto_tail(thr,perc,j)
		def pdf(x):			
			# ALL variabls are pdf constrained
			return np.prod(cython_har.piecewise_pareto_vec(x[0],data,thr[j], k , a))
		# no init!!				
		A = np.vstack((-np.eye(n), np.eye(n)))
		b=Mtot*np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])	# scale to max wage threshold
		# x0 needs not to check any equality condition		
		x0 = 20000*np.ones(n).reshape([n,1])
		# sample only the ineq: Ay<=b
		x,reject_rate = cython_har.hitandrun(A,b,x0,nsamp,pdf,uniform,basis,translation)				
		# plot	
		h=har(0,0,0,0,0,uniform=False)
		h.reject_rate=reject_rate
		h.plot_metrics(x)	
	def test_piecewise_pareto_nD_eqineq_native(self):
		"""
		test full CSP problem (eq + ineq), with piecewise constant pdf +pareto tail
		pdf constraint on ALL dimensions
		"""
		pass							
	def test_pareto_inequality_purepython(self):
		"""
		test inequality only with pareto distrib
		"""
		n=2; nsamp = 1000
		#
		h=har(0,0,0,0,0,uniform=False)	
		h.basis = np.eye(n)
		h.translation = np.zeros(n).reshape((n,1))
		h.debug=True
		uniform=False
		# no init!!
		pdf = pdf_pareto_one
		n=2; nsamp = 500
		A = np.vstack((-np.eye(n), np.eye(n)))
		b=10*np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])	
		x0 = 1./n*np.ones(n).reshape([n,1])
		# sample only the ineq: Ay<=b
		x = h._hitandrun(A,b,x0,nsamp,pdf,uniform,h.basis,h.translation)
		px = np.zeros(nsamp)
		for i in range(nsamp):
			px[i]=pdf(x[i,:])
		plt.scatter(x[:,0], x[:,1], c=px, alpha=1)
		plt.show()
		print "h.reject_rate=",h.reject_rate
		#assert((h.uniform==False)&(h.reject_rate<0.3))
		assert(False)
	def test_sampling_uniform(self):
		"""
		test uniform sampling
		"""
		assert(True)
	def test_sampling_acor(self):
		"""
		test that autocorrelation decreases fast
		"""
		assert(True)	

	def test_unif_gibbs(self):
		"""
		uniform sampling expected, but in high dim mixing is too slow
		"""		
		C,b,F,g,x0=example_CSP_problems("gibbs30")
		h=har(C,b,F,g,x0,uniform=True)	
		h.init()
		x=h.sample(10000,native=True)
		f=fig_with_function_name()		
		h.plot_metrics(x)
		assert(h.reject_rate<0.3)
	def test_unif_cube5(self):
		"""
		uniform sampling. 
		"""
		C,b,F,g,x0=example_CSP_problems("cube",n=5)
		h=har(C,b,F,g,x0,uniform=True)	
		h.init()		
		x=h.sample(5000)
		f=fig_with_function_name()		
		h.plot_metrics(x)		
		assert(h.reject_rate<0.01)
		
	def test_noeq_nonunif_pareto_cube_1component(self):
		"""
		no equality in this test; only pareto + ineq
		"""
	def test_pdf_pareto(self):
		"""
		test the pareto probability density function
		"""	
		n= 1000; dim=200
		x = 2*np.random.rand(n*dim).reshape((n,dim))	
		px = np.zeros(n)
		#pdf=pdf_normal_all
		#pdf=pdf_normal_one
		#pdf=pdf_pareto_one
		pdf=pdf_pareto_all
		#pdf=pdf_uniform
		for i in range(n):
			px[i]=pdf(x[i,:])
		plt.scatter(x[:,0], x[:,1], c=px, alpha=1)
		plt.show()
		assert(True)
	
	def test_pareto_one_cube_lowdim_purepython(self):
		"""
		nonuniform sampling, pareto distribution along one component, low dimension.
		must converge to pareto.
		check: first component ~ pareto; others more uniform
		"""
		pdf = pdf_pareto_one
		C,b,F,g,x0=example_CSP_problems("cube",n=3)
		h=har(C,b,F,g,x0,uniform=False)	
		h.init()
		t=time.time()
		x=h.sample(2000, pdf,native=False)		
		t = time.time()	-t
		f=fig_with_function_name( "\nt="+str(t)+ ' sec')		
		h.plot_metrics(x)		
		assert(h.reject_rate<0.3)	
	def test_pareto_one_cube_hidim_purepython(self):
		"""
		nonuniform sampling, pareto distribution along one component, hi dimension.
		must converge to pareto.
		check: first component ~ pareto; others more uniform
		"""
		pdf = pdf_pareto_one
		#pdf = pdf_normal_one
		C,b,F,g,x0=example_CSP_problems("cube",n=30)
		h=har(C,b,F,g,x0,uniform=False)	
		h.init()
		t=time.time()
		x=h.sample(2000, pdf,native=False)
		t = time.time()	-t		
		f=fig_with_function_name("\nt="+str(t)+ ' sec')		
		h.plot_metrics(x)		
		assert(h.reject_rate<0.3)
			
	def test_pareto_one_cube_hidim_native(self):
		"""
		nonuniform sampling, pareto distribution along one component, hi dimension.
		must converge to pareto.
		check: first component ~ pareto; others more uniform
		"""		
		dim=20
		nsamp=dim**4
		#pdf = cython_har.pdf_pareto_all
		pdf = cython_har.pdf_pareto_one
		C,b,F,g,x0=example_CSP_problems("cube",n=dim)
		h=har(C,b,F,g,x0,uniform=False)	
		h.init()
		t=time.time()
		x=h.sample(nsamp, pdf,native=True)
		t = time.time()	-t		
		f=fig_with_function_name("\nt="+str(t)+ ' sec')		
		h.plot_metrics(x)		
		assert(h.reject_rate<0.3)	

	def test_unif_cube_hidim_native(self):
		"""
		uniform sampling, hi dimension.
		must converge to uniform if sampling time long enough		
		"""		
		dim=20
		nsamp=dim**4
		C,b,F,g,x0=example_CSP_problems("cube",n=20)
		h=har(C,b,F,g,x0,uniform=True)	
		h.init()
		t=time.time()
		x=h.sample(nsamp, None,native=True)
		t = time.time()	-t		
		f=fig_with_function_name("\nt="+str(t)+ ' sec')		
		h.plot_metrics(x)		
		assert(h.reject_rate<0.3)
		
	def test_pareto_piecewise_1D_cube_hidim_native(self):
		"""
		nonuniform sampling, pareto distribution along one component, hi dimension.
		CSP problem= equality + inequality. 		
		check: first component ~ piecewise pareto; others more uniform
		OBSERVATION: there is large pie to share; agent 0 is contrained, the
					others have a higher mean value
		"""		
		dim=20
		nsamp=dim**4
		pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=True)
		C,b,F,g,x0=example_CSP_problems("cube",n=dim)
		b= x_max*b # rescale the inequality constraint
		# shouldn't we rescale x0 also ? 
		h=har(C,b,F,g,x0,uniform=False)	
		h.init()
		t=time.time()
		x=h.sample(nsamp, pdf,native=True)
		t = time.time()	-t		
		f=fig_with_function_name("\nt="+str(t)+ ' sec')		
		h.plot_metrics(x)		
		assert(h.reject_rate<0.3)		
	def test_pareto_piecewise_nD_cube_hidim_native(self):
		"""
		nonuniform sampling, pareto distribution along one component, hi dimension.
		CSP problem= equality + inequality. 		
		check: first component ~ piecewise pareto; others more uniform
		OBSERVATION: !! rejection rate becomes prohibitive, e.g. dim=10 !!!
		OBSERVATION: les courbes ne sont pas symétriques (NB: la contrainte non plus dans ce cas)
		"""		
		dim=10
		nsamp=30*dim**2
		pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False)
		C,b,F,g,x0=example_CSP_problems("cube",n=dim)
		b= x_max*b # rescale the inequality constraint
		x0 = x_max* x0 
		h=har(C,b,F,g,x0,uniform=False)	
		h.init()
		t=time.time()
		x=h.sample(nsamp, pdf,native=True)
		print x
		t = time.time()	-t		
		f=fig_with_function_name("\nt="+str(t)+ ' sec')		
		h.plot_metrics(x)		
		assert(h.reject_rate<0.3)		
	def test_BMW_error_QR(self):
		"""
		check relative error for basis vectors provided by QR decomposition		
		count the number of basis vectors that obew Fw< tol, must be >= dim
		"""	
		nb,nf,nw=1,1,9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		#F=F.todense()
		F_pinv = np.linalg.pinv(F)				
		nr=F_pinv.shape[0]
		IminusFpinvF = np.eye(nr) - np.dot(F_pinv, F)
		q,r = np.linalg.qr( IminusFpinvF  )	
		#d=nw-1 # can be shown by gaussian elimination 		
		#basis= q[:,0:d]
		d = q.shape[1]
		basis = q 
		err_rel_max = 0.01 
		err_abs_max = 5E-2 
		err_rel=np.zeros(d)
		err_abs=np.zeros(d)
		for j in range(d):
			delta = np.dot(F, basis[:,j].reshape(( basis.shape[0] ,1)) ) 			
			err_abs[j] =np.linalg.norm(delta)
			err_rel[j] = np.linalg.norm(delta)/np.linalg.norm(x0)
			print "j=",j," d=",d," err_rel=",err_rel				
		plt.subplot(2,1,1)
		plt.plot(err_abs)
		plt.subplot(2,1,2)	
		plt.plot(err_rel)
		plt.show()
		assert(np.sum(1*(err_abs< err_abs_max)) >= nw-1 )	
	def test_BMW_QR_basis_constant_sums(self):
		"""
		check that basis vectors for equality Fw=g respect equality constraints
		"""	
		nb,nf,nw=1,1,9		
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		dim=nw-1				
		h=har(C,b,F,g,x0,uniform=True,dim=dim)	
		h.init()
		d=h.basis.shape[1]	
		# indices
		l,ll=get_bmw_labels(nb,nf,nw)
		idx_M = np.array(ll)=="M"
		for j in range(d):
			v = h.basis[:,j]			
			# check sum Mi=Mtot				
			Mi= (v + np.dot(h.F_pinv, g).flatten())[idx_M]
			delta = np.abs(Mtot - np.sum(Mi) )
			#print "j, delta,Mtot,sum=" ,j, delta , Mtot, v[idx_M] + g			
			assert(delta<1E-3)
				
	def test_BMW_1b1fnW_dimension_sampling_space(self):
		"""
		expect dim(F) = nw-1 in this case
		"""
		nb,nf,nw=1,1,9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		h=har(C,b,F,g,x0,uniform=False)	
		h.init()
		d=h.basis.shape[1]	
		print "d=",d
		assert(d==nw-1)	
	def test_BMW_x0_lb_ub(self):
		"""
		check that upper bound is always greater than eps+lower bound in full space
		"""
		eps=1E-3
		nw=9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		n = b.shape[0] 
		assert(n%2==0)
		lb = b[0:n/2] ; ub = b[n/2:n]
		delta = ub-lb
		print "delta",delta
		assert(np.all(delta>eps))
	def test_BMW_y0_lb_ub(self):
		"""
		check that upper bound is always greater than eps+lower bound in reduced space
		PROBLEM: in 
		"""
		eps=1E-3
		nw=9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		dim=nw-1
		h=har(C,b,F,g,x0,uniform=True,dim=dim)
		h.init()	
		A = h.A		
		b = h.b_new	
		n=b.shape[0]
		assert(n%2==0)
		lb = b[0:n/2] ; ub = b[n/2:n]
		delta = ub-lb
		print "delta",delta
		assert(np.all(delta>eps))	
		raise NotImplementedError	
	def test_BMW_y0(self):
		"""
		check that y0 respects the constraint up to some threshold: b-A*y0 + eps >= 0 
		see code by Cousins	https://fr.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/43596/versions/10/previews/MFE_version/preprocess.m/index.html?access_key=
		"""
		eps_cutoff = 1e-7;
		nw=9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		dim=nw-1
		h=har(C,b,F,g,x0,uniform=True,dim=dim)
		h.init()	
		A = h.A		
		b = h.b_new		
		y0 = h.y0.reshape((dim,1))		
		delta = b.flatten() - np.dot(A,y0).flatten()
		print "np.min(delta)", np.min(delta), " -eps_cutoff", -eps_cutoff, " delta=",delta
		assert(False)
		#assert(np.min(delta) > -eps_cutoff)
		
	def test_BMW_get_y0_linprog(self):
		"""
		find a point y0 inside the polytope
		see code by Cousins	https://fr.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/43596/versions/10/previews/MFE_version/preprocess.m/index.html?access_key=
		"""
		eps_cutoff = 1e-7;
		nw=9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		dim=nw-1
		h=har(C,b,F,g,x0,uniform=True,dim=dim)
		h.init()	
		A = h.A		
		b = h.b_new		
		bmw = bmw_sym_1F1BnW(nw)
		x0_optim = bmw.get_x0_linprog(A,b)		
		delta = b.flatten() - np.dot(A,x0_optim)
		assert(np.min(delta) > -eps_cutoff)	
		
	def test_BMW_1b1fnW_bounds_l0_l1(self):
		"""
		check that l0 and l1 are sufficiently separated
		"""
		nb,nf,nw=1,1,9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		dim=nw-1
		nsamp=1000#*dim**4
		h=har(C,b,F,g,x0,uniform=True,dim=dim)
		h.init()
		# 		
		# this is from _hitandrun
		A = h.A		
		b = h.b_new
		y=h.y0
		dim_y = A.shape[1]
		d = np.random.normal(size=dim_y).reshape((dim_y,1))
		# this is from _bounds				
		a = (b - np.dot(A,y)).flatten()
		c = np.dot(A,d).flatten()
		nrow = a.shape[0]
		l0=-np.inf
		l1=np.inf
		for i in range(nrow):
			if (c[i] < 0.0): 
				t = a[i] / c[i]
				if (t > l0): 
					l0 = t				
			elif (c[i] > 0.0) :
				t = a[i] / c[i]
				if ( t < l1) :
					l1 = t
		print "i=",i," l0,l1=",	l0,l1
		assert(l1-l0>1E-3)

	def test_BMW_1b1fnW_uniform_native(self):
		"""
		bmw 1b1fnW + uniform_native
		"""
		nb,nf,nw=1,1,9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		dim=nw-1
		nsamp=10000 #dim**4
		# rescale pdf and ub constaint		
		# hit and run
		#h=har(C,b,F,g,x0,uniform=True,dim=dim)	
		h=har(C,b,F,g,None,uniform=True,dim=dim)	#x0=NONE
		h.init()
		t=time.time()
		x=h.sample(nsamp, native=True)
		t = time.time()	-t		
		f=fig_with_function_name("\nt="+str(t)+ ' sec')		
		h.plot_metrics(x)		
		assert(h.reject_rate<0.01)
	def test_BMW_1b1fnW_pareto_piecewise_nD_lodim_native(self):
		"""
		bmw 1b1fnW + pareto_piecewise nD (i.e all income have a constrained pdf)
		"""
		nb,nf,nw=1,1,9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		dim=nw-1
		nsamp=10000 #30*dim**4
		# rescale pdf rather than CSP problem (using income average, and not Mtot)
		thr,perc, bb, yav, total_income,nb_household = income_tabulation() # yav is needed before calling get_..
		scale = WBs_avg / yav
		# constrain the pdf of ALL INCOMES (which indices are given by idx_WBs)
		pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False,idx = idx_WBs,
																			rescale_factor=scale)				
		# hit and run
		h=har(C,b,F,g,x0,uniform=False,dim=dim)			
		h.init()
		t=time.time()
		x=h.sample(nsamp, pdf,native=True)
		t = time.time()	-t		
		f=fig_with_function_name("\nt="+str(t)+ ' sec')		
		h.plot_metrics(x)		
		assert(h.reject_rate<0.3)
	def test_BMW_1b1fnW_constant_sum_uniform(self):
		"""
		check that total income is constant. uniform pdf
		"""
		nb,nf,nw=1,1,9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		dim=nw-1
		nsamp=50000		
		# remove constraints
		C= C[0:-1,:]		
		b= b[0:-1,:]		
		# hit and run
		h=har(C,b,F,g,x0,uniform=True,dim=dim)			
		h.init()
		t=time.time()
		x=h.sample(nsamp,native=True)
		sum_WBs = np.sum(x[:,idx_WBs],axis=1)
		istart=nf+nw+nf+nf+nw+nf+nf+nf+nb+nf+nw+nb
		idx_M = slice(istart,istart+nw)
		sum_M = np.sum(x[:, idx_M],axis=1)
		delta = sum_WBs - sum_WBs[0]		
		plt.subplot(3,1,1)
		plt.plot(sum_WBs)
		plt.subplot(3,1,2)
		plt.plot(sum_M)
		plt.subplot(3,1,3)
		plt.plot(x[:, idx_M])
		plt.show()
		assert(np.all(np.abs(delta) < 1E-3))			
	def test_BMW_1b1fnW_constant_sum_nonuniform(self):
		"""
		check that total income is constant. nonuniform pdf
		"""
		nb,nf,nw=1,1,9
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		dim=nw-1
		nsamp=30000
		# rescale pdf rather than CSP problem (using income average, and not Mtot)
		thr,perc, bb, yav, total_income,nb_household = income_tabulation() # yav is needed before calling get_..
		scale = WBs_avg / yav
		# constrain the pdf of ALL INCOMES (which indices are given by idx_WBs)
		pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False,idx = idx_WBs,
																			rescale_factor=scale)				
		# hit and run
		h=har(C,b,F,g,x0,uniform=False,dim=dim)			
		h.init()
		t=time.time()
		x=h.sample(nsamp, pdf,native=True)
		sum_WBs = np.sum(x[:,idx_WBs],axis=1)
		istart=nf+nw+nf+nf+nw+nf+nf+nf+nb+nf+nw+nb
		idx_M = slice(istart,istart+nw)
		sum_M = np.sum(x[:, idx_M],axis=1)
		delta = sum_WBs - sum_WBs[0]
		"""
		plt.subplot(2,1,1)
		plt.plot(sum_WBs)
		plt.subplot(2,1,2)
		plt.plot(sum_M)
		plt.show()
		"""
		assert(np.all(np.abs(delta) < 1E-3))		
			
	def test_BMW_1b1fnW_pareto_piecewise_1D_lodim_native_relative_error(self):
		"""
		check relative error
		"""			
		# constrain the pdf of 1 INCOME ONLY, which index is given by idx_WBs
		pass
	def test_BMW_1b1fnW_pareto_piecewise_1D_lodim_native(self):		
		"""
		bmw 1b1fnW + pareto_piecewise nD (i.e ALL incomes have a constrained pdf)
		slow convergence is expected if dim high
		Q: LOW DIM ????
		PROBLEME: 
		"""
		# CONSTRAIN INCOME ONLY !!!!!!!!!!!!!!!!!!!
		raise NotImplementedError('rescaling')
	def test_BMW_1b1fnW_pareto_piecewise_1D_hidim_native(self):		
		"""
		bmw 1b1fnW + pareto_piecewise 1D
		slow convergence is expected if dim high
		"""	
		raise NotImplementedError('rescaling')
	def test_BMW_1b1fnW_pareto_piecewise_nD_hidim_native(self):
		"""
		bmw 1b1fnW + pareto_piecewise nD
		slow convergence is expected if dim high
		"""	
		raise NotImplementedError('rescaling')
	
	def test_har_rand(self):	
		"""
		"""
		from har import test_rand
		test_rand()
		assert(False)
		
class TestClass_har_hd:
	"""	
	To run just a single test:
	$nosetests har_nonuniform_test.py:TestClass_har_hd.test_pareto
	"""
	def setUp(self):
		"""
		"""
		self.tol = 1e-4
		self.nsamp = 1000
	def tearDown(self):
		pass				
	def test_uniform_ineq(self):
		"""		
		sample only the ineq: Ay<=b
		NO EQUALITY enforced in this test
		pdf = uniform
		"""		
		n=20; nsamp =1E4 #n**2
		basis = np.eye(n)
		translation = np.zeros(n)
		def pdf(x):						
			return 1.
		M=10.
		A  = np.vstack((-np.eye(n), np.eye(n)))
		b  = np.hstack((np.zeros(n),M*np.ones(n)))
		x0 = M/float(n)*np.ones(n)		
		seed=0
		thin=10
		log=False
		x,reject_rate=cython_har.har_hd( A, b, x0, nsamp, pdf,  basis, 
										translation, seed, thin, log)		
		# plot	
		h=har(0,0,0,0,0)
		h.reject_rate=reject_rate
		h.plot_metrics(x)	
	def test_pareto_ineq(self):
		"""		
		sample only the ineq: Ay<=b
		NO EQUALITY enforced in this test
		pdf = pareto
		WARNING: since x >1 for pareto(2), the constraint is  1<= x <= M 
		"""		
		n=20; nsamp =100 #n**2
		basis = np.eye(n)
		translation = np.zeros(n)		
		b=2. # NB: b must be > 1 so that E[x] exists.
		rv = pareto(b)
		def pdf(x):						
			return np.prod( rv.pdf(x)) # + 1E-10		
		def logpdf(x):						
			return np.sum( rv.logpdf(x))	
		M  = n * rv.mean()
		A  = np.vstack((-np.eye(n), np.eye(n)))
		b  = np.hstack((np.ones(n),M*np.ones(n))) # 1<= . <= M 
		x0 = rv.mean()*np.ones(n) 
		seed=0
		thin=10
		log=False
		x,reject_rate=cython_har.har_hd( A, b, x0, nsamp, pdf,  basis, 
										translation, seed, thin, log)		
		# plot	
		h=har(0,0,0,0,0)
		h.reject_rate=reject_rate
		h.plot_metrics(x,rv.pdf)			
	def test_har_sample():
		pass
		
	def test_WID():
		year = 2010
		pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea = get_WID_FR(year,value='wealth')
		
		
class TestClass_har_cd:
	"""	
	To run just a single test:
	$nosetests har_nonuniform_test.py:TestClass_har_cd.test_init
	"""
	def setUp(self):
		"""
		"""
		self.tol = 1e-4
		self.nsamp = 1000
	def tearDown(self):
		pass		
	def test_pareto_native(self):
		from har import har_cd_native
		dim= 10		 
		nsamp= 1000
		# choose pdf, make sure that E(X) = M/dim 
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		# pareto.pdf(x, b) = b / x**(b+1)
		b=2. # NB: b must be > 1 so that E[x] exists.
		rv = pareto(b)
		M = dim * rv.mean()
		x0= M * np.ones(dim)/dim				
		f= rv.pdf
		seed=None
		thin= 10
		x,reject_rate = har_cd_native(x0, dim, M,nsamp, f, seed, thin)		
		assert(True)
		
	def test_pareto_empirical_mean(self):
		"""
		np.mean(x[:,0])
		compute empirical mean and compare to theoretical
		"""
		from har import har_cd_native,har_cd_np
		dim= 20	
		f_thin = thin_function()	 
		thin = 10#f_thin(dim)
		
		nsamp= 100
		# choose pdf, make sure that E(X) = M/dim 
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		# pareto.pdf(x, b) = b / x**(b+1)
		b_ =  [1.02, 1.05, 1.5 , 2]
		err_= []
		accel=0
		lb=0
		log=0
		for b in b_:			
			rv = pareto(b)
			M = dim * rv.mean()
			x0= M * np.ones(dim)/dim				
			f= rv.pdf
			seed=None			
			#x,reject_rate = har_cd_native(x0, dim, M,nsamp, f, seed, thin,accel)
			#x,reject_rate = har_cd_np(x0, dim, M,nsamp, f, seed, thin,accel)
			x,reject_rate = har_cd_np(x0, dim,lb, M,nsamp, f, seed, thin,accel,log)
			#err = 100. * (np.mean(x) - rv.mean())/rv.mean()
			err = 100. * (np.mean(x[:,0]) - rv.mean())/rv.mean()
			print err
			err_.append(err)
		print "thin=",thin," err_=",err_	
		assert(True)	
	
	def test_pareto_empirical_excess(self):
		"""
		remove the condensed max
		then, compute 
		np.mean(x,axis=1)
		compute empirical mean and compare to theoretical
		"""
		from har import har_cd_native,har_cd_np
		dim= 20	
		f_thin = thin_function()	 
		thin = 10#f_thin(dim)		
		nsamp= 100
		
		# choose pdf, make sure that E(X) = M/dim 
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		# pareto.pdf(x, b) = b / x**(b+1)
		b_ =  [2]#[1.02, 1.05, 1.5 , 2]
		mean_= []
		excess=[]
		accel=1
		seed=None			
		log=0	
		lb=0		
		err_=[]
		for b in b_:	
			# sample data		
			rv = pareto(b)
			M = dim * rv.mean()
			x0= M * np.ones(dim)/dim				
			f= rv.pdf						
			x,reject_rate = har_cd_np(x0, dim,lb, M,nsamp, f, seed, thin,accel,log)
			# compute IPR
			# sort, remove max and compute mean
			x.sort() # in-place
			m = np.mean(x[nsamp/2:,0:-1], axis=1)
			exc = 	np.mean(x[nsamp/2:,-1])		
			err_.append(m)
			excess.append(exc)
		print " err_=",err_	, " excess=",excess
		assert(True)
		
	def test_pareto_plot(self):	
		"""
		har_cd_nd + pareto; 
		"""
		from har import har_cd_np
		dim= 30	 
		nsamp= 60
		# choose pdf, make sure that E(X) = M/dim 
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		# pareto.pdf(x, b) = b / x**(b+1)
		#alpha = .75; b=1. # NB: b must be > 1 so that E[x] exists. !!!TOO LONG!!!!
		alpha = .75; b=2. 
		rv = pareto(b)
		M = (dim * rv.mean()) * 1. * alpha
		x0= M*np.ones(dim)/dim				
		seed=None
		thin= 10
		accel=1
		log= 0
		lb=0.
		if(log==1): 
			f= rv.logpdf
		else:	
			f= rv.pdf
		x,reject_rate = har_cd_np(x0, dim,lb, M,nsamp, f, seed, thin,accel,log)
		# plot	
		p = 4	
		plt.subplot(p,1,1)
		#plt.hist(x.flatten(), bins=30, normed = True, log=True)		
		plt.hist(x[:,0], bins=30, normed = True, log=True)		
		xx=np.linspace(0,M,100); plt.plot(xx  , rv.pdf(xx) )
		plt.subplot(p,1,2)
		plt.plot(x)
		plt.subplot(p,1,3)
		plt.acorr(x[:,0],detrend=plt.mlab.detrend_mean,normed=True, maxlags=50, lw=2)
		plt.subplot(p,1,4)
		plt.plot(IPR(x))
		title = "dim=%d M= %f mean= %f reject=%f"%(dim,M,rv.mean(),reject_rate)
		plt.suptitle(title)
		plt.show()
		assert(True)
		
	def test_exp(self):	
		"""
		har_cd_nd + exponential
		"""
		from har import har_cd_np
		dim= 300	 
		nsamp= 1000
		# choose pdf, make sure that E(X) = M/dim 
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		# pareto.pdf(x, b) = b / x**(b+1)
		alpha = 1; b=1. # NB: b must be > 1 so that E[x] exists.
		lb=1.
		rv = expon(b)
		M = (dim * rv.mean()) * 1. * alpha
		x0= M*np.ones(dim)/dim				
		
		seed=None
		thin= 50
		accel=0
		pdf= rv.pdf
		log=0
		x,reject_rate = har_cd_np(x0, dim, lb,M,nsamp, pdf, seed, thin,accel,log)		
		h=har(0,0,0,0,0)
		h.reject_rate=reject_rate
		h.plot_metrics(x,pdf,log=True)				# msg=msg			
		assert(False)
		
	def test_lognorm(self):	
		"""
		har_cd_nd + lognorm
		"""
		from har import har_cd_np
		
		
		#alpha = 1.2; sigma = 0.8
		dim= 500; nsamp= 1000;	 alpha = 1.5; sigma = 1.8 ; lb=0; Mtot = 15; thin= 1000 ; accel=1
		#dim= 500; nsamp= 100; alpha = 1.5; sigma = 1. ; lb=0; Mtot = 15; thin= 1000 ; accel=1
		#sigma=1.85 # NB: b must be > 1 so that E[x] exists.
		scale = alpha*np.exp(-0.5*sigma**2) * Mtot/float(dim)
		rv = lognorm(sigma,scale=scale)
		#M = (dim * rv.mean()) * 1. * alpha
		x0= Mtot*np.ones(dim)/dim						
		seed=None
		pdf= rv.pdf
		log=0
		x,reject_rate = har_cd_np(x0, dim, lb,Mtot,nsamp, pdf, seed, thin,accel,log)		
		h=har(0,0,0,0,0)
		h.reject_rate=reject_rate
		msg = "rv.mean=%1.2f"%(rv.mean())
		h.plot_metrics(x,pdf,log=True, msg=msg)				# msg=msg			
		assert(False)
		
	def test_gamma(self):	
		"""
		har_cd_nd + gamma
		"""
		from har import har_cd_np		
		#alpha = 1.2; sigma = 0.8
		dim= 100; nsamp= 2000;	 alpha = 1.; sigma = 1.46 ; lb=0; Mtot = 15; thin= 50 ; accel=0
		#dim= 100; nsamp= 2000;	 alpha = 1.0; sigma = 1.46 ; lb=0; Mtot = 1500; thin= 50 ; accel=1
		#dim= 500; nsamp= 500;	 alpha = 1.0; sigma = .46 ; lb=0; Mtot = 15; thin= 50 ; accel=1
		#dim= 500; nsamp= 100; alpha = 1.5; sigma = 1. ; lb=0; Mtot = 15; thin= 1000 ; accel=1		
		scale = alpha*  Mtot/(sigma*float(dim)) # so that E(x) = Mtot/nw
		rv = gamma(sigma,scale=scale)
		#M = (dim * rv.mean()) * 1. * alpha
		x0= Mtot*np.ones(dim)/float(dim)						
		seed=None
		pdf= rv.pdf
		log=0
		x,reject_rate = har_cd_np(x0, dim, lb,Mtot,nsamp, pdf, seed, thin,accel,log)		
		h=har(0,0,0,0,0)
		h.reject_rate=reject_rate
		msg = "rv.mean=%1.2f"%(rv.mean())
		h.plot_metrics(x,pdf,log=False, msg=msg)				# msg=msg			
		assert(False)	
"""
test: cube hidim nsamp faible => pas uniforme
test: cube hidim nsamp O(n^k) => à peu près uniforme
test: cube hidim uniform vs pareto => il y a différence
"""

"""
observations:
* uniform cube: meme avec sample uniform et peu de contraintes dans F, en grande dim (30), il faut bcp d'échantillons pour obtenir uniformité; et l'autocorr est grande.
** nb: même avec dim 5, c'est lent.
** TODO: comparer autocor avec résultats theor
		idem avec thick
* uniform gibbs:
** normalement, on doit obtenir échantillonnage uniforme, mais on voit plutot convergence vers exp, quand dim augmente
** c'est dû à mixing time qui devient trop grand ??
* non unif: pareto: 
** compte-tenu des remarques précédentes, on doit se placer en dim pas trop grande 
	pour mixing time, mais pas trpo petite pour que la contrainte sur une dim ne perturbe pas trop les autres.	
** le taux de reject ??
** quand c augmente: ?
** ressemblance avec pareto ??: pas terrible (petit nb de samples?), se termine 
	en uniforme
** mais du coup les autres compo restent uniformes 
* nonunif gaussienne toutes composantes: 
** ca marche plutot bien
* nonunif gaussienne 1 composante: 
**  

* cube hidim:
** c'est un exemple difficile, http://www.cc.gatech.edu/~bcousins/volume.html
** constat: meme pour un cube uniforme, on trouve distrib exponentielle => augementer nb samp
** seulement après comparer avec gibbs uniforme, et pareto

* fournier pc nonparametric:
** !! F ne colle pas à ecdf !!!

* piecewise:
** si histo=0 sur [0,thr[0]], la condition initiale est difficile à trouver, car la solution triviale
	ne marche pas car elle tombe en desous de  thr[0]
	=> smoother , ou ajouter epsilon
** bcp de solutions nulle échantillonnées ???? pourquoi ???	
** autocor importante	

* har+piecewise_pareto + nd
 => 0.94 rejection rate !!!!!!!!

* FAIL to sample full system because of REDUNDANT constraints => switching to reduced system sampling
  see transmat.bmw_1N1FnW.get_har_samples()

TODO:
* ajouter gestion des paramètres pour pareto (sera nécessaire pour fit)
* cython cdef si nécessaire (dim 100)
* séparer test native et purepython
* ajouter time pour tt le monde
* paralléliser ? (i3 a 4 proc) http://docs.cython.org/en/latest/src/userguide/parallelism.html
* benchmarker har en purepython et cython
* heroku est-il compatible cython ? http://stackoverflow.com/questions/16932312/heroku-sh-cython-not-found

CYTHON:
http://docs.cython.org/en/latest/src/tutorial/numpy.html
https://linuxfr.org/users/serge_ss_paille/journaux/pythran-chatouille-cython
https://zestedesavoir.com/forums/sujet/2431/fonction-magique-numpy/
https://gist.github.com/zed/2051661
http://stackoverflow.com/questions/10788267/optimizing-numpy-dot-with-cython
et ~/local/git/8432f362a008901e732c/monte carlo cython.ipynb

* DONE:
** passer sur i3; ajouter numba ?; plotter pdf cube hidim en fonction de sampling time;
   augmenter en conséquence en fct du taux de rejet
** modifier har_nonuniform pour supprimer ttes les self, et la remplacer facilement
** impleme cython
*benchmarfs perfs   

PYTHRAN BUGS: 
* infered type:
https://linuxfr.org/users/serge_ss_paille/journaux/pythran-chatouille-cython
pythra/pythran/tests/test_numpy_func3.py: 
    def np_dot5(x, y):
		test_numpy_func3.py:                      np_dot5=[List[List[float]], List[List[float]]])
* ascii codec: https://github.com/serge-sans-paille/pythran/issues/300	
 => j'ai viré tous les #
* mais pb de compil qd meme 	

""" 
