# -*- coding: utf-8 -*- 

#  transmat_test.py
#  
#  Copyright 2016 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import sys
sys.path.append('../');sys.path.append('../experimental')
sys.path.append('../cython');

import numpy as np
from transmat import *

from gpareto import *
from functools import partial

from gpareto import get_WID_FR,histogram_from_tabulation
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
from rpy2.robjects import r 
importr('MonoPoly')
import interpolate
import har as cython_har
from scipy.integrate import quad

from scipy.stats import lognorm

from utils import save_h5
from transmat import get_bmw_labels
import datetime
import json

import network
from scipy.sparse.linalg import spsolve,lsqr,svds

class TestClass_CSP_sfc_FiCM:
	"""
	To run all tests of the class:
	$nosetests transmat_test.py:TestClass_CSP_sfc_FiCM
	$nosetests transmat_test.py:TestClass_CSP_sfc_FiCM.test_init
	$nosetests transmat_test.py:TestClass_CSP_sfc_FiCM.test_rank
	"""
	def setUp(self):
		"""
		block
		"""
		fname='../data/eurostat_network.h5'
		#self.params_num = [1.0, 0.1 ,0.75 , 0.1,0.1,0.03] #k,al0,al1,al2,delta,r
		n=network.NaioNetwork()
		p={'network':{'year':'2010','geo':'CZ','nb':3,'nf':50,'nh':500,
					  'nlink_per_firm_cons': 2,  
					'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20},#3 is better for plotting
				'mc':{'niter':10}}
		nb = p['network']['nb']
		nf = p['network']['nf']
		nh = p['network']['nh']
		year = p['network']['year']
		geo = p['network']['geo']
		#L_invest = int(np.floor(p['network']['nlink_per_firm'] * nf)) # [Atalay11]		
		L_invest= int(p['network']['nlink_per_firm_invest'] *nf)
		L_cons_firm=int(p['network']['nlink_per_firm_cons']*nf)
		L_cons_hh = int(p['network']['nlink_per_hh_cons'] *nh)
		n=network.init_network_block(year=year,geo=geo,nb=nb,nf=nf,nh=nh,fname=fname,L_invest=L_invest, L_cons_firm=L_cons_firm , L_cons_hh = L_cons_hh,proxy_invest_conso=True	)
		self.A,self.B,self.C,self.D,self.E,self.F = n.sample_full_network(nb,nf,nh)
		self.n = n	
		self.nb,self.nf,self.nh = nb,nf,nh
	def setUp_rndfitness(self):
		"""
		NOT USED 
		rndfitness
		"""
		fname='../data/eurostat_network.h5'
		#self.params_num = [1.0, 0.1 ,0.75 , 0.1,0.1,0.03] #k,al0,al1,al2,delta,r
		n=network.NaioNetwork()
		p={'network':{'year':'2010','geo':'CZ','nb':3,'nf':100,'nh':1000,
					  'nlink_per_firm_cons': 2,  
					'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20,#3 is better for plotting
					'nlink_per_hh_wage':1},
				'mc':{'niter':10}}
		nb = p['network']['nb']
		nf = p['network']['nf']
		nh = p['network']['nh']
		year = p['network']['year']
		geo = p['network']['geo']
		L_invest= int(p['network']['nlink_per_firm_invest'] *nf)
		L_cons_firm=int(p['network']['nlink_per_firm_cons']*nf)
		L_cons_hh = int(p['network']['nlink_per_hh_cons'] *nh)
		L_wage = int(p['network']['nlink_per_hh_wage']*nh)
		# pareto.pdf(x, b) = b / x**(b+1) for x >= 1, b > 0.
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		b=1.0; 	rv = pareto(b)
		fitness = rv.rvs(nf)
		#fitness = np.random.rand(nf)
		#warnings.warn('UNIFORM fitness!!')
		n=network.init_network_rndfitness(year=year,geo=geo,nb=nb,nf=nf,nh=nh,
								fname=fname,
								L_invest=L_invest, L_cons_firm= L_cons_firm,
								L_cons_hh = L_cons_hh,
								L_wage= L_wage,
								fitness=fitness,proxy_invest_conso=True)
		self.A,self.B,self.C,self.D,self.E,self.F = n.sample_full_network(nb,nf,nh)
		self.n = n	
		self.nb,self.nf,self.nh = nb,nf,nh
	def tearDown(self):
		pass
	def test_init(self):	
		F,g,lb,ub,prior = CSP_sfc_FiCM_noMLK(self.nb,self.nf,self.nh,
										self.A,self.B,self.C,self.D,self.E)
	def test_init_IOinvest(self):	
		IO_mat= np.random.rand(9**2).reshape((9,9))
		sectors_vec= np.hstack((np.zeros(2),np.ones(2),2*np.ones(3),
							3*np.ones(1),4*np.ones(2),5*np.ones(3),
							6*np.ones(30),7*np.ones(4),8*np.ones(3)	))
		F,g =CSP_sfc_FiCM_IOinvest_noMLK(self.nb,self.nf,self.nh,
									IO_mat,sectors_vec,
									self.A,self.B,self.C,self.D,self.E)									
	def test_solve_IOinvest(self):	
		IO_mat= np.random.rand(9**2).reshape((9,9))
		sectors_vec= np.hstack((np.zeros(2),np.ones(2),2*np.ones(3),
							3*np.ones(1),4*np.ones(2),5*np.ones(3),
							6*np.ones(30),7*np.ones(4),8*np.ones(3)	))
		F,g =CSP_sfc_FiCM_IOinvest_noMLK(self.nb,self.nf,self.nh,
									IO_mat,sectors_vec,
									self.A,self.B,self.C,self.D,self.E)									
									
		x, istop, itn, r1norm=scipy.sparse.linalg.lsqr(F.astype(float),
													g)[:4]	
		print "np.max(g)=",np.max(g)," np.max(x)=",np.max(x)
		print "istop, itn, r1norm=",istop, itn, r1norm
		assert False							
									
	def test_init_alpha0(self):	
		F,g,lb,ub,prior = CSP_sfc_FiCM_noMLK_alpha0(self.nb,self.nf,self.nh,
										self.A,self.B,self.C,self.D,self.E,
										alpha0=0.5)								
	def test_plot_alpha0(self):	
		F,g,lb,ub,prior = CSP_sfc_FiCM_noMLK_alpha0(self.nb,self.nf,self.nh,
										self.A,self.B,self.C,self.D,self.E,
										alpha0=0.5)	
		nb,nf,nh=self.nb,self.nf,self.nh
		l = [(slice(nh,nh+nf),slice(nf*nh,nf*nh+nf**2),'If2' )]								
		nplot = len(l)
		F=F.tolil()
		for i,(idxx,idxy,label) in enumerate(l):	
			plt.subplot(nplot,1,i+1)																									
			submat = F[idxx,idxy].todense()
			plt.matshow(submat,cmap="binary",fignum=False, 
						aspect="auto")
			plt.ylabel(label);		
			plt.xlabel('shape=%d,%d'%(submat.shape[0],submat.shape[1]))						
		plt.show()
	def test_rank(self):	
		# https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html#module-scipy.sparse.linalg
		F,g,lb,ub,prior = CSP_sfc_FiCM_noMLK(self.nb,self.nf,self.nh,
										self.A,self.B,self.C,self.D,self.E)		
		k=min(F.shape)-1										
		u, s, vt =scipy.sparse.linalg.svds(F.astype(float), k=k)
		#print s
		#assert False
	def test_rank_alpha0(self):	
		# https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html#module-scipy.sparse.linalg		
		F,g,lb,ub,prior = CSP_sfc_FiCM_noMLK_alpha0(self.nb,self.nf,self.nh,
										self.A,self.B,self.C,self.D,self.E,
										alpha0=0.5)		
		k=min(F.shape)-1										
		u, s, vt =scipy.sparse.linalg.svds(F.astype(float), k=k)										
		print "s=",s
		#assert False	
	def test_solve(self):	
		#https://docs.scipy.org/doc/scipy/reference/generated/generated/scipy.sparse.linalg.spsolve.html#scipy.sparse.linalg.spsolve
		F,g,lb,ub,prior = CSP_sfc_FiCM_noMLK(self.nb,self.nf,self.nh,
										self.A,self.B,self.C,self.D,self.E)		
		# if system is square
		#x = spsolve(F, g)
		x=scipy.sparse.linalg.lsqr(F.astype(float), g.todense())
	def test_solve_alpha0(self):	
		#https://docs.scipy.org/doc/scipy/reference/generated/generated/scipy.sparse.linalg.spsolve.html#scipy.sparse.linalg.spsolve
		F,g,lb,ub,prior = CSP_sfc_FiCM_noMLK_alpha0(self.nb,self.nf,self.nh,
										self.A,self.B,self.C,self.D,self.E,
										alpha0=0.5)		
		# if system is square
		#x = spsolve(F, g)
		x, istop, itn, r1norm=scipy.sparse.linalg.lsqr(F.astype(float),
								g)[:4]	
		print "np.max(g)=",np.max(g)," np.max(x)=",np.max(x)
		print "istop, itn, r1norm=",istop, itn, r1norm
		assert False
			
class TestClass_bmw_1F1B2W:
	"""
	test class bmw_1F1B2W
	To run just a single test:
	$nosetests transmat_test.py:TestClass_bmw_1F1B2W.test_get_samples
	"""
	def setUp(self):
		"""
		"""
		self.b = bmw_1F1B2W()
		self.b.init() 
		# see GL12 p.237 for selected values of: alpha_1,alpha_2, gamma, k, delta
		# alpha_0 ??
		self.params_num = [1.0, 0.1 ,0.75 , 0.1,0.1,0.03] #k,al0,al1,al2,delta,r
	def tearDown(self):
		pass

	def test_init(self):
		"""
		check that init is done
		"""				
		#print 'type=',type(b.A),type(b.B)
		assert(self.b.init_done==1)		
		#assert (scale>0).all() 

	def test_get_Mtot_num(self):
		"""
		check that Mtot is positive
		"""
		k_num,al0_num,al1_num,al2_num,delta_num,r_num = self.params_num
		Mtot_num=self.b.get_Mtot_num(k_num,al0_num,al1_num,al2_num,delta_num)
		assert(Mtot_num>=0)
		
	def test__get_interior_point(self):
		"""
		check that the interior point is positive and <= Mtot
		"""
		k_num,al0_num,al1_num,al2_num,delta_num,r_num = self.params_num
		Mtot_num=self.b.get_Mtot_num(k_num,al0_num,al1_num,al2_num,delta_num)
		x0 = self.b._get_interior_point(k_num,al0_num,al1_num,al2_num,delta_num,r_num)		
		assert( np.all(x0>=0) & np.any(x0>0)& np.all(x0<=Mtot_num) )
		
	def test_get_csp_num(self):
		"""
		check that the CSP is coherent
		"""
		k_num,al0_num,al1_num,al2_num,delta_num,r_num = self.params_num
		A_num,B_num,lb,ub,x0 = self.b.get_csp_num(k_num,al0_num,al1_num,al2_num,delta_num,r_num)
		# remark: check_constr is called inside get_csp_num
		assert( isinstance(A_num, np.ndarray) & isinstance(B_num, np.ndarray) )
		
	def test_get_samples(self):
		"""
		"""
		# compute ref			
		AB_ref=bmw_gauss_elimination_by_hand(self.b.AB, **self.b.p)		
		#AB_ref=bmw_gauss_elimination_by_hand(AB,k=k,r=r,delta=delta,al1=al1)		
		# har
		nsamp=100
		k_num,al0_num,al1_num,al2_num,delta_num,r_num = self.params_num
		x=self.b.get_samples(AB_ref,k_num,al0_num,al1_num,al2_num,delta_num,r_num,nsamp)	
		#test samp
		A_num,B_num,lb,ub,x0 = self.b.get_csp_num(k_num,al0_num,al1_num,al2_num,delta_num,r_num)
		for i in range(x.shape[0]):
			assert(check_constr(x[i,:],A_num,B_num,lb,ub)) # ugly; should use check_constr_vec 

class TestClass_bmw_1F1BnW:
	"""
	test class bmw_1F1B2W
	To run just a single test:
	$nosetests transmat_test.py:TestClass_bmw_1F1BnW.test_get_samples
	"""
	def setUp(self):
		"""
		"""
		self.p_num = dict(bmw_example_param(random=False)) #[1.0, 0.1 ,0.75 , 0.1,0.1,0.03] #k,al0,al1,al2,delta,r
	def tearDown(self):
		pass

	def test_BMW_1b1fnW_reduced_uniform_native(self):
		"""
		sample reduced model then check constraint against FULL model
		"""
		# sample reduced model
		nw=9
		nsamp = 1000
		bmw = bmw_sym_1F1BnW(nw)
		bmw.init()	
		x = bmw.get_har_samples(nsamp,native=True)	
		print x
		# check constraint against FULL model
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
		utils.check_constr_vec(C,b,F,g,x)
		assert(True)


	def test_BMW_1b1fnW_reduced_pareto_piecewise_nD_lodim_native(self):
		"""
		bmw 1b1fnW reduced + pareto_piecewise nD (i.e all income have a constrained pdf)		
		ATTENTION:  the pdf corresponds to INCOME.
					bmw.get_har_samples corresponds to WEALTH	
		=>DO NOT USE the code below, except for testing	purpose						
		"""
		nw=9
		nsamp = 20000
		# prepare pdf
		# WBs_avg is necessary
		C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_11'+str(nw))
		# rescale pdf rather than CSP problem (using income average, and not Mtot)
		thr,perc, bb, yav, total_income,nb_household = income_tabulation() # yav is needed before calling get_..
		scale = (135./float(nw))/yav #WBs_avg / yav				
		# constrain the pdf of ALL dimensions
		pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False,idx = range(nw),
																			rescale_factor=scale)
		# sample reduced model		
		bmw = bmw_sym_1F1BnW(nw)
		bmw.init()	
		x = bmw.get_har_samples(nsamp,pdf=pdf,uniform=False,native=True)				
		# check constraint against FULL model
		utils.check_constr_vec(C,b,F,g,x)		
		assert(True)
		#plt.hist( x[:,-3],30 ); plt.show()

	def test_BMW_1b1fnW_reduced_pareto_piecewise_nD_lodim_native_rescaled(self):
		"""
		rescaling to Mtot
		ATTENTION:  the pdf corresponds to INCOME.
					bmw.get_har_samples corresponds to WEALTH	
		=>DO NOT USE the code below, except for testing	purpose	
		"""
		alpha0 = [1,1.5]
		nw=9
		nsamp = 15000
		p_num = dict(self.p_num) # deep copy
		thr,perc, b, yav,total_income,nb_household=income_tabulation()						
		# sample reduced model
		avg = [0,0]
		for i,a in enumerate(alpha0):
			p_num['alpha0']=a
			bmw = bmw_sym_1F1BnW(nw, p_num)
			bmw.init()	
			scale = a						
			scale_all = scale * (bmw.Mtot/float(nw))/ yav	# average M
			pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False,idx = range(nw),
																				rescale_factor=scale_all)
			x = bmw.get_har_samples(nsamp,pdf=pdf,uniform=False,native=True)
			avg[i] = np.mean(x[:,-3])
			#plt.subplot(2,1,i+1); plt.hist( x[:,-3],30 )
		print avg[0], avg[1]
		assert( avg[0] < 1.1*avg[1])	# may fail
		#plt.show()

	def test_hd_pareto_rescaled(self):
		"""
		sampler = har hd
		pdf = pareto, rescaled to Mtot
		"""
		alpha0 = [1,2.]
		nw=9
		nsamp = 100		
		b=2.
		# sample reduced model
		avg = [0,0]
		for i,a in enumerate(alpha0):
			p_num = dict(self.p_num) # deep copy
			p_num['alpha0']=a
			bmw = bmw_sym_1F1BnW(nw, p_num=p_num)
			bmw.init()	
			Mtot = bmw.Mtot			
			rv = pareto(b,scale=(Mtot/float(nw))/(b/(b-1) ))
			def pdf(x):
				return np.prod(rv.pdf(x))
			x = bmw.get_har_samples(nsamp,pdf=pdf,uniform=False,
									native="har_hd",algo='HD')								
			avg[i] = np.mean(x[:,-3])
			plt.subplot(2,1,i+1); plt.hist( x[:,-3],30 )
			plt.xlabel('alpha0=%1.1f Mtot/nw=%1.1f'%( bmw.p_num['alpha0'], Mtot/float(nw) ))			
		print avg[0], avg[1]
		plt.show()
		assert( avg[0] < 1.1*avg[1])	# may fail
	
	def test_hd_lognorm(self):
		"""
		sampler = har hd
		pdf = pareto, rescaled to Mtot
		"""
		# make dataset
		nw= 100; nsamp= 2000;	 alpha = 1.; sigma = 1.72 ; lb=0; thin= 1000 ;
		#nw= 100; nsamp= 100; alpha = 1.5; sigma = 1. ; lb=0; Mtot = 15; thin= 1000 ; 
		algo = "HD"				
		p_num = dict(self.p_num) # deep copy
		p_num['alpha0']=1.
		bmw = bmw_sym_1F1BnW(nw, p_num=p_num)
		bmw.init()	
		Mtot = bmw.Mtot											
		scale = alpha*np.exp(-0.5*sigma**2) * Mtot/float(nw)
		rv = lognorm(sigma,scale=scale)		
		def pdf(x):
			return np.prod(rv.pdf(x))
		x,reject_rate = bmw.get_har_samples(nsamp,pdf=pdf,uniform=False,algo=algo, thin=thin)												
		# save
		if(True):
			label,label_long = get_bmw_labels(1,1,nw)
			idx_first_WBs = label_long.index('WBs')
			idx_first_M = label_long.index('M')
			label = 'test_hd_lognorm' # REPLACE IT WITH CALLING FUNC NAME 
			config = { 
					'label': label,
					'model_params':{'model':'bmw','nw':nw,'nb':1, 'nf':1, 
								   'params': p_num,
								   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
								   'idx_first_M':idx_first_M },
				    'sampler_params':{'seed':123, 'uniform':False,'native':False, 'algo': 'HD',
										'nsamp':nsamp, 'thin':thin},
					'experiment_params': {'sigma':sigma,'scale':scale,'rv':'lognorm'},
					'experiment_results': { 'reject_rate':reject_rate},
					"datetime": str(datetime.datetime.today())								   
				  	}
			
			fname= '../data/samples.h5'
			groupname = '/bmw_11N/transmat_test/lognorm'	
			save_h5(x,config,fname,groupname)	
		# load and plot	
		h=har(0,0,0,0,0)
		store = pd.HDFStore(fname)
		c=store[groupname+'/config/'+label]
		config=json.loads( c[0].decode())
		label = config['label'] 
		ncol = store[groupname+'/samples/'+label].shape[1]
		idx_bool= np.array(config['model_params']['labels'])=='M'
		idx_M = np.arange(ncol)[idx_bool]
		Mi=store[groupname+'/samples/'+label][idx_M].values
		#Mi = x[:,-3-nw+1:-2]
		h.reject_rate=config['experiment_results']['reject_rate']
		print Mi	
		h.plot_metrics(Mi,rv.pdf,log=False)	
		assert(False)		
	
	def test_pdf_prod():
		"""
		def pdf_prod(x):
			if np.any(x/scale<cutoff): 
				return 1e-10
			else:
				return np.prod(interpolate.C_Horner_vec(c, np.log10(x/scale)))	
		"""
		pass
		
	def test_hd_WID_MonoPoly_rescaled(self):
		"""
		pdf = WID + MonoPoly
		sampler= har+hd
		"""				
		# param
		nw=50
		nsamp = 1000
		algo = "HD"
		thin=20
		# get pdf
		year=2010
		pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')
		thweal = thr_wea
		degree=17
		r.assign("degree",degree)
		logthweal = np.log10(thr_wea[1:])
		r.assign("x", robjects.FloatVector(logthweal))
		r.assign("y", robjects.FloatVector(percentile_num[1:]))
		coef= list(r( 'coef(monpol(y~x, data=data.frame(x,y),  degree=degree))'))		
		coef_f = coef[1:]					
		c = np.array(coef_f) * np.arange(1,degree+1)				
		# sample
		p_num = dict(self.p_num) # deep copy		
		bmw = bmw_sym_1F1BnW(nw, p_num=p_num)
		bmw.init()	
		Mtot = bmw.Mtot					
		#scale=(Mtot/float(nw))/mean_wea 
		scale = Mtot/max(thr_wea)
		cutoff=1500	
		
		def pdf_prod(x):
			# WARN: the factor 1/x is included in pdf_horner_prod
			return 1./np.prod(x)*cython_har.pdf_horner_prod(c,x, scale, cutoff)		
		x,reject_rate = bmw.get_har_samples(nsamp,pdf=pdf_prod,uniform=False,algo=algo, thin=thin)									
		# plot	
		h=har(0,0,0,0,0)
		h.reject_rate=reject_rate
		def pdf_vec(x):
			# 
			y=interpolate.C_Horner_vec(c, np.log10(x/scale))/x
			idx   = x/scale<cutoff
			y[idx]= 1e-10	
			return y		
		Mi = x[:,-3-nw+1:-2]		
		print Mi	
		h.plot_metrics(Mi,pdf_vec,log=True)		
		assert(False)							

	def test_cd_WID_MonoPoly_rescaled(self):
		"""
		pdf = WID + MonoPoly
		sampler= har+hd
		"""				
		# param
		nw=100
		nsamp = 1000
		algo = "CD"
		alpha = 1 # alpha>1 => increase \int x pdf
		accel=0
		thin=50
		# get pdf
		year=2010
		pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')
		thweal = thr_wea
		degree=17
		r.assign("degree",degree)
		logthweal = np.log10(thr_wea[1:])
		r.assign("x", robjects.FloatVector(logthweal))
		r.assign("y", robjects.FloatVector(percentile_num[1:]))
		coef= list(r( 'coef(monpol(y~x, data=data.frame(x,y),  degree=degree))'))		
		coef_f = coef[1:]					
		c = np.array(coef_f) * np.arange(1,degree+1)				
		# sample
		p_num = dict(self.p_num) # deep copy		
		bmw = bmw_sym_1F1BnW(nw, p_num=p_num)
		bmw.init()	
		Mtot = bmw.Mtot					
		#scale=alpha * (Mtot/float(nw))/mean_wea 
		scale = alpha*Mtot/max(thr_wea)
		cutoff= 1500	
		lb = cutoff*scale
		print "lower bound=",lb, " Mtot/nw=", Mtot/nw
		def pdf(x):
			#return cython_har.pdf_horner(c,x, scale, cutoff)/x
			#return cython_har.pdf_horner(c,x, scale, cutoff)/x
			#return np.exp(-x**1.5/(Mtot/20))*cython_har.pdf_horner(c,x, scale, cutoff)/x
			#return np.exp(-np.log(x)**2/(Mtot/2))*cython_har.pdf_horner(c,x, scale, cutoff)/x
			return np.exp(-np.log(x)**2/(Mtot/2))
			#return np.exp(-x/(Mtot/1))			
		x,reject_rate = bmw.get_har_samples(nsamp,pdf=pdf,uniform=False,algo=algo, thin=thin,accel=accel,lb=lb)									
		# plot	
		h=har(0,0,0,0,0)
		h.reject_rate=reject_rate
		def pdf_vec(x):
			#y=np.exp(-x**1.5/(Mtot/20))* interpolate.C_Horner_vec(c, np.log10(x/scale))/x
			#y=interpolate.C_Horner_vec(c, np.log10(x/scale))/x
			#y=np.exp(-np.log(x)**2/(Mtot/2))* interpolate.C_Horner_vec(c, np.log10(x/scale))/x
			y=np.exp(-np.log(x)**2/(Mtot/2))
			#y= np.exp(-x/(Mtot/1))
			idx   = x/scale<cutoff
			y[idx]= 1e-10	
			return y		
		Mi = x[:,-3-nw+1:-2]		
		print Mi
		mean_pdf =  quad( lambda x: x*pdf(x) ,0,Mtot)[0]/quad( pdf ,0,Mtot)[0]			
		print "mean_pdf=",mean_pdf
		msg = ' mean_pdf=%1.1f'%(mean_pdf)
		h.plot_metrics(Mi,pdf_vec,log=True,msg=msg)				
		assert(False)							
