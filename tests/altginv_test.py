# -*- coding: utf-8 -*-
#  altginv_test.py
#  
#  Copyright 2018 aurelien <aurelien@aurelien-300E4A-300E5A-300E7A-3430EA-3530EA>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import sys
sys.path.append('../');sys.path.append('../cython')

import numpy as np
from scipy import optimize
		
from cylp.cy import CyClpSimplex
from cylp.py.modeling.CyLPModel import CyLPArray,CyLPModel
from cylp.py.utils.sparseUtil import csr_matrixPlus, csc_matrixPlus,sparseConcat

import altginv

from transmat import *
import network,network_weights

import matplotlib.pyplot as plt

def eval_sparsity(A,B):
	n, m=A.shape
	pseudoI = A.dot(B)		
	sparsity_B = float(B.getnnz())/float(B.shape[0]*B.shape[1])
	title= 'sparsity(B)='+str(sparsity_B)
	plt.subplot(411)
	plt.matshow(A.todense(), cmap="binary",aspect="auto",fignum=False)
	plt.ylabel('A')
	plt.subplot(412)
	plt.plot(B.todense()); plt.ylabel('B')
	plt.subplot(413)
	plt.plot(pseudoI.diagonal())
	plt.ylabel('pseudoI.diagonal')
	pseudoI.setdiag(np.zeros(m))
	plt.subplot(414)
	#plt.hist(pseudoI.todense().flatten())
	sparsityI=float((pseudoI>0.001).getnnz())/float(pseudoI.shape[0]*pseudoI.shape[1])
	title += " sparsity(pseudoI)="+str(sparsityI)
	plt.suptitle(title)
	plt.show()


class TestClass_altginv:
	"""

	
	To run all tests of the class:
	$nosetests altginv_test.py:TestClass_altginv
	"""
	def setUp(self):
		pass
	def tearDown(self):
		pass

	def test_cylp(self):
		"""
		test the example
		copied from http://mpy.github.io/CyLPdoc/#modeling-example
		"""
		s = CyClpSimplex()
		# Add variables
		x = s.addVariable('x', 3)
		y = s.addVariable('y', 2)
		# Create coefficients and bounds
		A = np.matrix([[1., 2., 0],[1., 0, 1.]])
		B = np.matrix([[1., 0, 0], [0, 0, 1.]])
		D = np.matrix([[1., 2.],[0, 1]])
		a = CyLPArray([5, 2.5])
		b = CyLPArray([4.2, 3])
		x_u= CyLPArray([2., 3.5])
		# Add constraints
		s += A * x <= a
		s += 2 <= B * x + D * y <= b
		s += y >= 0
		s += 1.1 <= x[1:3] <= x_u
		# Set the objective function
		c = CyLPArray([1., -2., 3.])
		s.objective = c * x + 2 * y.sum()
		# Solve using primal Simplex
		s.primal()
		print s.primalVariableSolution['x']

	def test_cylp_sparse(self):
		"""
		copied from https://github.com/coin-or/CyLP/blob/master/cylp/tests/test_CyClpSimplex_CyLPModel.py: test_removeConstraint()
		"""
		model = CyLPModel()
		
		
		x = model.addVariable('x', 3)
		y = model.addVariable('y', 2)
		
		A = csc_matrixPlus(([1, 2, 1, 1], ([0, 0, 1, 1], [0, 1, 0, 2])),  shape=(2, 3))
		B = csc_matrixPlus(([1, 1], ([0, 1], [0, 2])),  shape=(2, 3))
		D = np.matrix([[1., 2.],[0, 1]])
		a = CyLPArray([3, 2.5])
		b = CyLPArray([4.2, 3])
		x_u= CyLPArray([2., 3.5])
		model.addConstraint(A * x <= a, 'res1')
		model.addConstraint(2 <= B * x + D * y <= b, 'res2')
		model.addConstraint(y >= 0)
		model.addConstraint(1.1 <= x[1:3] <= x_u)
		model.addConstraint(x[0] >= 0.1)
		c = CyLPArray([1., -2., 3.])
		model.objective = c * x + 2 * y[0] + 2 * y[1]
		#
		s = CyClpSimplex(model)
		s.primal()
		sol = np.concatenate((s.primalVariableSolution['x'],
							  s.primalVariableSolution['y']))
		assert((abs(sol -
		np.array([0.1, 1.45, 1.1, 0, 0.95]) ) <= 10**-6).all())

	
	def test_cylp_equality(self):
		"""
		Ax=b
		copied from: https://github.com/coin-or/CyLP/blob/master/cylp/tests/test_CyClpSimplex_CyLPModel.py : test1
		"""
		model = CyLPModel()
		
		x = model.addVariable('x', 3)
		
		A = np.matrix([[1,2,3], [1,1,1]])
		b = CyLPArray([5, 3])
		
		model.addConstraint(A * x == b)
		model.addConstraint(x >= 0)
		
		model.objective = 1*x[0]  + 1*x[1] + 1.1 * x[2]
		
		# Solve it a first time
		s = CyClpSimplex(model)
		s.primal()
		sol = s.primalVariableSolution['x']
		assert((abs(sol - np.array([1,2,0]) ) <= 10**-6).all())
		# Add a cut
		s.addConstraint(x[0] >= 1.1)
		s.primal()
		sol = s.primalVariableSolution['x']
		assert((abs(sol - np.array([1.1, 1.8, 0.1]) ) <= 10**-6).all())
		
		# Change the objective function
		c = csr_matrixPlus([[1, 10, 1.1]]).T
		s.objective = c.T * x
		s.primal()
		sol = s.primalVariableSolution['x']
		assert((abs(sol - np.array([2, 0, 1]) ) <= 10**-6).all())
	
	

	def test_spinv_scipy(self):
		"""
		adaptation of Gribonval altginv (L1 sparse generalized inverse)
		with scipy.optimize (not suited to large sparse matrices)
		https://stackoverflow.com/questions/51273038/calculating-spinv-with-svd
		"""
		# argmin ||B_i||_1 stubect to A.B_i = I_i, where _i is the ith column
		# let B_i = u_i - v_i where u_i >= 0 and v_i >= 0
		# then ||B_i||_1 = [1' 1'][u_i;v_i] which is the objective function
		# and A.B_i = I_i becomes
		# A.[u_i - v_i] = I_i
		# [A -A][u_i;v_i] = I_i which is the equality constraint
		# and [u_i;v_i] >= 0 the bounds
		# here A is n x m (as opposed to m x n in paper)	
		A = np.random.randn(4, 6)
		n, m = A.shape
		I = np.eye(n)		
		Aeq = np.hstack((A, -A))
		# objective
		c = np.ones((2*m))
		# spinv
		B = np.zeros((m, n))		
		for i in range(n):
		    beq = I[:, i]
		    result = optimize.linprog(c, A_eq=Aeq, b_eq=beq)
		    x = result.x[0:m]-result.x[m:2*m]
		    B[:, i] = x	
		print('spinv(A) = \n' + str(B))
		print('pinv(A) = \n' + str(np.linalg.pinv(A)))
		print('A.B = \n' + str(np.dot(A, B)))
		#assert False
	def test_spinv_cypl(self):
		"""
		reproduce test_spinv_scipy with cypl
		https://stackoverflow.com/questions/51273038/calculating-spinv-with-svd
		"""
		# problem
		A = np.random.randn(4, 6)
		n, m = A.shape
		I = np.eye(n)		
		Aeq = np.matrix(np.hstack((A, -A)))
		# objective
		c = np.ones((2*m))
		# spinv
		B = np.zeros((m, n))		
		for i in range(n):
			# cylp setup
			model = CyLPModel()
			x = model.addVariable('x', 12)
			beq = CyLPArray(I[:, i]) #beq = I[:, i]
			model.addConstraint(Aeq * x == beq)
			model.objective = 1*x[0]  + 1*x[1] + 1*x[2] + 1*x[3]
			#model.addConstraint(x >= 0)
		    # Solve it a first time
			s = CyClpSimplex(model)
			s.primal()
			result = s.primalVariableSolution['x']
			#
			sol = result[0:m]-result[m:2*m]
			B[:, i] = sol
		print('spinv(A) = \n' + str(B))
		print('pinv(A) = \n' + str(np.linalg.pinv(A)))
		print('A.B = \n' + str(np.dot(A, B)))
		assert False

	
	def test_spinv_cylp_large(self):	
		"""
		same as test_spinv_cylp, with large scipy.sparse matrix A
		"""
		# problem
		n,m=40,10000
		data = 2*np.random.randn(240)-1
		row = np.random.randint(0, high=n, size=240)
		col = np.random.randint(0, high=m, size=240)		
		A = csr_matrixPlus((data, (row, col)), shape=(n, m))
		B=altginv.altginv2(A)
		eval_sparsity(A,B)
		#assert False	
	
	def test_spinv_cylp_large_SFC(self):
		"""
		same as test_spinv_cylp_large, with SFC
		"""
		# setup
		fname='../data/eurostat_network.h5'
		n=network.NaioNetwork()
		p={'network':{'year':'2010','geo':'CZ','nb':3,'nf':30,'nh':300,
					  'nlink_per_firm_cons': 2,  
					'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20},#3 is better for plotting
				'mc':{'niter':10}}
		nb = p['network']['nb']
		nf = p['network']['nf']
		nh = p['network']['nh']
		year = p['network']['year']
		geo = p['network']['geo']
		L_invest= int(p['network']['nlink_per_firm_invest'] *nf)
		L_cons_firm=int(p['network']['nlink_per_firm_cons']*nf)
		L_cons_hh = int(p['network']['nlink_per_hh_cons'] *nh)
		n=network.init_network_block(year=year,geo=geo,nb=nb,nf=nf,nh=nh,fname=fname,L_invest=L_invest, L_cons_firm=L_cons_firm , L_cons_hh = L_cons_hh,proxy_invest_conso=True	)
		self.A,self.B,self.C,self.D,self.E,self.F = n.sample_full_network(nb,nf,nh)
		self.n = n	
		self.nb,self.nf,self.nh = nb,nf,nh
		# get matrix
		F,g,lb,ub,prior = CSP_sfc_FiCM_noMLK_alpha0(self.nb,self.nf,self.nh,
								self.A,self.B,self.C,self.D,self.E,
								alpha0=0.5)	
		print "F.shape=",F.shape							
		# get spinv
		B=altginv.altginv2(F,threshold=0.001)
		eval_sparsity(F,B)

	def test_lsqr_sparse_pos_L1(self):
		"""
		solve lsqr with positive solution
		and SFC model
		"""
		n,m=40,1000
		ndat = 240
		data = 2*np.random.randn(ndat)-1
		row = np.random.randint(0, high=n, size=ndat)
		col = np.random.randint(0, high=m, size=ndat)		
		A = csr_matrixPlus((data, (row, col)), shape=(n, m))
		b = np.random.rand(n)
		x = altginv.lsqr_sparse_pos(A,b,norm=1)
		print "x=",x
		print "A.dot(x)-b",A.dot(x)-b
		assert False
	
	def test_lsqr_sparse_pos_L2(self):
		"""
		solve lsqr with positive solution
		and SFC model
		"""
		n,m=40,1000
		ndat = 240
		data = 2*np.random.randn(ndat)-1
		row = np.random.randint(0, high=n, size=ndat)
		col = np.random.randint(0, high=m, size=ndat)		
		A = csr_matrixPlus((data, (row, col)), shape=(n, m))
		b = np.random.randint(n)
		x = altginv.lsqr_sparse_pos(A,b,norm=2)
		
		
	def test_lsqr_sparse_pos_large(self):
		"""
		solve lsqr with positive solution
		and SFC model
		"""
		pass		
		
	def test_lsqr_sparse_pos_large_SFC(self):
		"""
		solve lsqr with positive solution
		and SFC model
		"""
		pass
