# -*- coding: utf-8 -*- 

#  monopoly_test.py
#  
#  Copyright 2017 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#

"""
Test Python call to MonoPoly package, using rpy2
https://cran.r-project.org/web/packages/MonoPoly/index.html

Reference:
[MMT16] Murray, K., Müller, S. and Turlach, B.A. (2016). 
Fast and flexible methods for monotone polynomial fitting, 
Journal of Statistical Computation and Simulation. 
doi:10.1080/00949655.2016.11395

R code:
>library('MonoPoly')
>m=monpol(y~x, data=w0, degree=3)
>x_new <-seq(-1,1,length.out=200); y_new = evalPol(x_new,coef(m))
>plot(y~x, w0); lines(x_new,y_new)

See also:
http://rpy.sourceforge.net/rpy2/doc-dev/html/introduction.html
"""

""" WONT WORK:
# http://rpy.sourceforge.net/rpy2/doc-dev/html/vector.html?highlight=dataframe#creating-objects
d = {'x': numpy2ri(x) , 'y': numpy2ri(y)}
dataf = DataFrame(d)
r.assign("dataf",dataf); coef= r( 'coef(monpol(y~x, data=dataf, degree=degree))')"""

import numpy as np
import matplotlib.pylab as plt

import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
from rpy2.robjects import r #,DataFrame
#from rpy2.robjects.numpy2ri import numpy2ri
#from rpy2.rinterface import RRuntimeError
importr('MonoPoly')
	

def w0(n=20):	
	x = np.linspace(-1.,1.,n)
	e = 0.01 * np.random.rand(n)
	y = 0.1 * x**3 + e
	return x,y

def poly_horner(lst, x):
	"""
	Evaluate a polynomial in reverse order using Horner's Rule,
	for example: a3*x^3+a2*x^2+a1*x+a0 = ((a3*x+a2)x+a1)x+a0
	https://stackoverflow.com/questions/16927229/evaluating-polynomial-coefficients#16927534 
	"""
	total = 0
	for a in reversed(lst):
		total = total*x+a
	return total


def polylog(lst,logx):
	"""
	not efficient
	"""
	total = 0 
	for i,beta in enumerate(lst):
		#total = total + (i+1)*beta* (np.log(x))**i
		total = total + (i+1)*beta* (logx)**i
	return total / np.exp(logx)
	#return total / x

class TestClass_MonoPoly:
	"""
	test class bmw_1F1B2W
	To run just a single test:
	$nosetests monopoly_test.py:TestClass_MonoPoly.test_w0
	"""
	def setUp(self):
		"""
		"""
		pass
	def tearDown(self):
		pass

	def test_w0(self):
		"""
		data=w0
		"""
		x,y=w0() 
		degree=3
		r.assign("degree",degree)
		r.assign("x", robjects.FloatVector(x))
		r.assign("y", robjects.FloatVector(y))
		coef= list(r( 'coef(monpol(y~x, data=data.frame(x,y),  degree=degree))'))
		n=200; 	x_new = np.linspace(-1.,1.,n)
		y_new = poly_horner(coef, x_new)
		plt.plot(x,y,'o'); plt.plot(x_new,y_new); 
		#plt.show()
		assert(True)
	
	def test_WID_F(self):
		"""
		data=WID
		"""
		import sys
		sys.path.append('../');sys.path.append('../experimental')
		from gpareto import get_WID_FR
		thweal, percentile_num,mean_wea =get_WID_FR(year='2010',value='wealth',interp='raw')
		degree=13
		r.assign("degree",degree)
		r.assign("x", robjects.FloatVector(thweal))
		r.assign("y", robjects.FloatVector(percentile_num))
		coef= list(r( 'coef(monpol(y~x, data=data.frame(x,y),  degree=degree))'))
		n=5000; 	x_new = np.linspace(1, max(thweal) ,n)
		y_new = poly_horner(coef, x_new)
		plt.plot(thweal,percentile_num,'o'); plt.plot(x_new,y_new);
		plt.show()
		assert(False)
	def test_WID_F_log(self):
		"""
		data=WID
		"""
		import sys
		sys.path.append('../');sys.path.append('../experimental')
		from gpareto import get_WID_FR
		thweal, percentile_num,mean_wea =get_WID_FR(year='2010',value='wealth',interp='raw')
		degree=17
		r.assign("degree",degree)
		logthweal = np.log(thweal[1:])
		r.assign("x", robjects.FloatVector(logthweal))
		r.assign("y", robjects.FloatVector(percentile_num[1:]))
		coef= list(r( 'coef(monpol(y~x, data=data.frame(x,y),  degree=degree))'))
		n=5000; 	x_new = np.linspace(min(logthweal)-.1, max(logthweal) ,n)
		y_new = poly_horner(coef, x_new)
		plt.plot(logthweal,percentile_num[1:],'o'); 	plt.plot(x_new,y_new);
		#plt.show()
		assert(True)
	def test_C_horner_vec(self):
		"""
		check that C_Horner and C_Horner_vec get the same result
		"""	
		x= np.linspace(1000,2000,100)
		coef= np.random.rand(10)
		y_c = [interpolate.C_Horner(np.array(coef),xx ) for xx in x ]	
		y = interpolate.C_Horner_vec(coef, x)
		assert(np.allclose(y,y_c) )
			
	def test_C_horner(self):
		"""
		compare poly_horner and C_horner
		"""				
		import sys
		sys.path.append('../');sys.path.append('../experimental')
		from gpareto import get_WID_FR
		import interpolate
		thweal, percentile_num,mean_wea =get_WID_FR(year='2010',value='wealth',interp='raw')
		degree=17
		r.assign("degree",degree)
		logthweal = np.log(thweal[1:])
		r.assign("x", robjects.FloatVector(logthweal))
		r.assign("y", robjects.FloatVector(percentile_num[1:]))
		coef= list(r( 'coef(monpol(y~x, data=data.frame(x,y),  degree=degree))'))
		n=5000; 	x_new = np.linspace(min(logthweal)-.1, max(logthweal) ,n)
		y_py = poly_horner(coef, x_new)
		y_c = [interpolate.C_Horner(np.array(coef),x ) for x in x_new ]
		#plt.plot(logthweal,percentile_num[1:],'o'); 	plt.plot(x_new,y_new);
		assert(np.allclose(y_py,y_c, rtol=1e-05, atol=1e-08)  )
	def test_poly_horner_polylog(self):
		"""
		compare poly_horner and polylog
		"""				
		import sys
		sys.path.append('../');sys.path.append('../experimental')
		from gpareto import get_WID_FR,histogram_from_tabulation
		# get data
		year=2010
		pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')
		thweal = thr_wea		
		logthweal = np.log10(thweal[1:])
		degree=17
		r.assign("degree",degree)		
		r.assign("x", robjects.FloatVector(logthweal))
		r.assign("y", robjects.FloatVector(percentile_num[1:]))
		coef= list(r( 'coef(monpol(y~x, data=data.frame(x,y),  degree=degree))'))		
		coef_f = coef[1:]			
		n=5000; 	x_new = np.linspace(min(logthweal)-.1, max(logthweal) ,n)		
		# comparison 
		f_polylog = polylog(coef_f, x_new)
		f_poly_horner = poly_horner(np.array(coef_f) * np.arange(1,degree+1),	 x_new) / np.exp(x_new)	
		delta = np.abs(f_polylog-f_poly_horner)
		print "max delta=",np.max(delta)
		assert(np.allclose(f_polylog,f_poly_horner, rtol=1e-05, atol=1e-06)  )
	def test_WID_pdf(self):
		"""
		data=WID
		pdf=MonoPoly
		"""
		import sys
		sys.path.append('../');sys.path.append('../experimental')
		from gpareto import get_WID_FR,histogram_from_tabulation
		# get data
		year=2010
		pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')
		thweal = thr_wea
		degree=17
		r.assign("degree",degree)
		logthweal = np.log10(thweal[1:])
		r.assign("x", robjects.FloatVector(logthweal))
		r.assign("y", robjects.FloatVector(percentile_num[1:]))
		coef= list(r( 'coef(monpol(y~x, data=data.frame(x,y),  degree=degree))'))		
		coef_f = coef[1:]			
		n=5000; 	logx_new = np.linspace(min(logthweal)-.1, max(logthweal) ,n)
		F_new = poly_horner(coef, logx_new)
		#f_new = polylog(coef_f, x_new)
		f_new = poly_horner(np.array(coef_f) * np.arange(1,degree+1),	 logx_new) / np.exp(logx_new)
		# hist		
		h = np.diff(percentile_num)	
		ymax = np.max(thweal)+10000
		width = np.diff(thweal) 
		h=h/width		
		# plot
		nplot = 5
		plt.subplot(nplot,1,1)
		plt.plot(logthweal,percentile_num[1:],'o'); 	plt.plot(logx_new,F_new);
		plt.subplot(nplot,1,2)
		plt.plot(logx_new,f_new);
		plt.subplot(nplot,1,3)
		#plt.bar(left=np.log10(thweal[0:-1]),height=h, width=np.log10(width), alpha=0.5)	
		plt.bar(left=np.log10(thweal[0:-1]),height=h, width=0.1, alpha=0.5)
		plt.subplot(nplot,1,4)		
		wea = np.logspace(2,7,1000)
		plt.plot(np.log10(wea), pdf_wea(wea) )
		plt.subplot(nplot,1,5)
		x_new = np.linspace(500, 1e5,n)		
		f = polylog(coef_f, np.log10(x_new))		
		h = np.diff(percentile_num)			
		width = np.diff(thweal)
		h = 1e2 * h/width#np.mean(f)/np.mean(h)# rescale				
		plt.bar(left=thweal[0:-1],height=np.log10(h), width=width,alpha=0.5)
		plt.plot( x_new, np.log10(f) )
		plt.xlim([1000,10000])
		plt.show()
		assert(True)			
