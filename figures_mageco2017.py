#  figures.py
#  
#  Copyright 2017 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


########################################################
# bmw network
import graph_transaction
example_bmw()

########################################################
# FIG?? : random networks
# http://mriduls-networkx.readthedocs.io/en/betweenness_subset/reference/generators.html
#http://mriduls-networkx.readthedocs.io/en/betweenness_subset/tutorial/tutorial.html#drawing-graphs
#http://mriduls-networkx.readthedocs.io/en/betweenness_subset/reference/drawing.html
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
options = {
    'node_color': 'C0',
    'node_size': 100,
}

G=nx.complete_graph(30)

s=8
pos0 ={0:(0,0), 1:(s,0), 2:(s,s), 3:(0,s)}
pos ={}
for i,n in enumerate(G.nodes_iter()):
	posx,posy = pos0[i%4]
	pos[i]= posx+np.random.rand(),posy+np.random.rand()
	#G.node[n]['group']=

nx.draw(G,pos)
#nx.draw_circular(G)

plt.savefig("graph.pdf")
plt.show()
#################################################
## x-> x/(1+x)

import matplotlib.pyplot as plt
x = np.linspace(0,100,100); y = x/(1+x); plt.plot(x,y); plt.show()


#################################################
## consommation intermédiaire firmes
#http://holoviews.org/reference/elements/bokeh/Graph.html#bokeh-gallery-graph
#http://holoviews.org/user_guide/Network_Graphs.html

# creer circle layout avec secteurs indus A,B..
# creer graph random ficm
# pour chaque secteur, ajouter nb de sommets 
# ajouter connexions
import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
from network import *

def get_network(n,nf = 20,L=100):
	"""
	for more details: see network.py, test_NaioNetwork_sample_consumption_firm_network()
	nf=nb of firms
	L=nb of links
	"""
	fname='data/eurostat_network.h5'
	year='2008';geo='CZ'
	nw_notused = 100	
	n.aggregate_supplyuse(year,geo,unit='MIO_EUR',fname_in=fname)	
	n.get_bd_from_h5(fname, 'bd_9ac_l_form_r2')	
	n.aggregate_bd(year,geo,fname_in=fname)
	n.init_employment_network(nw_notused,nf,unemploy_rate=0)
	n.init_consumption_firm_network_block(L)		
	aij=n.sample_consumption_firm_network()
	return aij,n.result_firm_to_sector.copy(), n.sectors_aggregate.keys() # n.label_sector.copy()

# get aij matrix 
n=NaioNetwork()
nf=1000
L = int(np.floor(1.06 * nf)) # [Atalay11]
aij,result_firm_to_sector,label_sector = get_network(n,nf = nf,L=L)
sectors = np.unique(result_firm_to_sector)
nsec = sectors.shape[0]

# params
padding = dict(x=(-1.2, 1.2), y=(-1.2, 1.2))
scale = 0.1

# position of center nodes
pos_center = {}
for i in range(nsec):
	sec_  = sectors[i]
	#position of centers
	theta = i*2.*np.pi/float(nsec)
	pos_center[sec_]= (np.cos(theta),np.sin(theta), label_sector[sec_])

# positions of nodes	
n_node = aij.shape[0]	
pos_nodes={}
for i in range(n_node):
	sec_ = result_firm_to_sector[i]
	posx_center,posy_center,label = pos_center[sec_]	
	posx,posy = posx_center+np.random.rand()*scale ,posy_center+np.random.rand()*scale
	pos_nodes[i]=(posx,posy)

# edges
DG = nx.DiGraph()
for i in range(aij.shape[0]):
	idx = np.argwhere(aij[i,:])
	s= idx.size
	if s>0:
		#print zip(i*np.ones(s,dtype=int),idx.ravel())
		DG.add_edges_from(zip(i*np.ones(s,dtype=int),idx.ravel()))
	else:
		DG.add_node(i)
	# add attributes	
	sec_  = result_firm_to_sector[i]
	DG.node[i]['sector'] = label_sector[sec_]

# draw
nx.draw(DG,pos_nodes)
#plt.savefig("graph.pdf")
plt.show()

# degree, ...
# https://pypi.python.org/pypi/powerlaw
# http://nbviewer.jupyter.org/github/jeffalstott/powerlaw/blob/master/manuscript/Manuscript_Code.ipynb
# http://www.josephshaheen.com/fitting-power-law-comparing-degree-distribution-synthetic-network-collected-one/2506
# https://github.com/jothemachine/fitting-a-powerlaw/blob/master/Network_Fitting.ipynb
d = DG.degree().values()
import powerlaw
# degree: cdf
powerlaw.plot_cdf(d, color='r')
plt.title("Cumulative Distribution Function")
plt.ylabel("F(x)")
plt.xlabel("Degree Distribution")
plt.show()
# degree: ccdf
powerlaw.plot_ccdf(d, color ='b')
plt.title("CCDF of Degree distribution")
plt.ylabel("P(X > x)")
plt.xlabel("Degree Distribution")
plt.show()
# fit
res = powerlaw.Fit(d,discrete=True)
print("Max Likelihood Estimate of Alpha: %5.3f" % (res.power_law.alpha)) 
cutoffString_gen = '{:,.2f}'.format(res.power_law.xmin)  # I'm just formatting the output
print("Cutoff value, xmin:  "+cutoffString_gen)
#R, p = results.distribution_compare('power_law', 'lognormal')
# fit:plot
fig4 = res.plot_ccdf(linewidth=1.5)
res.power_law.plot_ccdf(ax=fig4, color='r', linestyle='--', label='PL Fit')
res.lognormal.plot_ccdf(ax=fig4, color='g', linestyle='--', label='Lognormal Fit') 
res.exponential.plot_ccdf(ax=fig4, color='y', linestyle='--', label='Exponential Fit') 
res.truncated_power_law.plot_ccdf(ax=fig4, color='m', linestyle='--', label='truncated_power_law')
plt.title("Comparison of Power Law with Other Fits")
plt.legend(loc="lower left")
plt.xlabel("Degree Distribution (log)")
plt.show()

# holoviews from networkx
import holoviews as hv
hv.extension('bokeh')
%opts Graph [width=800 height=800]
renderer=hv.renderer('bokeh')
layout_function = lambda DG: pos_nodes
graph = hv.Graph.from_networkx(DG,layout_function).redim.range(**padding)
		
s=renderer.static_html(graph)
f= open('graph.html', 'w'); 
f.write(s) ; f.close()
# labels ???

#####################
# holoviews only

x = np.zeros(n_node)
y = np.zeros(n_node)
node_indices = range(n_node)
node_labels=[]
for i in range(n_node):
	sec_ = result_firm_to_sector[i]
	posx_center,posy_center,label = pos_center[sec_]	
	x[i]= posx_center+np.random.rand()*scale 
	y[i] = posy_center+np.random.rand()*scale
	node_labels.append(label)
source=[]
target=[]	
for i in range(aij.shape[0]):
	idx = np.argwhere(aij[i,:])
	s= idx.size
	if s>0:
		print idx, s*[i] , idx.ravel().tolist()
		source.extend(s*[i]); target.extend(idx.ravel().tolist() )


nodes = hv.Nodes(( x, y, node_indices, node_labels), vdims='Type')
# FAILS !!
g = hv.Graph((( np.array(source), np.array(target)), nodes), vdims='Label').redim.range(**padding)

s=renderer.static_html(g)
f= open('graph.html', 'w'); 
f.write(s) ; f.close()

###################################################
# wage network
#  http://mriduls-networkx.readthedocs.io/en/latest/reference/algorithms/bipartite.html?highlight=bipartite

#####
# EXAMPLE
# example: https://stackoverflow.com/questions/27084004/bipartite-graph-in-networkx#27085151
import networkx as nx
from networkx.algorithms import bipartite
B = nx.Graph()
# Add nodes with the node attribute "bipartite"
B.add_nodes_from([1, 2, 3, 4], bipartite=0)
B.add_nodes_from(['a', 'b', 'c'], bipartite=1)
# Add edges only between nodes of opposite node sets
B.add_edges_from([(1, 'a'), (1, 'b'), (2, 'b'), (2, 'c'), (3, 'c'), (4, 'a')])
# pos
X, Y = bipartite.sets(B)
pos = dict()
pos.update( (n, (1, i)) for i, n in enumerate(X) ) # put nodes from X at x=1
pos.update( (n, (2, i)) for i, n in enumerate(Y) ) # put nodes from Y at x=2
nx.draw(B, pos=pos)
plt.show()

#########
# The same with wage network
# network.py: test_NaioNetwork_sample_consumption_hh_network

# get data
nw=60;nf=10	
fname='data/eurostat_network.h5'
year='2008';geo='CZ'
n=NaioNetwork()	
# init employment network first (necessary)
n.get_bd_from_h5(fname, 'bd_9ac_l_form_r2')	
n.aggregate_bd(year,geo,fname_in=fname)
n.init_employment_network(nw,nf,unemploy_rate=0)	
# init consumption of hh network
L=3*nw # nb of links ??????????????????????????????????? REAL DATA NEEDED
n.aggregate_dot_products(year,geo,fname_in=fname)
n.init_consumption_hh_network(L)
 #"A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
aij=n.sample_consumption_hh_network()

# make graph
# FAILS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! NODES must be created first: see B.add_nodes_from([1, 2, 3, 4], bipartite=0)
B = nx.Graph()
nf,nh = aij.shape[0], aij.shape[1]

B.add_nodes_from(range(nf), bipartite=0)
B.add_nodes_from(range(nf,nf+nh), bipartite=1)

for i in range(nf):
	idx = np.argwhere(aij[i,:])
	s= idx.size
	if s>0:
		#print zip(i*np.ones(s,dtype=int),idx.ravel())
		B.add_edges_from(zip(i*np.ones(s,dtype=int),nf+idx.ravel()))
# plot
# pos
X, Y = bipartite.sets(B)
pos = dict()
pos.update( (n, (1, i*float(nh)/float(nf))) for i, n in enumerate(X) ) # put nodes from X at x=1
pos.update( (n, (2, i)) for i, n in enumerate(Y) ) # put nodes from Y at x=2
nx.draw(B, pos=pos)
plt.show()
###################################################
# flow / topology correlations

from EP_network import *
e = EP_network()
e.init()
p = e.p
nb,nf,nw = p['nb'],p['nf'],p['nw']
r,alpha0,alpha1,alpha2,delta,k,M = p['r'][0],p['alpha0'][0],p['alpha1'][0] ,p['alpha2'][0],	p['delta'][0],p['k'][0],p['M'][0]
mu,s,A,B,C,D,E,FF = e.run_single(nb,nf,nw,r,alpha0,alpha1,alpha2,delta,k,M)
e.plot_moments_vs_topology(mu,s,A,B,C,D,E,FF)
# corr
