# https://github.com/anna-pa-m/Metabolic-EP
# https://github.com/JuliaPy/PyCall.jl	
# https://github.com/JuliaPy/pyjulia 
#    issue: https://github.com/JuliaPy/pyjulia/issues/119
# jupyter: http://people.duke.edu/~ccc14/sta-663/FromJuliaToPython.html
# https://github.com/JuliaLang/IJulia.jl
# http://gadflyjl.org/stable/tutorial.html#Tutorial-1



# TODO: 
# 0- creer un grand réseau random
# 1- appeler transmat depuis julia, ub=Mtot, lb=0 
# 2- calculer et afficher les pdfs pyimport plt ou https://github.com/stevengj/PyPlot.jl
# 3- manipuler ub, voir résultat
# 4- modifier MetabolicEP pour fixer plusieurs flux experim
# 5- observer l'effet de chgts de params
#	braunstein2008 2017:
#	partir des grandeurs calculées dans GL12: la moyenne doit coincider
#	role spécifiq du réseau ?
#	effets sur les ratio
#	comparaison avec FBA: https://github.com/opencobra/COBRA.jl

# !ne converge pas?? => debug : {sparse par supporté ; float; network incorrect }
# récupérer résulat julia->python, en variant les params
# scenarios param test, faits styliz connus (M, variabilité topo, )
#        + harmoniser HDFStore MultiIndex et Holomap
# rank
# simplif: rref, gaussian elim
#    https://github.com/blegat/RowEchelon.jl , https://github.com/JuliaLang/julia/pull/9804
#    modern theory: http://www-users.cselabs.umn.edu/classes/Spring-2017/csci8314/FILES/LecN6.pdf     
#              impleme ? http://www-users.cselabs.umn.edu/classes/Spring-2017/csci8314/matlab/index.html

# -----------------------------------------------------------------
# MetabolicEP.ipynb

Pkg.add("COBRA")
Pkg.add("SpecialFunctions")
Pkg.add("PyCall")

# 
using COBRA
include("/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl");
using MetabolicEP
using Distributions, Gadfly

# some other models: http://bigg.ucsd.edu/models/iYO844
# fails: met=MetabolicEP.ReadMatrix("/home/aurelien/local/git/Metabolic-EP/iYO844.mat")
met=MetabolicEP.ReadMatrix("/home/aurelien/local/git/Metabolic-EP/Ec_iJR904.mat")
r=MetabolicEP.metabolicEP(met.S,met.b,met.lb,met.ub, beta=1e10)

function plotmarg(i)
    μ,s,l,u=r.μ[i],r.σ[i],met.lb[i],met.ub[i]
    d=Normal(μ,√s);
    D=Truncated(d,l,u);
    #choose bounds to have a sensible plotting range
    m=max(pdf(d,l),pdf(d,u),l<μ<u?1/sqrt(2pi*s):0);
    v=sqrt(-2*s*log(sqrt(2pi*s)*1e-5m));
    l1,l2=max(l,μ-v),min(u,μ+v);
    return Gadfly.plot(x->pdf(D,x),l1,l2,Guide.YLabel(""),Guide.XLabel(met.rxns[i]))
end

#build plot grid with R rows and 5 columns (up to R=19)
R=19
P=[plotmarg(i) for i=1:5R]
Gadfly.set_default_plot_size(30cm,R*5cm)
Gadfly.gridstack(reshape(P,R,5))

#to svg
img = SVG("/home/aurelien/Téléchargements/pdf_Ec_iJR904.svg", 29cm, 21cm)
draw(img, plotmarg(1))
#draw(img, P) # does'nt work

# -----------------------------------------------------------------
# MetabolicEP.ipynb with nontruncated normal superposed
# 
# layers: http://gadflyjl.org/stable/man/layers.html
# set colors manually: https://github.com/GiovineItalia/Gadfly.jl/issues/526

function plotmarg2(i)
    μ,s,l,u=r.μ[i],r.σ[i],met.lb[i],met.ub[i]
    d=Normal(μ,√s);
    D=Truncated(d,l,u);
    #choose bounds to have a sensible plotting range
    m=max(pdf(d,l),pdf(d,u),l<μ<u?1/sqrt(2pi*s):0);
    v=sqrt(-2*s*log(sqrt(2pi*s)*1e-5m));
    l1,l2=max(l,μ-v),min(u,μ+v);
    #plot1 = Gadfly.plot(x->pdf(D,x),l1,l2,Guide.YLabel(""),Guide.XLabel(met.rxns[i]))
    #plot2= Gadfly.plot(x->pdf(d,x),l1,l2,Guide.YLabel(""),Guide.XLabel(met.rxns[i]))
    #return plot( layer(x->pdf(D,x),l1,l2,Guide.YLabel(""),Guide.XLabel(met.rxns[i])),
	#			layer(x->pdf(d,x),l1,l2,Guide.YLabel(""),Guide.XLabel(met.rxns[i]))) 
	return Gadfly.plot( layer(x->pdf(D,x),l1,l2),
				layer(x->pdf(d,x),l1,l2, Geom.point,Theme(default_color=color("orange")))) 
	#return Gadfly.plot(x->pdf(d,x),l1,l2,Guide.YLabel(""),Guide.XLabel(met.rxns[i]))
end

img = SVG("/home/aurelien/Téléchargements/pdf_Ec_iJR904.svg", 29cm, 21cm)
draw(img, plotmarg2(1))

# -----------------------------------------------------------------
# MetabolicEP.ipynb with non-adaptive normal priors 
# see [Braunstein et al.17] eq.16
# 
# dot syntax (".*"): https://docs.julialang.org/en/stable/manual/functions/#man-vectorized-1


met.S,met.b,met.lb,met.ub
# compute mean and variance of a uniform random var with moments given by lb and ub
a = (met.lb+met.ub)/2.
d = (met.lb.^2 + met.ub.^2 - met.lb .* met.ub)/12.

maxvar=1e50   # maximum numerical variance
minvar=1e-50  # minimum numerical variance

beta = 1e10
T= Float64
N = size(a)[1]
#zeros(T,N,N)
v = zeros(T,N)
K=met.S
KKPD = full(beta * K' * K)
KY = beta * met.S' * met.b

# "D is a diagonal matrix with components D_mm = 1/d_m" []p.11
for i in eachindex(d)
        KKPD[i,i] = KKPD[i,i] + 1.0/d[i]
end

invKKPD = clamp(Base.LinAlg.inv(KKPD),minvar,maxvar)
A_mul_B!(v,invKKPD, (KY + a./d)) # since D is diagonal 


function plotmarg3(i,v,KKPD)
    μ,s,l,u=r.μ[i],r.σ[i],met.lb[i],met.ub[i]
    d=Normal(μ,√s);
    d_naive=Normal(v[i],1./√KKPD[i,i]);
    D=Truncated(d,l,u);
    #choose bounds to have a sensible plotting range
    m=max(pdf(d,l),pdf(d,u),l<μ<u?1/sqrt(2pi*s):0);
    v=sqrt(-2*s*log(sqrt(2pi*s)*1e-5m));
    l1,l2=max(l,μ-v),min(u,μ+v);
	return Gadfly.plot( layer(x->pdf(D,x),l1,l2),
				layer(x->pdf(d_naive,x),l1,l2, Geom.point,Theme(default_color=color("orange")))) 
end

# check: should not be too far
sqrt(r.va)
1./√diag(KKPD)
R = similar(KKPD); A_mul_B!(R,KKPD,invKKPD)

# PLOT
img = SVG("/home/aurelien/Téléchargements/pdf_Ec_iJR904.svg", 29cm, 21cm)
i=1
draw(img, plotmarg3(i,v,KKPD))

R=19
P=[plotmarg(i) for i=1:5R]
Gadfly.set_default_plot_size(30cm,R*5cm)
Gadfly.gridstack(reshape(P,R,5))


# -----------------------------------------------------------------
# MetabolicEP.ipynb with nontruncated normal superposed
# with bmw ??

# -----------------------------------------------------------------
# network corresponds to BMW model, random uniform topology in each subnet

using PyCall
using COBRA
include("/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl")
using MetabolicEP
using Distributions, Gadfly

unshift!(PyVector(pyimport("sys")["path"]), "/home/aurelien/local/git/sfc_proba")
@pyimport transmat
C,b,F,g,x0=transmat.example_CSP_problems("bmw_112")
lb = b[1:19]
ub = b[20:38]
res=MetabolicEP.metabolicEP(F,g[:,1],lb,ub)

res.av  # = < nu >_{Q^{(n)}}   => what we're interested in 
res.va  # 

# -----------------------------------------------------------------
# network corresponds to BMW model, random topology from eurostat data with FiCM model

using PyCall
using COBRA
include("/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl")
using MetabolicEP
using Distributions, Gadfly

unshift!(PyVector(pyimport("sys")["path"]), "/home/aurelien/local/git/sfc_proba")
@pyimport transmat
@pyimport network
#nb,nf,nw=10,100,1000  # does't work
nb,nf,nw=3,10,60
p = transmat.bmw_example_param()
Mtot = 1
A,B,C,D,E,FF=network.get_network_matrix(nb=nb,nf=nf,nw=nw,fname="/home/aurelien/local/git/sfc_proba/data/eurostat_network.h5")
F,g,lb,ub = transmat.CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p, M=Mtot)

# conversion from scipy.sparse to julia (https://github.com/JuliaPy/PyCall.jl/issues/204)
const scipy_sparse_find = pyimport("scipy.sparse")["find"]
function mysparse(Apy::PyObject)
    IA, JA, SA = scipy_sparse_find(Apy)
    return sparse(Int[i+1 for i in IA], Int[i+1 for i in JA], SA)
end

res=MetabolicEP.metabolicEP(mysparse(F),g[:,1],lb,ub)

res.av  # = < nu >_{Q^{(n)}}   => what we're interested in 
res.va  #



function plotmarg(i)
    μ,s,l,u=r.μ[i],r.σ[i],met.lb[i],met.ub[i]
    d=Normal(μ,√s);
    D=Truncated(d,l,u);
    #choose bounds to have a sensible plotting range
    m=max(pdf(d,l),pdf(d,u),l<μ<u?1/sqrt(2pi*s):0);
    v=sqrt(-2*s*log(sqrt(2pi*s)*1e-5m));
    l1,l2=max(l,μ-v),min(u,μ+v);
    return Gadfly.plot(x->pdf(D,x),l1,l2,Guide.YLabel(""),Guide.XLabel(met.rxns[i]))
end

#build plot grid with R rows and 5 columns (up to R=19)
R=19
P=[plotmarg(i) for i=1:5R]
Gadfly.set_default_plot_size(30cm,R*5cm)
Gadfly.gridstack(reshape(P,R,5))

#
display(plotmarg(1))
#to svg
img = SVG("iris_plot.svg", 6cm, 4cm)
draw(img, plotmarg(1))


#-------------------------------------------
# CALLING JULIA FROM PYTHON (first install pyjulia and solve issue 119; install pycall
# example = Ec_iJR904.mat
import julia
j = julia.Julia()
j.eval('1 + 1')
j._call("""
	using COBRA
	include("/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl");
	using MetabolicEP
	met=MetabolicEP.ReadMatrix("/home/aurelien/local/git/Metabolic-EP/Ec_iJR904.mat")
""" )
j._call('r=MetabolicEP.metabolicEP(met.S,met.b,met.lb,met.ub)')
mu=j.eval(u'r.μ')
s=j.eval(u'r.σ')

#-------------------------------------------
# CALLING JULIA FROM PYTHON 
# example= random economic network
# NB: mu may be <0 , not necessarily an error since pdf is truncated
#

import julia
j = julia.Julia()
j.eval('PyDict(Dict([("A", 1), ("B", 2)]))')
nb,nf,nw=3,10,600
r_num =0.03  ; delta_num=0.1; k_num = 6. ;
al0_num=0.1 ; al1_num=0.75 ;  al2_num=0.01 ;
Mtot = 10.
j._call(u""" 
	using PyCall
	using COBRA
	include("/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl")
	using MetabolicEP

	unshift!(PyVector(pyimport("sys")["path"]), "/home/aurelien/local/git/sfc_proba")
	@pyimport transmat
	@pyimport network		
	nb,nf,nw=%d,%d,%d	
	p = PyDict(Dict([("r", %f), ("delta", %f), ("k", %f),
				("alpha0", %f),("alpha1", %f), ("alpha2", %f)  ]))
	Mtot = %f
	A,B,C,D,E,FF=network.get_network_matrix(nb=nb,nf=nf,nw=nw,fname="/home/aurelien/local/git/sfc_proba/data/eurostat_network.h5")
	F,g,lb,ub = transmat.CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p, M=Mtot)

	# conversion from scipy.sparse to julia (https://github.com/JuliaPy/PyCall.jl/issues/204)
	const scipy_sparse_find = pyimport("scipy.sparse")["find"]
	function mysparse(Apy::PyObject)
		IA, JA, SA = scipy_sparse_find(Apy)
		return sparse(Int[i+1 for i in IA], Int[i+1 for i in JA], SA)
	end

	r=MetabolicEP.metabolicEP(mysparse(F),g[:,1],lb,ub)
"""%(nb,nf,nw,r_num,delta_num,k_num,al0_num,al1_num,al2_num,Mtot))
mu=j.eval(u'r.μ')
s=j.eval(u'r.σ')
