# -*- coding: utf-8 -*-
"""
Created on september 14th 2018

Module:
    mcmc_topology_flow -  mcmc algorithm to fit both the network topology
						  and the continuous weights.
    
Author:
    Aurélien Hazan
    
Description:
    Implementation of ...
    
Usage:
    Be ``strn_in,strn_out`` two 1-dimensional NumPy arrays. 
    
    Import the module and initialize the ... ::
        >>> import 
        >>> cs = ...
    To create ::
        >>> cs.do_smthg()

References:
.. [Saracco2015] `F. Saracco, R. Di Clemente, A. Gabrielli, T. Squartini,
    Randomizing bipartite networks: the case of the World Trade Web,
    Scientific Reports 5, 10595 (2015)
    <http://www.nature.com/articles/srep10595>`_   
   
https://github.com/umatter/netensembleR/blob/master/R/MnS.MAXandSAM.R

    
""" 
import numpy as np
import scipy
import matplotlib.pyplot as plt
import ficm
import sys; sys.path.append('cython')
import ficm_cython
import nls_solvers
#from scipy.optimize import nnls
from network import *
from network_weights import plot_weighted_network_sfc_FiCM_noMLK
from transmat import *
# ------------------------------------------------------------------------------
# Utils functions
# ------------------------------------------------------------------------------	

def get_strength_CSP_sfc_FiCM_IOinvest_noMLK(x,nb,nf,nh,transac='Id'):
	"""
	see transmat.CSP_sfc_FiCM_IOinvest_noMLK()
	"""
	s_in,s_out=[],[]
	if transac=='Id':
		s_in = np.zeros(nf**2)
		s_out=np.zeros(nf**2)
		# label=['Cd','Id','WBd','ILd','IDd']
		# nf*nh , nf**2, nf*nh, nf*nb, nb*nh
		Id = x[nf*nh:nf*nh + nf**2]
		s_in= [np.sum(x[i::nf]) for i in range(nf)]
		s_out = [np.sum(x[i*nf:(i+1)*nf]) for i in range(nf)]
	else:
		return ValueError('unsupported transaction')
	return np.array(s_in),np.array(s_out)
	
def plot(s_in,s_out):
	"""
	"""
	plt.subplot(2,1,1)
	n = s_in.shape[0]
	plt.plot(range(n),s_in)
	plt.subplot(2,1,2)
	plt.plot(range(n),s_out)
	plt.show()
# ------------------------------------------------------------------------------
# Main class
# ------------------------------------------------------------------------------	

def mcmc_ficm_Ax_eq_b(nb,nf,nh,s_in_0,s_out_0,A,C,D,E,
					IO_mat,sectors_vec,
					fun_get_strength,
					L,	
					solver='nnls',imax=1000, tol=0.001, lambd=0.2)	:
	# check args	
	
	# init
	x0=0.5
	directed = True
	f=ficm.FiCM(s_out_0,s_in_0,L,
				directed=directed,bipartite=False)	
	nnls = nls_solvers.LbfgsNNLS()			
	i=0	
	eps = 1e10	
	s_in = 	s_in_0
	s_out = s_out_0
	s_in_old = s_in 
	s_out_old= s_out
	while(eps>tol and i<imax):	
		# sample B			
		args = (s_out,s_in,s_out.size,s_in.size,1*directed,float(L))
		f.make_ficm(x0=x0,func=ficm_cython._func_cython,args=args)				
		B=f.get_random_biadjacency_matrix()
		# get F,g
		#F,g =CSP_sfc_FiCM_IOinvest_noMLK(nb,nf,nh,
		#								IO_mat,sectors_vec,
		#								A,B,C,D,E)
		F,g,lb,ub,mu_prior = CSP_sfc_FiCM_noMLK_alpha0(nb,nf,nh,A,B,C,D,E,
										alpha0=0.1)									
		# solve nnls
		x=[]
		if solver=='nnls':
			nnls.fit(scipy.sparse.csr_matrix(F),scipy.sparse.csr_matrix(g) 		)
			x= nnls.coef_
			fval = np.mean( np.abs(F.dot(x)-g))# nnls.fval
		else: raise ValueError('unknown solver')
		# get new strengths
		s_in,s_out= get_strength_CSP_sfc_FiCM_IOinvest_noMLK(x,nb,nf,nh,transac='Id')			
		s_in = s_in/np.sum(s_in)
		s_out = s_out/np.sum(s_out)
		# plot
		r={'p':{'network':{'nb':[],'nf':[],'nh':[] }}};
		r['p']['network']['nb']=nb; r['p']['network']['nf']=nf; r['p']['network']['nh']=nh
		r['mu_prior']=[]
		r['mu_post']=x
		r['F'],r['g']=F,g
		#plot_weighted_network_sfc_FiCM_noMLK(r)
		#plot(s_in,s_out)
		# compute residual
		eps = np.mean(np.abs(s_in-s_in_old) ) +np.mean(np.abs(s_out-s_out_old) )
		s_in_old = lambd*s_in  + (1-lambd)*s_in_old
		s_out_old= lambd*s_out  + (1-lambd)*s_out_old
		print "i=",i," eps=",eps, " fval=",fval
		i+=1
	pij = f.get_biadjacency_matrix(f._pij)	#ficm_cython._func_cython ?
									
	return s_in,s_out,pij,eps

def test_mcmc_ficm_Ax_eq_b():
	# see network_weights.py:make_dataset_sfc_*
	fname_eurostat_in = 'data/eurostat_network.h5'
	path_fname_in=fname_eurostat_in 
	nb=3;nf=100;nh=1000 	
	p_fix = {'fname_eurostat_in':path_fname_in,
			'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,#!!!!!instead of 2, to avoid sparse investment
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 
			'nlink_per_hh_wage':1,			
			'algo_weights':'nnls',#{'nnls','bayesian','sparse','leastnorm_pos'}
			#'algo_weights_param':None, 
			'proxy_invest_conso':True,
			'network_model':'rndfitness_unif',
			'sfc_model':"noMLK_alpha0"
			#'sfc_model':"noMLK"
			}
	p_var  = {'year':['2010'], 'geo':['CZ'], 'niter':10,
			 'alpha0':1.}		
	p={'fix':p_fix, 'var':p_var}	
	
	# sample A,C,D,E
	p_fix=p['fix']
	p_var=p['var']
	nb=p_fix['nb']
	nf=p_fix['nf']
	nh=p_fix['nh']
	year=p['var']['year']
	geo=p['var']['geo']
	L_invest= int(p_fix['nlink_per_firm_invest'] *nf)
	L_cons_firm=int(p_fix['nlink_per_firm_cons']*nf)
	L_cons_hh = int(p_fix['nlink_per_hh_cons'] *nh)
	L_wage = int(p_fix['nlink_per_hh_wage'] *nh)
	proxy_invest_conso = p_fix['proxy_invest_conso']
	fitness = np.random.rand(nf)	
	print "init_network_rndfitness..."
	n=init_network_rndfitness(year=year,geo=geo,nb=nb,nf=nf,nh=nh,
		fname=fname_eurostat_in,
		L_invest=L_invest, L_cons_firm= L_cons_firm,
		L_cons_hh = L_cons_hh,
		L_wage= L_wage,
		fitness=fitness,proxy_invest_conso=proxy_invest_conso)
	A,B,C,D,E,FF = n.sample_full_network(nb,nf,nh)
	
	fun_get_strength=get_strength_CSP_sfc_FiCM_IOinvest_noMLK
	IO_mat = np.zeros((nf,nf))
#	for s_i in range(nf):
#		for s_j in range(nf):
#			IO_mat[s_i,s_j] = n.result_io_indus_indus_agg.iloc[s_i,s_j]
	IO_mat = np.random.rand(nf**2).reshape((nf,nf))

	sectors_vec=n.result_firm_to_sector
	s_in_0,s_out_0 = np.ones(nf),np.ones(nf)
	# 
	print "mcmc_ficm_Ax_eq_b..."						
	s_in,s_out,pij,eps = mcmc_ficm_Ax_eq_b(nb,nf,nh,s_in_0,s_out_0,A,C,D,E,
						IO_mat,sectors_vec,
						fun_get_strength,
						L_invest,	
						solver='nnls',imax=1000, tol=0.001)	

# ------------------------------------------------------------------------------
# Test  functions
# ------------------------------------------------------------------------------	


if __name__ == "__main__":
	test_mcmc_ficm_Ax_eq_b()
