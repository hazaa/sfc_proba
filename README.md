Probabilistic tools for SFC models
======

## How to install

*   get source: 

        git clone https://gitlab.com/hazaa/sfc_proba.git
        
*   dependencies:
    *    python2.7, numpy, R, rpy2    

## Description of directories and files

*    transmat.py
*    har_nonuniform.py
*    har_wrap.py
*    /notebooks
*    /tests

Numerical tools
------

*   reduced row echelon form of the augmented matrix of the system: see the [Sage](http://www.sagemath.org/) worksheet [bmw_rref](bmw_rref.sws), in [html](worksheet.html).
*   non-uniform hit-and-run, hypersphere direction, coordinate direction algorithms: see [har_nonuniform](har_nonuniform.py)
*   [numerical issues](numerical_issues.md)


References
------
*    A.Hazan, "Volume of the steady-state space of financial flows in a monetary stock-flow-consistent mode", [arxiv](http://arxiv.org/abs/1601.00822), [Physica A](http://dx.doi.org/10.1016/j.physa.2017.01.050), 2017.
*    WID

TODO
------
*    [DONE] get_WID_FR : smooth pdf


*    suppress the link between har and classes bmw_...
*    use dist to build har 
*    use randkit everywhere
*    set seed everywhere
*    har_hd: how to avoid computing np.prod( p_vec(x)) ?
