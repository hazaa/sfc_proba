# -*- coding:utf-8
#  topological_vs_flow_measures.py
#  
#  Copyright 2018 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
TODO: get rid of PerformanceWarning 'your performance may suffer as PyTables will pickle ...'
https://stackoverflow.com/questions/14355151/how-to-make-pandas-hdfstore-put-operation-faster/14370190#14370190

nb,nf,nw are kept fixed in order to simplify the structure of the DataFrame.

refs:
	pandas: http://pandas.pydata.org/pandas-docs/stable/

defs

* [H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to th
e household j"
* [H17] "B_{nf,nf}[i,j]= 1 if the firm j is selling capital goods t
o the firm i."
	

"""
import os.path
import warnings
from EP_network import *
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pandas as pd
import json
import networkx as nx
from networkx.algorithms import bipartite
from network import from_numpy_matrix_bipartite
from scipy.stats import gaussian_kde
import statsmodels.api as sm

label_sector_agg = ['B-E', 'F', 'G-I', 'J', 'K', 'L', 'M_N', 'O-Q', 'R-U']
marker = ["o","v","^","1","D","s","p","+","*"]	#colors https://matplotlib.org/tutorials/colors/colors.html#sphx-glr-tutorials-colors-colors-py
color = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'purple', 'brown']

######################
## utils

"""
def init_EP_network_rnd(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh):
	# prepare EP_network
	p={'nb':nb,'nf':nf,'nw':nw}
	e = EP_network(p)
	p={}
	p['fitness'] = {'seed':None, 'thin': 1, 'accel':1, 'log':1,
				'f':'UNUSED',#cython_har.pdf_pareto_cython,
				'f_mean': 0,
				'f_nsamp': 'UNUSED', #lambda x:x**2,
				'param1':1.1 , 'param2':0.1	}
	p['fitness']['f_mean'] = p['fitness']['param2']* p['fitness']['param1']/(p['fitness']['param1']-1.)  # b*xlim/b-1	
	e.init_rndfitness_network(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh,fitness)
	return e,p


def init_EP_network_block(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh):	
	r,alpha0,alpha1,alpha2,delta,k,M = p['r'],p['alpha0'],p['alpha1'],p['alpha2'], p['delta'],p['k'],p['M']
	p={'nb':nb,'nf':nf,'nw':nw}
	e = EP_network(p)
	e.init()
	e.init_block_network(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh)
	return e
"""	
def average_neighbor_degree_from_matrix(M):
	"""
	get ANN from numpy matrix
	the results of average neighbor degree can be sorted in multiple ways:
	
	sorted by DEGREE: https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.assortativity.average_degree_connectivity.html?highlight=nearest%20neighbor#networkx-algorithms-assortativity-average-degree-connectivity
	>$ANND_in_out=nx.average_degree_connectivity(g, source='in', target='out')
	sorted by NODE: https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.assortativity.average_neighbor_degree.html#networkx.algorithms.assortativity.average_neighbor_degree
	"""
	g,X,Y = from_numpy_matrix_bipartite(M)
	AND_x = nx.average_neighbor_degree(g, nodes=range(M.shape[0]))				
	AND_x= np.asarray(AND_x.items())
	AND_x= AND_x[ np.array(AND_x[:,0],dtype=int) ,1]  # sort
	AND_y = nx.average_neighbor_degree(g, nodes=range(M.shape[1]))				
	AND_y= np.asarray(AND_y.items())
	AND_y= AND_y[ np.array(AND_y[:,0],dtype=int) ,1] # sort
	return AND_x,AND_y	
	

def test_independence(x,y):
	"""
	test for statistical independence between two multivariate samples
	
	https://arxiv.org/abs/1610.04782
	https://github.com/wittawatj/fsic-test
	copied from https://github.com/wittawatj/fsic-test/blob/master/ipynb/demo_nfsic.ipynb
	"""
	import fsic.util as util
	import fsic.data as data
	import fsic.kernel as kernel
	import fsic.indtest as it
	import fsic.glo as glo
	import matplotlib.pyplot as plt
	import numpy as np
	import scipy.stats as stats
	import scipy
	import theano
	# reshape	
	if x.ndim==1: x.reshape((x.shape[0],1))
	if y.ndim==1: y.reshape((y.shape[0],1))	
	# create ps, train and test
	pdata=data.PairedData(x,y)
	tr, te = pdata.split_tr_te(tr_proportion=0.5)
	# J is the number of test locations
	J = 1
	# Significance level of the test
	alpha = 0.01
	# There are many options for the optimization. 
	# Almost all of them have default values. 
	# Here, we will list a few to give you a sense of what you can control.
	op = {
		'n_test_locs':J,  # number of test locations
		'max_iter':200, # maximum number of gradient ascent iterations
		'V_step':1, # Step size for the test locations of X
		'W_step':1, # Step size for the test locations of Y
		'gwidthx_step':1, # Step size for the Gaussian width of X
		'gwidthy_step':1, # Step size for the Gaussian width of Y
		'tol_fun':1e-4, # Stop if the objective function does not increase more than this
#		'seed':None # random seed
	}
	# Do the optimization with the options in op.
	op_V, op_W, op_gwx, op_gwy, info = it.GaussNFSIC.optimize_locs_widths(tr, alpha, **op )
	# construct an NFSIC test.
	nfsic_opt = it.GaussNFSIC(op_gwx, op_gwy, op_V, op_W, alpha)
	# Perform the independence test on the testing data te.
	# return a dictionary of testing results
	r = nfsic_opt.perform_test(te)
	return r

#######
# plot functions
def plot_kde_by_sector(transtype,xtitle,it=0,
							bw_method='scott'):	
	"""
	"""
	min_ = np.min(transtype[it,:])
	max_ = np.max(transtype[it,:])
	xx = np.linspace(min_,max_, 1000)
	for s in sectors:
		idx = s==firm_to_sector[it,:]
		transtype_by_sector = transtype[it,idx]
		if(transtype_by_sector.size>1):
			kde = gaussian_kde(transtype_by_sector,bw_method=bw_method)# "silverman", 'scott',0.1	
			pdf_xx = kde.evaluate(xx) 
			plt.plot(xx,pdf_xx)
	plt.ylabel("kernel pdf")
	plt.xlabel(xtitle)	

def scatter_plot_by_sector(E,firm_to_sector,
							title,loc='lower right',isec=None):
	if isec==None: I=enumerate(label_sector_agg)
	else:
		I=[(isec,label_sector_agg[isec])]
	for i,l_s in I :		
		m = marker[i]
		idx = np.where(i==firm_to_sector[0,:])[0]
		plt.scatter( i*np.ones(idx.shape[0]*niter ),   E[:,idx].flat,
					color= color[i],#sector_color,
					#s = 1E6 * s_Is.flatten(), 	alpha=0.5,
					marker=m,
					label = l_s			
					)	
		plt.annotate(l_s,xy=(i, 0.5),fontsize=fs,va="center", ha="center")			
	plt.xlabel("sector",fontsize=fs)
	plt.ylabel(title,fontsize=fs)	
	ax = plt.gca()	
	ax.set_xticklabels([])	
	plt.legend(loc=loc)

def hist_by_sector(E,title,loc='lower right',isec=None,n_bins=50,
				histtype='step',fill=False,lim=None,**kwargs):
	#https://matplotlib.org/gallery/statistics/histogram_multihist.html#sphx-glr-gallery-statistics-histogram-multihist-py
	if isec==None: 
		I=enumerate(label_sector_agg)
		c=color
	else:
		I=[(isec,label_sector_agg[isec])]
		c=color[isec]
	x_multi=[]	
	for i,l_s in I :		
		#m = marker[i]
		idx = np.where(i==firm_to_sector[0,:])[0]
		if lim==None:
			x_multi.append(E[:,idx].flatten())
		else:
			dummy = E[:,idx].flatten()	
			idx= (dummy>=lim[0])*(dummy<=lim[1])
			x_multi.append(dummy[idx])				
	plt.hist(x_multi,n_bins,histtype=histtype, normed=True, 
			fill=fill,color=c,**kwargs)
		

def scatter_plot_XY_by_sector(X,Y,firm_to_sector,xlabel,ylabel,isec=None,
							marker=marker,label_sector_agg=label_sector_agg,
							color=color):	
	"""
	for firm data only
	"""
	if not X.shape==Y.shape: 
		raise ValueError('array dimension mismatch')
	if isec==None: I=enumerate(label_sector_agg)
	else:
		I=[(isec,label_sector_agg[isec])]
	for i,l_s in I:		
		m = marker[i]
		idx = np.where(i==firm_to_sector[0,:])[0]
		plt.scatter( X[:,idx].flatten(),  Y[:,idx].flatten(),
					color= color[i],#sector_color,
					#s = 1E6 * s_Is.flatten(), 	alpha=0.5,
					marker=m,
					label = l_s			
					)	
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)	
	plt.legend()

def MetabolicEP_plot(i,mu,s,av,va,lb,ub, samplePts=None,ndots=300):
	# DUPLICATED:!!! (from test_optgpsampler)
	# copied from MetabolicEP.ipynb
	#x = l<mu[ii]<u?1/np.sqrt(2*np.pi*sig[ii]):0
	rv=norm(loc=mu[i],scale=np.sqrt(s[i]))
	x=0
	if(lb[i]<mu[i] and mu[i]<ub[i]): x= 1./np.sqrt(2.*np.pi*s[i])				
	m=max( rv.pdf(lb[i]), rv.pdf(ub[i]),x )
	v=np.sqrt(-2.*s[i]*np.log(np.sqrt(2*np.pi*s[i])*1e-3*m));
	l1,l2=max(lb[i],mu[i]-v),min(ub[i],mu[i]+v)	
	xvals = np.linspace(l1,l2,ndots)
				
	"""xvals=np.linspace(lb[i],ub[i],ndots)"""
	a, b = (lb[i] - mu[i]) / s[i], (ub[i] - mu[i]) /s[i]
	y=truncnorm.pdf(xvals,a,b,loc=mu[i],scale=np.sqrt(s[i]) )
	if not samplePts==None:
		if isinstance(samplePts,pd.DataFrame):
			plt.hist(samplePts.iloc[:,i])
		else:	
			plt.hist(samplePts[:,i])
	else:		
		plt.plot(xvals,y)

def plot_network(A,B,av,va,idx=0,plot_neighbors=False,plot_widget=False):
	""" get solution matrix: transmat * av, transmat*std
	
	input:
	------
  	A: array, shape=[nf,nh]
	[H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
    
    B:array, shape=[nf,nf]
    [H17] "B_{nf,nf}[i,j]= 1 if the firm j is selling capital goods to the firm i."
    
    idx: int
    index of agent
	"""
	pass
	# A:
	# B:investment
	# see plot_network

#############################################
# CREATE DATASET NETWORK
def make_dataset_rndfitness():		
	path_fname_out = 'data/topo_vs_flow_bmw.h5'
	groupname='/rnd_fitness_small_investfirm_L_cons_hh_20'
	maxiterEP = 1000
	nb=3;nf=50;nw=300 

	p_fix = {'nb':nb, 'nf':nf, 'nw':nw, 'nlink_per_firm_cons': 2,  
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 
			'nlink_wage': 1,
			'maxiterEP':maxiterEP, 'proxy_invest_conso':True,
			'network_model':'rndfitness','fitness_param':1.5}
	p_var  = {'year':['2010'], 'geo':['CZ'], 
		'r': [0.03],'alpha0':[0.1] , 'alpha1':[0.75],'alpha2':[0.01],
		'delta' :[0.1],'k':[6], 'M':[10.] ,'niter':5}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset(path_fname_out,groupname,p)

	
def make_dataset_block():
	path_fname_out = 'data/topo_vs_flow_bmw.h5'
	groupname='/small_investfirm_L_cons_hh_20'
	#groupname='/small_investfirm'
	maxiterEP = 1000#!
	nb=3;nf=50;nw=300 

	p_fix = {'nb':nb, 'nf':nf, 'nw':nw, 'nlink_per_firm_cons': 2,  
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 			
			'maxiterEP':maxiterEP, 'proxy_invest_conso':True,
			'network_model':'block','fitness_param':1.5}
	p_var  = {'year':['2010'], 'geo':['CZ'], 
		'r': [0.03],'alpha0':[0.1] , 'alpha1':[0.75],'alpha2':[0.01],
		'delta' :[0.1],'k':[6], 'M':[10.] ,'niter':5}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset(path_fname_out,groupname,p)
	
def make_dataset(path_fname_out,groupname,p):
	"""
	rnd_fitness init can fail => must repeat
	"""	
	# constant params
	repeat = 10
	p_fix=p['fix']
	p_var=p['var']
	maxiterEP = p_fix['maxiterEP']
	nb=p_fix['nb']
	nf=p_fix['nf']
	nw=p_fix['nw']
				
	L_invest= int(p_fix['nlink_per_firm_invest'] *nf)
	L_cons_firm=int(p_fix['nlink_per_firm_cons']*nf)
	L_cons_hh = int(p_fix['nlink_per_hh_cons'] *nw)
	L_wage = None
	proxy_invest_conso = p_fix['proxy_invest_conso']
	label_short,label_long = get_bmw_labels(nb,nf,nw)
	nflow_short=len(label_short)
	nflow_long=len(label_long)	
	p_fix['label_short']=label_short
	p_fix['label_long']=label_long
	fitness_param = None
	if p_fix['network_model']=='rndfitness':
		fitness_param=p_fix['fitness_param']
		L_wage = int(p_fix['nlink_wage'] *nw)
	# !! TODO: save p_fix somewhere in the h5 file !!
	
	# create index and dataframe 
	iterables = [p_var['year'],p_var['geo'],  p_var['r'],
				p_var['alpha0'],p_var['alpha1'] ,p_var['alpha2'],
				p_var['delta'],p_var['k'],p_var['M'], range(p_var['niter'])]
	idx=pd.MultiIndex.from_product(iterables,
							names=['year','geo','r','alpha0','alpha1','alpha2',
							'delta','k','M','niter'])
	df = pd.DataFrame({
					   'mu': [np.zeros(nflow_long) for i in range(len(idx))],
					   'sigma': [np.zeros(nflow_long) for i in range(len(idx))],
					   'av': [np.zeros(nflow_long) for i in range(len(idx))],
					   'va': [np.zeros(nflow_long) for i in range(len(idx))],
					   'ub': [np.zeros(nflow_short) for i in range(len(idx))],
					   'cons_hh_firm_degree_in':  [np.zeros(nw) for i in range(len(idx))],
					   'cons_hh_firm_degree_out':  [np.zeros(nf) for i in range(len(idx))],
					   'cons_hh_firm_AND_in':  [np.zeros(nw) for i in range(len(idx))],
					   'cons_hh_firm_AND_out':  [np.zeros(nf) for i in range(len(idx))],				   
					   'invest_degree_in':  [np.zeros(nf) for i in range(len(idx))],
					   'invest_degree_out':  [np.zeros(nf) for i in range(len(idx))],
					   'invest_AND_out':  [np.zeros(nf) for i in range(len(idx))],
					   'invest_AND_in':  [np.zeros(nf) for i in range(len(idx))],				    
					   'firm_to_sector': [np.zeros(nf) for i in range(len(idx))]
					   },index=idx)
	
	# check file already exists
	if(os.path.exists(path_fname_out)): 	# ALSO CHECK GROUP EXISTS
		#raise IOError('file already exists')
		store = pd.HDFStore(path_fname_out)
		try:
			store.select(groupname+'/dat')
			ans = input("overwrite?")
			if ans=='y': store.remove(groupname+'/dat')
			else:
				store.close()
				raise IOError('file already exists')
		except KeyError:
			pass	
		store.close()
	
	# store config
	p_fix_str = pd.Series(json.dumps(p_fix))
	p_fix_str.to_hdf(path_fname_out,groupname+'/config/'+'p_fix')
	p_var_str = pd.Series(json.dumps(p_var))
	p_var_str.to_hdf(path_fname_out,groupname+'/config/'+'p_var')
	
	# loop through index	
	record_F_done=0				
	for (year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it) in df.index:	
		p  = {'year':year, 'geo':geo, 'nb':nb, 'nf':nf, 'nw':nw,'r': r,
			'alpha0': alpha0, 'alpha1':alpha1,'alpha2':alpha2,
			'delta' : delta,'k':k, 'M':M ,'niter': 0}
		init_network_done = 0	
		# setup	block
		#e = init_EP_network_block(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh)
		e = EP_network(p)
		e.init()
		if p_fix['network_model']=='block':
			e.init_block_network(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh,proxy_invest_conso)		
			init_network_done=1
		elif p_fix['network_model']=='rndfitness':
			for i in range(repeat):
				try:
					e.init_rndfitness_network(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh,L_wage,fitness_param,proxy_invest_conso)
					init_network_done=1
				except julia.core.JuliaError:
					print "!!!!!PyError!!!!! i=%d"%(i)
				if init_network_done: break			
		else:
			raise ValueError('network_model not recognized')	
		# sample a full network
		if init_network_done:
			mu,s,av,va,ub_long,A,B,C,D,E,FF,F,g,firm_to_sector = e.run_network(nb,nf,nw,r,alpha0,alpha1,alpha2,delta,k,M,maxiter=maxiterEP)
		else: 
			raise ValueError('network could not be initialized')	
		# Assortativity vs flow		
		AND_out_A,AND_in_A = average_neighbor_degree_from_matrix(A)
		AND_out_B,AND_in_B = average_neighbor_degree_from_matrix(B)
		"""
		# Degree vs flow
		# firms: nb of buyers (among firms AND hh); sales (invest); sales (custom)
		# firms: nb of banks; amount of interest
		# hh: nb of suppliers, total amount of consumption
		# banks: nb of customers (firms AND hh), total amount of loans per category
		"""
		# to df	
		#self.df.to_hdf(self.path_fname_out,groupname)
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].mu = mu
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].sigma = s
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].av = av
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].va = va
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].ub = e._get_short_ub(ub_long)
		# A: household consumption
		# [H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].cons_hh_firm_degree_in = A.sum(axis=0) 
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].cons_hh_firm_degree_out = A.sum(axis=1) 
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].cons_hh_firm_AND_in = AND_in_A
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].cons_hh_firm_AND_out = AND_out_A
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].firm_to_sector=firm_to_sector 
		# B: firm investments
		if 	p_fix['proxy_invest_conso']:
			# see network.py:init_network_block
			#               :init_consumption_firm_network_block "dij[i,j] : float, increases with the propensity of firms of sector j buying intermediate goods from firms of sector i "
			df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_degree_in = B.sum(axis=0) # check definition above.
			df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_degree_out = B.sum(axis=1)
		else:	
			# [H17] "B_{nf,nf}[i,j]= 1 if the firm j is selling capital goods to the firm i."
			df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_degree_in = B.sum(axis=1) 
			df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_degree_out = B.sum(axis=0)
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_AND_in = AND_in_B
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_AND_out = AND_out_B
		# to hdf
		df.to_hdf(path_fname_out,groupname+'/data')
		if not record_F_done:
			df_F = pd.DataFrame(F)
			df_F.to_hdf(path_fname_out,groupname+'/F')
			record_F_done=1
		# sauvegarder les fitness une fois au moins !!!!!!!
		# !!!!!!!!!!!!!!!!!!!!!
	


###############################################################
###############################################################
# READ DATASET and stack over iterations
# 'r': [0.03],'alpha0':[0.1] , 'alpha1':[0.75],'alpha2':[0.01],'delta' :[0.1],'k':[6], 'M':[10.] ,'niter':2 }
# other accessors:
#	df.xs(0,level="niter").mu
#	df2.iloc[0]
#	np.asarray(df2.mu)

def read_dataset(path_fname_out,groupname):
	"""
	how to use it in jupyter:
	%matplotlib
	from topological_vs_flow_measures import *
	%load -r 486-538 topological_vs_flow_measures.py 
	%load -s scatter_plot_XY_by_sector,hist_by_sector topological_vs_flow_measures.py
	
	path_fname_out= 'data/topo_vs_flow_bmw.h5' #
	#groupname='/small_investfirm' # '/small'
	groupname='/small_investfirm_L_cons_hh_20'
	groupname='/rnd_fitness_small_investfirm_L_cons_hh_20'
	read_dataset(path_fname_out,groupname)
	"""
	warnings.warn('ONLY BLOCK NETWORK DONE SO FAR!!!!!!!!!!')
	
	store = pd.HDFStore(path_fname_out)
	p_fix=store[groupname+'/config/'+'p_fix']
	p_fix =json.loads( p_fix[0].decode())
	p_var=store[groupname+'/config/'+'p_var']
	p_var =json.loads( p_var[0].decode())
	nb, nf,nw = p_fix['nb'],p_fix['nf'],p_fix['nw']
	df = store[groupname+'/data']
	
	# choose a slice corresponding to a specific set of parameters
	#df2 = df.loc[ ('2010', 'CZ' , 0.03, 0.1, 0.75, 0.01, 0.1, 6, 10,  slice(0,1) ) ]
	df2 = df.loc[ ('2010', 'CZ' , 0.03, 0.1, 0.75, 0.01, 0.1, 6, 10,  slice(0,10) ) ]
	mu    = np.vstack(df2.mu)
	sigma = np.vstack(df2.sigma)
	av    = np.vstack(df2.av)
	va = np.vstack(df2.va)
	ub    = np.vstack(df2.ub)
	lb    = np.zeros((ub.shape[0],ub.shape[1]))
	# A
	cons_hh_firm_degree_in = np.vstack(df2.cons_hh_firm_degree_in)
	cons_hh_firm_degree_out = np.vstack(df2.cons_hh_firm_degree_out)
	cons_hh_firm_AND_in  = np.vstack(df2.cons_hh_firm_AND_in)	
	cons_hh_firm_AND_out = 	np.vstack(df2.cons_hh_firm_AND_out)
	# B
	try:
		invest_degree_in  = np.vstack(df2.invest_degree_in)
		invest_degree_out = np.vstack(df2.invest_degree_out)
		invest_AND_in = np.vstack(df2.invest_AND_in)
		invest_AND_out = np.vstack(df2.invest_AND_out)
	except:
		print 'invest_** absent in this dataset'
	
	firm_to_sector = np.vstack(df2.firm_to_sector)
	sectors = np.unique(firm_to_sector)
	nsec = sectors.shape[0]
	label_short,label_long = get_bmw_labels(nb,nf,nw)
	store.close()
	niter = mu.shape[0]
	
	# markers and colors
	L = np.asarray(label_sector_agg,dtype="S");
	# markers https://matplotlib.org/api/markers_api.html#module-matplotlib.markers
	#marker = ["o","v","^","1","D","s","p","+","*"]
	#colors https://matplotlib.org/tutorials/colors/colors.html#sphx-glr-tutorials-colors-colors-py
	#color = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'purple', 'brown', 'tab:pink']
	#color = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'purple', 'brown']
	M=np.asarray(marker,dtype="S"); 
	
	fs = 20 # fontsize
	plt.rcParams['font.size']=20
	plt.rcParams['xtick.labelsize']='large'
	plt.rcParams['ytick.labelsize']='large'
	
def plot_dataset_Cd_Cs():	
	"""
	Cd= household demand, Cs=consumption supply by firms
	select specific flows: Cd  (firm / hh)
	extract from network.py:
	 * [H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
	 * [H18?] exponential graph model: bipartite undirected
	     get houshold consumption demand
	"""
	l = "Cd"
	idx=np.where(np.array(label_long)==l)[0]
	mu_Cd = mu[:,idx]
	s_Cd = sigma[:,idx]
	av_Cd = av[:,idx]
	va_Cd = va[:,idx]
	
	# plot FLATTENED (==merge all iterations)
	nr=2
	nc=2
	plt.subplot(nr,nc,1)
	plt.hist(av_Cd.flatten());
	plt.xlabel("Cd of households")
	plt.subplot(nr,nc,2)
	plt.hist(cons_hh_firm_AND_in.flatten());
	plt.xlabel("AND of households")
	plt.subplot(nr,nc,3)
	plt.scatter( cons_hh_firm_degree_out.flat, av_Cd.flat)
	plt.xlabel("degree of households")
	plt.ylabel("Mean Cd flow of households")		
	plt.subplot(nr,nc,4)
	plt.scatter( cons_hh_firm_AND_in.flat, av_Cd.flat)
	plt.xlabel("AND of households")
	plt.ylabel("Mean Cd flow of households")
	plt.show()
	# plot BY ITERATION
	# plot BY SECTOR
	# WARNING: the following works for Cd data, but returns meaningless results
	#scatter_plot_XY_by_sector(cons_hh_firm_AND_in,av_Cd,"AND_in","av_Cd")
	# => plot by sector is harder, because hh are connected to many firms
	#    that belong to different sectors
	###############################################################
	# select specific flows: Cs  (firm / hh)
	#    get firm consumption supply
	l = "Cs"
	idx=np.where(np.array(label_long)==l)[0]
	mu_Cs = mu[:,idx]
	s_Cs = sigma[:,idx]
	av_Cs = av[:,idx]
	va_Cs = va[:,idx]
	
	# any relation between std and mu ? ????????????????????????
	# plot s_Cs = f(mu_Cs)
	it = 0
	plt.plot(mu_Cs[it,:], s_Cs[it,:])
	
	plt.show()
	plt.xlabel("s_Cs")
	plt.ylabel("mu_Cs")
	
	# plot FLATTENED (==merge all iterations)
	nr=5
	nc=2
	plt.subplot(nr,nc,1)
	plt.hist(av_Cs.flatten(),bins=30);
	plt.xlabel("av Cs")
	plt.subplot(nr,nc,2)
	plt.hist(av_Cd.flatten(),bins=30);
	plt.xlabel("av Cd")
	plt.subplot(nr,nc,3)	
	plt.hist(cons_hh_firm_degree_out.flatten());
	plt.xlabel("degree out")
	plt.subplot(nr,nc,4)
	plt.scatter( cons_hh_firm_degree_out.flat, av_Cd.flat)
	plt.xlabel("degree out")
	plt.ylabel("Mean Cd flow of hh")		
	plt.subplot(nr,nc,5)
	plt.hist(cons_hh_firm_degree_in.flatten());
	plt.xlabel("degree in")		
	plt.subplot(nr,nc,6)
	plt.scatter( cons_hh_firm_degree_in.flat, av_Cs.flat)
	plt.xlabel("degree in ")
	plt.ylabel("Mean Cs flow of firms")		
	plt.subplot(nr,nc,7)
	plt.hist(cons_hh_firm_AND_out.flatten()); 
	plt.xlabel("AND out of firms")
	plt.subplot(nr,nc,8)
	plt.hist(cons_hh_firm_AND_in.flatten()); 
	plt.xlabel("AND in of firms")
	plt.subplot(nr,nc,9)
	plt.scatter( cons_hh_firm_AND_out.flat, av_Cs.flat)
	plt.xlabel("AND out of households")
	plt.ylabel("Mean Cs flow of firms")
	plt.subplot(nr,nc,10)
	plt.scatter( cons_hh_firm_AND_in.flat, av_Cd.flat)
	plt.xlabel("AND in of households")
	plt.ylabel("Mean Cd flow of firms")
	plt.subplots_adjust(wspace=0.5, hspace=0.4)
	plt.show()
	
	# plot BY ITERATION
	#https://matplotlib.org/gallery/lines_bars_and_markers/scatter_demo2.html#sphx-glr-gallery-lines-bars-and-markers-scatter-demo2-py
	nr=2
	nc=2
	niter = mu_Cs.shape[0]
	dim = mu_Cs.shape[1]
	plt.subplot(nr,nc,1)
	for i in range(niter):
		plt.hist(mu_Cs[i,:]);
	plt.xlabel("Cs of firms")
	plt.subplot(nr,nc,2)
	for i in range(niter):
		plt.hist(cons_hh_firm_AND_out[i,:]); 
	plt.xlabel("AND of firms")
	plt.subplot(nr,nc,3)
	plt.scatter( cons_hh_firm_degree_in.flat, mu_Cs.flat,
				c= np.repeat(np.linspace(0,1,niter),dim), 
				s = 1E6 * s_Cs.flatten(), 	alpha=0.5)
				#color= np.repeat(['b','r'],dim))			
	plt.xlabel("degree of firms")
	plt.ylabel("Mean Cs flow of firms")	
	plt.subplot(nr,nc,4)
	plt.scatter( cons_hh_firm_AND_out.flat, mu_Cs.flat,
				c= np.repeat(np.linspace(0,1,niter),dim),
				s = 1E6 * s_Cs.flatten(), 	alpha=0.5
					 )
	plt.xlabel("AND of households")
	plt.ylabel("Mean Cs flow of firms")
	plt.show()
	###############
	# plot BY SECTOR
	nr=2
	nc=2
	fs=20
	"""
	sector_color = np.repeat(np.asarray(firm_to_sector,dtype=np.float),niter)/float(nsec)
	sector_color = np.asarray(sector_color,dtype='U' )
	#sector_label = np.repeat(firm_to_sector,niter)
	"""
	# markers and labels
	sector_label = np.tile(L[firm_to_sector[0,:]],niter)
	markers=np.tile(M[firm_to_sector[0,:]],niter)
	
	# mu_Cs with sector label
	plt.scatter(firm_to_sector.flat, av_Cs.flat)
	for l_s,i in zip(label_sector_agg, np.unique(firm_to_sector)):
		plt.annotate(l_s,xy=(i, 1.1* max(av_Cs.flat)),
					fontsize=fs,va="center", ha="center")
	ax = plt.gca()	
	ax.set_xticklabels([])	
	plt.ylabel('$E[C_s]$',fontsize=fs)
	plt.xlabel('sector',fontsize=fs)
	# calculer moy(moyennes), 
	# degree, mu_Cs
	plt.subplot(nr,nc,1)
	it=0
	title= 'AND of households'
	plot_kde_by_sector(cons_hh_firm_AND_out,title,it=0,bw_method='scott')
	plt.subplot(nr,nc,2)
	it=0
	title= 'Mean Cs flow of firms'
	plot_kde_by_sector(av_Cs,title,it=0,bw_method='scott')
	# with markers	+ colors
	scatter_plot_XY_by_sector(cons_hh_firm_degree_in, av_Cs, firm_to_sector,
								"degree of firms","Mean Cs flow of firms" )
	# AND, mu_Cs
	scatter_plot_XY_by_sector(cons_hh_firm_AND_out,av_Cs, firm_to_sector,
								"AND of households",
								"Mean Cs flow of firms"	)
	# --	
	# SYNTHESE A LA SEABORN (joinplot): av_Cs/degree
	gs = gridspec.GridSpec(2, 2,
	                       width_ratios=[3, 1], height_ratios=[1,3])
	plt.subplot(gs[0])
	plt.hist(cons_hh_firm_degree_out.flatten(),bins=30);
	ax=plt.gca(); ax.set_xticklabels([]);ax.set_yticklabels([])
	min_= np.min(cons_hh_firm_degree_out)-0.5
	max_=np.max(cons_hh_firm_degree_out)+10
	ax.set_xlim((min_,max_))
	# --	
	plt.subplot(gs[2])	
	scatter_plot_XY_by_sector(cons_hh_firm_degree_out, av_Cs,
		 firm_to_sector,
		"out degree of firms","average Cs")
	ax=plt.gca();ax.set_xlim((min_,max_))
	ax.set_ylim((np.min(av_Cs)-0.001,np.max(av_Cs)))	
	#
	# REGRESSION by sector
	X=cons_hh_firm_degree_out ; Y=av_Cs
	l=[]
	for i,l_s in enumerate(label_sector_agg):
		#m = marker[i]
		idx = np.where(i==firm_to_sector[0,:])[0]
		l.append((np.mean(X[:,idx]), np.mean(Y[:,idx]) ))
	l=np.array(l)	
	model = sm.OLS(l[:,1],l[:,0])
	res = model.fit()
	print(res.summary())	
	plt.plot( l[:,0] , res.fittedvalues, 'r--', label="OLS")
	#scatter_plot_XY_by_sector(X,Y,"out degree","average Cs" )
	#plt.xlim([0,350])
	# --	
	plt.subplot(gs[3])	
	plt.hist(av_Cs.flatten(),bins=30,orientation=u'horizontal');
	ax=plt.gca();ax.set_ylim((np.min(av_Cs)-0.001,np.max(av_Cs)))
	ax.set_xticklabels([]);ax.set_yticklabels([])	
	plt.subplots_adjust(wspace=0.001, hspace=0.01)
	
	# SYNTHESE A LA SEABORN (joinplot): av_Cd/degree
	gs = gridspec.GridSpec(2, 2, width_ratios=[3, 1], height_ratios=[1,3])
	plt.subplot(gs[0])
	plt.hist(cons_hh_firm_degree_in.flatten(),bins=50,);
	ax=plt.gca(); ax.set_xticklabels([]);ax.set_yticklabels([])
	min_= np.min(cons_hh_firm_degree_in)-0.5
	max_=np.max(cons_hh_firm_degree_in)+1
	ax.set_xlim((min_,max_))
	plt.subplot(gs[2])	
	plt.scatter( cons_hh_firm_degree_in.flat, av_Cd.flat)
	plt.xlabel("in degree of households")
	plt.ylabel("average Cd")
	#NOT ADAPTED!!:scatter_plot_XY_by_sector(cons_hh_firm_degree_out, av_Cd,
	#							"in degree of households","average Cd")
	ax=plt.gca();ax.set_xlim((min_,max_))
	ymin=np.min(av_Cd)-0.001
	ymax=0.15 #np.max(av_Cd)
	ax.set_ylim((ymin,ymax))								
	plt.subplot(gs[3])	
	plt.hist(av_Cd.flatten(),bins=100,orientation=u'horizontal');
	ax=plt.gca();	ax.set_ylim((ymin,ymax))
	ax.set_xticklabels([]);ax.set_yticklabels([])	
	plt.subplots_adjust(wspace=0.001, hspace=0.01)


	##########
	# SYNTHESE A LA SEABORN: av/AND
		#https://matplotlib.org/api/_as_gen/matplotlib.gridspec.GridSpec.html#examples-using-matplotlib-gridspec-gridspec


	#plt.subplot(nr,nc,1)
	#hist_by_sector(invest_degree_out,"out-degree of firms",isec=None,
	#				stacked=True,fill=False)
	hist_by_sector(invest_degree_out,"out-degree of firms",isec=None,
					histtype='bar',n_bins=10,fill=True)		
	ax=plt.gca(); ax.set_xticklabels([]);ax.set_yticklabels([])
	ax.set_xlim((np.min(invest_degree_out)-0.5,np.max(invest_degree_out)))			
	plt.subplot(gs[2])
	#plt.subplot(nr,nc,3)	
	scatter_plot_XY_by_sector(invest_degree_out,av_Is,"out-degree","average Is")	
	plt.plot(invest_degree_out.flatten(), res.fittedvalues, 'r--', label="OLS")
	ax=plt.gca();ax.set_xlim((np.min(invest_degree_out)-0.5,np.max(invest_degree_out)))
	ax.set_ylim((np.min(av_Is)-0.001,np.max(av_Is)))
	plt.subplot(gs[3])
	#plt.subplot(nr,nc,4)
	#hist_by_sector(av_Is,"av_Is",isec=None,orientation=u'horizontal',fill=False)
	hist_by_sector(av_Is,"av_Is",isec=None,orientation=u'horizontal',
		histtype='bar',n_bins=10,fill=True)
	ax=plt.gca();ax.set_ylim((np.min(av_Is)-0.001,np.max(av_Is)))
	ax.set_xticklabels([]);ax.set_yticklabels([])	
	plt.subplots_adjust(wspace=0.001, hspace=0.01)
	
def check_consistency_intermediate_consumption_firm():
	"""
	compare aggregated intermediate consumption of firm
	with empirical IO table used to build the network
	(for a given iteration)
	"""
	# aggregation
	# get IO table
	# comparison
	pass
def check_consistency_final_consumption_hh():
	"""
	"""
	# aggregation
	# get IO table
	# comparison
	pass
		
	
	
def plot_dataset_Is():				
	######################################################
	# Is,Id,K  (firm / firm)
	# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# recalculer table IO empirique
	# comparer à IO
	#    get firm consumption supply
	#    NB: investment  Is  -Id
	l = "Is"
	idx=np.where(np.array(label_long)==l)[0]
	mu_Is = mu[:,idx]
	s_Is = sigma[:,idx]	
	av_Is = av[:,idx]
	va_Is = va[:,idx]
	l = "Id"
	idx=np.where(np.array(label_long)==l)[0]
	mu_Id = mu[:,idx]
	s_Id = sigma[:,idx]	
	av_Id = av[:,idx]
	va_Id = va[:,idx]
	l = "K"
	idx=np.where(np.array(label_long)==l)[0]
	mu_K = mu[:,idx]
	s_K = sigma[:,idx]
	av_K = av[:,idx]
	va_K = va[:,idx]
	idx_bounds=np.where(np.array(p_fix['label_short'])==l)[0]
	lb_K = lb[:,idx_bounds]
	ub_K = ub[:,idx_bounds]
	
	# plot all pdfs
	i=0
	mu=mu_K[0,:]
	s=s_K[0,:]
	av=av_K[0,:]
	va=va_K[0,:]
	lb=lb_K[0,:][0]*np.ones(nf)
	ub=ub_K[0,:][0]*np.ones(nf)
	f=plt.figure()
	for i in range(10):
		MetabolicEP_plot(i,mu,s,av,va,lb,ub, samplePts=None,ndots=300)
	plt.xlabel("K")
	plt.ylabel("pdf(K)")
	
	# plot FLATTENED (==merge all iterations)
	
	# plot BY ITERATION
	
	# plot BY SECTOR
	nr=2
	nc=2
	# any relation between std and mu ? ????????????????????????
	it = 0
	plt.scatter(av_Is[it,:], s_Is[it,:])
	plt.show()
	plt.ylabel("s_Cs")
	plt.xlabel("mu_Cs")
	# histograms
	plt.hist(av_Is.flatten(),30)
	hist_by_sector(av_Is,"av_Is",loc='lower right',isec=None)
	# mu_Is/av_Is by sector
	scatter_plot_by_sector(mu_Is,firm_to_sector,"mu_Is of firms")
	scatter_plot_by_sector(av_Is,firm_to_sector,"av_Is of firms")
	scatter_plot_by_sector(av_K,firm_to_sector,"av_K firms",loc='upper right')
	
	# AND by sector ???????????
	# kde: mu, AND by sector
	plt.subplot(nr,nc,1)
	it=0
	title= 'AND of firms'
	plot_kde_by_sector(invest_AND_out,title,it=0,bw_method='scott')	
	plt.subplot(nr,nc,2)
	title= 'Mean Is flow of firms'
	plot_kde_by_sector(mu_Is,title,it=0,bw_method='scott')	
	it=0
	
	# in-degree vs mu
	plt.subplot(nr,nc,3)
	scatter_plot_by_sector(invest_degree_out,firm_to_sector,
							"out-degree of firms")
																		
	scatter_plot_XY_by_sector(invest_degree_in,mu_Is,firm_to_sector,"in-degree of firms",
								"mu_Is flow of firms")				
	scatter_plot_XY_by_sector(invest_degree_in,av_Is,firm_to_sector,"in-degree of firms",
								"av_Is flow of firms")
	# out-degree vs mu
	scatter_plot_XY_by_sector(invest_degree_out,av_Is,firm_to_sector,"out-degree of firms",
								"av_Is flow of firms")	
	# OLS regression
	#http://www.statsmodels.org/stable/examples/notebooks/generated/ols.html
	model = sm.OLS(av_Is.flatten(), invest_degree_out.flatten())
	res = model.fit()
	print(res.summary())	
	scatter_plot_XY_by_sector(invest_degree_out,av_Is,firm_to_sector,"out-degree",
								"average Is")
	plt.plot(invest_degree_out.flatten(), res.fittedvalues, 'r--', label="OLS")
	# OLS REGRESSION WITH BREAK
	idx_gt4=invest_degree_out>=4
	model_gt4 = sm.OLS(av_Is[idx_gt4].flatten(), invest_degree_out[idx_gt4].flatten())
	res_gt4 = model_gt4.fit()
	idx_lt4=invest_degree_out<=4
	model_lt4 = sm.OLS(av_Is[idx_lt4].flatten(), invest_degree_out[idx_lt4].flatten())
	res_lt4 = model_lt4.fit()
	scatter_plot_XY_by_sector(invest_degree_out,av_Is,"out-degree",
								"average Is")
	plt.plot(invest_degree_out[idx_lt4].flatten(), res_lt4.fittedvalues, 'r--', label="OLS")
	plt.plot(invest_degree_out[idx_gt4].flatten(), res_gt4.fittedvalues, 'r--')

	
	plt.subplot(nr,nc,4)
	# AND vs mu	
	scatter_plot_XY_by_sector(invest_AND_out,av_Is,firm_to_sector,"AND_out of firms",
								"av_Is flow of firms")
	model = sm.OLS(av_Is.flatten(), invest_AND_out.flatten())
	res2 = model.fit()
	print(res2.summary())										 
	# mu_K vs mu_Is
	scatter_plot_XY_by_sector(av_K,av_Is,"av_K flow of firms",
								"av_Is flow of firms")								
	# SYNTHESE SEABORN (à la jointplot): av_Is/out-degree
	#https://matplotlib.org/api/_as_gen/matplotlib.gridspec.GridSpec.html#examples-using-matplotlib-gridspec-gridspec
	gs = gridspec.GridSpec(2, 2,
	                       width_ratios=[3, 1], height_ratios=[1,3])
	plt.subplot(gs[0])
	#plt.subplot(nr,nc,1)
	#hist_by_sector(invest_degree_out,"out-degree of firms",isec=None,
	#				stacked=True,fill=False)
	hist_by_sector(invest_degree_out,"out-degree of firms",isec=None,
					histtype='bar',n_bins=10,fill=True)		
	ax=plt.gca(); ax.set_xticklabels([]);ax.set_yticklabels([])
	ax.set_xlim((np.min(invest_degree_out)-0.5,np.max(invest_degree_out)))			
	# scatter+regression
	plt.subplot(gs[2])	
	scatter_plot_XY_by_sector(invest_degree_out,av_Is,"out-degree","average Is")	
	#plt.plot(invest_degree_out.flatten(), res.fittedvalues, 'r--', label="OLS")
	plt.plot(invest_degree_out[idx_lt4].flatten(), res_lt4.fittedvalues, 'r--', label="OLS")
	plt.plot(invest_degree_out[idx_gt4].flatten(), res_gt4.fittedvalues, 'r--')
	ax=plt.gca();ax.set_xlim((np.min(invest_degree_out)-0.5,np.max(invest_degree_out)))
	ymin=np.min(av_Is)-0.001;ymax=0.03#np.max(av_Is)
	ax.set_ylim((ymin,ymax))
	plt.subplot(gs[3])
	#plt.subplot(nr,nc,4)
	#hist_by_sector(av_Is,"av_Is",isec=None,orientation=u'horizontal',fill=False)
	hist_by_sector(av_Is,"av_Is",isec=None,orientation=u'horizontal',
		histtype='bar',n_bins=10,fill=True,lim=[ymin,ymax])
	ax=plt.gca();ax.set_ylim((ymin,ymax))
	ax.set_xticklabels([]);ax.set_yticklabels([])	
	plt.subplots_adjust(wspace=0.001, hspace=0.01)
									
	# independence tests							
	r1=test_independence(av_K,av_Is)
	r2=test_independence(np.asarray(invest_degree_out,dtype=float),av_Is)
	
def plot_dataset_ILs():		
	######################################################
	# select specific flows: ILs  (bank/firm)
	pass
	
