# -*- coding: utf-8
#  performance.py
#  
#  Copyright 2017 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
"""
http://perf.readthedocs.io/en/latest/
http://perf.readthedocs.io/en/latest/examples.html#bench-func
"""

import sys
sys.path.append('experimental')
try:
	import har as cython_har
except ImportError:
	print 'cython_har is missing, running pure python'
	raise
import perf
import numpy as np
import har_nonuniform
import gpareto


def bench_piecewise_pure(loops, x,data):
    range_it = range(loops)
    t0 = perf.perf_counter()
    n = x.shape[0]
    for loops in range_it:
		for i in range(n):
			p=gpareto.piecewise(x[i], data)
    return perf.perf_counter() - t0

def bench_piecewise_cython(loops, x,data):
    range_it = range(loops)
    t0 = perf.perf_counter()	
    n = x.shape[0]
    for loops in range_it:
		for i in range(n):
			p=cython_har.piecewise(x[i], data)

    return perf.perf_counter() - t0


def bench_piecewise_pareto_cython(loops, x,data,t, k , a):
    range_it = range(loops)
    t0 = perf.perf_counter()	
    n = x.shape[0]
    for loops in range_it:
		for i in range(n):
			p=cython_har.piecewise_pareto(x[i], data,t, k , a)
    return perf.perf_counter() - t0
	

def bench_piecewise_pareto_pure(loops, x,data,t, k , a):
    range_it = range(loops)
    t0 = perf.perf_counter()	
    n = x.shape[0]
    for loops in range_it:
		for i in range(n):
			p=gpareto.piecewise_pareto(x[i], data,t, k , a)
    return perf.perf_counter() - t0

def bench_piecewise_pareto_vec_pure(loops, x,data,t, k , a):
    range_it = range(loops)
    t0 = perf.perf_counter()	
    n = x.shape[0]
    d = 20
    for loops in range_it:
		for i in range(n-d):
			p=gpareto.piecewise_pareto_vec(x[i:i+d], data,t, k , a)
    return perf.perf_counter() - t0

def bench_piecewise_pareto_vec_native(loops, x,data,t, k , a):
    range_it = range(loops)
    t0 = perf.perf_counter()	
    n = x.shape[0]
    d = 20
    for loops in range_it:
		for i in range(n-d):
			p=cython_har.piecewise_pareto_vec(x[i:i+d], data,t, k , a)
    return perf.perf_counter() - t0

	
def bench_pareto_pure(loops, x,dim):
    range_it = range(loops)
    t0 = perf.perf_counter()

    for loops in range_it:
        p=har_nonuniform.pdf_pareto_one(x[(loops*dim):((loops+1)*dim)])

    return perf.perf_counter() - t0

def bench_pareto_cython(loops, x,dim):
    range_it = range(loops)
    t0 = perf.perf_counter()

    for loops in range_it:
        p=cython_har.pdf_pareto(x[(loops*dim):((loops+1)*dim)])

    return perf.perf_counter() - t0


	
def compare_pareto():	
	x= np.random.rand(1000)
	dim=10
	runner = perf.Runner()	
	# inner-loops: 'pareto_pure' is duplicated 10 times
	b_pareto_pure=runner.bench_sample_func('pareto_pure', bench_pareto_pure,x,dim, inner_loops=10)
	print b_pareto_pure
	b_pareto_cython=runner.bench_sample_func('pareto_cython', bench_pareto_cython,x,dim, inner_loops=10)
	print b_pareto_cython

def compare_piecewise():	
	# random function
	x= np.sort(np.random.rand(30))
	fx= np.random.rand(30)	
	data = np.vstack((fx,x)).T
	data=np.vstack((data,np.array([np.nan,4])))
	xtest = np.linspace(0,1,1000)	
	# inner-loops: 'piecewise_pure' is duplicated 10 times
	runner = perf.Runner()	
	b_piecewise_pure=runner.bench_sample_func('piecewise_pure', bench_piecewise_pure,xtest,data, inner_loops=5)
	print b_piecewise_pure
	b_piecewise_cython=runner.bench_sample_func('piecewise_cython', bench_piecewise_cython,xtest,data, inner_loops=5)
	print b_piecewise_cython

def compare_piecewise_pareto():	
	# random function
	x= np.sort(np.random.rand(30))
	fx= np.random.rand(30)	
	data = np.vstack((fx,x)).T
	data=np.vstack((data,np.array([np.nan,4])))
	xtest = np.linspace(0,1,1000)	
	# set params
	thr, k , a = 3,1,1
	# inner-loops: 'piecewise_pure' is duplicated 10 times
	runner = perf.Runner()	
	b_piecewise_pareto_pure=runner.bench_sample_func('piecewise_pareto_pure', bench_piecewise_pareto_pure,xtest,data,thr, k , a, inner_loops=5)
	print b_piecewise_pareto_pure
	b_piecewise_pareto_cython=runner.bench_sample_func('piecewise_pareto_cython', bench_piecewise_pareto_cython,xtest,data, thr, k , a, inner_loops=5)
	print b_piecewise_pareto_cython

def compare_piecewise_pareto_vec():	
	# random function
	x= np.sort(np.random.rand(30))
	fx= np.random.rand(30)	
	data = np.vstack((fx,x)).T
	data=np.vstack((data,np.array([np.nan,4])))
	xtest = np.linspace(0,1,1000)	
	# set params
	thr, k , a = 3,1,1
	# inner-loops: 'piecewise_pure' is duplicated 10 times
	runner = perf.Runner()	
	#b_piecewise_pareto_vec_pure=runner.bench_sample_func('piecewise_pareto_vec_pure', bench_piecewise_pareto_vec_pure,xtest,data,thr, k , a, inner_loops=5)
	#print b_piecewise_pareto_vec_pure
	b_piecewise_pareto_vec_native=runner.bench_sample_func('piecewise_pareto_vec_native', bench_piecewise_pareto_vec_native,xtest,data,thr, k , a, inner_loops=5)
	print b_piecewise_pareto_vec_native

if __name__ == "__main__":
	#compare_pareto()
	#compare_piecewise()
	#compare_piecewise_pareto()
	compare_piecewise_pareto_vec()
