# -*- coding: utf-8 -*-
"""
Created on sept 1 2017

Module:
    ficm - Fitness Configuration Model with p_ij = z x_i y_j / ( 1+ z x_i y_j)
    
Author:
    Aurélien Hazan, after the Python implementation of BiCM model by M. J. Straka
    
Description:
    Implementation of the Fitness Configuration Model (FiCM) for 
    undirected bipartite networks [Saracco2015]_ or directed monopartite networks.
    Given the strength sequences of a in the form of arrays as input, the module
    allows the user to calculate the biadjacency 
    matrix of the ensemble average graph :math:`<G>^*` of the FiCM null model.
    The matrix entries correspond to the link probabilities :math:`<G>^*_{rc} =
    p_{rc}` between nodes of the two distinct bipartite node sets, or between
    nodes of the directed graph.
    Subsequently, one can generate random networks.
    
Usage:
    Be ``strn_in,strn_out`` two 1-dimensional NumPy arrays, and L the 
    expected number of links. The nodes of the two
    input/output layers are ordered along the rows and columns, respectively. 
    Import the module and initialize the Fitness Configuration Model::
        >>> import ficm
        >>> cm = ficm(strn_in,strn_out,L)
    To create the biadjacency matrix of the FiCM, use::
        >>> cm.make_ficm()

References:
.. [Saracco2015] `F. Saracco, R. Di Clemente, A. Gabrielli, T. Squartini,
    Randomizing bipartite networks: the case of the World Trade Web,
    Scientific Reports 5, 10595 (2015)
    <http://www.nature.com/articles/srep10595>`_
.. [Straka2017] `M. J. Straka, Python implementation of BiCM model 
	<https://github.com/tsakim/bicm>`    
   
    
"""

import numpy as np
from scipy.optimize import root

def max_likelihood_symbolic():
	"""Compute differential of log-likelihood function
	"""
	import sympy 
	a,x, y, z,d,g = sympy.symbols('a x y z d g')
	# pij = z.x_i.y_j/(1+z.x_i.y_j)
	# log-likelihood:
	expr = a * sympy.log(z*x*y) - sympy.log(1+z*x*y)
	sympy.diff(expr, z)
	# pij = z.x_i.y_j.exp(-gamma d_ij)/(1+z.x_i.y_j.exp(-gamma dij))
	expr = a * sympy.log(z*x*y * sympy.exp(-g*d) ) - sympy.log(1+z*x*y* sympy.exp(-g*d))
	sympy.diff(expr, z)
	sympy.diff(expr, g)
	# jac_dyadic
	err0 = z*x*y* sympy.exp(-g*d)  / (1+z*x*y* sympy.exp(-g*d))
	err1 = z*x*y* d* sympy.exp(-g*d)  / (1+z*x*y* sympy.exp(-g*d))
	sympy.Matrix( [[sympy.diff(err0, z), sympy.diff(err1, z)], 
					[sympy.diff(err0, g), sympy.diff(err1, g)]
				  ]) 

def get_random_network_constraint():
	"""
	"""
	# strength up/low bound
	s_low = 1	     
	s_high = 10      
	# nb of nodes in set I
	n_I = 50		
	# nb of nodes in set J	 
	n_J = 100     
	# nb of links
	L = 1000           
	F=0
	# random strength
	s_I = np.random.rand(n_I) #np.random.randint(low=s_low, high=s_high, size=n_I) 
	s_J = np.random.rand(n_J) # np.random.randint(low=s_low, high=s_high, size=n_J)	
	dij=  np.random.rand(n_I,n_J)
	F = np.sum(dij.flat[0:L])
	return s_I,s_J,dij,L,F

# ------------------------------------------------------------------------------
# Test  functions
# ------------------------------------------------------------------------------	
	
	
def test_FiCM():
	"""
	the connection probability is the node-specific form
	pij = z.x_i.y_j/(1+z.x_i.y_j)
	"""	
	s_I,s_J,dij,L,F = get_random_network_constraint()
	# fit FiCM model
	#f=FiCM(s_I,s_J,L,directed=True,bipartite=False)					
	#f=FiCM(s_I,s_J,L,directed=False,bipartite=True)					
	f=FiCM(s_I,s_J,L,directed=False,bipartite=True)					
	x0=0.5	
	f.make_ficm(x0=x0,func=f._func,jac=f._jac)
	print f.sol
	pij = f.get_biadjacency_matrix(f._pij)
	print pij
	aij = f.get_random_biadjacency_matrix()
	print aij
	print f.test_nb_links(aij)
	f.test_average_degrees(aij, eps=1e-2,plot=True)

def test_FiCM_dij():	
	"""
	the connection probability has the node-specific form with a dyadic term dij
	pij = z. d_ij/(1+z.dij))
	"""
	s_I,s_J,dij,L,F = get_random_network_constraint()
	# fit FiCM model
	f=FiCM(s_I,s_J,L,directed=False,bipartite=True,dij=dij)		
	x0=1	
	f.make_ficm(x0=x0, 
				func=f._func_dij,
				jac=None)
	print f.sol
	pij = f.get_biadjacency_matrix(f._pij_dij)
	print pij
	aij = f.get_random_biadjacency_matrix()
	print aij
	print f.test_nb_links(aij)
	f.test_average_degrees(aij, eps=1e-2,plot=True)
	f.test_dij(aij)	
	#
	pij_hat = f.get_pij_empirical(nsamp=1000)
	f.test_pij(pij_hat)
	
def test_FiCM_xi_yj_dij():	
	"""
	the connection probability has the node-specific form with a dyadic term dij
	pij = z.x_i.y_j. d_ij/(1+z.x_i.y_j.dij))
	"""
	s_I,s_J,dij,L,F = get_random_network_constraint()
	# fit FiCM model
	f=FiCM(s_I,s_J,L,directed=False,bipartite=True,dij=dij)		
	x0=1	
	f.make_ficm(x0=x0, 
				func=f._func_xi_yj_dij,
				jac=None)
	print f.sol
	pij = f.get_biadjacency_matrix(f._pij_xi_yj_dij)
	print pij
	aij = f.get_random_biadjacency_matrix()
	print aij
	print f.test_nb_links(aij)
	f.test_average_degrees(aij, eps=1e-2,plot=True)
	f.test_dij(aij)

def test_FiCM_xi_yj_expdij():	
	"""
	the connection probability has the node-specific form with a dyadic term dij
	pij = z.x_i.y_j.exp(-gamma d_ij)/(1+z.x_i.y_j.exp(-gamma dij))
	"""
	s_I,s_J,dij,L,F = get_random_network_constraint()
	# fit FiCM model
	f=FiCM(s_I,s_J,L,directed=False,bipartite=True,F=F,dij=dij)		
	x0=1,10	
	f.make_ficm(x0=x0, 
				func=f._func_xi_yj_expdij,
				jac=None)
	print f.sol
	pij = f.get_biadjacency_matrix(f._pij_xi_yj_expdij)
	print pij
	aij = f.get_random_biadjacency_matrix()
	print aij
	print f.test_nb_links(aij)
	f.test_average_degrees(aij, eps=1e-2,plot=True)
	f.test_dij(aij)

# ------------------------------------------------------------------------------
# Main class
# ------------------------------------------------------------------------------	
	
	
class FiCM(object):
	"""Fitness Configuration Model for undirected binary bipartite networks or 
	directed binary monopartite networks.
	
    This class implements the Fitness Configuration Model (FiCM), which can
    be used as a null model for the analysis of undirected binary bipartite
    networks or directed binary monopartite networks. The class provides
    methods for calculating the biadjacency matrix
    of the null model and for generating random networks.
	
	"""
	def __init__(self, strn_in, strn_out,L,directed=False,bipartite=True,F=None,dij=None):
		"""Initialize the parameters of the CM model.
		:param strn_in: real or integer input matrix describing the strengths
				of the nodes of input layer (in the case of a bipartite network) or the
				input strength of nodes (in the case of a monopartite directed network).
		:type strn_in: numpy.array
		:param strn_out: real or integer input matrix describing the strengths
				of the nodes of output layer (in the case of a bipartite network) or the
				output strength of nodes (in the case of a monopartite directed network).
		:type strn_out: numpy.array
		:param L: number of links
		:type L: float or integer
        """
		self.strn_in = np.array(strn_in, dtype=np.float)
		self.strn_out = np.array(strn_out, dtype=np.float)
		self.dij = dij
		self.directed =directed
		self.bipartite = bipartite
		self.L = float(L)
		self.F = F 
		if not F==None:
			self.F = float(self.F)
		self.check_args()
		[self.dim_in, self.dim_out] = self.strn_in.shape[0],self.strn_out.shape[0]		
		self.sol= None
		self.mat = None
		self.ave_deg_columns = None
		self.ave_deg_rows = None
		if(self.directed==False and self.bipartite==False):
			raise NotImplementedError
		
	def check_args(self):	
		"""Check that the input arrrays have positive entries and ndim=1.
		:raise AssertionError: raise an error if either strn_in or strn_out 
		is not positive or ndim=1					
        """      
		assert np.all(self.strn_in >=0)&np.all(self.strn_out >=0), \
			"Input strength arrays must be >=0."
		assert self.strn_in.ndim==1 & self.strn_out.ndim==1 , 	"ndim must be 1"
		assert self.L>0 , "L must be >0"
		if not(self.F==None):
			pass#assert self.F>=0 , "F must be >=0"
		if isinstance(self.dij,np.ndarray):	
			assert self.dij.ndim==2, "dij must be 2-dim array"
		elif self.dij==None:
			pass	
		else:
			raise TypeError('self.dij has unsupported type')	
	def make_ficm(self, x0=None, method='hybr', tol=None, func=None, jac=None,
                  callback=None, options=None):
		"""Create the biadjacency matrix of the FiCM null model.
		 
		:raise RuntimeError: raise an error if root() fails to converge
		 
		Please refer to the `scipy.optimize.root documentation
		<https://docs.scipy.org/doc/scipy-0.19.0/reference/generated/
		scipy.optimize.root.html>`_ for detailed descriptions.
		"""
		if(func==None):
			raise ValueError('func not defined')
		else:	
			self.sol  = root(fun=func, x0=x0,jac=jac)		
		# check whether system has been solved successfully				
		if not(self.sol.success):
			errmsg = "Try different initial conditions and/or a" + \
                     "different solver, see documentation at " + \
                     "https://docs.scipy.org/doc/scipy-0.19.0/reference/" + \
                     "generated/scipy.optimize.root.html"
			print errmsg
			print self.sol
			raise RuntimeError
	
	def get_biadjacency_matrix(self,pij):
		""" Calculate the biadjacency matrix of the null model.
		The biadjacency matrix describes the FiCM null model, i.e. the optimal
		average graph :math:`<G>^*` with the average link probabilities
		:math:`<G>^*_{rc} = p_{rc}` ,
		:math:`p_{rc} = \\frac{z x_r \\cdot x_c}{1 + z x_r\\cdot x_c}.`
		:math:`z` is the solution of the equation system which has to be
		solved for the null model.
		Note that :math:`r` and :math:`c` may be taken from opposite bipartite
		node sets, if the bipartite case is considered.
		:returns: biadjacency matrix of the null model
		:rtype: numpy.array
		:raises ValueError: raise an error if :math:`p_{rc} < 0` or
			:math:`p_{rc} > 1` for any :math:`r, c`
		"""
		mat = np.zeros((self.dim_in, self.dim_out),dtype=np.float)		
		for i in range(self.dim_in):
			for j in range(self.dim_out):
				if not(i==j):
					mat[i,j]= pij(i,j)       
        # account for machine precision:
		mat += np.finfo(np.float).eps
		if np.any(mat < 0):
			errmsg = 'Error in get_adjacency_matrix: probabilities < 0 in ' \
					+ str(np.where(mat < 0))
			raise ValueError(errmsg)
		elif np.any(mat > (1. + np.finfo(np.float).eps)):
			errmsg = 'Error in get_adjacency_matrix: probabilities > 1 in' \
					+ str(np.where(mat > 1))
			print mat		
			raise ValueError(errmsg)		
		assert mat.shape == (self.dim_in, self.dim_out), \
			"Biadjacency matrix has wrong dimensions."
		if self.mat==None: 
			self.mat = mat    
		return mat
		
	def get_random_biadjacency_matrix(self):			
		"""Get a random adjacency matrix
		:returns: biadjacency matrix of the null model
		:rtype: numpy.array
		Please refer to the `numpy.fill_diagonal documentation
		<https://docs.scipy.org/doc/numpy/reference/generated/numpy.fill_diagonal.html>`_ 
		for detailed descriptions				
		"""
		rnd = np.random.rand(self.dim_in* self.dim_out).reshape((self.dim_in, self.dim_out))
		mat = np.array(rnd < self.mat , dtype=np.int64)
		# no self link
		np.fill_diagonal(mat,0)	
		return mat	
	def get_pij_empirical(self,nsamp=1000):			
		"""Get an estimate of pij
		:returns:  estimate of pij matrix 
		:rtype: numpy.array
		Please refer to the `numpy.fill_diagonal documentation
		<https://docs.scipy.org/doc/numpy/reference/generated/numpy.fill_diagonal.html>`_ 
		for detailed descriptions				
		"""
		av = np.zeros((self.dim_in, self.dim_out))
		for i in range(nsamp):
			rnd = np.random.rand(self.dim_in* self.dim_out).reshape((self.dim_in, self.dim_out))
			mat = np.array(rnd < self.mat , dtype=np.int64)
			av += mat
		# no self link
		np.fill_diagonal(mat,0)	
		return av/float(nsamp)			
# ------------------------------------------------------------------------------
# Model-specific functions
# ------------------------------------------------------------------------------	
		
	def _pij(self,i,j):
		"""
		p_ij = z xi yj / ( 1+ z xi yj)
		"""
		u = self.sol.x[0]
		return (u* self.strn_in[i]*self.strn_out[j])/ \
							(1.+u* self.strn_in[i]*self.strn_out[j]) 
	def _pij_xi_yj_dij(self,i,j):
		"""
		p_ij = z xi yj dij / ( 1+ z xi yj dij)
		"""
		u = self.sol.x[0]
		return (u* self.strn_in[i]*self.strn_out[j]*self.dij[i][j])/ \
							(1.+u* self.strn_in[i]*self.strn_out[j]*self.dij[i][j])
	def _pij_dij(self,i,j):
		"""
		p_ij = z dij / ( 1+ z dij)
		"""
		u = self.sol.x[0]
		return (u* self.dij[i][j])/ \
							(1.+u*self.dij[i][j])						
	def _pij_xi_yj_expdij(self,i,j):
		"""
		p_ij = z xi yj exp(gamma dij) / ( 1+ z xi yj exp(gamma dij))
		"""
		u = self.sol.x[0]
		gamma = self.sol.x[1]
		e_gamma_dij = np.exp( gamma*self.dij[i][j])					
		return (u* self.strn_in[i]*self.strn_out[j]*e_gamma_dij)/ \
							(1.+u* self.strn_in[i]*self.strn_out[j]*e_gamma_dij)		
													
	def _func(self,u):
		"""The equation that must be solved corresponds to :
			p_ij = z xi yj / ( 1+ z xi yj)
		:param u: candidate solution of the equation
		:type u: float
		"""
		r=0.
		for i in range(self.dim_in):
			for j in range(self.dim_out):
					if (self.directed==True) and (i==j):
							pass
					else:		
						r+=  self.strn_in[i]*self.strn_out[j]/ \
							(1.+u* self.strn_in[i]*self.strn_out[j])
		return r*u-float(self.L)	
		
	def _func_xi_yj_dij(self,u):
		"""The equation that must be solved corresponds to :
			p_ij = z xi yj dij/ ( 1+ z x_i y_j dij)
		:param u: candidate solution of the equation
		:type u: float
		"""
		r=0.
		for i in range(self.dim_in):
			for j in range(self.dim_out):
					if (self.directed==True) and (i==j):
							pass
					else:		
						r+=  self.strn_in[i]*self.strn_out[j]*self.dij[i][j]/ \
							(1.+u* self.strn_in[i]*self.strn_out[j]*self.dij[i][j])	
		return r*u-float(self.L)
	def _func_dij(self,u):
		"""The equation that must be solved corresponds to :
			p_ij = z dij/ ( 1+ z dij)
		:param u: candidate solution of the equation
		:type u: float
		"""
		r=0.
		for i in range(self.dim_in):
			for j in range(self.dim_out):
					if (self.directed==True) and (i==j):
							pass
					else:		
						r+=  self.dij[i][j]/ \
							(1.+u*self.dij[i][j])	
		return r*u-float(self.L)
	
	def _func_xi_yj_expdij(self,x):
		"""The equation that must be solved.		
		:param x: candidate solution of the equation
		:type x: array, shape=(2,)	
		:returns: error function
		:rtype: numpy.array, shape=(2,)	
		"""
		err0=0.
		err1=0.
		z,gamma = x[0],x[1]
		for i in range(self.dim_in):
			for j in range(self.dim_out):
					if (self.directed==True) and (i==j):
							pass
					else:		
						# e_gamma_dij = np.exp( -gamma*self.dij[i][j])
						e_gamma_dij = np.exp( gamma*self.dij[i][j])
						err0 +=  self.strn_in[i]*self.strn_out[j]*e_gamma_dij/ \
							(1.+z* self.strn_in[i]*self.strn_out[j]*e_gamma_dij)
		err0 = z*err0- self.L					
		for i in range(self.dim_in):
			for j in range(self.dim_out):
					if (self.directed==True) and (i==j):
							pass
					else:	
						# e_gamma_dij = np.exp( -gamma*self.dij[i][j])	
						e_gamma_dij = np.exp( gamma*self.dij[i][j])
						err1 +=  self.strn_in[i]*self.strn_out[j]*e_gamma_dij*self.dij[i][j]/ \
							(1.+z* self.strn_in[i]*self.strn_out[j]*e_gamma_dij)
		err1 = z*err1- self.F			
		return np.array([err0,err1])
		
	def _jac_xi_yj_dij(self,u):
		"""Jacobian of _func corresponds to :
			p_ij = z xi yj dij / ( 1+ z xi yj dij)
		:param u: Jacobian of the candidate solution of the equation
		:type u: float
		:returns: jacobian
		:rtype: float
		"""
		r=0.
		for i in range(self.dim_in):
			for j in range(self.dim_out):
				if (self.directed==True) and (i==j):
						pass
				else:		
					r+=  self.strn_in[i]*self.strn_out[j]*self.dij[i][j]	/ \
						(1.+u* self.strn_in[i]*self.strn_out[j]*self.dij[i][j]	 )**2					
		return r	
	def _jac_dij(self,u):
		"""Jacobian of _func corresponds to :
			p_ij = z dij / ( 1+ z dij)
		:param u: Jacobian of the candidate solution of the equation
		:type u: float
		:returns: jacobian
		:rtype: float
		"""
		r=0.
		for i in range(self.dim_in):
			for j in range(self.dim_out):
				if (self.directed==True) and (i==j):
						pass
				else:		
					r+= self.dij[i][j]	/ \
						(1.+u*self.dij[i][j] )**2					
		return r		
	def _jac(self,u):
		"""Jacobian of _func corresponds to :
			p_ij = z x_i y_j / ( 1+ z x_i y_j)
		:param u: Jacobian of the candidate solution of the equation
		:type u: float
		:returns: jacobian
		:rtype: float
		"""
		r=0.
		for i in range(self.dim_in):
			for j in range(self.dim_out):
				if (self.directed==True) and (i==j):
						pass
				else:		
					r+=  self.strn_in[i]*self.strn_out[j]/ \
						(1.+u* self.strn_in[i]*self.strn_out[j] )**2								
		return r	
	def _jac_xi_yj_expdij(self,x):
		"""Jacobian of _func_xi_yj_expdij
		:param x: Jacobian of the candidate solution of the equation
		:type x:  array, shape=(2,)	
		:returns: jacobian
		:rtype: numpy.array, shape=(2,2)	
		"""	
		z,g = x[0],x[1]	
		j00,j01,j10,j11=0.,0.,0.,0.
		
		for i in range(self.dim_in):
			for j in range(self.dim_out):
					if (self.directed==True) and (i==j):
							pass
					else:							
						x = self.strn_in[i]
						y = self.strn_out[j]
						e_gamma_dij = np.exp( -g*self.dij[i][j])
						j00+= -z*(x*y*e_gamma_dij)**2/(x*y*z*e_gamma_dij + 1)**2 \
							  + x*y*e_gamma_dij /(x*y*z*e_gamma_dij + 1)
		for i in range(self.dim_in):
			for j in range(self.dim_out):
					if (self.directed==True) and (i==j):
							pass
					else:		
						x = self.strn_in[i]
						y = self.strn_out[j]
						d = self.dij[i][j]
						e_gamma_dij = np.exp( -g*d)						
						j01+= -z*d*(x*y*e_gamma_dij)**2/(x*y*z*e_gamma_dij + 1)**2 \
							  + d*x*y*e_gamma_dij /(x*y*z*e_gamma_dij + 1)
		for i in range(self.dim_in):
			for j in range(self.dim_out):
					if (self.directed==True) and (i==j):
							pass
					else:		
						x = self.strn_in[i]
						y = self.strn_out[j]
						d = self.dij[i][j]
						e_gamma_dij = np.exp( -g*d)						
						j10+= d*(x*y*z*e_gamma_dij)**2/(x*y*z*e_gamma_dij + 1)**2 \
							  - d*x*y*z*e_gamma_dij /(x*y*z*e_gamma_dij + 1)
		for i in range(self.dim_in):
			for j in range(self.dim_out):
					if (self.directed==True) and (i==j):
							pass
					else:		
						x = self.strn_in[i]
						y = self.strn_out[j]
						d = self.dij[i][j]
						e_gamma_dij = np.exp( -g*d)						
						j11+= (d*x*y*z*e_gamma_dij)**2/(x*y*z*e_gamma_dij + 1)**2 \
							  - (d**2) *x*y*z*e_gamma_dij /(x*y*z*e_gamma_dij + 1)		
		return np.array([[j00,j01],[j10,j11]])

# ------------------------------------------------------------------------------
# Test correctness of results:
# ------------------------------------------------------------------------------	
		
	def test_nb_links(self,mat):	
		"""Test the total number of links in a matrix and compare with the constraint.
		:returns: {sum(mat), L, isclose(sum(mat),L)}
		:rtype: dict
		"""
		s=mat.sum()
		return {'isclose':np.isclose(s,self.L ), 'sum(mat)':s, 'L':self.L}
		
	def test_average_degrees(self, adj_mat, eps=1e-2,verbose=False, plot=False):
		"""Test the constraints on the node degrees.
		Check that the degree sequence of the solved FiCM null model graph
		corresponds to the strength sequence of the input graph.
		:param eps: maximum difference between normalized strength of the real network
			and the normalized degrees of the FiCM
		:type eps: float
		"""
		ave_deg_columns = np.squeeze(np.sum(adj_mat, axis=0))
		ave_deg_columns = ave_deg_columns.astype(np.float) / \
						float(ave_deg_columns.sum())
		ave_deg_rows = np.squeeze(np.sum(adj_mat, axis=1))
		ave_deg_rows = ave_deg_rows.astype(np.float) / float(ave_deg_rows.sum())
		strn_in = self.strn_in / self.strn_in.sum()
		strn_out = self.strn_out / self.strn_out.sum()
		if verbose:
			print np.vstack((ave_deg_rows,strn_in)).transpose()
			print np.vstack((ave_deg_columns,strn_out)).transpose()
		c_derr = np.where(np.logical_or(
			# average degree too small:
			ave_deg_rows + eps < strn_in,
			# average degree too large:
			ave_deg_rows - eps > strn_in ))
		p_derr = np.where(np.logical_or(
			ave_deg_columns + eps < strn_out,
			ave_deg_columns - eps >  strn_out ))
		# Check row-nodes degrees:
		if verbose:
			if not np.array_equiv(c_derr, np.array([])):
				print '...inaccurate row-nodes degrees:'
				for i in c_derr[0]:
					print 'Row-node ', i, ':',
					print 'input:', strn_in[i], 'average:', ave_deg_rows[i]
				return False
			# Check column-nodes degrees:
			if not np.array_equiv(p_derr, np.array([])):
				print '...inaccurate column-nodes degrees:'
				for i in c_derr[0]:
					print 'Column-node ', i, ':',
					print 'input:', strn_out[i], \
						'average:', ave_deg_columns[i]
				return False
		if (plot):
			import matplotlib.pyplot as plt 
			plt.subplot(2,1,1)
			plt.scatter(strn_in,ave_deg_rows )
			plt.xlabel('strn_in');plt.ylabel('ave_deg_rows')
			plt.subplot(2,1,2)
			plt.scatter(strn_out,ave_deg_columns )
			plt.xlabel('strn_out');plt.ylabel('ave_deg_columns')
			plt.show()
	def test_dij(self, adj_mat):
		"""
		"""
		import matplotlib.pyplot as plt 
		n=3
		plt.subplot(n,1,1)
		plt.scatter(self.dij.flat, adj_mat.flat)
		plt.xlabel('d_ij');plt.ylabel('a_ij')
		if not(self.F==None):
			print "F_target= %f   F=%f"%(self.F , np.sum(adj_mat*self.dij) )			
		#kernel-density-estimation
		#https://docs.scipy.org/doc/scipy/reference/tutorial/stats.html#kernel-density-estimation
		from scipy import stats
		from functools import partial
		def my_kde_bandwidth(obj, fac=1./2):
			"""We use Scott's Rule, multiplied by a constant factor."""
			return np.power(obj.n, -1./(obj.d+4)) * fac
		plt.subplot(n,1,2)
		fac= 0.2
		kernel0 = stats.gaussian_kde(self.dij.flat[adj_mat.flat==0],
									bw_method=partial(my_kde_bandwidth, fac=fac))
		kernel1 = stats.gaussian_kde(self.dij.flat[adj_mat.flat==1],
									bw_method=partial(my_kde_bandwidth, fac=fac))
		x = np.linspace( np.min(self.dij.flat), np.max(self.dij.flat) ,200 )
		plt.plot(x, kernel0(x),'--',x, kernel1(x))
		plt.subplot(n,1,3)
		if( self.sol.x.shape[0]==2 ):
			gamma = self.sol.x[1]
			e_gamma_dij = np.exp( -gamma*self.dij.flat)
			plt.hist(e_gamma_dij, log=True)
		else:
			plt.hist(self.dij.flat)
		plt.show()
	def test_pij(self, pij_hat):	
		"""
		"""
		import matplotlib.pyplot as plt 
		err= 100 * np.abs((self.mat-pij_hat)/self.mat)
		np.fill_diagonal(err,0)	
		n=2
		plt.subplot(n,1,1)
		plt.plot(err.flat)
		plt.subplot(n,1,2)
		#plt.plot(self.dij.flat,self.mat.flat,'--',	self.dij.flat,pij_hat.flat	)
		plt.scatter(self.dij.flat,self.mat.flat,s=10); 
		plt.scatter(self.dij.flat,pij_hat.flat,s=10,marker='+')
		plt.show()

if __name__ == "__main__":
	#test_FiCM()
	#test_FiCM_xi_yj_expdij()
	#test_FiCM_xi_yj_dij()
	test_FiCM_dij()
