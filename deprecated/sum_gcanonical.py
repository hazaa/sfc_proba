# -*- coding: utf-8 -*-
"""
Created on march 27 2018

Module:
    sum_gcanonical.py - 
    
Author:
    Aurélien Hazan, ...
    
Description:
    Implementation of the ...
    
Usage:
    Be ``strn_in,strn_out`` two ... 
    Import the module and initialize the ... ::
        >>> import sum_gcanonical
        >>> ...


References:
.. [EM06] `Evans Majumdar (2006)
    <http://www.>`_
.. [Filiasi et al] `Filiasi
	<https://...>`    
.. [RC09] `Robert,Casella (2009), Introducing Monte-Carlo methods with R
	<https://...>`	
	
   
"""
from scipy.optimize import root
from scipy.stats import pareto
from scipy.integrate import quad
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.lines as lines
from warnings import warn

def solve_mu(mean,f,x0=0,x1=1E5,mu0=1E-4):
	"""
	find the solution mu* of E:( mean - \int x f(x) exp(-x.mu) dx / \int f(x) exp(-x.mu) dx = 0)
	
	Refs: [EM06]
	
	See also:
	optimize with argument-passing
	https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html#constrained-minimization-of-multivariate-scalar-functions-minimize
	quadrature	with argument-passing
	https://docs.scipy.org/doc/scipy/reference/tutorial/integrate.html#general-integration-quad
	"""
	def xfx_exp(x,mu):
		return x*f(x) * np.exp(-x*mu) 
	def fx_exp(x,mu):
		return f(x) * np.exp(-x*mu) 
	
	def objective_func(mu, m, x0, x1):
		err = m*quad( fx_exp,x0,x1,args=(mu))[0] -quad( xfx_exp,x0,x1,args=(mu))[0]
		return err
	
	sol  = root(fun=objective_func, x0=mu0, args=(mean, x0, x1) )
	return sol

def accept_reject(f,g,g_rng,upper_bound,nsamp=1000):
	"""
	X~f,Y~g. sample from Y, U.
	accept Y=X if U . upper_bound <= f(Y)/g(Y)

	Refs: 	[RC09] §2.3
	
	params:
	-------
	f: function
	target pdf. Takes one real input.
	
	g: function
	instrumental pdf. Takes one real input.
	
	g_rng: function
	random number generation function. Takes one interger input.
	
	upper_bound: float
	see definition above
	
	nsamp: int
	requested number of samples.
	
	output:
	-------	
	x: array, shape=(n,)
	randomly generated samples. size depends on rejection rate.
	
	"""
	U = np.random.uniform(0, upper_bound, nsamp)
	Y = g_rng(nsamp)	
	return Y[ U*g(Y)< f(Y)]
	
def accept_reject_fixed_length(n,f,g,g_rng,upper_bound):
	"""
	get a random vector of n components, in spite of rejection rate,
	using accept-reject.	
	
	refs:
	[RC09]§2.3 for the rejection rate
	"""	
	# compute rejection rate and the size of the sample
	nsamp = int(2*float(n)* upper_bound) # IMPROVE THAT !!!!
	# sample
	x=accept_reject(f,g,g_rng,upper_bound,nsamp=nsamp)
	if x.shape[0]<n: raise RuntimeError('sample is too small')
	return x[0:n]

def init_sums(n,s,f,x0=0,x1=1E5,mu0=1E-4):
	"""
	x = sample_sum(mean,n,f,g,g_rng,upper_bound)	
	"""
	mui=[]
	for ni,si in zip(n,s):
		mean = si/float(ni)
		sol = solve_mu(mean,f,x0=x0,x1=x1,mu0=mu0)		
		if sol.success:
			mui.append(sol.x[0])				
		else:	
			mui.append(np.nan)
			warn('did not converge:'+'ni=%d si=%f'%(ni,si))		
	return np.array(mui)	

def sample_sums(mu,n,f,g,g_rng,xm):
	"""
	sample several subsamples with specified size and mean value.
	this is equivalent to sample several subsamples with specified sums
	(in the mean)

	parameters:
	------------
	mu: array, shape=(m,)
	coefficients of the exponential cutoff for each subsample	
	
	n: array, shape=(m,)
	sizes of each subsample
	
	f: function
	target pdf. Takes one real input.
	
	g: function
	instrumental pdf. Takes one real input.
	
	g_rng: function
	random number generation function. Takes one interger input.

	xm: float
	left side of the support of the pdf, used to evaluate the bound
	in accept-reject method. 

	outputs:
	---------
	x: array, shape=(sum(n),)
	stacked subsamples
	"""	
	x = np.array(())
	for ni,mui in zip(n,mu):
		if not np.isnan(mui):
			def f_target_i(x):
				return f(x)*np.exp(-mui*x)
			bound = f_target_i(xm)/g(xm)
			xi = accept_reject_fixed_length(ni,f_target_i,g,g_rng,bound)			
		else:	
			xi = np.nan * np.ones(ni)
		x= np.hstack((x,xi))
	return x

def example_accept_reject_pareto():
	# Y (=instrumental)
	xm =1.0
	b = 0.5 
	rv = pareto(b)
	g = rv.pdf
	g_rng = rv.rvs
	# X (=target density)
	mean = 5.0
	mu = 0.040483438347899602# such that mean has the specific value above
	f = lambda x: np.exp(-mu*x)*g(x)
	# compute upper bound	
	upper_bound = f(xm)/g(xm)
	return f,g,g_rng,upper_bound,mean
	
###########################################################
# TESTS	
	
	
def test_init_sums():
	"""
	"""
	n= np.array([10,20,30,40])
	s= 5*n + np.array([-1,2,5,10])
	f,g,g_rng,upper_bound,mean = example_accept_reject_pareto()
	mu = init_sums(n,s,f,x0=0,x1=1E5,mu0=1E-4)
	assert not np.any(np.isnan(mu))
	
def check_sample_mean(n,s,x):	
	"""
	"""
	sample_mean = np.zeros(n.shape[0])
	n0=0
	n1 = n[0]
	for i,(ni,si) in enumerate(zip(n,s)):
		#print n0,n1
		xi  = x[n0:n1]
		n0=n1
		n1=n1+ni
		sample_mean[i]= np.mean(xi)
	theor_mean = s/np.asarray(n,float)	
	assert np.all(np.isclose(theor_mean, sample_mean)),theor_mean-sample_mean
	
def test_sample_sums():
	"""
	"""
	n= 1000* np.array([1,1,1,1])
	s= n* (5 + np.array([-0.5,-0.25,0.25,0.35]))
	f,g,g_rng,upper_bound,mean = example_accept_reject_pareto()
	mu = init_sums(n,s,f,x0=0,x1=1E5,mu0=1E-4)
	xm = 1.
	x = sample_sums(mu,n,f,g,g_rng,xm)
	check_sample_mean(n,s,x)
	
def test_sample_sums_plot():
	"""
	"""
	n= 10000* np.array([1,1,1,1])
	s= n* (5 + np.array([-0.5,-0.25,0.25,0.35]))
	f,g,g_rng,upper_bound,mean = example_accept_reject_pareto()
	mu = init_sums(n,s,f,x0=0,x1=1E5,mu0=1E-4)
	xm = 1.
	x = sample_sums(mu,n,f,g,g_rng,xm)
	# plot	
	n0=0
	n1 = n[0]	
	#fig, ax = plt.subplots()	
	#x= np.linspace(0,20,1000)
	for i,(ni,si) in enumerate(zip(n,s)):
		#print n0,n1
		xi  = x[n0:n1]
		n0=n1
		n1=n1+ni
		plt.hist(xi, histtype='step',bins=100,normed=True,stacked=True)		
	plt.show()	
		
		
def test_accept_reject_fixed_length():
	"""
	sample from pareto with fixed sum  (in the mean)
	compute residual
	"""	
	f,g,g_rng,upper_bound,mean = example_accept_reject_pareto()
	n = 100
	x = accept_reject_fixed_length(n,f,g,g_rng,upper_bound)	
	# check that |mean - s/n| is small
	sample_mean = x[0:n]/float(n)
	if np.all(np.isclose( mean, sample_mean)):
		print mean, sample_mean
		raise warnings.warn('sample mean differs from expected')
	

def test_accept_reject_pareto():
	"""
	Y is pareto 
	X target distribution has pdf $f(x).e^{-\mu.x}$ and mean=5.0
	
	see also:
	https://fr.wikipedia.org/wiki/Loi_de_Pareto_(probabilit%C3%A9s)
	"""
	f,g,g_rng,upper_bound,mean = example_accept_reject_pareto()
	# sample
	x = accept_reject(f,g,g_rng,upper_bound,nsamp=10000)
	# compute sample mean
	np.mean(x)
	# plot
	plt.hist(x, histtype='step',bins=100,normed=True,stacked=True)
	xx = np.linspace(0,100,1000)
	plt.plot(xx, f(xx), '--')
	plt.show()
	
	
def test_solve_mu_pareto():
	"""
	Refs: [EM06]
	
	See also:
	https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
	"""
	#####################
	# xm = 1 , 	pareto.pdf(x, b) = b / x**(b+1)		
	mean  = 5.0
	b = 0.5
	# E[X] = +infty since b<=1    (E[X]= b*xlim/(b-1) if b>1 )
	rv = pareto(b)
	sol=solve_mu(mean,rv.pdf,x0=0,x1=1E3,mu0=1E-4)
	if not sol.success:
		raise RuntimeError('did not converge')
	mu = sol.x[0]
	# plot
	x= np.linspace(0,20,1000)
	fig, ax = plt.subplots()
	ax.plot(x, rv.pdf(x),c='g',ls='--', label='pareto')
	ax.plot(x , f(x) * np.exp(-x*mu),c='b',ls='-', label = '$f(x).e^{-\mu.x}$' )
	ax.plot(x , np.exp(-x*mu),c='y',ls='-.', label = '$e^{-\mu.x}$' )
	ax.plot([mean, mean], [0, rv.pdf(1)],c='r',ls='-.', label='mean')
	plt.ylim([0,0.6])
	plt.legend()
	plt.show()
	
	fig, ax = plt.subplots()
	x= np.linspace(0,1000,1000)
	ax.semilogx(x, rv.pdf(x),c='g',ls='--', label='pareto')
	ax.semilogx(x , f(x) * np.exp(-x*mu),c='b',ls='-', label = '$f(x).e^{-\mu.x}$' )
	ax.semilogx(x , np.exp(-x*mu),c='y',ls='-.', label = '$e^{-\mu.x}$' )
	ax.semilogx([mean, mean], [0, rv.pdf(1)],c='r',ls='-.', label='mean')	
	plt.legend()
	plt.show()	
	#####################
	# xm!=1
