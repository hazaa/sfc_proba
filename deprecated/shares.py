# -*- coding: utf-8
#  shares.py
#  
#  Copyright 2017 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


import sys
sys.path.append('experimental')  # REMOVE THIS
import numpy as np
from transmat import example_CSP_problems, get_bmw_labels,bmw_sym_1F1BnW, bmw_get_Mtot
from har_nonuniform import *
from scipy.stats import pareto, genpareto
import time
from gpareto import *
import warnings
import pandas as pd 
from utils import fig_with_function_name
import json
import matplotlib.pylab as plt
from functools import partial


def test_iter_pareto_bmw_alpha0():
	"""
	test that functions are accessible
	"""
	#from shares import * 
	i=iter_pareto_bmw_alpha0()
	groupname,label,config,func_marginal,func_sample,func_icdf=i[0]
	x=func_marginal(1.)
	assert(x>0)
	nsamp = 10 ; s=func_sample(nsamp) 
	assert(s.shape[0]==nsamp)
	x=func_icdf(.5)

def test_iter_piecewise_pareto_bmw_scale():
	"""
	test that functions are accessible
	"""
	#from shares import * 
	i=iter_piecewise_pareto_bmw_scale()
	groupname,label,config,func_marginal,func_sample,func_icdf=i[0]
	x=func_marginal(1.)
	assert(x>0)
	nsamp = 10 ; s=func_sample(nsamp) 
	assert(s.shape[0]<=nsamp)
	x=func_icdf(.5)

def test_iter_piecewise_pareto_WID_bmw_scale():
	"""
	test that functions are accessible
	WID data
	"""
	i=iter_piecewise_pareto_WID_bmw_scale()
	groupname,label,config,func_marginal,func_sample,func_icdf=i[0]
	x=func_marginal(1.)
	assert(x>0)
	nsamp = 10 ; s=func_sample(nsamp) 
	print "s=",s, "s.shape=",s.shape
	assert(s.shape[0]<=nsamp)
	x=func_icdf(.5)

def test_iter_piecewise_pareto_WID_bmw_scale_plot():
	"""
	test that functions plots are ok
	WID data
	"""
	# get iterable
	i=iter_piecewise_pareto_WID_bmw_scale()
	groupname,label,config,func_marginal,func_sample,func_icdf=i[0]
	Mtot = config['model_params']['Mtot']		
	# plot
	x = np.linspace(0,Mtot,1000)	
	plt.subplot(3,1,1)
	plt.plot(x,func_marginal(x))
	plt.subplot(3,1,2)
	p = np.linspace(0,0.999,1000)	
	plt.plot(p,func_icdf(p))
	plt.subplot(3,1,3)
	plt.hist(func_sample(100).flatten(),50)
	plt.show()

def test_iter_piecewise_pareto_WID_bmw_rescaled_pdf():
	"""
	test that marginals in the iterable have different scales
	WID data
	"""
	# get iterable	
	i=iter_piecewise_pareto_WID_bmw_scale()
	groupname,label,config0,func_marginal0,func_sample,func_icdf=i[0]
	Mtot0 = config0['model_params']['Mtot']
	scal0 = config0['model_params']['scale']
	groupname,label,config1,func_marginal1,func_sample,func_icdf=i[1]
	Mtot1 = config1['model_params']['Mtot'] 
	scal1 = config1['model_params']['scale']	
	print "Mtot0",Mtot0,"Mtot1",Mtot1
	print "scal0",scal0,"scal1",scal1
	print func_marginal0,func_marginal1
	#if lambdas
	#func_marginal0.func_closure[1].cell_contents	
	#func_marginal1.func_closure[1].cell_contents
	# checksum
	# plot
	x = np.linspace(0,max(Mtot0,Mtot1),10000)
	plt.plot(x,func_marginal0(x))
	plt.plot(x,func_marginal1(x),'+' )
	plt.show()

def test_iter_piecewise_pareto_bmw_scale_plot():
	"""
	test that functions plots are ok
	"""	
	raise NotImplementedError
	
	i=iter_piecewise_pareto_bmw_alpha0()
	groupname,label,config,func_marginal,func_sample,func_icdf=i[0]
	
	# get xmax, see gpareto.py:get_piecewise_pareto()
	thr,perc, b, yav,total_income,nb_household=income_tabulation()	
	thr *= np.array(rescale_factor) # r*list(...) raises an error
	xmax  = np.max(thr)+rescale_factor*1000
	
	x = np.linspace(0,xmax,100)
	p = np.linspace(0,1,100)
	plt.subplot(2,1,1); plt.plot(x, func_marginal(x))
	plt.subplot(2,1,2); plt.plot(p, func_icdf(p))
	plt.show()

def test_iter_pareto_bmw_alpha0_plot():
	"""
	test that functions plots are ok
	"""
	i=iter_pareto_bmw_alpha0()
	groupname,label,config,func_marginal,func_sample,func_icdf=i[0]
	Mtot = config['model_params']['Mtot']
	x = np.linspace(0,Mtot,100)
	p = np.linspace(0,1,100)
	plt.subplot(2,1,1); plt.plot(x, func_marginal(x))
	plt.subplot(2,1,2); plt.plot(p, func_icdf(p))
	plt.show()
	
def test_iter_pareto_bmw_alpha0_quad():
	"""
	test that functions quadratures are ok
	
	see gareto.py:test_share_of_revenue_from_quad_pdf()
	"""	
	
	i=iter_pareto_bmw_alpha0()
	groupname,label,config,func_marginal,func_sample,func_icdf=i[0]	
	def f(x):
		return x*func_marginal(x)		
	x0= func_icdf( 0.0 )
	x1= func_icdf( 0.5 )	
	print "x0=",x0, "x1=",x1
	int_0_50 =  quad(f,x0,x1)[0]
	x0= func_icdf( 0.5 )
	x1= func_icdf( 0.9 )
	print "x0=",x0, "x1=",x1
	int_50_90 =  quad(f,x0,x1)[0]	
	## doesnt work
	x0= func_icdf( 0.9 )
	x1= func_icdf( 1. )
	print "x0=",x0, "x1=",x1
	int_90_100 =  quad(f,x0,x1)[0]	
	print "int_0_50=",int_0_50, " int_50_90=",int_50_90, " int_90_100=", int_90_100
	s = int_0_50+int_50_90+int_90_100
	print "sha_0_50=",int_0_50/s, " sha_50_90=",int_50_90/s, " sha_90_100=", int_90_100/s
	# checksum
	Mtot = config['model_params']['Mtot']
	nw = config['model_params']['nw']
	mean = Mtot/float(nw)
	delta = abs(mean - s)
	err_rel = 100.*delta/Mtot
	print "Mtot/nw=%f  sum=%f  err=%f   relative err=%f  pct "%(mean,s,delta,err_rel)
	
	
def test_pareto_compute_shares_WID_bmw():
	"""
	WID data
	"""
	fname = 'data/share.h5'	
	it= iter_piecewise_pareto_WID_bmw_scale()
	compute_shares_bmw(it,fname)
			


def test_pareto_compute_shares_bmw():
	"""
	"""
	fname = 'data/share.h5'	
	it=iter_pareto_bmw_alpha0()
	compute_shares_bmw(it,fname)
	
# ---------------------------------------------------------------

def iter_pareto_bmw_alpha0():
	"""
	create list of parameters and functions ready to be called 
	(e.g. by routine that computes and records)
	
	bmw model with nf=nb=1
	
	alpha0 is the control param
	
	pdf = pareto
	
	input:
	------
	alpha0: list,len=n
	
	output:
	------
	l: list, len=n
	
	"""
	groupname = '/bmw_11N/pareto'	
	nb,nf,nw=1,1,50	
	nsamp = 1000
	label,label_long = get_bmw_labels(nb,nf,nw)
	idx_first_WBs = label_long.index('WBs')
	idx_first_M = label_long.index('M')
	# thin 
	thin = 10
	accel = 0
	# make list: config
	config = [{ 
			'label':'cd_0_01',
			'model_params':{'model':'bmw','nw':nw,'nb':nb, 'nf':nf, 
						   'r': 0.03,'alpha0':0.01 , 'alpha1':0.75,'alpha2':0.01,'delta' :0.1,'k':6. ,
						   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
						   'idx_first_M':idx_first_M },
		    'sampler_params':{'seed':123, 'uniform':False,'native':True, 'algo': 'CD',
								'nsamp':nsamp, 'thin':thin},
			'experiment_params': {'percentiles': [0.9,0.5,0.1]}								   
		  	},
		  	{ 'label': 'cd_0_02',
			'model_params':{'model':'bmw','nw':nw,'nb':nb, 'nf':nf, 
						   'r': 0.03,'alpha0':0.02, 'alpha1':0.75,'alpha2':0.01,'delta' :0.1,'k':6. ,
						   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
						   'idx_first_M':idx_first_M },
		    'sampler_params':{'seed':123, 'uniform':False,'native':True, 'algo': 'CD',
								'nsamp':nsamp,'thin':thin},
			'experiment_params': {'percentiles': [0.9,0.5,0.1]}								   
		  	},
		  { 'label': 'cd_0_1',
			'model_params':{'model':'bmw','nw':nw,'nb':nb, 'nf':nf, 
						   'r': 0.03,'alpha0':0.1 , 'alpha1':0.75,'alpha2':0.01,'delta' :0.1,'k':6. ,
						   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
						   'idx_first_M':idx_first_M },
		    'sampler_params':{'seed':123, 'uniform':False,'native':True, 'algo': 'CD',
								'nsamp':nsamp,'thin':thin},
			'experiment_params': {'percentiles': [0.9,0.5,0.1]}								   
		  	},
		  	{ 'label':'hd_0_1',
			'model_params':{'model':'bmw','nw':nw,'nb':nb, 'nf':nf, 
						   'r': 0.03,'alpha0':0.1 , 'alpha1':0.75,'alpha2':0.01,'delta' :0.1,'k':6. ,
						   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
						   'idx_first_M':idx_first_M },
		    'sampler_params':{'seed':123, 'uniform':False,'native':True, 'algo': 'HD',
								'nsamp':nsamp,'thin':thin},
			'experiment_params': {'percentiles': [0.9,0.5,0.1]}								   
		  	}		
	]
	# make list: groupname
	n = len(config)
	groupname = [groupname]*n
	# make list: label	
	label = [ c['label']  for c in config  ]
	""" DOESN'T WORK. WHY ??
	config = []
	for a in alpha0:		
		config.append( dict(conf) )
	for i,a in enumerate(alpha0):				
		config[i]['model_params']['alpha0']=a  """
	# make list: func_marginal
	func_marginal=[]
	for c in config	:
		# Mtot is necessary
		a = c['model_params']['alpha0']
		alpha0_agreg = a*nw		
		Mtot = bmw_get_Mtot(c['model_params']['k'], alpha0_agreg, 
							c['model_params']['alpha1'],
						    c['model_params']['alpha2'],c['model_params']['delta'])						    
		# prepare pdf
		b= 	float(Mtot) / (Mtot -float(nw)	)
		if(c['sampler_params']['algo']=="HD"):
			print "UGLY HACK"
			func_marginal.append( lambda x: np.prod(pareto(b).pdf(x)) )
		else:	
			func_marginal.append( pareto(b).pdf )
		# add Mtot and b to config
		c['model_params']['Mtot']=Mtot
		c['model_params']['b']=b
		# check mean is close to Mtot/nw
		delta = abs( pareto(b).mean() - Mtot/float(nw) )
		if( delta>0.01 ): 
			print "b=",b, " pareto(b).mean() ",pareto(b).mean(), " Mtot/nw=",Mtot/float(nw)
			raise ValueError("mean is inconsistent")
	# make list: func_sample
	func_sample=[]		
	for c,f_m in zip(config,func_marginal):
		p_num   = c['model_params']
		uniform = c['sampler_params']['uniform']
		native  = c['sampler_params']['native']
		algo    = c['sampler_params']['algo'] 	
		thin    = c['sampler_params']['thin']
		bmw = bmw_sym_1F1BnW(nw,p_num=p_num)
		bmw.init()	
		f = partial(bmw.get_har_samples, pdf=f_m, uniform=uniform,native=native, algo=algo,thin=thin,accel=accel)		
		func_sample.append( f )
	# make list: func_icdf
	func_icdf=[]
	for c in config	:
		alpha0_ = c['model_params']['alpha0']
		k_ =c['model_params']['k']
		alpha1_=c['model_params']['alpha1']
		alpha2_=c['model_params']['alpha2']
		delta_=c['model_params']['delta']
		# Mtot is necessary
		alpha0_agreg = alpha0_*nw		
		Mtot = bmw_get_Mtot(k_, alpha0_agreg, alpha1_, alpha2_, delta_)						    		
		# prepare pdf
		b= 	float(Mtot) / (Mtot -float(nw)	)		
		func_icdf.append( pareto(b).ppf )
	return zip(groupname,label,config,func_marginal,func_sample,func_icdf)		


def iter_piecewise_pareto_bmw_scale(s = [1,1.1]):
	"""
	same as iter_pareto_bmw_alpha0, except:
	pdf = piecewise_pareto instead of pareto. data taken from income tabulation
		  see gpareto.py:income_tabulation()
	"""
	n = len(s)
	nb,nf,nw=1,1,30	
	nsamp = 10000
	label,label_long = get_bmw_labels(nb,nf,nw)
	idx_first_WBs = label_long.index('WBs')
	idx_first_M = label_long.index('M')
	# make list: groupname
	groupname = ['/bmw_11N/piecewise_pareto']*n
	# make list: label
	label = s
	# make list: config
	config = [{ 
			'model_params':{'model':'bmw','nw':nw,'nb':nb, 'nf':nf, 
						   'r': 0.03,'alpha0':s[0] , 'alpha1':0.75,'alpha2':0.01,'delta' :0.1,'k':6. ,
						   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
						   'idx_first_M':idx_first_M },
		    'sampler_params':{'seed':123, 'uniform':False,'native':False, 'algo': 'CD',
								'nsamp':nsamp},
			'experiment_params': {'percentiles': [0.9,0.5,0.1]}								   
		  	},
		  { 
			'model_params':{'model':'bmw','nw':nw,'nb':nb, 'nf':nf, 
						   'r': 0.03,'alpha0': s[1], 'alpha1':0.75,'alpha2':0.01,'delta' :0.1,'k':6. ,
						   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
						   'idx_first_M':idx_first_M },
		    'sampler_params':{'seed':123, 'uniform':False,'native':False, 'algo': 'CD',
								'nsamp':nsamp},
			'experiment_params': {'percentiles': [0.9,0.5,0.1]}								   
		  	}		
	]
	# make list: func_marginal
	func_marginal=[]
	#for a in alpha0:
	for c in config	:		
		# Mtot is necessary
		a = c['model_params']['alpha0']
		alpha0_agreg = a*nw		
		Mtot = bmw_get_Mtot(c['model_params']['k'], alpha0_agreg, 
							c['model_params']['alpha1'],
						    c['model_params']['alpha2'],c['model_params']['delta'])
		# compute scale				    
		# ?????? scale = WBs_avg / yav      =>   scale = (Mtot/nw) / average_empirical_wealth
		func_marginal.append( get_piecewise_pareto(rescale_factor=a) ) # ???????? a ??
	# make list: func_sample
	func_sample=[]		
	for c,f_m in zip(config,func_marginal):
		p_num   = c['model_params']
		uniform = c['sampler_params']['uniform']
		native  = c['sampler_params']['native']
		algo    = c['sampler_params']['algo'] 	
		bmw = bmw_sym_1F1BnW(nw,p_num=p_num)
		bmw.init()	
		f = partial(bmw.get_har_samples, pdf=f_m, uniform=uniform,native=native, algo=algo)		
		func_sample.append( f )	
	# make list: func_icdf
	func_icdf=[]
	for a in s:
		thr,perc, b, yav,total_income,nb_household=income_tabulation()
		Finv = icdf_from_tabulation_linear(thr,perc)				
		func_icdf.append( Finv )
	return zip(groupname,label,config,func_marginal,func_sample,func_icdf)		


def iter_piecewise_pareto_WID_bmw_scale(s = [1,1.1]):
	"""
	same as iter_pareto_bmw_alpha0, except:
	pdf = piecewise_pareto instead of pareto. data taken from WID 
		  see gpareto.py:wealth_income_FR_2013()
	"""	
	n = len(s)
	nb,nf,nw=1,1,30	
	nsamp = 100000
	label,label_long = get_bmw_labels(nb,nf,nw)
	idx_first_WBs = label_long.index('WBs')
	idx_first_M = label_long.index('M')
	# WID data
	year = 2010
	pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea = get_WID_FR(year,value='wealth')
	# make list: groupname
	groupname = ['/bmw_11N/piecewise_pareto_WID']*n
	# make list: label
	label = s
	# make list: config
	config = [{ 
			'model_params':{'model':'bmw','nw':nw,'nb':nb, 'nf':nf, 
						   'r': 0.03,'alpha0':0.1 *s[0] , 'alpha1':0.75,'alpha2':0.01,'delta' :0.1,'k':6. ,
						   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
						   'idx_first_M':idx_first_M },
		    'sampler_params':{'seed':123, 'uniform':False,'native':False, 'algo': 'CD',
								'nsamp':nsamp},
			'experiment_params': {'percentiles': [0.9,0.5,0.1]}								   
		  	},
		  { 
			'model_params':{'model':'bmw','nw':nw,'nb':nb, 'nf':nf, 
						   'r': 0.03,'alpha0':0.1*s[1] , 'alpha1':0.75,'alpha2':0.01,'delta' :0.1,'k':6. ,
						   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
						   'idx_first_M':idx_first_M },
		    'sampler_params':{'seed':123, 'uniform':False,'native':False, 'algo': 'CD',
								'nsamp':nsamp},
			'experiment_params': {'percentiles': [0.9,0.5,0.1]}								   
		  	}		
	]
	# make list: func_marginal
	func_marginal=[]	
	def pdf_wea_rescale(x,scale):
			# nb: not necessarily normalized, because of MCMC algorithm
			return pdf_wea(x*scale)
	for c in config	:		
		# Mtot is necessary
		a = c['model_params']['alpha0']
		alpha0_agreg = a*nw		
		Mtot = bmw_get_Mtot(c['model_params']['k'], alpha0_agreg, 
							c['model_params']['alpha1'],
						    c['model_params']['alpha2'],c['model_params']['delta'])
		# add Mtot,scale to params				    
		c['model_params']['Mtot']=Mtot				    					   
		scal =  mean_wea / (Mtot/float(nw))
		c['model_params']['scale']=scal					
		#func_marginal.append(pdf_wea_rescale )
		#func_marginal.append( lambda x : pdf_wea(x*scal) )
		func_marginal.append( partial(pdf_wea_rescale,scale=scal))
	# make list: func_sample
	func_sample=[]		
	for c,f_m in zip(config,func_marginal):
		p_num   = c['model_params']
		uniform = c['sampler_params']['uniform']
		native  = c['sampler_params']['native']
		algo    = c['sampler_params']['algo'] 	
		bmw = bmw_sym_1F1BnW(nw,p_num=p_num)
		bmw.init()	
		f = partial(bmw.get_har_samples, pdf=f_m, uniform=uniform,native=native, algo=algo)		
		func_sample.append( f )	
	# make list: func_icdf
	func_icdf=[]
	for c in config	:		
		# Mtot is necessary
		Mtot = 	c['model_params']['Mtot']
		scal =  mean_wea / (Mtot/float(nw))
		def Finv_wea_rescale(x):
			# NB: output may be > Mtot, but not important for our purpose 
			# (which is finding threshold p50, p90)
			return Finv_wea(x)/scal
		func_icdf.append( Finv_wea_rescale )
	return zip(groupname,label,config,func_marginal,func_sample,func_icdf)		

# ---------------------------------------------------------------	

def share_income_from_pdf_analytic_marginal(scale):
	"""
	compute the share of total income of three groups of households:
	with incomes in percentile 0 to 50; 50 to 90; 90 to 100%
	using integration of the analytic marginal pdf
	
	see Evans et al http://arXiv.org/abs/cond-mat/0510512v1
	§2 "the characteristic mass diverges giving rise to a power law p(m) ~= f (m) at the critical density."
	
	in:
	---
	scale: list, len=n
	contains floats or None. Example: scale= [None, 1.1]
	
	out:
	---
	shares: list
	contains n lists with 3 floats each. shares = [[s1_1,s1_2,s1_3],...]
	"""
	raise DeprecationWarning
	
	shares=[]
	for s in scale:	
		# the marginal can be approximated by the single-site weight in this case
		pdf,xmax,yav,total_income=get_piecewise_pareto_with_parameters(onedim=True,
														rescale_factor=s)
		def f(x):
			return pdf([[x]])
		thr,perc, b, yav,total_income,nb_household=income_tabulation()
		# this should be justified
		xmax = 30*thr[-1]
		# linear approximation of inverse cdf, necessary to get x0
		Finv = icdf_from_tabulation_linear(thr,perc)
		# see the definition of this global variable 
		x0 = float(Finv( percentiles[0] ))
		# compute shares
		share90_100 =  share_of_revenue_from_pdf(f,x0,xmax)
		x0 = float(Finv( percentiles[1] ))
		share50_90 = share_of_revenue_from_pdf(f,x0,xmax) - share90_100
		# this step is bad: share90_100 carries a large error, which is propagated to the others
		share0_50 = 1- (share90_100+share50_90)
		shares.append([share0_50, share50_90,share90_100])
	return shares	



def sample_BMW_1b1fnW_reduced_pareto_piecewise(nsamp, scale):
	"""
	get samples for the bmw problem 1b1fnW 
	pdf constraint on all incomes (pareto_piecewise)
	"""
	nw=9	
	# prepare pdf
	# WBs_avg is necessary
	C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_11'+str(nw))
	# rescale pdf rather than CSP problem (using income average, and not Mtot)
	thr,perc, bb, yav, total_income,nb_household = income_tabulation() # yav is needed before calling get_..
	scale = WBs_avg / yav
	# constrain the pdf of ALL dimensions
	pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False,idx = range(nw),
																		rescale_factor=scale)
	# sample reduced model		
	bmw = bmw_sym_1F1BnW(nw)
	bmw.init()	
	x = bmw.get_har_samples(nsamp,pdf=pdf,uniform=False,native=True, algo="CD")	
	return x,nb,nf,nw



		
# --------------------------------------


def compute_shares_bmw(it,fname):
	"""
	compute share of total income for the three percentile groups [0-50%],[50-90%],[90-100%]
	bmw model
	"""
	# open h5 store	
	store = pd.HDFStore(fname)
	#-------
	# init
	shares_inc = np.zeros((3,len(it) ))
	shares_wea = np.zeros((3,len(it) ))
	shares_anal=[]
	# from hit-and-run data: compute shares of revenue & wealth 
	for j,(groupname,label,config,func_marginal,func_sample,func_icdf) in enumerate(it):
		# get params
		nsamp         = config['sampler_params']['nsamp']		
		percentiles   = config['experiment_params']['percentiles']	
		idx_first_WBs = config['model_params']['idx_first_WBs']
		idx_first_M   = config['model_params']['idx_first_M']
		nw 			  = config['model_params']['nw']
		Mtot          = config['model_params']['Mtot']
		M_mean        = Mtot / float(nw)
		#-------
		# montecarlo sampling
		x = func_sample(nsamp)	
		# store only first sample set	
		
		#df = pd.DataFrame(x, columns=label_long) # BUG	
		df = pd.DataFrame(x) 
		store[groupname+'/samples/'+label] = df	
		#-------	
		# thinning ?
		"""
		n = nw-1
		thin = int(np.ceil(np.log(n + 1.)/4. * n**3)) # see tervonen
		idx_thin = (np.arange(n) % thin) ==0
		"""
		#-------	
		# shares of income from mc sample	
		#  get all income	
		#data=x[idx_thin,idx_first_WBs:idx_first_WBs+nw].flatten() 		
		data=x[:,idx_first_WBs:idx_first_WBs+nw].flatten() 		
		shares_inc[0,j]= share_of_revenue_from_data(data,0.0,0.5)
		shares_inc[1,j]= share_of_revenue_from_data(data,0.5,0.9)
		shares_inc[2,j]= share_of_revenue_from_data(data,0.9,1.0)		
		# shares of wealth: from mc sample			
		#data = 	x[idx_thin,idx_first_M:idx_first_M+nw].flatten() #ALL
		data = 	x[:,idx_first_M:idx_first_M+nw].flatten() #ALL
		#data=x[:,-3].flatten() # ONLY ONE COMPONENT
		shares_wea[0,j]=share_of_revenue_from_data(data,0.0,0.5)
		shares_wea[1,j]= share_of_revenue_from_data(data,0.5,0.9)
		shares_wea[2,j]= share_of_revenue_from_data(data,0.9,1.0)	
		#-------
		# store config
		conf_str = pd.Series(json.dumps(config))
		store[groupname+'/config/'+label]= conf_str
		#-------
		# shares of wealth: from numerical integration of pdf 
		def f(x):
			return x*func_marginal(x)		
		x0= func_icdf( 0.0 )
		x1= func_icdf( 0.5 )	
		int_0_50 =  quad(f,x0,x1)[0]
		x0= x1		
		x1= func_icdf( 0.9 )
		int_50_90 =  quad(f,x0,x1)[0]	
		"""x0= x1
		x1 =Mtot
		#x1= func_icdf( 0.99999 )
		int_90_100 =  quad(f,x0,x1)[0]"""	
		print "int_0_50=",int_0_50, " int_50_90=",int_50_90 #," int_90_100=", int_90_100
		#s = int_0_50+int_50_90+int_90_100
		sha_0_50=int_0_50/M_mean #s 
		sha_50_90=int_50_90/M_mean  #s
		sha_90_100=  1.0 - (sha_0_50 + sha_50_90) #int_90_100/M_mean	 #s
		shares_anal.append([sha_0_50, sha_50_90,sha_90_100])
		#-------
		# shares of wealth: from analytic results
				
	print "shares_wea=",shares_wea				
	# store	income	
	col_label = [str(label)  for _,label,_,_,_,_ in it ]	
	store[groupname+'/shares/sample_income']=pd.DataFrame(shares_inc,	columns=col_label)	
	# store	wealth
	col_label = [str(label)  for _,label,_,_,_,_ in it ]		
	store[groupname+'/shares/sample_wealth']=pd.DataFrame(shares_wea,	columns=col_label)
	# store analytic
	col_label = [str(label)  for _,label,_,_,_,_ in it ]
	store[groupname+'/shares/pdf_wealth']=pd.DataFrame(np.array(shares_anal).T, columns=col_label)

	#----------															
	store.close()
	


def read_plot_shares(fname):
	"""
	"""
	import pandas as pd
	store = pd.HDFStore('data/shares.h5')
	groupname = '/bmw_11N/pareto' 
	#groupname = '/bmw_11N/piecewise_pareto_WID'
	store[groupname+'/shares/sample_income']
	store[groupname+'/shares/sample_wealth']
	store[groupname+'/shares/pdf_wealth']
	
	#label='cd_0_1'
	label='cd_0_01'
	label='cd_0_02'
	c=store[groupname+'/config/'+label]
	config=json.loads( c[0].decode())
	idx_first_WBs = config['model_params']['idx_first_WBs']
	idx_first_M = config['model_params']['idx_first_M']
	b = config['model_params']['b']	
	Mtot = config['model_params']['Mtot']
	
	plt.subplot(3,1,1)
	x=store[groupname+'/samples/'+label].icol(idx_first_M)
	plt.hist(x,50);
	plt.title(" wealth")
	
	plt.subplot(3,1,2)
	x=store[groupname+'/samples/'+label].icol(idx_first_WBs)
	plt.hist(x,50);
	plt.title( " income")
	
	plt.subplot(3,1,3)
	x=store[groupname+'/samples/'+label].icol(idx_first_M)
	plt.acorr(x,detrend=plt.mlab.detrend_mean,normed=True, maxlags=50, lw=2)
	plt.title( " acorr(wealth)")
	plt.suptitle(label + '  b='+str(b))
	plt.show()
	
	# get all M
	ncol = store[groupname+'/samples/'+label].shape[1]
	idx_bool= np.array(config['model_params']['labels'])=='M'
	idx_M = np.arange(ncol)[idx_bool]
	M_all=store[groupname+'/samples/'+label][idx_M]
	plt.plot(M_all); plt.show()
	# plot all
	# compute: empirical mean (compare to theoretic); IPR
	
	#k=store.keys().index(groupname+'/samples')
	#if k.count('/'+groupname)==0:
	#else:
	#	print "reading har samples..."
	#	df = store[groupname+'/samples']
	#   config=json.loads( store[groupname+'/config'][0].decode())


if __name__ == "__main__":
	#test_iter_piecewise_pareto_WID_bmw_rescaled_pdf()
	#test_iter_piecewise_pareto_WID_bmw_scale_plot() #OK
	#test_iter_piecewise_pareto_WID_bmw_scale() #OK
	#test_iter_piecewise_pareto_bmw_scale() #OK
	#test_iter_pareto_bmw_alpha0_plot()
	#test_iter_pareto_bmw_alpha0_quad()  #OK
	test_pareto_compute_shares_bmw()	
	#test_pareto_compute_shares_WID_bmw()
	#read_plot_shares(fname)

