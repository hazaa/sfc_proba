# -*- coding: utf-8
#  transmat.py
#  
#  Copyright 2015 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
# 

"""
CHANGELOG: 
2016/12/30: class bmw_1F1B2W() calls har_wrap, performs har sampling (?)
2016/12/27: make a class bmw_1F1B2W(), to group symbolic and numeric functions.
2016/12/26: moving creation of 1F1B2W matrix (from gaussian_elimination.py to here)
			adding dependency to sympy
2016/12/13: moved example_CSP_problems bp_beta.py from to here
2016/11/25: added full bmx model (with K^T=...)
"""

"""
TODO
* naming convention: bmw_1F1BxW_sym() => remplacer bmw_sym
"""

import numpy as np
import scipy.sparse
import sys
from sympy import Matrix,Symbol, symbols
from sympy.matrices.dense import matrix2numpy
#from har_wrap import check_constr
import warnings
#import utils
import sympy

try:
	from scipy.optimize import linprog
except:
	pass	

# not sure if har should be called in transmat. This is because of bmw_1F1BnW
import sys
sys.path.append('experimental')
try:
	from har_nonuniform import *
	from har import har_cd_native,har_cd_np,har_hd	
except:
	pass	


def bmw_rng_params(nmin=10,itermax=10000):
	"""
	random parameters generator for the bmw model
	p=[k,al0,al1,al2,delta,r]
	such that (r>0) & ( (1.-al1)*(1-delta*k) > k*al2  )
	see GL12 eq 7.31 p.229 for an explanation
	"""
	p=np.random.rand(itermax,6)
	k=p[:,0];al0=p[:,1];al1=p[:,2];al2=p[:,3];delta=p[:,4];r=p[:,5]
	i = (r>0) & ( (1.-al1)*(1-delta*k) > k*al2  )
	p_check = p[i,:]
	if p_check.shape[0]<nmin:
		raise ValueError('failed to generate the requested number of samples')
	return p_check	

def bmw_example_param(random=True):
	"""
	return a dictionary containing a set of random params 
	computed 
	"""
	if(random):
		p_vec=bmw_rng_params(nmin=10,itermax=100) 	
		k_num,al0_num,al1_num,al2_num,delta_num,r_num = p_vec[0,:]				
	else:
		r_num =0.03  ; delta_num=0.1; al0_num=0.1 ; al1_num=0.75 ;  al2_num=0.01 ;	# if al2=0.1, M<0
		k_num = 6. ; 		
	p={}; 	p['r']	= r_num ; p['alpha0']= al0_num;
	p['alpha1']= al1_num; p['alpha2']= al2_num
	p['delta'] =delta_num ; p['k']=k_num	
	return p

def bmw_sym_Mtot(k,al0,al1,al2,delta):
	"""
	compute the theroretical quantity of money in bmw model
	in:
	---
	k,al0,al1,al2,delta: sympy.core.symbol.Symbol
	
	out:
	---
	r: sympy expression
	"""
	if(isinstance(k,Symbol)&isinstance(al0,Symbol)&isinstance(al1,Symbol)&
		isinstance(al2,Symbol)&isinstance(delta,Symbol)):
		return k*al0 / (-k*al2 + (1-al1)*(1-delta*k))
	else:
		raise TypeError('input must be symbolic')	

def bmw_num_WBs_avg(nw,k,al0,al1,al2,delta,r):
	"""
	compute the theroretical average wage
	
	in:
	---
	k,al0,al1,al2,delta: 
	
	out:
	---
	r: sympy expression
	"""
	return (1-(delta+r)*k)*al0 / (-k*al2 + (1-al1)*(1-delta*k)) * 1/nw
	
def bmw_get_Mtot(k_num,al0_num,al1_num,al2_num,delta_num):
	"""
	compute the numerical value of the theoretical Mtot returned by
	bmw_sym_Mtot(); check positiveness; check that the numerical parameters
	are floats in [0,1]
	"""
	if(isinstance(k_num,float)&isinstance(al0_num,float)&isinstance(al1_num,float)&
		isinstance(al2_num,float)&isinstance(delta_num,float)):		
		# check numerical values
		p=np.array([k_num,al0_num,al1_num,al2_num,delta_num])
		if(np.any(p<0) ):
			raise ValueError('params must be positive')
		if( al1_num>1 or al2_num>1 or delta_num>1 ):
			# NB: alpha0 can be >1 in aggregate cases
			raise ValueError('k,alpha1,alpha2,delta must be <=1 ')
		# get symbolic expression
		delta, r, al0, al1, al2, k = symbols('delta r al0 al1 al2 k')			
		Mtot=bmw_sym_Mtot(k,al0,al1,al2,delta)
		# substitute numerical values
		Mtot_num= Mtot.subs([(k, k_num), (al0, al0_num), (al1, al1_num),(al2, al2_num),
							(delta, delta_num)])
		if(Mtot_num<0):
			print "Mtot_num=",Mtot_num
			raise ValueError('Mtot_num must be >=0')
		return float(Mtot_num)						
	else:
		print k_num,al0_num,al1_num,al2_num,delta_num
		raise TypeError('input must be numeric')	

class bmw_1F1B2W():
	"""
	this class computes the symbolic and numeric matrices A,B for a bmw 
	system with 1 firm, 1 bank, 2 workers.
	Does hitandrun sampling also.
	the state vect: x=[Cs,K, L,Is,Id,AF,DA, Cd1, Cd2,WBs1,WBs2,M1, M2]
	"""
	def __init__(self):
		self.A=[]
		self.B=[]
		self.AB=[]
		self.var=[]
		self.p=[]
		self.init_done = 0		
	def init(self):
		"""
		"""	
		res=bmw_sym_1F1B2W()
		self.A=res['A']
		self.B=res['B']
		self.AB=res['AB']
		# make a dictionary d={'delta':delta, ... } with the sympy symbols
		self.p = dict(zip( ['delta', 'r', 'al0', 'al1', 'al2', 'k' ],res['param'] ))
		self.init_done = 1
	def _get_interior_point(self,k_num,al0_num,al1_num,al2_num,delta_num,r_num):
		"""
		compute a particular solution of the system 1F1B2W
		with M^1=M^2, alpha_0^1=alpha_0^2
		
		x0 = [Cs,K, L,Is,Id,AF,DA, Cd1, Cd2,WBs1,WBs2,M1, M2]	
		(see bmw_sym_1F1B2W() )
		"""
		if((r_num<=0)|(r_num>=1)):
			raise ValueError('r must be in ]0,1[')
		Mtot = self.get_Mtot_num(k_num,al0_num,al1_num,al2_num,delta_num)		
		Cs= 1./(1.-al1_num)*(al0_num + al2_num*Mtot)
		K= Mtot
		L= Mtot
		Is=delta_num*K
		Id=Is
		AF=Id
		DA= AF
		Cd1= Cs / 2.
		Cd2= Cd1
		M1= Mtot/2.
		M2= M1
		WBs1= Cd1-r_num*M1
		WBs2= WBs1				
		x0=np.array([Cs,K, L,Is,Id,AF,DA, Cd1, Cd2,WBs1,WBs2,M1, M2])	
		return x0
		
	def get_Mtot_num(self,k_num,al0_num,al1_num,al2_num,delta_num):			
		"""
		"""		
		return bmw_get_Mtot(k_num,al0_num,al1_num,al2_num,delta_num)
	
	def get_dof_csp_sym(self):
		"""
		compute the number of degrees of freedom for the symbolic CSP
		using the rank of the nullspace of A
		"""
		if not(self.init_done):
			raise RuntimeError('init must be done first')
		return self.A.nullspace()[0].rank()
	
	def get_samples(self, ref,k_num,al0_num,al1_num,al2_num,delta_num,r_num,nsamp):
		"""
		!!!NB: random.rand() sampling, not har sampling!!!!!
		
		x0=np.array([Cs,K, L,Is,Id,AF,DA, Cd1, Cd2,WBs1,WBs2,M1, M2])
		"""
		# check the ref 
		# ???????
		# har sample independent variables M1+M2=Mtot
		samples = np.zeros((nsamp,13))
		M1_num = np.random.rand(nsamp)
		M2_num = 1.-  M1_num
		samples[:,-1]=M2_num
		samples[:,-2]=M1_num
		# scale to Mtot
		Mtot = self.get_Mtot_num(k_num,al0_num,al1_num,al2_num,delta_num)
		samples = Mtot * samples
		"""
		#gauss elimination is used to find the dependent variables
		#ref: row echelon form of the augmented matrix, without null rows
		#(nb: we may also ask the rref)   
		
		# get the dependent variables
		dof = self.get_dof_csp_sym()
		nrow = ref.shape[0]
		for i in range(nrow-1-dof,-1,-1):
			# get the current subrow
			lead_term=ref[i,i]
			subrow = ref[i,(i+1):-1]
			b = ref[i,-1]			
			# substitute numeric values
			s=[(self.p['k'], k_num),(self.p['al0'], al0_num),
				(self.p['al1'], al1_num),
				(self.p['al2'], al2_num), (self.p['delta'], delta_num),
										 (self.p['r'],r_num) ]
			subrow_num = matrix2numpy(subrow.subs(s), dtype=float ).transpose()			
			b_num = float(b.subs(s)	)
			lead_term_num= float(lead_term.subs(s))
			# multiply by terms already computed
			print "subrow_num=",subrow_num, "b_num=",b_num,type(b_num), " lead_term_num=",lead_term_num, type(lead_term_num)
			print "np.dot(samples[:,(i+1):], subrow_num)=",np.dot(samples[:,(i+1):], subrow_num)
			samples[:,i] = ((-np.dot(samples[:,(i+1):], subrow_num) +b_num)/lead_term_num).flatten()			
		"""	
		return samples	
		
	def get_csp_num(self,k_num,al0_num,al1_num,al2_num,delta_num,r_num):
		"""
		compute the CSP in numerical form
		"""	
		if not(self.init_done):
			raise RuntimeError('init must be done first')
		if((r_num<=0)|(r_num>=1)):	
			raise ValueError('r must be in ]0,1[')	
		A_num= matrix2numpy(self.A.subs([(self.p['k'], k_num), 
										 (self.p['al1'], al1_num),
										 (self.p['al2'], al2_num), 
										 (self.p['delta'], delta_num),
										 (self.p['r'],r_num) ]),
										 dtype=float )
		B_num= matrix2numpy(self.B.subs( self.p['al0'], al0_num),
							dtype=float)
		# bounds
		# the state vect: x=[Cs,K, L,Is,Id,AF,DA, Cd1, Cd2,WBs1,WBs2,M1, M2]
		# lb: all must be >=0
		# ub: see paper 1FBnW
		Mtot_num=self.get_Mtot_num(k_num,al0_num,al1_num,al2_num,delta_num)
		lb = np.zeros( A_num.shape[1])
		ub = np.array([Mtot_num/k_num,Mtot_num,Mtot_num,
						delta_num*Mtot_num,delta_num*Mtot_num,delta_num*Mtot_num,delta_num*Mtot_num,
						Mtot_num/k_num,Mtot_num/k_num,
						(1/k_num-r_num-delta_num)*Mtot_num,(1/k_num-r_num-delta_num)*Mtot_num,
						Mtot_num,Mtot_num])		
		# interior point
		x0=self._get_interior_point(k_num,al0_num,al1_num,al2_num,delta_num,r_num)
		# check interior point
		warnings.warn('use transmat.check_constr instead', Warning)
		if not(check_constr(x0,A_num,B_num,lb,ub)):
			# remark: in check_const, it is verified that lb<ub	
			raise ValueError('x0 doesnt check CSP')
		return A_num,B_num,lb,ub,x0

def bmw_sym_aggregated():
	"""
	symbolic matrix A,B for a bmw system with 1 firm, 1 bank, 1 worker
	"""
	delta, r, al0, al1, al2, k = symbols('delta r al0 al1 al2 k')	
	A = Matrix([[-1, 1 , -delta, 0, 0],
				[0,-1, delta+r, 1,0],
				[1,0,0,-1, -r],
				[1-al1,0,0,0,-al2],
				[0,k, -1,0,0]])	
	B = Matrix([0,0,0,al0,0])
	AB = Matrix.hstack(A,B)			
	res = {'A':A, 'B':B, 'AB':AB, 'param':[delta, r, al0, al1, al2, k ] }
	return res			
		
	
def bmw_sym_1F1B2W():
	"""
	symbolic matrix A,B for a bmw system with 1 firm, 1 bank, 2 workers
	"""
	# params
	#delta, r, al0, al1, al2, Mtot,k = symbols('delta r al0 al1 al2 Mtot k')	
	delta, r, al0, al1, al2, k = symbols('delta r al0 al1 al2 k')	
	# var firm
	Cs,K, L,Is,Id,AF,DA= symbols('Cs K  L Is Id AF DA')
	# var workers
	M1,M2     = symbols('M1:3')
	Cd1,Cd2   = symbols('Cd1:3')
	WBs1,WBs2 = symbols('WBs1:3')	
	system = Matrix([ Cs -Cd1 -Cd2,     # Cs = \sum Cd_i
				 Is - Id,		   # Is = Id	
				 - Cs -Is + r*L + AF+ WBs1+WBs2,   # WBd = Y - r L -AF
				 AF - delta*K,      # AF = delta K
				 Id - AF,          # Delta L = Id-AF = 0
				 -Cd1 + WBs1+ r*M1,  # Delta M=YD-Cd = 0
				 -Cd2 + WBs2+ r*M2,
				 -Cd1 + al0 + al1*(WBs1+r*M1) + al2*M1,  # Cd = alpha_0 + alpha_1 YD + alpha_2 M
				 -Cd2 + al0 + al1*(WBs2+r*M2) + al2*M2,
				 Id - DA,          # Delta_K = 0
				 DA - delta*K,
				 K-k*(Cs+Is),            # K=KT, KT = kY
				 #-Id+gamma*(KT-K)+DA
				 K-L,              # balance: K=L, L=\sum M_i
				 K-M1-M2#,
				 #Mtot-M1-M2        # M1+M2 = Mtot
					])
	#s=Matrix.vstack()			
	var = Matrix([Cs,K, L,Is,Id,AF,DA, Cd1, Cd2,WBs1,WBs2,M1, M2])  # this is needed for the jacobian
	A = system.jacobian(var)
	#B = A*var - system # doesn'work ??
	#B = Matrix([0,0,0,0,0,0,0,al0,al0,0,0,0,0,0,Mtot ])
	B = Matrix([0,0,0,0,0,0,0,al0,al0,0,0,0,0,0])
	AB = Matrix.hstack(A,B)
	res = {'A':A, 'B':B, 'AB':AB, 'var': [Cs,K, L,Is,Id,AF,DA,M1,M2,Cd1,Cd2,WBs1,WBs2],'param':[delta, r, al0, al1, al2, k ] }
	return res
	
def bmw_gauss_elimination_by_hand(*args,**kwargs):
	"""
	compute row-echelon form (ref) by hand
	we do this because A.rref returns a strange result
	"""
	M= args[0]
	k = kwargs['k']
	delta = kwargs['delta']
	r = kwargs['r']
	al1 = kwargs['al1']
	M[2,:] = M[2,:] +M[0,:]
	M[11,:] = M[11,:] + k*M[0,:]
	M.row_swap(1,3)
	M[10,:] = M[10,:] - M[1,:]
	M[11,:] = M[11,:] +1/delta *M[1,:]
	M[12,:] = M[12,:] +1/delta *M[1,:]
	M[13,:] = M[13,:] +1/delta *M[1,:]
	M.row_swap(2,12)
	M[12,:] = M[12,:] +r *M[2,:]
	M[11,:] = M[11,:] +k *M[3,:]
	M[12,:] = M[12,:] + M[3,:]
	#
	M[9,:] = M[9,:] - M[4,:]
	M[11,:] = M[11,:] + k* M[4,:]
	M[12,:] = M[12,:] + M[4,:]
	#
	M.row_swap(5,9)
	M[10,:] = M[10,:] + M[5,:]
	M[11,:] = M[11,:] + (k-1/delta)*M[5,:]
	M[12,:] = M[12,:]  -r/delta*M[5,:]
	M[13,:] = M[13,:]  -1/delta*M[5,:]
	# 
	M.row_swap(6,13)
	M[6,:] = delta*M[6,:]
	M[11,:] = M[11,:]  +(k-1/delta)*M[6,:]
	M[12,:] = M[12,:]  -r/delta*M[6,:]		
	# 
	M.row_swap(8,12)
	M[8,:] = M[8,:] - M[7,:]
	M[9,:] = M[9,:] - M[7,:]
	M[11,:] = M[11,:] - k*M[7,:]
	#
	M.row_swap(9,13)
	M[9,:] = M[9,:] - M[8,:]
	M[11,:] = M[11,:] - k*M[8,:]
	M[12,:] = M[12,:] - M[8,:]
	#
	M[11,:] = M[11,:] + k/(al1-1)*M[9,:]
	M[12,:] = M[12,:] - M[9,:]
	M[13,:] = M[13,:] + M[9,:]		
	# 
	M.simplify()
	M[12,:] = M[12,:] + (al1-1)/k*M[11,:]
	if M.shape[1]>14:
		M.row_swap(13,14)
	M.row_del(-1)	
	M.row_del(-3)		
	# REMARK:
	# 2 rows are zeroed 
	# the last equation has three nonzero terms AB_[-2,:][-1],AB_[-2,:][-2],AB_[-2,:][-3]	
	# => this is coherent with one degree of freedom found with A.gauss_jordan(B)
	return M		


class bmw_sym_1F1BnW():
	"""
	symbolic and numeric bmw model with aggregate firm and bank sectors
	
	M = symbols('M:%d'%(nw))
	WBs = symbols('WBs:%d'%(nw))
	Cd = symbols('Cd:%d'%(nw))
	eq = []
	for i in range(nw):
		# append  -Cd1 + WBs1+ r*M1 , -Cd1 + al0 + al1*(WBs1+r*M1) + al2*M1
		eq.append( )
	# -Cs-Is+r*L+AF+WBs1+WBs2	  => avec sum(WBs)
	#  K-M1-M2                    => avec sum(M) 
	#  Cs -Cd1 -Cd2               => avec sum(Cd) 
	s=Matrix.vstack()
	"""
	def __init__(self,nw,*args, **kwargs):
		self.C = []
		self.b = []
		self.F = []
		self.g = []		
		self.x0 = []
		self.nw = nw		
		self.p_num = None
		if kwargs.has_key('p_num'):
			self.p_num = kwargs['p_num']					   
		self.har = []     # hit and run instance	
		self.Mtot = -1
		self.WBs_avg = -1
		self.init_done =False
	def init(self):
		"""
		"""
		# numeric
		if self.p_num==None:		
			self.p_num = bmw_example_param(random=False)	
		# symbolic
		# make a dictionary d={'delta':delta, ... } with the sympy symbols
		#self.p = dict(zip( ['delta', 'r', 'al0', 'al1', 'al2', 'k' ],res['param'] ))
		#res=bmw_sym_1F1B2W() # CHANGE THIS ? with ..._1F1BnW
		#self.A=res['A']
		#self.B=res['B']	
		# Mtot
		alpha0_agreg = self.p_num['alpha0']*float(self.nw	)
		self.Mtot = bmw_get_Mtot(self.p_num['k'], alpha0_agreg,  # scaling 
								self.p_num['alpha1'],
								self.p_num['alpha2'],self.p_num['delta'])	
		self.WBs_avg = bmw_num_WBs_avg(self.nw,self.p_num['k'],alpha0_agreg,self.p_num['alpha1'],
								self.p_num['alpha2'],self.p_num['delta'] ,self.p_num['r'])
				
		# har	
		self._init_har()		
		self.init_done = 1
	def _init_har(self):
		"""
		see example_CSP_problem
		"""			
		nb,nf,nw=1,1,self.nw				
		# CSP	
		dim=nw-1 # number of degrees of freedom
		self.C = np.vstack((-np.eye(nw), np.eye(nw)))
		self.b=np.hstack((np.zeros(nw),self.Mtot * np.ones(nw))).reshape([2*nw,1])			# Mtot	
		self.F = np.ones((1,nw)) 
		self.g = self.Mtot *  np.array([[1]])												# Mtot
		x0 = self.Mtot/float(nw)*np.ones((nw,1)) # NOT: self.get_x0(nf,nb,nw,self.p_num,self.Mtot)
		self.x0 = x0
		# create and init har				
		self.har =	har(self.C,self.b,self.F,self.g,x0,dim=dim)
		self.har.init()
	def get_WBs_avg(self):
		"""
		"""
		if self.init_done:
			return self.WBs_avg
		else: raise RuntimeError('init must be done first')	
	def get_har_samples(self,nsamp,pdf=None, native=False, uniform=True, algo='CD',thin=None,accel=1, seed=0, log=False,lb=0.):
		"""
		hit-and-run samples of the BMW problem 
		
		input:
		------
		algo: string
		"CD" = coordinate direction algorithm; "HD" = hypersphere direction
		
		output:
		-------
		
		see Zabinksy
		"""
		if not self.init_done: raise RuntimeError('init must be done first')
		# get params	
		delta     = self.p_num['delta']	
		r     = self.p_num['r']
		k = self.p_num['k']
		alpha0 =  self.p_num['alpha0']
		alpha0_agreg= self.p_num['alpha0']*self.nw
		alpha1= self.p_num['alpha1']
		alpha2= self.p_num['alpha2']
		nf,nb,nw=1,1, self.nw		
		# sample
		Mtot= self.Mtot
		if algo=='HD' :
			M,reject=  self.har.sample(nsamp,pdf=pdf, native='har_hd', uniform=False,
										thin=thin, seed=seed, log=log)			
		elif algo=='CD':			
			if(uniform == True): 
				raise NotImplementedError
			x0= Mtot * np.ones(self.nw)/self.nw					
			if(native==False ): 
				if thin==None: thin = 10
				#M,reject = har_cd_native(x0,self.nw,Mtot,nsamp,pdf,None,thin)
				M,reject = har_cd_np(x0,self.nw,lb,Mtot,nsamp,pdf,None,thin,accel,log=log)
			else:
				raise NotImplementedError	
				#M,reject = har_cd_cstsum(x0,self.nw,Mtot,nsamp,pdf,seed=None)
				#nsamp = M.shape[0]
		else:
			raise ValueError('unknown algorithm')		
		# compute output 		
		K= Mtot/float(nf) * np.ones((nsamp,nf))		
		Cs= 1/(1-alpha1)*  1/float(nf)* (alpha0_agreg + alpha2*Mtot) * np.ones((nsamp,nf))				
		Is= delta* K * np.ones((nsamp,1))		
		Id= Is* np.ones((nsamp,1))
		WBs= 1/(1-alpha1)* ( alpha0 - (r*(1-alpha1)-alpha2) *M )  # NB: COMPUTED from M_i 
		Cd= WBs+r*M  # NB: COMPUTED from M_i
		AFs= Is
		AFd= Is
		ILs= r*Mtot/float(nb)* np.ones((nsamp,nb))
		ILd= r*Mtot/float(nf)* np.ones((nsamp,nf))
		IDs= r*M   # NB: COMPUTED from M_i
		IDd= r*Mtot/float(nb)*np.ones((nsamp,nb))	
		L= Mtot/float(nf) * np.ones((nsamp,nf))		
		WBd= Cs + Is - r* L - AFs			
		# concatenate constant vectors and samples
		x= np.hstack((Cs,Cd,Is,Id,WBs,WBd,AFs,AFd,ILs,ILd,IDs,IDd,M,L,K))
		# check shape
		nflow = 9*nf+4*nw+2*nb # see rnd_trans_mat_bmw_full()
		if not( x.shape[1] ==nflow ): raise ValueError('x has incorrect dimension')
		return x,reject
	
	def get_sym_CSP(self):			
		nw = self.nw
		delta, r, al0, al1, al2, k = symbols('delta r al0 al1 al2 k')	
		# var firm
		#Cs,K, L,Is,Id,AF,DA= symbols('Cs K  L Is Id AF DA')
		"""system = Matrix([  Is - Id,		   # Is = Id	
					  AF - delta*K,      # AF = delta K
					 Id - AF,          # Delta L = Id-AF = 0				 
					 Id - DA,          # Delta_K = 0
					 DA - delta*K,
					 K-k*(Cs+Is),            # K=KT, KT = kY
					 #-Id+gamma*(KT-K)+DA
					 K-L,              # balance: K=L, L=\sum M_i
						])"""
		# import sympy
		# from sympy.matrices import Matrix, eye
		# >>> Matrix.hstack(eye(2), 2*eye(2))
		# !!!!!!!! A = Matrix.hstack( Matrix([[1,0,0,0,0,0,0]]), -sympy.ones(1,nw), sympy.zeros(1,nw), sympy.zeros(1,nw))				
		A = Matrix.vstack(
		Matrix.hstack(Matrix([[1,0,0,0,0,0,0]]), -sympy.ones(1,nw), sympy.zeros(1,nw), sympy.zeros(1,nw), Matrix([[0]]) ) ,
		Matrix.hstack(Matrix([[0,0,0,1,-1,0,0]]), sympy.zeros(1,nw), sympy.zeros(1,nw), sympy.zeros(1,nw), Matrix([[0]]) ),
		Matrix.hstack(Matrix([[-1,0,r,-1,0,1,0]]),sympy.zeros(1,nw), sympy.ones(1,nw), sympy.zeros(1,nw) ,Matrix([[0]]) ),
		Matrix.hstack(Matrix([[0,-delta,0,0,0,1,0]]), sympy.zeros(1,nw), sympy.zeros(1,nw), sympy.zeros(1,nw), Matrix([[0]])),		
		Matrix.hstack(Matrix([[0,0,0,0,1,-1,0]]), sympy.zeros(1,nw), sympy.zeros(1,nw), sympy.zeros(1,nw),Matrix([[0]])),
		Matrix.hstack(sympy.zeros(nw,7), -sympy.eye(nw), sympy.eye(nw), r*sympy.eye(nw),  sympy.zeros(nw,1) ),		
		Matrix.hstack(sympy.zeros(nw,7), -sympy.eye(nw), al1*sympy.eye(nw), (al1*r+al2)*sympy.eye(nw), al0 * sympy.ones(nw,1)),		
		Matrix.hstack(Matrix([[0,0,0,0,1,0,-1]]), sympy.zeros(1,nw), sympy.zeros(1,nw), sympy.zeros(1,nw),Matrix([[0]])),		
		Matrix.hstack(Matrix([[0,-delta,0,0,0,0,1]]), sympy.zeros(1,nw), sympy.zeros(1,nw), sympy.zeros(1,nw),Matrix([[0]])),		
		Matrix.hstack(Matrix([[-k,1,0,-k,0,0,0]]), sympy.zeros(1,nw), sympy.zeros(1,nw), sympy.zeros(1,nw),Matrix([[0]])),		
		Matrix.hstack(Matrix([[0,1,-1,0,0,0,0]]), sympy.zeros(1,nw), sympy.zeros(1,nw), sympy.zeros(1,nw),Matrix([[0]])),				
		Matrix.hstack(Matrix([[0,1,0,0,0,0,0]]), sympy.zeros(1,nw), sympy.zeros(1,nw), -sympy.ones(1,nw),Matrix([[0]]) )				
		)
		return A
	"""
	def get_sym_CSP_sage(nw):
		# ATTENTION: change_ring necessary, e.g.: matrix.zero(3).change_ring(SR).augment(matrix.diagonal((r,r,r)))
		# Q: block_matrix ? HOWTO ?
		
		delta, r, al0, al1, al2, k = var('delta, r, al0, al1, al2, k')			
		A = Matrix([[1,0,0,0,0,0,0]]).augment( -matrix.ones(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]]))
		A=A.stack( Matrix([[0,0,0,1,-1,0,0]]).augment( matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]])) )				
		A=A.stack( Matrix([[-1,0,r,-1,0,1,0]]).augment( matrix.zero(1,nw) ).augment(  matrix.ones(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]])) )				
		A=A.stack( Matrix([[0,-delta,0,0,0,1,0]]).augment( matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]])) )
		A=A.stack( Matrix([[0,0,0,0,1,-1,0]]).augment( matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]])) )		
		A=A.stack( matrix.zero(nw,7).augment( -matrix.identity(nw) ).augment(  matrix.identity(nw) ).change_ring(SR).augment( r* matrix.identity(nw)  ).augment(matrix.zero(nw,1) ))
		A=A.stack( matrix.zero(nw,7).augment( -matrix.identity(nw) ).change_ring(SR).augment( al1*matrix.identity(nw) ).augment( (al1*r+al2)*matrix.identity(nw) ).augment(al0 * matrix.ones(nw,1) ))
		A=A.stack( Matrix([[0,0,0,0,1,0,-1]]).augment( matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]])) )
		A=A.stack( Matrix([[0,-delta,0,0,0,0,1]]).augment( matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]])) )
		A=A.stack( Matrix([[-k,1,0,-k,0,0,0]]).augment( matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]])) )
		A=A.stack( Matrix([[0,1,-1,0,0,0,0]]).augment( matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]])) )		
		A=A.stack( Matrix([[0,1,0,0,0,0,0]]).augment( matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  -matrix.ones(1,nw) ).augment(Matrix([[0]])) )
		
	def get_sym_CSP_sage_polynomial(nw):
		# ATTENTION: 
		# Q: block_matrix ? HOWTO ?	
		P.<r,delta,k,al0,al1,al2> = QQ[]
		nw = 2
		A = Matrix(P, 14, 14, 
		Matrix([[1,0,0,0,0,0,0]]).augment( -matrix.ones(1,nw) ).augment(  matrix.zero(1,nw) ).augment(  matrix.zero(1,nw) ).augment(Matrix([[0]]))
			)
			
	"""	
	
	@staticmethod
	def get_x0(nf,nb,nw,p,Mtot):
		"""
		compute a particular solution to the (1 Firm, 1 Bank, nw Workers) problem
		NB: check if still correct for (nf,nb,nw) problem
		"""
		if(nf>1 or nb>1): raise ValueError('not applicable when nf>1 or nb>1')
		# get params	
		delta     = p['delta']	
		r     = p['r']
		alpha0_agreg= p['alpha0']*nw
		alpha1= p['alpha1']
		alpha2= p['alpha2']		
		"""
		cf fin_flow_constr.R
		l_bmw <-c("Cs","Cd","Is","Id","WBs","WBd","AFs","AFd",
		"ILs","ILd","IDs","IDd","M","L","K")
		label_long<-c(rep(l[1],nf), rep(l[2],nw),rep(l[3],nf),
		rep(l[4],nf), rep(l[5],nw), rep(l[6],nf), rep(l[7],nf),
		rep(l[8],nf), rep(l[9],nb), rep(l[10],nf),rep(l[11],nw),
		rep(l[12],nb), rep(l[13],nw), rep(l[14],nf), rep(l[15],nf) )
		"""
		M= Mtot/float(nw) * np.ones(nw)
		K= Mtot/float(nf) * np.ones(nf)
		Cd= 1/(1-alpha1)* 1/float(nw)* (alpha0_agreg + alpha2*Mtot) *np.ones(nw)
		Cs= 1/(1-alpha1)*  1/float(nf)* (alpha0_agreg + alpha2*Mtot) * np.ones(nf)		
		Is= delta* K 
		Id= Is
		WBs= 1/float(nw)* ( 1/(1-alpha1) *(alpha0_agreg + alpha2*Mtot) -r*Mtot) * np.ones(nw)
		WBd= np.sum(WBs) * 1/float(nf) * np.ones(nf)		#???????
		AFs= Is
		AFd= Is
		ILs= r*Mtot/float(nb)* np.ones(nb)
		ILd= r*Mtot/float(nf)* np.ones(nf)
		IDs= r*Mtot/float(nw)*np.ones(nw)
		IDd= r*Mtot/float(nb)*np.ones(nb)	
		L= Mtot/float(nf) * np.ones(nf)
		
		nflow = 9*nf+4*nw+2*nb # see rnd_trans_mat_bmw_full()
		
		#x0= np.concatenate ((Cs,Cd,Is,Id,WBs,WBd,AFs,AFd,ILs,ILd,IDs,IDd,M,L,K)).reshape([nflow,1 ])
		x0= np.hstack((Cs,Cd,Is,Id,WBs,WBd,AFs,AFd,ILs,ILd,IDs,IDd,M,L,K)).reshape([nflow,1 ])
		return x0

	@staticmethod					
	def get_x0_linprog(A,b):
		"""
		see code by Cousins
		https://fr.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/43596/versions/10/previews/MFE_version/preprocess.m/index.html?access_key=
		https://docs.scipy.org/doc/scipy-0.15.1/reference/generated/scipy.optimize.linprog.html
		"""	
		# CHECK SHAPES	
		dim = A.shape[1]
		#p =np.zeros(dim)
		p = np.random.uniform(size=dim)
		for i in range(dim):
			#f_index = np.argmin(b - np.dot(A,p))
			f_index	=	np.argmin(b - np.dot(A,p).reshape((A.shape[0],1)))
			f = A[f_index,:]
			res=linprog(f,A, b);
			y = res.x
			p = (i*p+y)/(i+1);
		return p	
	


def test_get_x0_linprog():
	"""
	FAILS
	
	see code by Cousins
		https://fr.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/43596/versions/10/previews/MFE_version/preprocess.m/index.html?access_key=
	"""
	eps_cutoff = 1e-7;
	C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
	bmw = bmw_sym_1F1BnW(10) #dummy
	x0_optim = bmw.get_x0_linprog(C,b)
	
	b = b - np.dot(C,x0_optim)
	assert(min(b) > -eps_cutoff)
	#raise ValueError('We tried to find a point inside the polytope but failed.')

def test_get_x0_234():	
	"""
	FAILS
	"""	
	nb,nf,nw=2,3,4
	p = bmw_example_param()
	b=bmw_1F1B2W()	
	C,b,F,g,x0 = example_CSP_problems('bmw_234')
	# check x0
	try:
		utils.check_constr(C,b,F,g,x0)
	except ValueError as e:
		print "ValueError ",e
		raise e
	except:
		print "unhandled exception"	
		raise		
		
	
def test_get_x0_119():	
	"""
	OK
	"""
	nb,nf,nw=1,1,9
	C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
	print "x0.shape[0]=",x0.shape[0], " x0=",x0
	# check x0
	try:
		utils.check_constr(C,b,F,g,x0)
	except ValueError as e:
		print "ValueError ",e
		raise e
	except:
		print "unhandled exception"	
		raise	
 

def example_CSP_problems(k,**kwargs):
	"""
	some very simple CSP problems in various forms:
	  Ax=b, lb<=x<=ub
	  or the more general form Fx=g; Cx<=b in Tervonen
	"""
	if kwargs.has_key('n'):
			n = kwargs['n']
			if (n<0) |(not(isinstance(n,int))): 
				raise ValueError
	else:
		n=3
			
	if(k==0):
		A = np.array([[1.,0.,-1.]])
		b = np.array([[0.]])
		lb = np.array([0.,0.,0.])
		ub = np.array([1.,1.,1.])
		x0 = []
		return A,b,lb,ub,x0
	elif(k==1):
		A = np.array([[1.,0.,-1.]])
		b = np.array([[0.]])
		lb = np.array([0.,0.,0.])
		ub = np.array([1.,1.,2.])	
		x0 = []
		return A,b,lb,ub,x0
	elif(k=="gibbs"):
		"""
		conserved positive quantity M shared among n agents
		(boltzmann-gibbs distribution is expected)
		"""		
		M = 1.
		A = np.ones(n).reshape((1,n))	
		b = np.array([[M]])
		lb = np.zeros(n)
		ub = M*np.ones(n)  # no agent can take more than the sum
		x0 = M/float(n)*np.ones(n)
		return A,b,lb,ub,x0	
	#below: 
	# set of problems in the form: Cw<=b  ; Fw = g	(more general than Fw=g, lb<=w<=ub)		
	elif(k=="gibbs3"):
		"""see Tervonen13
		forall i wi>=0
		w1+w2+w3 =1"""
		C = -np.eye(3)
		b=np.zeros(3).reshape([3,1])
		F = np.array([1,1,1])
		g = np.array([1])
		x0 = np.array([0.3333333,0.3333333,0.3333333]).reshape([3,1])
		return C,b,F,g,x0
	elif(k==3):
		"""
		forall i, 0<= w_i  <= 1
		w1+w2+w3=1
		w1=w3
		"""
		C = np.vstack((-np.eye(3), np.eye(3)))
		b=np.hstack((np.zeros(3),np.ones(3))).reshape([2*3,1])
		F = np.array([[1,1,1],[-1,0,1]])
		g = np.array([[1],[0]])	
		x0 = np.array([0.333333,0.3333333,0.3333333]).reshape([3,1])
		return C,b,F,g,x0
	elif(k=="gibbs30"):
		"""
		forall i, 0<= w_i  <= 1
		sum w_i = 1
		"""
		n=30
		C = np.vstack((-np.eye(n), np.eye(n)))
		b=np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])		
		#F = np.ones((2,n)) # repeat equality constraint
		#g = np.array([[1],[1]])
		F = np.ones((1,n)) 
		g = np.array([[1]])
		x0 = 1./n*np.ones(n).reshape([n,1])	
		return C,b,F,g,x0
	elif(k=="cube"):
		"""
		forall i, 0<= w_i  <= 1		
		w_0=w_{-1}
		"""		
		C = np.vstack((-np.eye(n), np.eye(n)))	
		b=np.hstack((np.zeros(n),10*np.ones(n))).reshape([2*n,1])		
		F = np.zeros((2,n)) # repeat equality constraint
		F[0,0]=1; F[0,-1]=-1
		F[1,0]=1; F[1,-1]=-1
		g = np.array([[0],[0]])
		x0 = 1./n*np.ones(n).reshape([n,1])	
		return C,b,F,g,x0
	elif(k=="bmw_234"):		
		"""
		TO BE VERIFIED (see warning)
		"""
		nb,nf,nw=2,3,4
		p = bmw_example_param()	
		b=bmw_1F1B2W()
		Mtot = b.get_Mtot_num(p['k'],p['alpha0'],p['alpha1'],p['alpha2'],p['delta'])
		warnings.warn('Mtot formula applicable to nf>1 case ???', Warning)
		
		C,b,F,g,x0=CSP_bmw_full(nb,nf,nw, p, M=Mtot)
		return C,b,F,g,x0
	elif(k=="bmw_112"):		
		"""
		TO BE VERIFIED (see warning)
		
		cf fin_flow_constr.R
		l_bmw <-c("Cs","Cd","Is","Id","WBs","WBd","AFs","AFd",
		"ILs","ILd","IDs","IDd","M","L","K")
		label_long<-c(rep(l[1],nf), rep(l[2],nw),rep(l[3],nf),
		rep(l[4],nf), rep(l[5],nw), rep(l[6],nf), rep(l[7],nf),
		rep(l[8],nf), rep(l[9],nb), rep(l[10],nf),rep(l[11],nw),
		rep(l[12],nb), rep(l[13],nw), rep(l[14],nf), rep(l[15],nf) )
		"""
		nb,nf,nw=1,1,2
		p = bmw_example_param(random=False)	
		b=bmw_1F1B2W()
		alpha0_agreg = p['alpha0']*nw
		Mtot = b.get_Mtot_num(p['k'],alpha0_agreg,p['alpha1'],p['alpha2'],p['delta'])	
		WBs_avg = bmw_num_WBs_avg(nw,p['k'],alpha0_agreg,p['alpha1'],p['alpha2'],p['delta'] ,p['r'])		
		C,b,F,g,x0=CSP_bmw_full(nb,nf,nw, p, M=Mtot)
		idx_WBs = range(3*nf+nw,3*nf+2*nw)
		return C,b,np.array(F.todense()),g,x0,Mtot,WBs_avg,idx_WBs	
	elif(k=="bmw_119"):		
		"""
		TO BE VERIFIED (see warning)
		
		cf fin_flow_constr.R
		l_bmw <-c("Cs","Cd","Is","Id","WBs","WBd","AFs","AFd",
		"ILs","ILd","IDs","IDd","M","L","K")
		label_long<-c(rep(l[1],nf), rep(l[2],nw),rep(l[3],nf),
		rep(l[4],nf), rep(l[5],nw), rep(l[6],nf), rep(l[7],nf),
		rep(l[8],nf), rep(l[9],nb), rep(l[10],nf),rep(l[11],nw),
		rep(l[12],nb), rep(l[13],nw), rep(l[14],nf), rep(l[15],nf) )
		"""
		nb,nf,nw=1,1,9
		p = bmw_example_param(random=False)	
		b=bmw_1F1B2W()
		alpha0_agreg = p['alpha0']*nw
		Mtot = b.get_Mtot_num(p['k'],alpha0_agreg,p['alpha1'],p['alpha2'],p['delta'])	
		WBs_avg = bmw_num_WBs_avg(nw,p['k'],alpha0_agreg,p['alpha1'],p['alpha2'],p['delta'] ,p['r'])		
		C,b,F,g,x0=CSP_bmw_full(nb,nf,nw, p, M=Mtot)
		idx_WBs = range(3*nf+nw,3*nf+2*nw)
		return C,b,np.array(F.todense()),g,x0,Mtot,WBs_avg,idx_WBs		
	else:
		raise ValueError('unknown example id')	
		



def bmw_stationary():
	alpha1=0.1
	return np.array([[1,-1,0,0,0,0,0,0,0,0,0,0],
					 [0,0,1,-1,0,0,0,0,0,0,0,0],
					 [0,0,0,0,1,-1,0,0,0,0,0,0],
					 [0,0,0,0,0,0,1,-1,0,0,0,0],
					 [0,0,0,0,0,0,0,0,1,-1,0,0],
					 [0,0,0,0,0,0,0,0,0,0,1,-1],
					 [0,-1,0,0,1,0,0,0,0,0,1,0],
					 [1,0,1,0,0,-1,0,-1,0,-1,0,0],
					 [0,0,0,-1,0,0,1,0,0,0,0,0],
					 [0,0,0,0,0,0,0,0,1,0,0,-1],
					 [0,-1,0,0,alpha1,0,0,0,0,0,0,0],
					 [0,0,0,0,0,0,0,-1,0,0,0,0],
					 [0,0,0,0,0,0,0,0,0,-1,0,0],
					 [0,0,0,0,0,0,0,0,0,0,1,0],
					])

def rnd_trans_mat_bmw(nb,nf,nw, p):
	"""
	generate a random transition matrix for the BMW model in [GL07]	
	NB: the capital target equation  (7.20) in [GL07] IS NOT included
	
	parameters:
	-----------
	nb: int
	number of banks
	
	nf: int
	number of firms
	
	nw: int
	number of workers
	
	returns:
	--------
	tm: array-like, shape=[4, 2*(nb+nf)+nw]
	
	see :
	[GL07] Godley, Lavoie, "Monetary Economics", 2007.
	http://docs.scipy.org/doc/scipy/reference/sparse.html
	"""		
	# create random connectivity matrices
	Alpha   = rnd_col_mat(nf,nw,False) 
	Beta    = rnd_rowcol_mat(nf) # row and column sum = [1,...,1]
	Gamma   = rnd_col_mat(nf,nw,False)
	LambdaT = rnd_col_mat(nb,nf,False) 
	MuT     = rnd_col_mat(nb,nw,False) 
	# get params
	delta = p['delta']
	r     = p['r']
	alpha0= p['alpha0']
	alpha1= p['alpha1']
	alpha2= p['alpha2']
	# get identity matrices
	Ib= scipy.sparse.identity(nb, dtype='int8', format='csc')
	If= scipy.sparse.identity(nf, dtype='int8', format='csc')
	Iw= scipy.sparse.identity(nw, dtype='int8', format='csc')
	one_nw=scipy.sparse.csc_matrix(np.ones((1,nw)),dtype='int8')
	one_nf=scipy.sparse.csc_matrix(np.ones((1,nf)),dtype='int8')
	# make block matrix
	"""
	tm=scipy.sparse.bmat([[If,-Alpha ]+[None]*13,          # start rowsums equations
						 [None]*2+[Beta,-If]+[None]*11,
						 [None]*4+[Gamma,-If]+[None]*9,
						 [None]*6+[-If,If]+[None]*7,
						 [None]*8+[Ib,-LambdaT]+[None]*5,
						 [None]*10+[MuT,-Ib]+[None]*3,	   # end rowsums equations
						 [None,-Iw,None,None,Iw]+[None]*5+[Iw]+[None]*4, # start colsums equations
						 [If,None,If,None,None,-If,None,-If,None,-If]+[None]*5,
						 [None]*8+[Ib,None,None,-Ib]+[None]*3,# end colsums equations
						 [None,Iw,None,None,-alpha1*Iw]+[None]*7+[-(alpha1*r+alpha2)*Iw,None,None], #start beahvioral equations
						 [None]*7+[If]+[None]*6+[-delta*If],
						 [None]*9+[If]+[None]*3+[-r*If,None],
						 [None]*10+[Iw,None,-r*Iw,None,None]#,
						 [None]*12+one_nw+[None,None] #stop beahvioral equations
						 ])"""
	tm=scipy.sparse.bmat([[If,-Alpha, None,None,None,None,None,None,None,None,None,None,None,None,None],          # start rowsums equations
						 [None,None,Beta,-If,None,None,None,None,None,None,None,None,None,None,None],
						 [None,None,None,None,Gamma,-If,None,None,None,None,None,None,None,None,None],
						 [None,None,None,None,None,None,If,-If,None,None,None,None,None,None,None],
						 [None,None,None,None,None,None,None,None,Ib,-LambdaT,None,None,None,None,None],
						 [None,None,None,None,None,None,None,None,None,None,MuT,-Ib,None,None,None],	   # end rowsums equations
						 [None,-Iw,None,None,Iw,None,None,None,None,None,Iw,None,None,None,None], # start colsums equations
						 [If,None,If,None,None,-If,None,-If,None,-If,None,None,None,None,None],
						 [None,None,None, -If, None,None,If,None,None,None,None,None,None,None,None ],
						 [None,None,None,None,None,None,None,None,Ib,None,None,-Ib,None,None,None],# end colsums equations
						 [None,Iw,None,None,-alpha1*Iw,None,None,None,None,None,None,None,-(alpha1*r+alpha2)*Iw,None,None], #start beahvioral equations
						 [None,None,None,None,None,None,None,If,None,None,None,None,None,None,-delta*If],
						 [None,None,None,None,None,None,None,None,None,If,None,None,None,-r*If,None],
						 [None,None,None,None,None,None,None,None,None,None,Iw,None,-r*Iw,None,None], #stop beahvioral equations
						 [None,None,None,None,None,None,None,None,None,None,None,None,one_nw,None,None] , # start balance sheet eqn
						 [None,None,None,None,None,None,None,None,None,None,None,None,None,None,one_nf] ,
						 [None,None,None,None,None,None,None,None,None,None,None,None,None,If,-If],
						  [None,None,None,None,None,None,None,None,None,None,None,None,-MuT,LambdaT,None] #stop balance sheet equations
						 ])					 
						 
	# check size
	nr,nc=tm.shape
	if(nr!=(4*nb+9*nf+3*nw+2)): raise IndexError
	if(nc!=(9*nf+4*nw+2*nb)): raise IndexError
	return tm
	

def test_interior_point_gaussian_augmented_system():
	"""
	"""
	C,b,F,g,x0 = example_CSP_problems('bmw_234')
	x0=interior_point_gaussian_augmented_system(F,g)
	

def interior_point_gaussian_augmented_system(A,b):
	"""
	INCOMPLETE; must also add Cx<b
	"""
	warings.warn("INCOMPLETE; must also add Cx<b", Warning)
	nr,nc = A.shape	
	# reduce!
	d = np.linalg.matrix_rank(A)	
	if(d >= nc): 
		print "d,nr,nc=",d,nr,nc
		raise ValueError('method not applicable')
	# get A full rank
	raise NotImplementedError
	# add random gaussian rows
	rnd_rows = np.random.normal(size=(nc-nr)*nc).reshape(((nc-nr),nc))
	A_aug = np.vstack((A,rnd_rows))
	b_aug = np.concatenate((b, np.zeros(nc-nr) ))
	# check col rank
	d = np.linalg.matrix_rank(A_aug)
	if(d!=nr ): raise ValueError('not full rank')
	# solve for x0
	x0_aug = np.linalg.solve(A_aug,b_aug)
	# check partial solution
	return x0_aug[0:r]
	

def test_CSP_bmw_full():
	nb,nf,nw=2,3,4
	p = bmw_example_param()
	b=bmw_1F1B2W()
	Mtot = b.get_Mtot_num(p['k'],p['alpha0'],p['alpha1'],p['alpha2'],p['delta'])	
	warnings.warn('Mtot formula applicable to nf>1 case ???', Warning)
	C,b,F,g,x0=CSP_bmw_full(nb,nf,nw, p, M=Mtot)
	# check: x0 verifies all constraints ?
	# ?

def test_CSP_bmw_FiCM():
	import network
	nb,nf,nw=10,100,1000
	p = bmw_example_param()
	Mtot = 1
	A,B,C,D,E,F=network.get_network_matrix()	
	F,g,lb,ub = CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p, M=Mtot)
		
	
def CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p, **kwargs):
	"""
	"""
	import network
	# get params
	delta = p['delta']
	r     = p['r']
	alpha0= p['alpha0']
	M= kwargs['M']
	# main matrix Fx=g lb< x < ub
	F=rnd_trans_mat_bmw_full(nb,nf,nw, p,Alpha=A,Beta=B,
							Gamma=C,LambdaT=D,MuT=E)
	nflow =F.shape[1]
	neq = F.shape[0]	
	g= np.concatenate(( np.zeros(6*nf + nw + 3*nb), # start transaction mat
				alpha0* np.ones(nw),
				np.zeros(2*nf+nw),
				np.zeros(nf),# NEW EQUATION (7.19+7.20) stop transaction mat
				[M],					# start balance sheet
				[M],
				np.zeros(nf),
				np.zeros(nb) )).reshape([10*nf+3*nw+4*nb+2,1]) # stop balance sheet	
	lb =np.zeros(nflow)			
	ub = np.hstack(( M * np.ones(nf), M* np.ones(nw), delta*M *np.ones(nf),
			delta*M*np.ones(nf), M* np.ones(nw), M*np.ones(nf),
			delta*M*np.ones(nf), delta*M * np.ones(nf), r*M* np.ones(nb),
			r*M * np.ones(nf), r*M * np.ones(nw), r*M * np.ones(nb),
			M* np.ones(nw), M * np.ones(nf),  M * np.ones(nf) ))			
	b=np.concatenate( (np.zeros(nflow),ub)).reshape([2*nflow,1])
	# check size
	if not( F.shape[0] == g.shape[0] ): 
		print F.shape[0] , g.shape[0] , nflow	
		raise ValueError
	return F,g,lb,ub
	
def CSP_bmw_full(nb,nf,nw, p, **kwargs):
	"""
	generate a random CSP problem (equality and inequality constraints
	in the more general form Fx=g; Cx<=b )
	for the BMW model in [GL07]	
	with all equations, including the capital target equation  (7.20) in [GL07]
	
	copied from fin_flow_constr.R:readtmBMW_bounded()
	"""	
	if(nb==1 and nf==1):
		warnings.warn('Mtot constraint might be inconsistent in the case nb=1 and nf=1')
	else:	
		warnings.warn('is Mtot constraint consistent ?', Warning)
	
	# get params
	delta = p['delta']
	r     = p['r']
	alpha0= p['alpha0']
	M= kwargs['M']
	# main matrix
	F=rnd_trans_mat_bmw_full(nb,nf,nw, p)
	nflow =F.shape[1]
	neq = F.shape[0]
	g= np.concatenate(( np.zeros(6*nf + nw + 3*nb), # start transaction mat
				alpha0* np.ones(nw),
				np.zeros(2*nf+nw),
				np.zeros(nf),# NEW EQUATION (7.19+7.20) stop transaction mat
				[M],					# start balance sheet
				[M],
				np.zeros(nf),
				np.zeros(nb) )).reshape([10*nf+3*nw+4*nb+2,1]) # stop balance sheet
	C = np.vstack((-np.eye(nflow), np.eye(nflow)))
	"""
	if(nb>1 and nf>1 and nw >1):
		ub = np.concatenate(( M * np.ones(nf), M* np.ones(nw), delta*M *np.ones(nf),
				delta*M*np.ones(nf), M* np.ones(nw), M*np.ones(nf),
				delta*M*np.ones(nf), delta*M * np.ones(nf), r*M* np.ones(nb),
				r*M * np.ones(nf), r*M * np.ones(nw), r*M * np.ones(nb),
				M* np.ones(nw), M * np.ones(nf),  M * np.ones(nf) ))
	else:"""
	ub = np.hstack(( M * np.ones(nf), M* np.ones(nw), delta*M *np.ones(nf),
			delta*M*np.ones(nf), M* np.ones(nw), M*np.ones(nf),
			delta*M*np.ones(nf), delta*M * np.ones(nf), r*M* np.ones(nb),
			r*M * np.ones(nf), r*M * np.ones(nw), r*M * np.ones(nb),
			M* np.ones(nw), M * np.ones(nf),  M * np.ones(nf) ))			
	b=np.concatenate( (np.zeros(nflow),ub)).reshape([2*nflow,1])
	# get x0
	#x0 = np.zeros(nflow).reshape([nflow,1])
	if(nb==1 and nf==1): 
		bmw= bmw_sym_1F1BnW(nw)
		x0=bmw.get_x0(nf,nb,nw,p,M)
	else:
		x0 = None
	# check size
	if( not( C.shape[0] == b.shape[0] ) or
	    not( F.shape[0] == g.shape[0] ) or
	    not( x0.shape[0]== nflow)   ): 
		print C.shape[0], b.shape[0], F.shape[0] , g.shape[0] , x0.shape[0], nflow	
		raise ValueError
	return C,b,F,g,x0

def get_bmw_labels(nb,nf,nw):
	"""
	get the labels of a state vector of a bmw problem
	see fin_flow
	"""
	l =["Cs","Cd","Is","Id","WBs","WBd","AFs","AFd",
	"ILs","ILd","IDs","IDd","M","L","K"]
	ll=[l[0]] *nf + [l[1]]*nw + [l[2]]*nf + [l[3]] *nf + [l[4]]*nw + [l[5]]*nf + [l[6]]*nf \
		+ [l[7]] *nf + [l[8]]*nb + [l[9]]*nf + [l[10]] *nw \
		+ [l[11]] *nb + [l[12]]*nw + [l[13]]*nf + [l[14]] *nf
	#label_long<-c(rep(l[1],nf), rep(l[2],nw),rep(l[3],nf),
	#rep(l[4],nf), rep(l[5],nw), rep(l[6],nf), rep(l[7],nf),
	#rep(l[8],nf), rep(l[9],nb), rep(l[10],nf),rep(l[11],nw),
	#rep(l[12],nb), rep(l[13],nw), rep(l[14],nf), rep(l[15],nf) )
	if( len(ll) !=(9*nf+4*nw+2*nb)): 
		print len(ll), 9*nf+4*nw+2*nb
		raise IndexError
	return l,ll
	
def rnd_trans_mat_bmw_full(nb,nf,nw, p,Alpha=None,Beta=None,Gamma=None,LambdaT=None,MuT=None):
	"""
	generate a random transition matrix for the BMW model in [GL07]	
	with all equations, including the capital target equation  (7.20) in [GL07]
	
	parameters:
	-----------
	nb: int
	number of banks
	
	nf: int
	number of firms
	
	nw: int
	number of workers
	
	Alpha: array, shape=(nf,nw)
	household consumption
	
	Beta: array, shape=(nf,nf)
	investements
	
	Gamma: array, shape=(nf,nw)
	wages
	
	LambdaT: array, shape=(nb,nf)
	interest on loans
	
	MuT: array, shape=(nb,nw)
	interests on deposits
	
	returns:
	--------
	tm: array-like, shape=[4, 2*(nb+nf)+nw]
	
	see :
	[GL07] Godley, Lavoie, "Monetary Economics", 2007.
	http://docs.scipy.org/doc/scipy/reference/sparse.html
	[H17] "Volume of the steady-state space of financial flows in a 
		   monetary stock-flow-consistent model", http://arxiv.org/abs/1601.00822,
		   http://dx.doi.org/10.1016/j.physa.2017.01.050
	"""	
	raise RuntimeError('incorrect model')	
	
	# create random connectivity matrices. see [H17] p.5 for explanations
	# household consumption:
	# aij= 1 if the firm i is selling consumption goods to the household j
	if (Alpha==None): 
		Alpha   = rnd_col_mat(nf,nw,False) 
	else:
		assert Alpha.shape==(nf,nw), "Alpha.shape="+str(Alpha.shape)+" "+str((nf,nw))
	# investements:
	# aij= 1 if the firm j is selling capital goods to the firm i.
	if (Beta ==None):  
		Beta    = rnd_rowcol_mat(nf) # row and column sum = [1,...,1]
	else:		
		assert(Beta.shape==(nf,nf))	
	# wages
	# aij= 1 if firm i pays a wage to household j
	if (Gamma==None): 
		Gamma   = rnd_col_mat(nf,nw,False)
	else:		
		assert Gamma.shape==(nf,nw), "Gamma.shape="+str(Gamma.shape)+" "+str((nf,nw))
	# interest on loans:
	# aij= 1 if bank i is being paid interests by firm j.
	if (LambdaT==None): 
		LambdaT = rnd_col_mat(nb,nf,False) 
	else:		
		assert(LambdaT.shape==(nb,nf))	
	# interests on deposits:
	# aij= 1 if bank i pays interests to household j.
	if (MuT==None): 
		MuT = rnd_col_mat(nb,nw,False) 
	else:
		assert(MuT.shape==(nb,nw))		
	# get params
	delta = p['delta']
	r     = p['r']
	k     = p['k']
	alpha0= p['alpha0']
	alpha1= p['alpha1']
	alpha2= p['alpha2']
	# get identity matrices
	Ib= scipy.sparse.identity(nb, dtype='int8', format='csc')
	If= scipy.sparse.identity(nf, dtype='int8', format='csc')
	Iw= scipy.sparse.identity(nw, dtype='int8', format='csc')
	one_nw=scipy.sparse.csc_matrix(np.ones((1,nw)),dtype='int8')
	one_nf=scipy.sparse.csc_matrix(np.ones((1,nf)),dtype='int8')
	# make block matrix	
	"""
	cf fin_flow_constr.R
	l_bmw <-c("Cs","Cd","Is","Id","WBs","WBd","AFs","AFd",
	"ILs","ILd","IDs","IDd","M","L","K")
	label_long<-c(rep(l[1],nf), rep(l[2],nw),rep(l[3],nf),
	rep(l[4],nf), rep(l[5],nw), rep(l[6],nf), rep(l[7],nf),
	rep(l[8],nf), rep(l[9],nb), rep(l[10],nf),rep(l[11],nw),
	rep(l[12],nb), rep(l[13],nw), rep(l[14],nf), rep(l[15],nf) )
	"""
#						  "Cs","Cd","Is","Id","WBs","WBd","AFs","AFd","ILs","ILd","IDs","IDd","M","L","K"
	tm=scipy.sparse.bmat([[If,-Alpha, None,None,None,None,None,None,None,None,None,None,None,None,None],          # start rowsums equations
						 [None,None,Beta,-If,None,None,None,None,None,None,None,None,None,None,None],
						 [None,None,None,None,Gamma,-If,None,None,None,None,None,None,None,None,None],
						 [None,None,None,None,None,None,If,-If,None,None,None,None,None,None,None],
						 [None,None,None,None,None,None,None,None,Ib,-LambdaT,None,None,None,None,None],
						 [None,None,None,None,None,None,None,None,None,None,MuT,-Ib,None,None,None],	   # end rowsums equations
						 [None,-Iw,None,None,Iw,None,None,None,None,None,Iw,None,None,None,None], # start colsums equations
						 [If,None,If,None,None,-If,None,-If,None,-If,None,None,None,None,None],
						 [None,None,None, -If, None,None,If,None,None,None,None,None,None,None,None ],
						 [None,None,None,None,None,None,None,None,Ib,None,None,-Ib,None,None,None],# end colsums equations
						 [None,Iw,None,None,-alpha1*Iw,None,None,None,None,None,None,None,-(alpha1*r+alpha2)*Iw,None,None], #start beahvioral equations
						 [None,None,None,None,None,None,None,If,None,None,None,None,None,None,-delta*If],
						 [None,None,None,None,None,None,None,None,None,If,None,None,None,-r*If,None],
						 [None,None,None,None,None,None,None,None,None,None,Iw,None,-r*Iw,None,None], 
						[k*If,None,k*If,None,None,None,None,None,None,None,None,None,None,None,-If],		# NEW EQUATION(7.19+7.20); stop behavioral equations
						 [None,None,None,None,None,None,None,None,None,None,None,None,one_nw,None,None] , # start balance sheet eqn
						 [None,None,None,None,None,None,None,None,None,None,None,None,None,None,one_nf] ,
						 [None,None,None,None,None,None,None,None,None,None,None,None,None,If,-If],
						  [None,None,None,None,None,None,None,None,None,None,None,None,-MuT,LambdaT,None] #stop balance sheet equations
						 ])					 						 
	# check size
	nr,nc=tm.shape
	if(nr!=(4*nb+10*nf+3*nw+2)): raise IndexError
	if(nc!=(9*nf+4*nw+2*nb)): raise IndexError
	return tm	
	
def rnd_trans_mat_noledge(nb,nf,nw):
	"""
	generate a random transition matrix for the linear part of the model in [Keen11]
	no ledger entry
	
	parameters:
	-----------
	nb: int
	number of banks
	
	nf: int
	number of firms
	
	nw: int
	number of workers
	
	returns:
	--------
	tm: array-like, shape=[4, 2*(nb+nf)+nw]
	
	see :
	[Keen11] "A monetary Minsky model of the great moderation and the
	great recession", J.econ.behav. organ. doi:10.1016/j.jebo.2011.01.010	
	"""	
	#http://docs.scipy.org/doc/scipy/reference/sparse.html
	A=rnd_col_mat(nb,nf,False)
	E=rnd_col_mat(nf,nw,False)
	F=rnd_col_mat(nb,nw,False) 
	G=rnd_col_mat(nf,nb,True) # fewer banks than firms, can't ask noemptyrow
	H=rnd_col_mat(nf,nw,False)
	Ib= scipy.sparse.identity(nb, dtype='int8', format='csc')
	If= scipy.sparse.identity(nf, dtype='int8', format='csc')
	Iw= scipy.sparse.identity(nw, dtype='int8', format='csc')	
	# make block matrix
	tm=scipy.sparse.bmat([[-A, None,None,None,None,None,None,A], 
						[None, A, -A, None, -F, -Ib, None, None],
						[If, -If, If , -E, None, G, H, -If],
						[None, None,None, Iw, Iw, None, -Iw, None] ])	
	# check size
	nr,nc=tm.shape
	if(nr!=(2*nb+nf+nw)): raise IndexError
	if(nc!=(4*nf+3*nw+nb)): raise IndexError
	return tm
	
def rnd_permut_mat(n):
	"""
	generate a random square int8 matrix with rowsum and colsum = [1,...,1]
	we choose Compressed Sparse Column matrix (csc) for fast column slicing
	http://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csc_matrix.html
	"""
	data= np.ones(n,dtype=np.int8)
	row = np.random.permutation(n)
	col = range(n)
	a=scipy.sparse.csc_matrix((data, (row, col)), shape=(n, n), dtype=np.int8)	
	return a

def rnd_rowcol_mat(n):
	"""
	generate a random int8 matrix with colsum = [1,...,1] (not rowsum)
	we choose Compressed Sparse Column matrix (csc) for fast column slicing
	http://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csc_matrix.html
	"""
	data= np.ones(n,dtype=np.int8)
	row = np.random.permutation(n)
	col = range(n)
	a=scipy.sparse.csc_matrix((data, (row, col)), shape=(n, n), dtype=np.int8)	
	return a

def rnd_col_mat(m,n,emptyrow=True):
	"""
	generate a random int8 matrix with colsum = [1,...,1] (not rowsum)
	we choose Compressed Sparse Column matrix (csc) for fast column slicing
	http://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.csc_matrix.html
	"""
	data= np.ones(n,dtype=np.int8)
	maxit=1000
	for i in range(maxit):		
		row = np.random.randint(0,m,n)
		# check that all rows 0:m have been sampled once
		testall= np.all(np.in1d(range(m),row ))
		if(emptyrow|testall): break
		# check max rejection is reached
		if(i==maxit-1): 
			print "i=",i
			print "row=",row
			print "testall=",testall
			raise ValueError('sampling failed')		
				
	col = range(n)
	a=scipy.sparse.csc_matrix((data, (row, col)), shape=(m, n), dtype=np.int8)	
	return a	

def test_MC_rnd_col_mat(m=10,n=20,imax=1000):
	"""
	Monte Carlo sampling
	"""		
	for i in range(imax):		
		try:
			tm=rnd_col_mat(m,n, False)
		except: 
			print "Unexpected error:", sys.exc_info()[0]
			print "i=",i,"tm=",tm.toarray()
			print "nnz=",tm.getnnz(1)
			raise
			
def test_MC_rnd_trans_mat_noledge(nb=2,nf=3,nw=4,imax=1000):			
	"""
	Monte Carlo sampling with Keen's model 
	"""	
	nb_fail=np.array([0,0])
	
	for i in range(imax):	
		try:	
			tm=rnd_trans_mat_noledge(nb,nf,nw)
		except: 
			print "Unexpected error:", sys.exc_info()[0]			
			nb_fail[0] +=1
		# column sums must be 0 in this model	
		try:			
			checkcolsum(tm)
		except:			
			print "tm.sum(0)",tm.sum(0)
			print "i=",i,"tm=",tm.toarray()
			print "nnz=",tm.getnnz(1)
			nb_fail[1] +=1
	print "percent fail=", 100.*nb_fail/float(imax)		
			
def test_MC_rnd_trans_mat_bmw(nb=2,nf=3,nw=4,imax=1000):			
	"""
	Monte Carlo sampling with BMW model 
	"""	
	nb_fail=np.array([0,0])
	tm=[]
	nb=2;nf=3;nw=4; p={}; 	p['r']		=0.03 ; 
	p['delta'] =0.1 ; p['alpha1']=0.75; p['alpha2']=0.1 # [GL07] p.237
	
	for i in range(imax):	
		try:	
			tm=rnd_trans_mat_bmw(nb,nf,nw,p)
		except: 
			print "Unexpected error:", sys.exc_info()[0]			
			nb_fail[0] +=1
		# column do not necessarily sum to 0 in this model				
	print "percent fail=", 100.*nb_fail/float(imax)		
			

			
def saveRhar(tm,fname):	
	"""
	in the R package har, linear constraints are endoded according to the
	convention	Ax<=b
	"""
	a=tm.toarray()
	if (a.dtype==np.int8):
		np.savetxt(fname,a, fmt='%d')
	else:	
		np.savetxt(fname,a)
	
	
def checkcolsum(tm):
	"""
	check that the sum along each column is zero
	"""
	if np.any(tm.sum(0)!=0): raise ValueError			
		
		
def checkemptyrow(tm):
	"""
	check that no row is empty
	"""		
	if (np.any(tm.getnnz(1)==0)): raise ValueError
		
	
	
if __name__ == "__main__":
	nb=2
	nf= 3
	nw= 6
	
	#test_MC_rnd_col_mat(nb,nf)
	#test_MC_rnd_trans_mat_noledge()
	#test_MC_rnd_trans_mat_bmw(nb=2,nf=3,nw=4,imax=1000)
	
	# Keen
	"""
	tm=rnd_trans_mat_noledge(nb,nf,nw)		
	checkcolsum(tm)
	checkemptyrow(tm)
	print tm.toarray()
	saveRhar(tm,"transition_matrix.txt")
	"""
	# BMW
	"""
	p={}; 	p['r']	=0.03 ; p['alpha0']=0
	p['delta'] =0.1 ; p['alpha1']=0.75; p['alpha2']=0.1 # [GL07] p.237
	tm=rnd_trans_mat_bmw(nb,nf,nw,p)
	saveRhar(tm,"transition_matrix_bmw_236.txt")
	"""
	# plot factor graph: pydot, 
	# cf http://pymc-devs.github.io/pymc/modelbuilding.html#graphing-models
	
	# TODO: nosetests 
	# nb=...=1  => on retombe sur la matrice de Keen
	# direction de chaque flux...		
	
	#test_CSP_bmw_full()
	#test_interior_point_gaussian_augmented_system()
	#test_get_x0_234()
	#test_get_x0_119()
	#test_get_x0_linprog()
	test_CSP_bmw_FiCM()
