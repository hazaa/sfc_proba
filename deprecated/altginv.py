# -*- coding: utf-8 -*-
#  altginv.py
#  
#  Copyright 2018 aurelien <aurelien@aurelien-300E4A-300E5A-300E7A-3430EA-3530EA>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import numpy as np
import scipy
#from scipy import optimize
		
from cylp.cy import CyClpSimplex
from cylp.py.modeling.CyLPModel import CyLPArray,CyLPModel
from cylp.py.utils.sparseUtil import csr_matrixPlus, csc_matrixPlus,sparseConcat

import matplotlib.pyplot as plt


def altginv():
	"""
	named after alternative generalized inverse
	see gribonval matlab altginv https://github.com/doksa/altginv
		and associated papers
		
	TODO: translate implementation in python	
	"""
	pass

def altginv2_old(A,threshold=None):
	"""
	MEMORY management is BAD
	
	compute generalized pseudo-inverse, with sparse constraint.
	copied from the adaptation of spinv: https://stackoverflow.com/questions/51273038/calculating-spinv-with-svd
	
	sparse lp solvers :
	example with csc sparse mat: https://github.com/coin-or/CyLP/blob/master/cylp/tests/test_CyClpSimplex_CyLPModel.py
	https://github.com/coin-or/CyLP
	(which LP solver ? : https://stackoverflow.com/questions/34821894/scipy-linear-programming-with-sparse-matrices)	
	
	input:
	---
	A: sparse array, shape=(n,m)
	
	output:
	---
	B: array, shape=(m,n)
	generalized inverse of A
	"""	
	raise NotImplementedError('do not use, deprecated')
	n, m = A.shape
	I = np.eye(n)		
	#Aeq = np.matrix(np.hstack((A, -A)))
	Aeq=sparseConcat(A, -A, 'h')
	# objective
	c = np.ones((2*m))
	c = csr_matrixPlus([c]).T
	#s.objective = c.T * x
	# spinv
	#B = np.zeros((m, n))
	B=scipy.sparse.lil_matrix((m,n))		
	for i in range(n):
		# cylp setup
		model = CyLPModel()
		x = model.addVariable('x', 2*m)
		beq = CyLPArray(I[:, i]) #beq = I[:, i]
		model.addConstraint(Aeq * x == beq)
		#model.objective = 1*x[0]  + 1*x[1] + 1*x[2] + 1*x[3]
		model.objective = c.T *x
	    # Solve it a first time
		s = CyClpSimplex(model)
		s.primal()
		result = s.primalVariableSolution['x']
		#
		sol = result[0:m]-result[m:2*m]
		if threshold>0:
			sol[np.abs(sol)<threshold]=0
		try:	
			B[:, i] = sol.reshape((m,1))
		except:
			print "type(sol),sol.shape,B[:, i].shape, m=",type(sol),sol.shape, B[:, i].shape,m	
			raise ValueError('shape mismatch')
	return B

def altginv2(A,threshold=None):
	"""
	compute generalized pseudo-inverse, with sparse constraint.
	copied from the adaptation of spinv: https://stackoverflow.com/questions/51273038/calculating-spinv-with-svd
	
	sparse lp solvers :
	example with csc sparse mat: https://github.com/coin-or/CyLP/blob/master/cylp/tests/test_CyClpSimplex_CyLPModel.py
	https://github.com/coin-or/CyLP
	(which LP solver ? : https://stackoverflow.com/questions/34821894/scipy-linear-programming-with-sparse-matrices)	
	
	cylp doc: http://coral.ie.lehigh.edu/~ted/files/coin-or/slides/COINPython.pdf
	
	input:
	---
	A: sparse array, shape=(n,m)
	
	output:
	---
	B: array, shape=(m,n)
	generalized inverse of A
	"""	
	n, m = A.shape
	I = np.eye(n)		
	#Aeq = np.matrix(np.hstack((A, -A)))
	Aeq=sparseConcat(A, -A, 'h')
	# objective
	c = np.ones((2*m))
	c = csr_matrixPlus([c]).T
	#s.objective = c.T * x
	# spinv
	#B = np.zeros((m, n))
	B=scipy.sparse.lil_matrix((m,n))		
	model = CyLPModel()
	x = model.addVariable('x', 2*m)
	model.objective = c.T *x
	s = CyClpSimplex(model)
	for i in range(n):
		# cylp setup
		beq = CyLPArray(I[:, i]) #beq = I[:, i]
		if i==0:
			#model.addConstraint(Aeq * x == beq,'cstr1')
			s.addConstraint(Aeq * x == beq,'cstr1')
		else:
			s.removeConstraint('cstr1')
			s.addConstraint(Aeq * x == beq,'cstr1')			
	    # Solve it a first time
		s.primal()
		result = s.primalVariableSolution['x']
		#
		sol = result[0:m]-result[m:2*m]
		if threshold>0:
			sol[np.abs(sol)<threshold]=0
		try:	
			B[:, i] = sol.reshape((m,1))
		except:
			print "type(sol),sol.shape,B[:, i].shape, m=",type(sol),sol.shape, B[:, i].shape,m	
			raise ValueError('shape mismatch')
	return B	
	

def solve_leastnorm_pos(A,b,norm=1,sparsity=None):
	"""
	least norm solution in the L1 sense of Ax=b 
	x >=0
	
	works with scipy.sparse matrix
	returns L1 sparse solution 
	
	https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.lsqr.html#scipy.sparse.linalg.lsqr
	!!scipy.sparse.linalg.lsqr(A, b)
	
	cylp doc: http://coral.ie.lehigh.edu/~ted/files/coin-or/slides/COINPython.pdf
	"""
	n, m = A.shape
	# objective
	c = np.ones((m))
	c = csr_matrixPlus([c]).T
	
	model = CyLPModel()
	x = model.addVariable('x', m)
	if norm==-1:
		model.objective = -c.T *x
	if norm==1:
		model.objective = c.T *x
	elif norm==2:
		model.objective = x*x # does't work
	model.addConstraint(x >= 0)
	
	if sparsity>0:
		# control sparsity level
		model.addConstraint(c.T * x >= sparsity*m)
	
	s = CyClpSimplex(model)
	beq = CyLPArray(b) #beq = I[:, i]
	
	s.addConstraint(A * x == beq)
	s.primal()
	return s.primalVariableSolution['x']
	#model.addConstraint(u >= v) ??????????????? => separer u et v ???

