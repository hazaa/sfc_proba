# -*- coding: utf-8 -*-
"""
Created on nov 22 2017

Module:
    EP_network - python wrapper to Julia implementation of expectation propagation
    MetabolicEP.jl; application to random economic network generated using network.py
    
Author:
    Aurélien Hazan, after the Julia implementation of MetabolicEP by Braunstein, Muntoni, Pagnani.
    
Description:
    Implementation of the ...
    
Usage:
    Be ``F,b,lb,ub`` NumPy arrays,... 
    Import the module and initialize the Fitness Configuration Model::
        >>> import EP_network
        >>> e = EP_network()
    To run, use::
        >>> e.run()
    To plot, use::    
		>>> e.plot()
References:
.. [Braunstein2017] `Braunstein, Muntoni, Pagnani,
    An analytic approximation of the feasible space of metabolic networks,
    Nature Communications 8 14915 (2017)
    <https://www.arxiv.org/abs/1702.05400>`_
.. [MetabolicEP] `Julia implementation of Expectation Propagation 
	<https://github.com/anna-pa-m/Metabolic-EP>`_    
.. [pyjulia] `Julia to Python  
	<https://github.com/JuliaPy/pyjulia>`_    
    
"""

"""
TOCHANGE: code depends on transmat.get_bmw_... => get rid of it
"""
import numpy as np
from scipy.stats import truncnorm,norm
import pandas as pd
from transmat import get_bmw_labels

try: import julia
except: print 'julia not available'


# -------------------------------------------------------------------
# TESTING holomaps

def test_melt_labels():
	n = 10
	lab = ["a","b","c"]
	longlabel =0
	df = pd.DataFrame()
	pd.melt()

def test_df():
	df1 = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
						'B': ['B0', 'B1', 'B2', 'B3'],	
						'C': ['C0', 'C1', 'C2', 'C3'],
						'D': ['D0', 'D1', 'D2', 'D3']},
	                    index=[0, 1, 2, 3])
	# différentes longueurs, avec np.array                    
	df2 = pd.DataFrame({'A': ['A0', 'A1', 'A2', 'A3'],
						'B': ['B0', 'B1', 'B2', 'B3'],	
						'C': [np.zeros(10), np.zeros(10), np.zeros(10), np.zeros(10)],
						'D': ['D0', 'D1', 'D2', 'D3']},
	                    index=[0, 1, 2, 3])
	df2.iloc[0]
	df2.iloc[0]['C']

def testDummy():
	""" 
	create a dummy dataset and a holomap that have the same structure as the target problem;	
	http://pandas.pydata.org/pandas-docs/stable/advanced.html#creating-a-multiindex-hierarchical-index-object
	"""
	# parameters
	p1val = [1,2,3]
	p2val = [0.1,0.2,0.3,0.4]
	p3val = [10,20]
	itval = range(10)
	iterables = [p1val,p2val,p3val,itval]
    # create index and dataframe (supposing data was already generated)
	idx=pd.MultiIndex.from_product(iterables, names=['p1','p2','p3','it'])
	df1 = pd.DataFrame(index=idx)
	nflow=10
	label_long = [i for i in 'aaabbbcccd']; #label_short=['a','b','c','d']
	assert(len(label_long)==nflow)
	df = pd.DataFrame({'mu': [np.random.rand(nflow) for i in range(len(idx))],
					   'sigma': [0.01*np.random.rand(nflow) for i in range(len(idx))]},
						index=idx)
	for i in range(5): print df.index.names,df.index[i],df.iloc[i]				
    # create a dummy hv.Holomap (NOT using df)
    # http://holoviews.org/getting_started/Tabular_Datasets.html
	# curve: dummy overlay 
	def overlay(p1,p2,p3,i):
		# this overlay doesn't use param values; just for test
		xvals = [0.1* i for i in range(100)]
		curve =  hv.Curve((xvals, [np.sin(x) for x in xvals]))
		scatter =  hv.Scatter((xvals[::5], np.linspace(0,1,20)))
		return curve * scatter
	dic={(p1,p2,p3,i):overlay(p1,p2,p3,i)
		for p1 in p1val for p2 in p2val for p3 in p3val for i in itval }		
	hmap = hv.HoloMap(dic, kdims=['p1val', 'p2val','p3val','it'])
	# create a hv.Holomap that uses df
    # group by label and overlay pdf
	a,b=-1,1
	xvals= np.linspace(a,b,20)    
	def overlay_pdf(p1,p2,p3,i,l):
		# get (mu,sig) that correspond to p1,p2,p3,i
		mu,sig=df.loc[(p1,p2,p3,i)]
		# get indexes in label_long that correspond to l
		idx=np.where(np.array(label_long)==l)[0]
		# compute pdf Curves	
		pdfs = { ii :   hv.Curve((xvals,truncnorm.pdf(xvals,a,b,loc=mu[ii],scale=sig[ii])))
				for ii in idx}				
		return 	hv.NdOverlay(pdfs, kdims='nagent')	
	dic={(p1,p2,p3,i,l):overlay_pdf(p1,p2,p3,i,l)
		for p1 in p1val for p2 in p2val for p3 in p3val for i in itval for l in label_short}		
	hmap = hv.HoloMap(dic, kdims=['p1val', 'p2val','p3val','it','label']) 
	# save to static html
	#renderer.save(hmap, './hmap', fmt='html')
	s=renderer.static_html(hmap)
	f= open('hmap.html', 'w'); f.write(s) ; f.close()
	#from ..util.settings import OutputSettings , max_frames
	#from holoviews.ipython.display_hooks import OutputSettings
	#max_frames=OutputSettings.options['max_frames']
	
    # group by label and plot histograms over iterations
    # create a hv.Holomap
    # 

# ------------------------------------------------------------------------
# TESTING class EP_network

def test_EP_network__get_short_ub():
	e = EP_network()
	#e.init()
	e.label_short = ['a','b','c']
	e.label_long = ['a','a','b','b','b','c','c']	
	e.nflow_short = len(e.label_short)
	ub_long = [1.,2.,3.,4.,5.,6.,7.]
	ub_short = e._get_short_ub(ub_long)	
	assert np.all(ub_short==np.array([1.,3.,6.]))
	
def test_EP_network_init():
	e = EP_network()
	e.init()
	print e.p

def test_EP_network_run():
	"""
	"""
	raise NotImplementedError
	import transmat
	import network		
	# params
	fname="/home/aurelien/local/git/sfc_proba/data/eurostat_network.h5"		
	nb,nf,nw=3,100,1000
	p_bmw  = {'r': 0.03,'alpha0':0.1 , 'alpha1':0.75,
		'alpha2':0.01,'delta' :0.1,'k':6, 'M':10.}	
	Mtot =p_bmw['M']
	# init
	e = EP_network()
	print e.p
	e.init()
	# create matrices
	#F,g,lb,ub = 
	A,B,C,D,E,FF=network.get_network_matrix(nb=nb,nf=nf,nw=nw,fname=fname)
	F,g,lb,ub = transmat.CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p_bmw, M=Mtot)
	#
	#j = e.julia.convert(T,x)
	#
	j.mysparse(F) # does't work
	#
	e.run(F,g,lb,ub)
	print e.df

def test_EP_network_init_block_network_julia():
	j=julia.Julia()
	j._call("""
	path_MetabolicEP = "/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl"
	path_sfc_proba="/home/aurelien/local/git/sfc_proba"
	path_cython = path_sfc_proba * "/cython"
	path_network_file="/home/aurelien/local/git/sfc_proba/data/eurostat_network.h5"
	using PyCall
	using COBRA
	include(path_MetabolicEP)
	using MetabolicEP
	unshift!(PyVector(pyimport("sys")["path"]), path_sfc_proba)
	unshift!(PyVector(pyimport("sys")["path"]), path_cython)
	@pyimport transmat
	@pyimport network
		
	#conversion from scipy.sparse to julia (https://github.com/JuliaPy/PyCall.jl/issues/204)
			const scipy_sparse_find = pyimport("scipy.sparse")["find"]
			function mysparse(Apy::PyObject)
				IA, JA, SA = scipy_sparse_find(Apy)
				return sparse(Int[i+1 for i in IA], Int[i+1 for i in JA], SA)
			end
			
	nb,nf,nw=3,300,1000	
	Mtot =10.
	p = PyDict(Dict([("r", 0.03), ("delta", 0.1), ("k", 6),                       
                         ("alpha0", 0.75),("alpha1", 0.01), ("alpha2", 0.01)  ]))
	L_invest=3*nf                   
	L_cons_firm=2*nf
	L_cons_hh = 4*nw
	year="2008"
	geo="CZ"
	n=network.init_network_block(year=year,geo=geo,nb=nb,nf=nf,nw=nw,
								fname=path_network_file,
								L_invest=L_invest, L_cons_firm=L_cons_firm, L_cons_hh = L_cons_hh)
	A,B,C,D,E,F = n[:sample_full_network](nb,nf,nw)
	F,g,lb,ub = transmat.CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p, M=Mtot)					
	r=MetabolicEP.metabolicEP(mysparse(F),g[:,1],lb,ub,maxiter=2)
	""")

def test_EP_network_sample_rnd_fitness_cd_julia():
	j=julia.Julia()
	j._call("""
	path_sfc_proba="/home/aurelien/local/git/sfc_proba"	
	path_experimental = path_sfc_proba * "/experimental"	
	using PyCall
	using COBRA
	unshift!(PyVector(pyimport("sys")["path"]), path_sfc_proba)
	unshift!(PyVector(pyimport("sys")["path"]), path_experimental)
	@pyimport har as cython_har
	@pyimport har_nonuniform 
	
	cython_har.pdf_pareto_cython(1,1.1,0.1)	
	p1=1.1
	p2=0.1
	f_mean = p2*p1/(p1-1.)
	fitness = PyDict(Dict([("seed", PyObject(py"None")), ("thin",1), ("accel",1),
		("log",1), ("f",cython_har.pdf_pareto_cython),("f_mean",f_mean),
		("f_nsamp",PyObject(py"lambda x: x**2") ),("param1",p1),("param2",p2) ]))
		
	groups = PyArray(PyObject([0,0,0,1,1,1,1,2,2]))
	sums = PyArray(PyObject([3,4,2]))
	xi_firm=har_nonuniform.sample_rnd_fitness_cd(groups, sums,fitness)
	"""
	)

def test_EP_network_init_rndfitness_network_julia():
	j=julia.Julia()
	j._call("""
	path_MetabolicEP = "/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl"
	path_sfc_proba="/home/aurelien/local/git/sfc_proba"
	path_cython = path_sfc_proba * "/cython"
	path_experimental = path_sfc_proba * "/experimental"
	path_network_file="/home/aurelien/local/git/sfc_proba/data/eurostat_network.h5"
	using PyCall
	using COBRA
	include(path_MetabolicEP)
	using MetabolicEP
	unshift!(PyVector(pyimport("sys")["path"]), path_sfc_proba)
	unshift!(PyVector(pyimport("sys")["path"]), path_cython)
	unshift!(PyVector(pyimport("sys")["path"]), path_experimental)
	@pyimport transmat
	@pyimport network
	@pyimport har as cython_har
		
	#conversion from scipy.sparse to julia (https://github.com/JuliaPy/PyCall.jl/issues/204)
			const scipy_sparse_find = pyimport("scipy.sparse")["find"]
			function mysparse(Apy::PyObject)
				IA, JA, SA = scipy_sparse_find(Apy)
				return sparse(Int[i+1 for i in IA], Int[i+1 for i in JA], SA)
			end
			
	nb,nf,nw=3,300,1000	
	Mtot =10.
	p1=1.1
	p2=0.1
	f_mean = p2*p1/(p1-1.)
	
	p = PyDict(Dict([("r", 0.03), ("delta", 0.1), ("k", 6),                       
                         ("alpha0", 0.75),("alpha1", 0.01), ("alpha2", 0.01)  ]))
	L_invest=3*nf                   
	L_cons_firm=2*nf
	L_cons_hh = 4*nw
	year="2008"
	geo="CZ"
	fitness = PyDict(Dict([("seed", nothing), ("thin",1), ("accel",1),
		("log",1), ("f",cython_har.pdf_pareto_cython),("f_mean",f_mean),
		("f_nsamp",x->x^2 ),("param1",1.1),("param2",0.1) ]))			
	n=network.init_network_rndfitness(year=year,geo=geo,nb=nb,nf=nf,nw=nw,
					fname=path_network_file,
					L_invest=L_invest, L_cons_firm=L_cons_firm, L_cons_hh = L_cons_hh, 
					fitness=fitness, 
					har="hd")				
	A,B,C,D,E,FF = n[:sample_full_network](nb,nf,nw)
	F,g,lb,ub = transmat.CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p, M=Mtot)					
	r=MetabolicEP.metabolicEP(mysparse(F),g[:,1],lb,ub,maxiter=2)
	""")	

def test_EP_network_init_rndfitness_network():
	nb,nf,nw=3,300,1000
	r=0.03 ;alpha0=0.1;	alpha1=0.75;alpha2=0.01;delta=0.1;k=6;M=10.
	L_invest=3*nf
	L_cons_firm=2*nf
	L_cons_hh = 4*nw
	har = "hd" # !!!!!!
	year="2008";geo="CZ"
	fitness = {'seed':0, 'thin': 1, 'accel':1, 'log':1,
				'f': 'UNUSED!!!!' , #cython_har.pdf_pareto_cython,
				'f_mean': 0,
				'f_nsamp': 'UNUSED!!!!', #lambda x:x**2,
				'param1':1.1 , 'param2':0.1	}
	fitness['f_mean'] = fitness['param2']* fitness['param1']/(fitness['param1']-1.)  # b*xlim/b-1
	# 	
	e = EP_network()
	e.init()
	e.init_rndfitness_network(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh,fitness,har)

def test_EP_network_init_block_network():	
	# params
	nb,nf,nw=3,50,500
	r=0.03 ;alpha0=0.1;	alpha1=0.75;alpha2=0.01;delta=0.1;k=6;M=10.
	L_invest=3*nf
	L_cons_firm=2*nf
	L_cons_hh = 4*nw
	year="2008";geo="CZ"
	# 	
	e = EP_network()
	e.init()
	e.init_block_network(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh)
	

def test_EP_network_run_block_network():
	# params
	nb,nf,nw=3,50,500
	r=0.03 ;alpha0=0.1;	alpha1=0.75;alpha2=0.01;delta=0.1;k=6;M=10.
	L_invest=3*nf
	L_cons_firm=2*nf
	L_cons_hh = 4*nw
	year='2008';geo='CZ'
	# 	
	e = EP_network()
	e.init()
	e.init_block_network(year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh)
	mu,s,av,va,ub,A,B,C,D,E,FF=e.run_block_network(nb,nf,nw,r,alpha0,alpha1,alpha2,delta,k,M,maxiter=1)

def test_EP_network_single_block():	
	# params
	nb,nf,nw=3,100,1000
	r=0.03 ;alpha0=0.1;	alpha1=0.75;alpha2=0.01;delta=0.1;k=6;M=10.
	e = EP_network()
	print e.p
	e.init()
	e.run_single_block(nb,nf,nw,r,alpha0,alpha1,alpha2,delta,k,M)
	print e.df

def test_EP_network_run_batch():
	e = EP_network()
	print e.p
	e.init()
	e.run_batch()
	print e.df

def test_EP_network_save():
	e = EP_network()	
	e.init()
	e.run_batch()
	print e.df
	groupname='testgroup'
	e.save(groupname)
	store = pd.HDFStore(e.path_fname_out)
	print store
	print store[groupname]
def test_EP_network_load():
	e = EP_network()	
	groupname='testgroup'
	e.load(e.path_fname_out,groupname)		
	print e.df
	
def test_EP_network_plot_holomap():
	e = EP_network()
	e.init()
	#e.run()
	groupname='testgroup'
	e.load(e.path_fname_out,groupname)	
	e.plot_holomap('/home/aurelien/local/git/sfc_proba/fig/hmap.html')	

def test_EP_network_large():
	p={'nb':3,'nf':10,'nw':100, 'niter': 5,
		'r': [0.01,0.03],'alpha0':[0.05,0.1] , 'alpha1':[0.5,0.75],
		'alpha2':[0.01],'delta' :[0.05,0.1],'k':[4,6], 'M':[1.,10.]}	
	e = EP_network(p)
	e.init()
	e.run_batch()	
	groupname='testgroup_large'
	e.save(groupname)
# ------------------------------------------------------------------------

class EP_network(object):
	"""EP_network class.
	
    This class implements the ...
	
	"""
	def __init__(self, p=None):
		"""Initialize the parameters of .
		:param p: contains all the parameters
		:type p: dict
        """
		if not p==None: self.p = p;
		else: self.p = self._default_params()		
		self.julia = julia.Julia()
		self.path_MetabolicEP = "/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl"
		self.path_sfc_proba="/home/aurelien/local/git/sfc_proba"
		self.path_network_file="/home/aurelien/local/git/sfc_proba/data/eurostat_network.h5"
		self.path_fname_out = "/home/aurelien/local/git/sfc_proba/data/EP_network.h5"
		self.df = []
		self.init_done = False
		self.init_block_network_done = False
		self.init_rndfitness_network_done =False
		self.label_short = []
		self.label_long = []
		self.nflow_short=0
		self.nflow_long=0
	def _default_params(self):						
		p={'nb':3,'nf':10,'nw':60, 'niter': 2,
		'r': [0.03],'alpha0':[0.1] , 'alpha1':[0.75],
		'alpha2':[0.01],'delta' :[0.1],'k':[6], 'M':[10.]}
		return p
	def init(self):
		# parameters		
		p=self.p
		iterables = [p['r'],p['alpha0'],p['alpha1'] ,p['alpha2'],
					p['delta'],p['k'],p['M'],
					range(p['niter'])]
	    # create index and dataframe (supposing data was already generated)
		idx=pd.MultiIndex.from_product(iterables,
		   names=['r','alpha0','alpha1','alpha2','delta','k','M','niter'])
		# prepare julia  
		self.julia._call(u""" 
			using PyCall
			using COBRA
			include("%s")
			using MetabolicEP
		
			path_sfc_proba="%s"
			path_cython = path_sfc_proba * "/cython"
			path_experimental = path_sfc_proba * "/experimental"	
			unshift!(PyVector(pyimport("sys")["path"]), path_sfc_proba)			
			unshift!(PyVector(pyimport("sys")["path"]), path_cython)
			unshift!(PyVector(pyimport("sys")["path"]), path_experimental)
			@pyimport transmat
			@pyimport network
			@pyimport har as cython_har
			#conversion from scipy.sparse to julia (https://github.com/JuliaPy/PyCall.jl/issues/204)
			const scipy_sparse_find = pyimport("scipy.sparse")["find"]
			function mysparse(Apy::PyObject)
				IA, JA, SA = scipy_sparse_find(Apy)
				return sparse(Int[i+1 for i in IA], Int[i+1 for i in JA], SA)
			end			
			"""%(self.path_MetabolicEP,self.path_sfc_proba))						
		# create df
		nb,nf,nw=self.p['nb'],self.p['nf'],self.p['nw']
		self.label_short,self.label_long = get_bmw_labels(nb,nf,nw)
		self.nflow_short=len(self.label_short)		
		self.nflow_long=len(self.label_long)		
		self.df = pd.DataFrame({'mu': [np.zeros(self.nflow_long) for i in range(len(idx))],
						   'sigma': [np.zeros(self.nflow_long) for i in range(len(idx))],
						   'ub': [np.zeros(self.nflow_short) for i in range(len(idx))]},
							index=idx)
		self.init_done =True					
	def _get_short_ub(self,ub_long):
		# idx=np.where(np.array(label_long)==l)[0]
		ub_short = np.zeros(self.nflow_short)
		ll_arr=np.array(self.label_long)
		for i,l in enumerate(self.label_short):
			idx_first=np.where(ll_arr==l)[0][0]			
			ub_short[i]=ub_long[idx_first]
		return ub_short
	def run(self,F,g,lb,ub):
		"""
		single run
		
		DOESN'T WORK (because pycall not mature)
				
		"""
		raise NotImplementedError
		if not self.init_done : raise RuntimeError('init not run yet')		
		# OPTION1: direct call, after import of julia module
		e.julia.using('MetabolicEP') 
		from julia import MetabolicEP
		res=MetabolicEP(F,g,lb,ub)
		# OPTION2:!!!!!!!!!!! convert to julia objects: (F),g[:,1],lb,ub			
		self.julia._call("""			
			r=MetabolicEP.metabolicEP(mysparse(F),g[:,1],lb,ub)
		""")
		mu=self.julia.eval(u'r.μ')
		s=self.julia.eval(u'r.σ')	
		return mu,s
	
	def init_block_network(self,year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh,proxy_invest_conso=False):
		"""
		block fitness
		"""
		self.julia._call("""			
			nb,nf,nw=%d,%d,%d
			n=network.init_network_block(year="%s",geo="%s",nb=nb,nf=nf,nw=nw,fname="%s",L_invest=%d, L_cons_firm=%d, L_cons_hh = %d, proxy_invest_conso=%d)
			"""%(nb,nf,nw,year,geo,self.path_network_file,L_invest,L_cons_firm,L_cons_hh,proxy_invest_conso)			
			)
		self.init_block_network_done = True

	def init_rndfitness_network_old(self,year,geo,nb,nf,nw,L_invest,L_cons_firm,L_cons_hh,fitness,har):
		"""
		fitness taken from arbitrary distribution with constrained block sums
		
		TODO: how to pass functions ??????????
		"""
		raise DeprecationWarning('deprecated function')
		thin, accel,log, p1,p2,f_mean = fitness['thin'],  fitness['accel'], fitness['log'],  fitness['param1'], fitness['param2'], fitness['f_mean']		
		self.julia._call("""			
			nb,nf,nw=%d,%d,%d			
			thin = %d
			accel = %d
			log = %d
			p1= %f
			p2= %f
			f_mean = %f
			f_mean = p2*p1/(p1-1.)
			fitness = PyDict(Dict([("seed", PyObject(py"None")), ("thin",thin), 
							("accel",accel),
					("log",log), ("f",cython_har.pdf_pareto_cython),("f_mean",f_mean),
					("f_nsamp",PyObject(py"lambda x: x**2") ),("param1",p1),("param2",p2) ]))
			fitness = PyDict(Dict([("seed", nothing), ("thin",1), ("accel",1),
               ("log",1), ("f",cython_har.pdf_pareto_cython),("f_mean",f_mean),
               ("f_nsamp",x->x^2 ),("param1",1.1),("param2",0.1) ]))
			n=network.init_network_rndfitness(year="%s",geo="%s",nb=nb,nf=nf,nw=nw,fname="%s",L_invest=%d, L_cons_firm= %d, L_cons_hh = %d, fitness=fitness, har="%s")
			"""%(nb,nf,nw,thin, accel,log, p1,p2,f_mean,year,geo,self.path_network_file,L_invest,L_cons_firm,L_cons_hh,har)			
			)
		self.init_rndfitness_network_done = True
		
	def run_rndfitness_network(self,nb,nf,nw,r,alpha0,alpha1,alpha2,delta,k,M,maxiter=2000):
		"""
		!!!!!!!!!!!!!!
		"""
		if not self.init_rndfitness_network_done : raise RuntimeError('init_rndfitness not run yet')
		pass
		
	def run_block_network(self,nb,nf,nw,r,alpha0,alpha1,alpha2,delta,k,M,maxiter=2000):
		"""
		doesn't initialise the ficm networks (this is done in init_rndfitness_network)
		this allows to resample from the same set of probability matrics {pij}
		"""
		if not self.init_block_network_done : raise RuntimeError('init_block not run yet')
		self.julia._call("""
			nb,nf,nw=%d,%d,%d
			p = PyDict(Dict([("r", %f), ("delta", %f), ("k", %f),
						("alpha0", %f),("alpha1", %f), ("alpha2", %f)  ]))
			Mtot = %f			
			A,B,C,D,E,FF = n[:sample_full_network](nb,nf,nw)
			F,g,lb,ub = transmat.CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p, M=Mtot)
			r=MetabolicEP.metabolicEP(mysparse(F),g[:,1],lb,ub,maxiter=%d)
			"""%(nb,nf,nw,r,delta,k,alpha0,alpha1,alpha2,M,maxiter)		
			)
		mu=self.julia.eval(u'r.μ')
		s=self.julia.eval(u'r.σ')
		av=self.julia.eval(u'r.av')
		va=self.julia.eval(u'r.va')
		ub_long = self.julia.eval(u'ub')
		A=self.julia.eval(u'A'); B=self.julia.eval(u'B'); C=self.julia.eval(u'C')
		D=self.julia.eval(u'D');E=self.julia.eval(u'E'); FF=self.julia.eval(u'FF')
		firm_to_sector = self.julia.eval(u'n[:result_firm_to_sector]')
		return mu,s,av,va,ub_long,A,B,C,D,E,FF,firm_to_sector
		
	def run_single_block(self,nb,nf,nw,r,alpha0,alpha1,alpha2,delta,k,M):	
		"""
		single run
		collect A,B,C, ...
		
		DEPRECATED: get_network_matrix_block runs init_network_block, then sample_full_network
		=> use EP.run_block_network instead
		
		"""
		raise DeprecationWarning("use EP.run_block_network instead!")
		
		if not self.init_done : raise RuntimeError('init not run yet')		
		
		self.julia._call("""			
			nb,nf,nw=%d,%d,%d	
			p = PyDict(Dict([("r", %f), ("delta", %f), ("k", %f),
						("alpha0", %f),("alpha1", %f), ("alpha2", %f)  ]))
			Mtot = %f
			A,B,C,D,E,FF=network.get_network_matrix_block(nb=nb,nf=nf,nw=nw,fname="%s")
			F,g,lb,ub = transmat.CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p, M=Mtot)
				
			r=MetabolicEP.metabolicEP(mysparse(F),g[:,1],lb,ub)
		"""%(nb,nf,nw,r,delta,k,alpha0,alpha1,alpha2,M, self.path_network_file))
		mu=self.julia.eval(u'r.μ')
		s=self.julia.eval(u'r.σ')
		A=self.julia.eval(u'A'); B=self.julia.eval(u'B'); C=self.julia.eval(u'C')
		D=self.julia.eval(u'D');E=self.julia.eval(u'E'); FF=self.julia.eval(u'FF')
		return mu,s,A,B,C,D,E,FF
	def run_batch(self):	
		"""
		batch run, with params in self.df.index
		does'n collect A,B,C..
		"""
		if not self.init_done : raise RuntimeError('init not run yet')
		nb,nf,nw=self.p['nb'],self.p['nf'],self.p['nw']
		
		for (r,alpha0,alpha1,alpha2,delta,k,M,niter) in self.df.index:
			self.julia._call("""			
				nb,nf,nw=%d,%d,%d	
				p = PyDict(Dict([("r", %f), ("delta", %f), ("k", %f),
							("alpha0", %f),("alpha1", %f), ("alpha2", %f)  ]))
				Mtot = %f
				A,B,C,D,E,FF=network.get_network_matrix(nb=nb,nf=nf,nw=nw,fname="%s")
				F,g,lb,ub = transmat.CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p, M=Mtot)
					
				r=MetabolicEP.metabolicEP(mysparse(F),g[:,1],lb,ub)
			"""%(nb,nf,nw,r,delta,k,alpha0,alpha1,alpha2,M, self.path_network_file))
			mu=self.julia.eval(u'r.μ')
			s=self.julia.eval(u'r.σ')
			ub_long = self.julia.eval(u'ub')								
			# save
			self.df.loc[(r,alpha0,alpha1,alpha2,delta,k,M,niter)].mu = mu
			self.df.loc[(r,alpha0,alpha1,alpha2,delta,k,M,niter)].sigma = s
			self.df.loc[(r,alpha0,alpha1,alpha2,delta,k,M,niter)].ub = self._get_short_ub(ub_long)
			# print smthg
			#print (r,alpha0,alpha1,alpha2,delta,k,M,niter)," mu=",self.df.loc[(r,alpha0,alpha1,alpha2,delta,k,M,niter)].mu
	def save(self,groupname):		
		# 
		self.df.to_hdf(self.path_fname_out,groupname)
		# other: p ?
	def load(self,fname,groupname):
		self.store = pd.HDFStore(fname)
		self.df = self.store[groupname]
		# other: p ?
	def plot_holomap(self,fname_out):	
		""" 
		interactive plot of the pdf of the different stocks and flows.
		simulation parameters can be selected with sliders.
		
		From MetabolicEP.ipynb: 
		d=Normal(μ,√s);\n",
        D=Truncated(d,l,u);\n",
        #choose bounds to have a sensible plotting range\n",
        m=max(pdf(d,l),pdf(d,u),l<μ<u?1/sqrt(2pi*s):0);\n",
        v=sqrt(-2*s*log(sqrt(2pi*s)*1e-5m));\n",
        l1,l2=max(l,μ-v),min(u,μ+v);\n",
        Gadfly.plot(x->pdf(D,x),l1,l2,..."""
		try:
			import holoviews as hv
			hv.extension('bokeh')				
		except:
			 print 'holoviews not available'
		renderer=hv.renderer('bokeh')	
		ndots = 100	 
        # iteration is fixed (also alpha0,alpha1,alpha2,delta)
		p=self.p
		alpha0,alpha1,alpha2,delta=p['alpha0'][0],p['alpha1'][0],p['alpha2'][0],p['delta'][0]
		def overlay_pdf(r,k,M,it,l):
			# get (mu,sig) that correspond to p1,p2,p3,i			
			mu,sig,ub=self.df.loc[(r,alpha0,alpha1,alpha2,delta,k,M,it)]
			# get indexes in label_long that correspond to l
			idx=np.where(np.array(self.label_long)==l)[0]
			# compute bounds
			lb=0 ; ub_l = ub[ self.label_short.index(l)]			
			# compute pdf Curves							
			pdfs={}
			for ii in idx:
				# copied from MetabolicEP.ipynb
				#x = l<mu[ii]<u?1/np.sqrt(2*np.pi*sig[ii]):0
				x=0
				if(lb<mu[ii] and mu[ii]<ub_l): x= 1./np.sqrt(2.*np.pi*sig[ii])				
				m=max( norm.pdf(lb,scale=np.sqrt(sig[ii])),
						norm.pdf(ub_l,scale=np.sqrt(sig[ii])),x);
				v=np.sqrt(-2.*sig[ii]*np.log(np.sqrt(2*np.pi*sig[ii])*1e-5*mu[ii]));
				l1,l2=max(lb,mu[ii]-v),min(ub_l,mu[ii]+v)				
				xvals = np.linspace(l1,l2,ndots)#np.linspace(0,M,ndots)
				# scipy.truncnorm uses a conversion formula: a, b = (myclip_a - my_mean) / my_std, (myclip_b - my_mean) / my_std
				a, b = (lb - mu[ii]) / sig[ii], (ub_l - mu[ii]) /sig[ii]
				pdfs[ii]=  hv.Curve((xvals,truncnorm.pdf(xvals,a,b,loc=mu[ii],scale=np.sqrt(sig[ii]) )))	
			#pdfs = { ii :   hv.Curve((xvals,truncnorm.pdf(xvals,l1,l2,loc=mu[ii],scale=np.sqrt(sig[ii]) )))	for ii in idx}				
			return 	hv.NdOverlay(pdfs, kdims='nagent')	
		#test
		r,k,M,it,l = p['r'][0],p['k'][0],p['M'][0],0,self.label_short[0]
		print overlay_pdf(r,k,M,it,l)
		#
		dic={(r,k,M,it,l):overlay_pdf(r,k,M,it,l) 
			for r in p['r'] for k in p['k'] for M in p['M'] for it in range(p['niter']) 
			for l in self.label_short}							
		hv.output(size=200)
		hmap = hv.HoloMap(dic, kdims=['r', 'k','M','it','label']) 		
		# save to static html
		#renderer.save(hmap, './hmap', fmt='html')
		s=renderer.static_html(hmap)
		f= open(fname_out, 'w'); f.write(s) ; f.close()
			
		# histogram over iterations	
	def plot_moments_vs_topology(self,mu,s,A,B,C,D,E,FF):
		"""
		"""
		# extract from network.py:
		# * [H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
		# * [H18?] exponential graph model: bipartite undirected
		#    get houshold consumption demand
		import matplotlib.pyplot as plt
		l = "Cd"
		idx=np.where(np.array(self.label_long)==l)[0]
		mu_Cd = mu[idx]
		s_Cd = s[idx]
		#    get degree of households
		d_hh = A.sum(axis=0)
		#    plot
		plt.subplot(1,2,1)
		plt.scatter(d_hh,mu_Cd)
		plt.xlabel("Degree of households")
		plt.ylabel("Mean Cd flow of households")
		plt.subplot(1,2,2)
		plt.scatter(d_hh,np.log(s_Cd))
		plt.xlabel("Degree of households")
		plt.ylabel("log Std Cd of households")
		plt.show()
		#    get firm consumption supply
		l = "Cs"
		idx=np.where(np.array(self.label_long)==l)[0]
		mu_Cs = mu[idx]
		s_Cs = s[idx]
		#    get degree of households
		d_f = A.sum(axis=1)
		#    plot
		plt.subplot(1,2,1)
		plt.scatter(d_f,mu_Cs)
		plt.xlabel("Degree of firms")
		plt.ylabel("Mean Cs flow of firms")
		plt.subplot(1,2,2)
		plt.scatter(d_f,np.log(s_Cs))
		plt.xlabel("Degree of firms")
		plt.ylabel("log Std Cs of firms")
		plt.show()
if __name__ == "__main__":
	#testDummy()
	#test_EP_network__get_short_ub()
	#test_EP_network_init()
	#test_EP_network_run_batch()
	#test_EP_network_save()
	#test_EP_network_load()
	#test_EP_network_plot_holomap()
	#test_EP_network_large()
	#test_EP_network_run()
	#test_EP_network_single_block()
	test_EP_network_run_block_network()
	#test_EP_network()
