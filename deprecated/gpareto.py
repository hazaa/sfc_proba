# -*- coding: utf-8
#  gpareto.py
#  
#  Copyright 2017 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
TODO:
* ajouter extrapolation bas revenus
* normalisation Fy ??

"""


"""
(piecewise 80% lognormal / 20% pareto 
fit from tabulated:
* lognormal: 
* pareto part: "old" method by piketty
)

compute p->Q(p) following Fournier
interpolate F using {(p,Q(p))}, with some polynomial basis (easy to port to cython)
compute f (analytically)

idem, sans passer par Q

test:
0 comparer frequencies microdata/ simulation: p.116 
0- pc: comparer empirique et interpolé
1- histogram from tabulations; compare to interpolated pdf
2- sample from pdf; get tabulations; get pc ; get pdf again and compare
   the same with cython

[Fournier 2015] "Generalized Pareto curves: Theory and application using income and
				inheritance tabulations for France 1901-2012", Master's thesis, 
				Paris School of Economics.
				
[FP 93] Feenberg, D. R. and Poterba, J. M. (1993). "Income Inequality and the Incomes of Very High-
				Income Taxpayers: Evidence from Tax Returns". In Tax Policy and the Economy, volume 7,
				pages 145–177. MIT Press.				
				
"""

"""
matlab stat tool box: ksdensity and paretotail https://fr.mathworks.com/help/stats/examples/nonparametric-estimates-of-cumulative-distribution-functions-and-their-inverses.html?requestedDomain=www.mathworks.com
	both need data (for the kernel; for the tails)
matlab histogram/curve fitting:  https://www.mathworks.com/examples/curvefitting/mw/curvefit-ex84637478-smoothing-a-histogram
	 	not a good idea: https://www.mathworks.com/examples/statistics/mw/stats_featured-ex82025955-curve-fitting-and-distribution-fitting?s_tid=examples_p2_MLT
python + linear interpolation: http://python.developermemo.com/2891_13493695/
convergence of ecdf: https://en.wikipedia.org/wiki/Empirical_distribution_function
https://rezahajargasht.wordpress.com/2015/10/15/income-density-estimation/
R semiparametric https://cran.r-project.org/web/packages/spd/
mc with cdf: https://www.researchgate.net/post/Is_there_a_Markov_chain_Monte_Carlo_sampling_method_that_uses_CDFs_instead_of_PDFs
		http://stats.stackexchange.com/questions/214215/markov-chain-monte-carlo-sampling-using-cdfs-instead-of-pdfs
		https://stats.stackexchange.com/questions/154946/what-is-the-equivalent-for-cdfs-of-mcmc-for-pdfs
ecdf-> hist (equal bins): https://fr.mathworks.com/help/stats/ecdfhist.html
"""

from scipy.interpolate import PchipInterpolator, interp1d
from scipy.integrate import quad,simps
from scipy.stats import gaussian_kde, pareto, genpareto
import numpy as np
import matplotlib.pyplot as plt
import operator
from scipy import interp
import pandas as pd

from kde import UnivariateKernelDensity

import sys
#sys.path.append('../');sys.path.append('../experimental')
sys.path.append('experimental')
try:
	import har as cython_har
except ImportError:
	print 'har is missing, running pure python'



def income_tabulation():
	"""
	see [Fournier 15] Table C.1
	definitions: p.14
	"""
	thr = [9400, 11250 , 13150, 15000,16900,18750,23750,28750,38750,48750,97500]
	perc = [0.268757, 0.335887, 0.407262, .482853, .548446, .602345, .702708, .780623, .877692, .927604, .98616]
	b = [3.01579, 2.68161 , 2.45805, 2.33289 , 2.2345, 2.15837, 1.97903 , 1.8927, 1.83887, 1.85842, 2.08967]
	yav = 21930.41
	total_income = 781464814714 # see [Fournier 15] p.53
	nb_household = 35633851
	return thr,perc, b, yav, total_income,nb_household

def fit_widget_pareto():
	"""
	plot a pareto cdf, adjust params with sliders
	
	http://matplotlib.org/api/widgets_api.html#matplotlib.widgets.Slider
	
	thanks to steeve limal
	"""	
	from matplotlib.widgets import Slider
	p = { 'b' : 2., 'k' : 1.}		
	#
	fig = plt.figure('estimation',figsize=(16,8))
	#curseurs communs
	sld_b= fig.add_axes([0.2, 0.2, 0.7, 0.03], label='param b', axisbg='grey')
	b = Slider(sld_b, 'F(x)= 1- (k/x)^b , param b', 0., 3.0, valinit=p['b'])
	
	sld_k=fig.add_axes([0.2, 0.1, 0.7, 0.03], label='Scale k', axisbg='grey')
	k = Slider(sld_k, "Coefficient d'echelle k", 0.1, 5., valinit=p['k'])
	# diag
	plt.subplots_adjust(0.2,0.3)
	DiagTemp=plt.subplot(121,title='pdf')
	DiagTemp.set_xlabel('x')
	DiagTemp.set_ylabel ('p(x)')
	rv = pareto(p['b'],scale=p['k'])
	xdatainit=np.linspace(0,10,100); ydatainit = rv.cdf(xdatainit)
	TraceTemp,=plt.plot(xdatainit,ydatainit) 
    #pl.semilogx(
    # erreur
	DiagErrTemp=plt.subplot(122,title='Ecart relatif')
	DiagErrTemp.set_xlabel('temps')
	DiagErrTemp.set_ylabel ('Ecart relatif (%)')
	TraceErrTemp,=plt.plot(xdatainit,ydatainit)
	
	def MajAffichage(val=1):
		"""Mise à jour des contenus des différents tracés
		en fonction des valeurs des différents curseurs
		en fonction des argument de départ"""        
        #Màj tracé théorique en temporel
		#print b.val,k.val
		rv = pareto( b.val, scale=k.val)
		TraceTemp.set_ydata( rv.cdf(xdatainit) )
		DiagTemp.relim()
		DiagTemp.autoscale_view(True,True,True)
		fig.canvas.draw_idle()
        
	#MajAffichage()
	b.on_changed(MajAffichage)
	k.on_changed(MajAffichage)
	plt.show()

def fit_widget_MtoWBs(data=None):
	"""
	plot the cdf of:
		M_i fitted to WID data
		WBs_i from the cdf of M, using the bmw model
		WBs_i fitted to WID data
	"""
	from matplotlib.widgets import Slider,Button
	from scipy.stats import lognorm,gamma
	from scipy.optimize import basinhopping
	
	nw = 100
	
	p_wea = {'distrib':'gamma', 'b' : .46, 'k' : 380000, 'blim': [0.25,5.], 'klim': [50000, 650000] }		
	p_inc = {'distrib':'lognormal', 'b' : .81, 'k' : 15542, 'blim': [0.,5.], 'klim': [0.1, 35000] }		
	p_model = {'r0': 0.03, 'rlim':[0,0.1] ,'alpha0': 0.001, 'alpha0lim':[0,0.1],
				'alpha1': 0.75, 'alpha1lim':[0.5,1], 'alpha2': 0.02, 'alpha2lim':[0,0.03],
				'delta': 0.1, 'k': 5.5,'Mtot':0, 'nw':nw, 'rho':0}
	def get_Mtot(delta,k,al0,al1,al2):			
		Mtot = k*al0 / (-k*al2 + (1-al1)*(1-delta*k))	
		if Mtot<=0: raise ValueError('Mtot<=0')						  
		return 	Mtot	
	p_model['Mtot']=get_Mtot(p_model['delta'],p_model['k'],p_model['alpha0'],
							  p_model['alpha1'],p_model['alpha2'])	
	p_model['rho'] = 	p_model['Mtot'] / float(p_model['nw'])	
	print p_model['Mtot'],p_model['rho'] 
	p = {'wealth':p_wea, 'income':p_inc, 'model':p_model}	
	
	fig = plt.figure('notitle' ,figsize=(16,8))
	#curseurs communs  add_axes[left, bottom, width, height]
	ax_r=      fig.add_axes([0.2, 0.2, 0.7, 0.03], label='r', axisbg='grey')
	sld_r =    Slider(ax_r, 'r', p['model']['rlim'][0], p['model']['rlim'][1], valinit=p['model']['r0'])	
	ax_alpha0= fig.add_axes([0.2, 0.15, 0.7, 0.03], label='alpha0', axisbg='grey')
	sld_alpha0 = Slider(ax_alpha0, 'alpha0', p['model']['alpha0lim'][0], p['model']['alpha0lim'][1], valinit=p['model']['alpha0'])
	ax_alpha1= fig.add_axes([0.2, 0.1, 0.7, 0.03], label='alpha1', axisbg='grey')
	sld_alpha1 = Slider(ax_alpha1, 'alpha1', p['model']['alpha1lim'][0], p['model']['alpha1lim'][1], valinit=p['model']['alpha1'])
	ax_alpha2= fig.add_axes([0.2, 0.05, 0.7, 0.03], label='alpha2', axisbg='grey')
	sld_alpha2 = Slider(ax_alpha2, 'alpha2', p['model']['alpha2lim'][0], p['model']['alpha2lim'][1], valinit=p['model']['alpha2'])
	ax_button = plt.axes([0.1, 0.9, 0.03, 0.06])
	bnext = Button(ax_button, 'SaveFig')
	ax_optim = plt.axes([0.1, 0.8, 0.03, 0.06])
	butt_optim = Button(ax_optim, 'Optimize')
	
	def WBStoM(w,r,alpha0,alpha1,alpha2):
		if (r*(1.-alpha1) > alpha2 ): raise ValueError('r*(1.-alpha1) > alpha2 ')
		m = ((1.-alpha1)*w -alpha0)/( -r*(1.-alpha1) +alpha2 )
		return m
	def MtoWBS(m,r,alpha0,alpha1,alpha2):
		if (r*(1.-alpha1) > alpha2 ): raise ValueError('r*(1.-alpha1) > alpha2 ')
		w = (alpha0 - m * (r*(1.-alpha1) -alpha2 ))/ (1-alpha1)
		return w		
	# wealth fit
	shape = p['wealth']['b']
	scale = p['model']['rho']/shape  # so that E(x) = Mtot/nw
	rv_wea = gamma(shape,scale=scale)
	cdf_wea_fit = rv_wea.cdf
	# income fit
	inc_mean = MtoWBS(p['model']['Mtot']/float(nw) , p['model']['r0'],p['model']['alpha0'],p['model']['alpha1'],p['model']['alpha2'])	
	sigma= p['income']['b']; scale = np.exp(-0.5*sigma**2)* inc_mean ;
	rv_inc = lognorm(sigma, scale = scale)
	cdf_inc_fit = rv_inc.cdf	
	# income computed
	cdf_inc_comput = lambda w: cdf_wea_fit( WBStoM(w,p['model']['r0'],p['model']['alpha0'],p['model']['alpha1'],p['model']['alpha2']))

	plt.subplots_adjust(0.2,0.3)
	DiagTemp=plt.subplot(121,title='wealth')	
	DiagTemp.set_xlabel('x')
	DiagTemp.set_ylabel ('cdf(x)')	
	xdata=np.linspace(0,20 * p['model']['rho'], 1000)	
	f_wea,=plt.plot(xdata, cdf_wea_fit(xdata),'k')
	
	DiagErrTemp=plt.subplot(122,title='income')	
	DiagErrTemp.set_xlabel('x')
	DiagErrTemp.set_ylabel ('cdf(x)')
	xdata=np.linspace(0,10 * rv_inc.mean(), 1000)	
	f_inc_fit,f_inc_comput=plt.plot(xdata, cdf_inc_fit(xdata),'k--' , 
									xdata, cdf_inc_comput(xdata),'k')
	def MajAffichage(val=1):
		r= sld_r.val ; alpha0 = sld_alpha0.val
		alpha1 = sld_alpha1.val ; alpha2 = sld_alpha2.val
		#
		Mtot = get_Mtot(p['model']['delta'],p['model']['k'],alpha0,alpha1,alpha2)
		#fig.text(0.1,0.7, "Mtot=%1.2f"%(Mtot))
		print "alpha0=%1.6f Mtot=%1.2f"%(alpha0,Mtot)
		inc_mean = MtoWBS(Mtot/float(nw) , r, alpha0, alpha1 , alpha2)
		sigma= p['income']['b']; scale = np.exp(-0.5*sigma**2)* inc_mean ; rv_inc = lognorm(sigma, scale = scale)
		cdf_inc_fit = rv_inc.cdf	
		f_inc_fit.set_ydata(cdf_inc_fit(xdata) )		
		#
		cdf_inc_comput = lambda w: cdf_wea_fit( WBStoM(w,r,alpha0,alpha1,alpha2))
		f_inc_comput.set_ydata( cdf_inc_comput(xdata ) )				
		DiagErrTemp.relim()
	def SaveFig(event):		
		"""
		"""
		plt.rc('font',size=18)
		r= sld_r.val ; alpha0 = sld_alpha0.val
		alpha1 = sld_alpha1.val ; alpha2 = sld_alpha2.val
		Mtot = get_Mtot(p['model']['delta'],p['model']['k'],alpha0,alpha1,alpha2)
		inc_mean = MtoWBS(Mtot/float(nw) , r, alpha0, alpha1 , alpha2)
		sigma= p['income']['b']; scale = np.exp(-0.5*sigma**2)* inc_mean ; rv_inc = lognorm(sigma, scale = scale)
		cdf_inc_fit = rv_inc.cdf
		cdf_inc_comput = lambda w: cdf_wea_fit( WBStoM(w,r,alpha0,alpha1,alpha2))		
		fig2 = plt.figure()
		ax = plt.gca()
		plt.xlabel('income')
		plt.ylabel ('cdf(income)')							
		plt.plot(xdata,cdf_inc_fit(xdata),'k--', label='direct fit')
		plt.plot(xdata,cdf_inc_comput(xdata ) 	,'k',label='bmw model fit') 		
		ax.legend()#'lower right'
		fig2.savefig("fig/cdf_MtoWBs.pdf", bbox_inches='tight')		
		plt.close(fig2)
	def Optimize(event):								
		"""
		https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.basinhopping.html#scipy.optimize.basinhopping		
		"""		
		r= sld_r.val
		def func(x):						
			alpha0=x[0];alpha1=x[1];alpha2=x[2]
			#
			vec = np.array([cdf_wea_fit( WBStoM(w,r,alpha0,alpha1,alpha2)) for w in xdata  ])						
			#
			Mtot = get_Mtot(p['model']['delta'],p['model']['k'],alpha0,alpha1,alpha2)
			constr = 0
			#fig.text(0.1,0.7, "Mtot=%1.2f"%(Mtot))
			#print "Mtot=%1.2f"%(Mtot)
			inc_mean = MtoWBS(Mtot/float(nw) , r,alpha0,alpha1,alpha2 )
			sigma= p['income']['b']; scale = np.exp(-0.5*sigma**2)* inc_mean ; rv_inc = lognorm(sigma, scale = scale)
			cdf_inc_fit = rv_inc.cdf	
			f_inc_fit.set_ydata(cdf_inc_fit(xdata) )
			#print cdf_inc_fit(xdata), vec
			return np.linalg.norm(cdf_inc_fit(xdata)- vec) + constr		
		x0=[p['model']['alpha0'],p['model']['alpha1'],p['model']['alpha2']]
		minimizer_kwargs = {"method": "BFGS"}
		ret = basinhopping(func, x0, minimizer_kwargs=minimizer_kwargs, niter=200)
		print ret.x
								
	sld_r.on_changed(MajAffichage)
	sld_alpha0.on_changed(MajAffichage)	
	sld_alpha1.on_changed(MajAffichage)	
	sld_alpha2.on_changed(MajAffichage)	
	bnext.on_clicked(SaveFig)
	butt_optim.on_clicked(Optimize)
	plt.show()								
	plt.show()

def fit_widget_chemical_potential(pdf=None,data=None):
	"""
	manually look for a solution mu to the equation :
		rho = \int x f(x) exp(mu x) / \int f(x) exp(mu x)
	
	rho and mu can be adjusted with two sliders	
	"""
	from matplotlib.widgets import Slider,Button
	from scipy.stats import lognorm,gamma
	from scipy.integrate import quad
	p = { 'mu0':0., 'mulim': [-1.,1.], 'rho0':1, 'rholim': [0.1,1000.],'x0quad':0}		

	
	fig = plt.figure('notitle' ,figsize=(16,8))
	#curseurs communs  add_axes[left, bottom, width, height]
	ax_mu= fig.add_axes([0.2, 0.2, 0.7, 0.03], label='mu', axisbg='grey')
	sld_mu = Slider(ax_mu, 'mu', p['mulim'][0], p['mulim'][1], valinit=p['mu0'])
	ax_rho= fig.add_axes([0.2, 0.1, 0.7, 0.03], label='rho', axisbg='grey')
	sld_rho = Slider(ax_rho, 'rho', p['rholim'][0], p['rholim'][1], valinit=p['rho0'])
	
	plt.subplots_adjust(0.2,0.3)
	DiagTemp=plt.subplot(121,title='pdf')	
	DiagTemp.set_xlabel('x')
	DiagTemp.set_ylabel ('pdf(x)')
	if pdf==None:
		sigma= 1.72; scale = np.exp(-0.5*sigma**2)* p['rho0']; rv = lognorm(sigma, scale = scale)
		#rv = pareto(p['shape'],scale=p['scale'])
		#rv = gamma(p['b'], scale = p['k'])
		pdf = rv.pdf
	else:
		pass
	def pdfexp0(x,mu):
		return pdf(x)*np.exp(-mu * x) 	

	xdata=np.linspace(0,10 * p['rho0'], 1000)
	ydata = pdf(xdata)
	f,=plt.plot(xdata,ydata,'-')
	fexp,=plt.plot(xdata, pdfexp0(xdata,p['mu0']) ,'-')
	
	def MajAffichage(val=1):
		"""Mise à jour des contenus des différents tracés
		en fonction des valeurs des différents curseurs"""               
		x0 = p['x0quad']		
		xmaxquad = 100 * sld_rho.val	
		#if pdf==None:
		sigma= 1.72; scale = np.exp(-0.5*sigma**2)* sld_rho.val; rv = lognorm(sigma, scale = scale)
			#rv = pareto(p['shape'],scale=p['scale'])
			#rv = gamma(p['b'], scale = p['k'])
		pdf = rv.pdf
		#else:
		#	pass		
		def pdfexp(x):
			return pdf(x)*np.exp(-sld_mu.val * x)
		def xpdfexp(x):
			return x*pdf(x)*np.exp(-sld_mu.val * x) 		
		f.set_ydata( pdf(xdata ) )	
		fexp.set_ydata( pdfexp(xdata ) )
		ratio = quad(xpdfexp,x0,xmaxquad)[0]/quad(pdfexp,x0,xmaxquad)[0]
		print sld_mu.val,sld_rho.val,ratio
		# texte
		
	sld_mu.on_changed(MajAffichage)
	sld_rho.on_changed(MajAffichage)	
	plt.show()

def fit_widget_pareto_WID():
	"""
	plot a pareto cdf wrt to emmpirical cdf, adjust params with sliders
		
	add_axes: http://matplotlib.org/api/figure_api.html?highlight=add_axes#matplotlib.figure.Figure.add_axes	
	slider: http://matplotlib.org/api/widgets_api.html#matplotlib.widgets.Slider
	button: http://matplotlib.org/api/widgets_api.html#matplotlib.widgets.Button
			http://matplotlib.org/examples/widgets/buttons.html
	
	thanks to steeve limal
	"""	
	
	from matplotlib.widgets import Slider,Button
	from scipy.stats import lognorm,gamma
	p = {'data':'wealth','distrib':'lognormal', 'b' : 1.72, 'k' : 46400., 'blim': [0.,5.], 'klim': [0.1, 55000] }		
	#p = {'data':'income','distrib':'lognormal', 'b' : .81, 'k' : 15542, 'blim': [0.,5.], 'klim': [0.1, 35000] }		
	#p = {'data':'income','distrib':'gamma', 'b' : 1.46, 'k' : 15542, 'blim': [0.,5.], 'klim': [0.1, 35000] }		
	#p = {'data':'wealth','distrib':'gamma', 'b' : .46, 'k' : 380000, 'blim': [0.25,5.], 'klim': [50000, 650000] }		
	#p = {'type':'pareto', 'b' : 2., 'k' : 1., 'blim': [0.,5.], 'klim': [0.1, 2000] }		
	
	# get WID functions
	year=2010
	if p['data']=='wealth':
		pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')	
		thr = thr_wea
	elif p['data']=='income':
		thr_inc,percentile_num = get_WID_FR(year,value='income')	
		thr = thr_inc
		
	fig = plt.figure(p['data']+" "+p['distrib'] ,figsize=(16,8))
	#curseurs communs  add_axes[left, bottom, width, height]
	sld_b= fig.add_axes([0.2, 0.2, 0.7, 0.03], label='param b', axisbg='grey')
	b = Slider(sld_b, 'shape', p['blim'][0], p['blim'][1], valinit=p['b'])
	
	sld_k=fig.add_axes([0.2, 0.1, 0.7, 0.03], label='Scale k', axisbg='grey')
	k = Slider(sld_k, "scale", p['klim'][0], p['klim'][1], valinit=p['k'])
	
	ax_button = plt.axes([0.1, 0.9, 0.03, 0.06])
	bnext = Button(ax_button, 'SaveFig')
	
	# diag
	plt.subplots_adjust(0.2,0.3)
	DiagTemp=plt.subplot(121,title='cdf')
	DiagTemp.set_xlabel('log10(x)')
	DiagTemp.set_ylabel ('cdf(x)')
	if p['distrib']=='pareto':
		rv = pareto(p['b'],scale=p['k'])
	elif p['distrib']=='lognormal':
		rv = lognorm(p['b'], scale = p['k'])
	elif p['distrib']=='gamma':	
		rv = gamma(p['b'], scale = p['k'])
	#xdatainit=np.linspace(0,10,100); ydatainit = rv.cdf(xdatainit)
	xdatainit=np.log10(thr[1:]); #ydatainit = rv.cdf(10**xdatainit)
	xdata = np.linspace( np.log10(min(thr[1:])), np.log10( max(thr[1:])),1000); ydata = rv.cdf(10**xdata)
	#TraceTemp,T=plt.plot(xdatainit,ydatainit, xdatainit,percentile_num[1:],'o') 
	TraceTemp,T=plt.plot(xdata,ydata, xdatainit,percentile_num[1:],'o') 
	
	plt.ylim([0,1.2])
    #pl.semilogx(
    # erreur
	DiagErrTemp=plt.subplot(122,title='pdf')
	DiagErrTemp.set_xlabel('log10(x)')
	DiagErrTemp.set_ylabel ('pdf(x)')
	#TraceErrTemp,=plt.plot(xdatainit,ydatainit)
	TraceErrTemp,=plt.plot(xdata,rv.pdf(10**xdata))
	
	def MajAffichage(val=1):
		"""Mise à jour des contenus des différents tracés
		en fonction des valeurs des différents curseurs
		en fonction des argument de départ"""        
        #Màj tracé théorique en temporel
		print b.val,k.val
		if p['distrib']=='pareto':			
			rv = pareto( b.val, scale=k.val)
		elif p['distrib']=='lognormal':
			rv = lognorm( b.val,scale = k.val)
		elif p['distrib']=='gamma':
			rv =gamma( b.val,scale = k.val)	
		
		#TraceTemp.set_ydata( rv.cdf(10**xdatainit) )
		#TraceErrTemp.set_ydata( rv.pdf(10**xdatainit) )
		TraceTemp.set_ydata( rv.cdf(10**xdata) )
		TraceErrTemp.set_ydata( rv.pdf(10**xdata) )
		DiagErrTemp.relim()
		DiagErrTemp.autoscale_view(True,True,True)
		fig.canvas.draw_idle()
		
	def SaveFig(event):		
		"""
		"""
		fs = 18
		fig2 = plt.figure()
		plt.xlabel('log10(x)', fontsize=fs)
		plt.ylabel ('cdf(x)', fontsize=fs)
		plt.plot(xdata,ydata,'k', xdatainit,percentile_num[1:],'k+') 
		ax = plt.gca()
		for label in ax.xaxis.get_ticklabels(): label.set_fontsize(fs)
		for label in ax.yaxis.get_ticklabels(): label.set_fontsize(fs)
		fig2.savefig("fig/cdf_lognorm.pdf", bbox_inches='tight')
		plt.close(fig2)
		fig3 = plt.figure()
		plt.xlabel('log10(x)', fontsize=fs)
		plt.ylabel ('pdf(x)', fontsize=fs)
		plt.plot(xdata,rv.pdf(10**xdata),'k')
		ax = plt.gca()
		for label in ax.xaxis.get_ticklabels(): label.set_fontsize(fs)
		for label in ax.yaxis.get_ticklabels(): label.set_fontsize(fs)
		fig3.savefig("fig/pdf_lognorm.pdf", bbox_inches='tight')
		plt.close(fig3)
        
	#MajAffichage()
	b.on_changed(MajAffichage)
	k.on_changed(MajAffichage)
	bnext.on_clicked(SaveFig)
	plt.show()

def get_WID_FR(year='2012',value='wealth',interp='pchip'):
	"""
	WID data, France.
	TODO: replace this by database
	http://wid.world/data/
	
	readme.txt: "g–percentile thresholds are the minimum income (or wealth) 
				levels required to belong to the group above a given g-percentile
				value. For instance, the fiscal income threshold at g-percentile p90
				is the minimum annual fiscal income required to belong to the top 10% group."	
	"""
	# read file
	fname = "/home/aurelien/local/git/sfc_proba/data/WID_full_dataset/WID_fulldataset_FR/Data/WID_FR_InequalityData.csv"
	fname_macro = "/home/aurelien/local/git/sfc_proba/data/WID_full_dataset/WID_fulldataset_FR/Data/WID_FR_MacroData.csv"
	df = pd.read_csv(fname, sep=";",skiprows=7)
	df_macro = pd.read_csv(fname_macro, sep=";",skiprows=7)
	# check args
	if isinstance(year,int ): year = str(year)		 
	# params
	nperc=127
	if(value=='wealth'):
		
		if(year=='2012'):			
			istart=144 ; 	i_macro=3
		elif(year=='2011'):	
			raise NotImplementedError
			istart=0	; 	i_macro=4	
		elif(year=='2010'):	
			istart=428	; 	i_macro=5
		else:
			raise ValueError	
		percentile_str = df.perc[istart:istart+nperc] 		
		percentile_num =0.01* np.array([ float(s[1:])  for s in percentile_str.tolist()] ,
					dtype=float)
		# todo: check percentile
		shweal = df.shweal992j[istart:istart+nperc] # AP9: Net personal wealth | equal-split adults | Share | Adults | share of total (ratio)
		thweal = df.thweal992j[istart:istart+nperc]  # AU1: Net personal wealth | equal-split adults | Threshold | Adults | constant 2015 local currency		
		year= df.year[istart:istart+nperc]
		y = year.values[0]	
		# mean wealth
		# get mean wealth to rescale, from MacroData : (total wealth: mhweal999i, total pop)	
		mean_wea = df_macro.apweal992i[i_macro]
		# interpolators. pchip is used (because monotonic)
		# (spline can be used to smooth, but not monotonic https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.make_lsq_spline.html#scipy.interpolate.make_lsq_spline
		if(interp=='linear'):
			raise NotImplementedError
		if(interp=='pchip'):
			F_wea = PchipInterpolator(thweal, percentile_num, axis=0, extrapolate=False)
			pdf_wea = F_wea.derivative()	
			Finv_wea = PchipInterpolator(percentile_num,thweal, axis=0, extrapolate=False)
			share_wea = PchipInterpolator(percentile_num,shweal, axis=0, extrapolate=False)
		elif(interp=='MonoPoly')	:
			raise NotImplementedError
			# TODO: monotonic smoothing 
			# fda: http://www.ugrad.stat.ubc.ca/R/library/fda/html/smooth.monotone.html
			# https://octave.sourceforge.io/statistics/function/monotone_smooth.html
		elif(interp=='raw')	:	
			return thweal, percentile_num,mean_wea 
		else:
			raise ValueError			
		thr_wea = thweal
		return pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num
	elif(value=='income'):
		if(year=='2010'):	
			istart=428	;
		percentile_str = df.perc[istart:istart+nperc] 		
		percentile_num =0.01* np.array([ float(s[1:])  for s in percentile_str.tolist()] ,
					dtype=float)	
		tfiinc = df.tfiinc992j[istart:istart+nperc] # AS1: Fiscal income  | equal-split adults | Threshold | Adults | constant 2015 local currency	
		return tfiinc,percentile_num


def test_get_WID_FR():
	"""
	"""
	year = 2010
	pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')
	assert(True)

def test_histogram_WID_FR():
	year = 2010
	pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')	
	h = np.diff(percentile_num)	
	ymax = np.max(thr_wea)+10000
	width = np.diff(thr_wea) 
	h=h/width
	plt.bar(left=thr_wea[0:-1],height=h, width=width, alpha=0.5)
	plt.show()
	
def test_equal_mean_WID_FR():
	"""
	check that mean wealth from WID database coincides with 
	mean wealth computed from pdf by numerical integration
	"""
	year = 2010
	pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')
	# check E[X]_empir = E[X]_theor (or condensation may occur)
	x0 =0; x1 = np.max(thr_wea)
	denom = quad(pdf_wea,x0,x1)[0]
	def f(y):
		return y*pdf_wea(y)
	num = quad(f,x0,x1)[0]	
	mean_wea_quad = num/denom
	print "mean_wea=", mean_wea, " mean_wea_quad=",mean_wea_quad	

def test_equal_mean_rescaled_WID_FR():
	"""
	same as test_equal_mean_WID_FR(), with rescaled pdf	
	f_wid(x*s) avec s =  E_wealth / (Mtot/nw)
	"""
	# get some Mtot	
	year = 2010
	nw = 10
	Mtot = 10. # change this ?
	# get distribution functions
	pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')
	# rescale
	s =  mean_wea / (Mtot/float(nw))
	def pdf_wea_rescale(x):
		# nb: not necessarily normalized, because of MCMC algorithm
		return pdf_wea(x*s)		
	# compute mean value of rescaled pdf using quad, compare to Mtot/nw
	x0 =0; x1 = Mtot
	denom = quad(pdf_wea_rescale,x0,x1)[0]
	def f(y):
		return y*pdf_wea_rescale(y)
	num = quad(f,x0,x1)[0]	
	mean_wea_quad_rescale = num/denom
	print "quad(x*pdf_wea_rescale,x0,x1)=",num, " quad(pdf_wea_rescale,x0,x1)=", denom, 
	print "mean_wea_rescale=", Mtot/float(nw), " mean_wea_quad_rescale=",mean_wea_quad_rescale	
	# F: test that tends to 1 when x->Mtot
	def F_wea_rescale(x):
		return F_wea(x*s)	
	print "F_wea_rescale(Mtot)=",F_wea_rescale(Mtot)
	assert(F_wea_rescale(Mtot)-1<0.001) 
	# Finv:
	def Finv_wea_rescale(x):
		# NB: output may be > Mtot, but not important for our purpose 
		# (which is finding threshold p50, p90)
		return Finv_wea(x)/s		
	# plot
	plt.subplot(2,1,1)
	m = np.linspace(0,Mtot,1000)
	plt.plot(m, pdf_wea_rescale(m))
	plt.subplot(2,1,2)
	p = np.linspace(0,1,1000)
	plt.plot( p , Finv_wea_rescale(p) )
	plt.show()
	# TODO: Finv + rescale !!!!!!!!!!!!!!!!!!!!!!!!
	

def test_wealth_income_WID_FR():
	"""
	compute cdf, pdf for wealth and income, after http://wid.world/
	
	WID_FR_InequalityData.csv ; colonne AT ; lignes 11-137
	http://wid.world/data/
	varcode=tfiinc992t
	
	readme.txt:
		"For each g-percentiles, we provide shares, averages, top averages and thresholds.
		i) g-percentiles shares correspond to the income (or wealth) share captured by the population group above a given g-percentile value.
		...
		iv) g-percentile thresholds correspond to minimum income (or wealth) level  required to belong to the population group above a given g-percentile value.
		"
	"""	
	year = 2010
	pdf_wea,F_wea,Finv_wea,share_wea,thr_wea,mean_wea,percentile_num = get_WID_FR(year,value='wealth')
	# plot	
	nplot=4
	plt.subplot(nplot,1,1)
	wea = np.linspace(0,10000000,1000)
	plt.plot(thr_wea,  percentile_num, '+'); 
	plt.plot(wea, F_wea(wea),'b' )
	plt.subplot(nplot,1,2)
	pp = np.linspace(0,1.,100)
	plt.plot(pp, Finv_wea(pp) )
	plt.subplot(nplot,1,3)	
	wea = np.linspace(0,10000000,10000)
	plt.plot(wea, pdf_wea(wea) )
	plt.subplot(nplot,1,4)	
	h = np.diff(percentile_num)	
	ymax = np.max(thr_wea)+10000
	width = np.diff(thr_wea) #np.hstack( (np.diff(thr_wea), [ ymax ]) ) #here
	h=h/width
	plt.bar(left=thr_wea[0:-1],height=h, width=width, alpha=0.5)
	plt.show()
	# rescaled	

def histogram_from_tabulation(thr,perc,ymax):
	"""	
	see [Fournier 15] eq.(2.14)	
	
	Warning: not suitable for WID g-percentile
	"""
	h=list(perc) #deep copy
	h.insert(0,0)
	h = np.diff(h)	
	width = np.hstack( (np.diff(thr), [ ymax ]) ) #here
	h=h/width
	return h,width

def extrap(x, xp, yp):
	"""np.interp function with linear extrapolation
	http://stackoverflow.com/questions/2745329/how-to-make-scipy-interpolate-give-an-extrapolated-result-beyond-the-input-range#8166155
	"""
	y = np.interp(x, xp, yp)
	y[x < xp[0]] = yp[0] + (x[x<xp[0]]-xp[0]) * (yp[0]-yp[1]) / (xp[0]-xp[1])
	y[x > xp[-1]]= yp[-1] + (x[x>xp[-1]]-xp[-1])*(yp[-1]-yp[-2])/(xp[-1]-xp[-2])
	return y

def cdf_from_tabulation_linear(thr,perc):
	"""
	interpolation of the cdf function with linear extrapolation below thr[0] and paretto above thr[-1]	
	see [Fournier 15] p.14 for notations
	
	in:
	---
	thr: array, shape=(n,)
	threshold 
	
	perc: array, shape=(n,)
	fractiles
	
	out:
	---
	function
	
	"""
	a,k=fit_pareto_tail(thr,perc,i=-2)	
	def extrap_paretto(x, xp, yp,a,k):
		"""np.interp function with linear extrapolation below xp[0] and paretto above xp[-1]
		https://docs.scipy.org/doc/numpy/reference/generated/numpy.interp.html#numpy.interp
		"""
		y = np.interp(x, xp, yp)		# is an array, not a function
		y = np.where(x<xp[0], yp[0]+(x-xp[0])*(yp[0]-yp[1])/(xp[0]-xp[1]), y)
		y = np.where(x>xp[-1],1- (k/x)**a,y)		
		return y		
	F = lambda y: extrap_paretto(y, thr, perc,a,k)		
	return F

def icdf_from_tabulation_linear(thr,perc):
	"""
	interpolation of the cdf function with linear extrapolation below thr[0] and paretto above thr[-1]	
	see [Fournier 15] p.14 for notations
	"""
	a,k=fit_pareto_tail(thr,perc,i=-2)	
	def extrap_paretto(y, xp, yp,a,k):
		"""np.interp function with linear extrapolation below xp[0] and paretto above xp[-1]
		https://docs.scipy.org/doc/numpy/reference/generated/numpy.interp.html#numpy.interp
		"""
		x = np.interp(y, yp, xp)		# is an array, not a function
		x = np.where(y<yp[0], xp[0]+(y-yp[0])*(xp[0]-xp[1])/(yp[0]-yp[1]), x)
		x = np.where(y>yp[-1], k * (1- y)**(-1/a),x)		
		return x		
	F = lambda z: extrap_paretto(z, thr, perc,a,k)		
	return F
	
def test_cdf_from_tabulation_linear():		
	"""
	"""
	thr,perc, b, yav,total_income,nb_household=income_tabulation()
	F = cdf_from_tabulation_linear(thr,perc)
	xmax  = np.max(thr)+20000
	n=1000
	xtest = np.linspace(0,xmax+100,n)
	y = np.zeros(n)
	for i in range(xtest.shape[0]):
		y[i] = F(xtest[i])
	plt.plot(xtest,y)	
	plt.plot(thr,perc, 'o')	
	plt.show()

def test_icdf_from_tabulation_linear():		
	"""
	"""
	thr,perc, b, yav,total_income,nb_household=income_tabulation()
	Finv = icdf_from_tabulation_linear(thr,perc)	
	n=1000
	ptest = np.linspace(0,1,n)
	y = np.zeros(n)
	for i in range(ptest.shape[0]):
		y[i] = Finv(ptest[i])
	plt.plot(ptest,y)	
	plt.plot(perc, thr, 'o')	
	plt.show()	

def test_quad_xfx_from_data():
	"""
	"""	
	algo,bw="gaussian",'scott'
	#algo,bw="log_gaussian",'silverman'
	algo,bw="log_gaussian",'lscv'
	b = [1.05, 2. ,3.]
	perc = [0.,0.5,0.9,1.]
	for b_ in b:	
		# generate data
		p=pareto(b_)	
		m = p.mean()
		data = p.rvs(size=1000)						
		#x0 = np.percentile(x,p0*100)
		#x1 = np.percentile(x,p1*100)
		for j,p0 in enumerate(perc[0:-1]):
			p1 = perc[j+1]
			# quad
			q = quad_xfx_from_data(data,p0,p1,algo=algo,bw=bw)			
			share = q / m
			print "b=%f mean=%f (%f,%f) share=%f"%(b_,m,p0,p1,share)

def quad_xfx_from_data(x,p0,p1,algo="gaussian",bw='scott'):	
	"""
	integral of total revenue that goes to percentile p0 to p1
	\int_{F^{-1}(p0)}^{F^{-1}(p1)} xf(x)dx
	
	input:
	------
	x: ndarray
	income
	
	p0,p1: float in [0,1]
	percentile 
	
	algo: str
	estimator algorithm 'gaussian', 'log_gaussian'
	
	bw: str
	bandwith method. log_gaussian accepts "lscv", "silverman", 
	gaussian accepts 'scott', 
	
	see:
	----
	gaussian_kde: https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.gaussian_kde.html#scipy-stats-gaussian-kde
	lognormal transformation: https://halshs.archives-ouvertes.fr/halshs-01115988/;https://github.com/nkuhlen/log-transform-kernel-density/
	simps: https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.simps.html#scipy.integrate.simps
	"""
	if not (isinstance(p0, float) and isinstance(p1, float)): raise TypeError('p0 p1 must be float')
	if not x.ndim==1: raise ValueError('1 dim array expected') 
	if(p0<0 or p0>1 or p1<0 or p1>1): raise ValueError('p0 p1 must be in [0,1]')	
	# compute threshold coresponding to percentile p
	
	if (algo=="gaussian"):
		x0 = np.percentile(x,p0*100)
		x1 = np.percentile(x,p1*100)
		# kde density estimation from data		
		kde = gaussian_kde(x,bw_method=bw)
		# call share_of_revenue_from_pdf
		def f(y):
			return y*kde.evaluate(y)	
		return quad(f,x0,x1)[0]		
	elif (algo=="log_gaussian"):		
		x0 = np.percentile(x,p0*100)
		x1 = np.percentile(x,p1*100)
		kde = UnivariateKernelDensity(data=x, kernel=algo)
		# NB : bw='lscv' uses rpy2 => slower
		kde(bw) 
		# trim and integrate
		idx  = np.where((kde.support>=x0) & (kde.support<=x1))[0]
		return simps(np.array(kde.estimated_density)[idx],kde.support[idx])		
		
		
def share_of_revenue_from_data(x,p0,p1):		
	"""
	input:
	------
	x: ndarray
	income
	
	p0,p1: float in [0,1]
	percentile 
	"""
	s = x.sum()
	n = x.size
	x.sort()
	i0 = np.trunc(p0*n)			
	i1 = np.trunc(p1*n)		
	s_part = np.sum(x[i0:i1])	
	return s_part/s
	
	
def share_of_revenue_from_data_DEPREC(x,p):	
	"""
	share of the total revenue that goes to upper p percentile
	\int_{F^{-1}(p)}^{+\infty} xf(x)dx /   \int xf(x)dx
	
	input:
	------
	x: ndarray
	income
	
	p: float in [0,1]
	percentile 
	
	see:
	----
	https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.gaussian_kde.html#scipy-stats-gaussian-kde
	"""
	raise DeprecationWarning()
	
	if not isinstance(p, float): raise TypeError('p must be float')
	if not x.ndim==1: raise ValueError('1 dim array expected') 
	if(p<=0 or p>=1): raise ValueError('p must be in [0,1]')	
	# compute threshold coresponding to percentile p
	x0 = np.percentile(x,p*100)
	# kde density estimation from data
	kde = gaussian_kde(x)
	# estimate upperbound from dataset
	xmax = 10 * np.max(x)
	# call share_of_revenue_from_pdf
	return 	share_of_revenue_from_pdf(kde.evaluate,x0,xmax)

def share_of_revenue_pareto(xm,b,p0,p1):
	"""
	share \int xf(x)  between percentiles p0 and p1 
	for a pareto distribution with xm
	https://en.wikipedia.org/wiki/Pareto_distribution	
	"""
	if(b<=1): raise ValueError
	s =	( (1./(1.-p0))**( (1.-b)/b )  -(1./(1.-p1))**( (1.-b)/b ) )
	return s
	
def share_of_revenue_from_quad_pdf(func_marginal,  func_icdf):
	def f(x):
		return x*func_marginal(x)		
	x0= func_icdf( 0.0 )
	x1= func_icdf( 0.5 )	
	print "x0=",x0, "x1=",x1
	int_0_50 =  quad(f,x0,x1)[0]
	x0= func_icdf( 0.5 )
	x1= func_icdf( 0.9 )
	print "x0=",x0, "x1=",x1
	int_50_90 =  quad(f,x0,x1)[0]	
	## doesnt work
	x0= func_icdf( 0.9 )
	x1= func_icdf( 1. )
	print "x0=",x0, "x1=",x1
	int_90_100 =  quad(f,x0,x1)[0]	
	print "int_0_50=",int_0_50, " int_50_90=",int_50_90, " int_90_100=", int_90_100
	return int_0_50,int_50_90,int_90_100

def test_share_of_revenue_from_data():
	"""
	INTERPRETATION: OK, except when nsamp small (was tested when b, xm vary)
	"""
	nsamp = 1000
	b=3 ; xm = 1#lower bound of revenue
	p = pareto(b,scale=xm)
	x= p.rvs(nsamp)	
	p_l = [(0.0, 0.5), (0.5,0.9), (0.9,0.999)]
	for p0,p1 in p_l: 
		share_data =  share_of_revenue_from_data(x,p0,p1)
		share_theor= share_of_revenue_pareto(xm,b,p0,p1) 	
		print "b=%f p0=%f p1=%f s_data=%f s_theor=%f "%(b,p0,p1,share_data,share_theor)

def test_share_of_revenue_from_quad_pdf_pareto():	
	"""
	pdf = pareto
	
	INTERPRETATION: OK, except (was tested when b, xm vary)
	test that functions quadratures are ok
	"""
	b=5 ; xm = 10 #lower bound of revenue
	p = pareto(b,scale=xm)	
	func_marginal = p.pdf
	func_icdf= p.ppf
	int_0_50,int_50_90,int_90_100 = share_of_revenue_from_quad_pdf(func_marginal,  func_icdf)	
	s = int_0_50+int_50_90+int_90_100
	print "sha_0_50=",int_0_50/s, " share_theor=", share_of_revenue_pareto(xm,b,0,0.5)
	print "sha_50_90=",int_50_90/s, " share_theor=", share_of_revenue_pareto(xm,b,0.5,0.9)
	print " sha_90_100=", int_90_100/s, " share_theor=", share_of_revenue_pareto(xm,b,0.9,0.99) 	


def test_share_of_revenue_from_quad_pdf_piecewise_pareto():	
	"""
	pdf = piecewise constant / pareto tail 
	
	INTERPRETATION: ?
	"""	
	thr,perc, b, yav,total_income,nb_household=income_tabulation()
	Finv = icdf_from_tabulation_linear(thr,perc)
	pdf = get_piecewise_pareto()
	
	func_marginal = pdf
	func_icdf= Finv
	
	int_0_50,int_50_90,int_90_100 = share_of_revenue_from_quad_pdf(func_marginal,  func_icdf)	
	s = int_0_50+int_50_90+int_90_100
	b=2;xm=1
	print "sha_0_50=",int_0_50/s, " share_theor=", share_of_revenue_pareto(xm,b,0.,0.5)
	print "sha_50_90=",int_50_90/s, " share_theor=", share_of_revenue_pareto(xm,b,0.5,0.9)
	print " sha_90_100=", int_90_100/s, " share_theor=", share_of_revenue_pareto(xm,b,0.9,0.99) 	
	

def test_share_of_revenue_from_data_logtransform():
	"""
	data -> kde +log transform -> (quad + share)
	
	https://github.com/nkuhlen/log-transform-kernel-density/
	
	log-transform-kernel-density/src/analysis/density_estimation_weighted.py
	figures:
	log-transform-kernel-density/src/final/fig_world_income_distribution_log.py

	ref: 
	A.Charpentier, E.Flachaire Log-transform kernel density estimation of income distribution. Actualité Economique, 91 :141-149, 2015.
	https://halshs.archives-ouvertes.fr/halshs-01115988/
	"""
	# generate data
	b=2
	p=pareto(b)	
	data = p.rvs(size=100)		
	x = np.linspace(0, p.ppf(0.99), 100 )	
	# Initialise the kernel density estimator.
	kde = UnivariateKernelDensity(data=data, kernel="log_gaussian")
	bandwidth = "lscv"#'silverman' #'lscv' uses rpy2 => slower
	kde(bandwidth) #, stretch=2.9
	# plot
	plt.plot(kde.support, kde.estimated_density)
	plt.plot(x, p.pdf(x), "--" )
	plt.show()
	# Q: integration ?


def test_share_of_revenue_from_data_pdf():
	"""
	using genpareto this time, to generate random values
	https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.genpareto.html#scipy.stats.genpareto
	
	NB: genpareto.pdf(x, c, loc, scale) is identically equivalent to genpareto.pdf(y, c) / scale with y = (x - loc) / scale.
	"""
	c=0.1
	p=0.9
	x = genpareto.rvs(c, size=1000)	
	def pdf(x):
		return genpareto.pdf(x, c)
	share_data= share_of_revenue_from_data(x,p)	
	x_theor = genpareto.ppf(p, c)
	share_pdf= share_of_revenue_from_pdf(pdf,x_theor, xmax=100)	
	print "share_data, share_pdf=",share_data, share_pdf
	#
	x_theor = genpareto.ppf(p, c)
	x_empir = np.percentile(x,p*100)
	print "x_theor,x_empir=",x_theor,x_empir
	

def test_F_from_pc():
	quad_limit = 50
	thr,perc, b, yav,total_income,nb_household=income_tabulation()
	# interpolate b = paretoc curve
	#interp_b= pc(perc,b) # in Fournier, abscissa  = perc 
	interp_b= pc(thr,b) 	# here, abscissa = y
	y=thr
	# integrate F
	theta_min = np.min(y)
	theta_max = np.max(y)
	y = np.linspace(theta_min,theta_max,30  )
	Fy = F_from_pc(interp_b, y, theta_min, quad_limit)
	# interpolate and derivative	
	f_interp = f_from_F(y, Fy) 
	yy = np.linspace(theta_min,theta_max,200  )
	fy = f_interp(yy)
	# plot
	plt.subplot(2,2,1)
	plt.plot(thr,b, 'x')
	plt.plot(y, interp_b(y)  )
	plt.title('interpolation of pareto curve')
	#P= np.linspace(0,1,100)
	#B_interp = interp_b(P)
	#plt.plot(P,B_interp)
	a=plt.subplot(2,2,2)
	plt.plot(y,Fy/Fy[-1])	
	xlim=a.get_xlim()
	width = np.hstack( (np.diff(thr), [xlim[1]]) )
	plt.bar(left=thr,height=perc, width=width, alpha=0.5) 
	plt.subplot(2,2,3)
	plt.plot(yy,fy, '-+')
	plt.show()
	
def fit_pareto_tail(thr,p,i=-2):
	"""	
	see [Fournier 15] p.15; [FP93]
	"""	
	a=np.log((1-p[i])/(1-p[i+1]))/np.log( thr[i+1]/thr[i] )
	if(a<=0): raise ValueError
	k=thr[i]*(1-p[i])**(1/a)
	if(k<=0): raise ValueError
	return a,k

def piecewise_pareto(x, data, thr, k , a):
	"""
	 piecewise function can be used to define a pdf from tabulated data 
	 and pareto for the tail
	 https://github.com/noahwaterfieldprice/python_primer/blob/master/ch_7/PiecewiseConstant2.py#L24
	 NB: hitandrun doesn't need the vectorized function
	"""	
	# pareto tail
	if(x>thr): return (a*k**a)/x**(a+1)
	# bulk
	for i in range(len(data) - 1):
		if data[i][1] <= x < data[i + 1][1] or x == data[-1][1]:
			return data[i][0]	
	return 0 # this should be removed

def piecewise_pareto_vec(x, data,thr, k , a):
	"""
	vectorized version
	adapted from https://github.com/noahwaterfieldprice/python_primer/blob/master/ch_7/PiecewiseConstant2.py#L24
	https://docs.python.org/2/library/operator.html
	"""
	r = np.zeros(len(x))
	#if idx.size==x.size:
	#	return r
	# bulk
	for i in xrange(data.shape[0] - 1):
		cond = operator.and_(data[i][1] <= x, x < data[i + 1][1])
		cond = operator.or_(cond, x == data[-1][1])
		r[cond] = data[i][0]
	# pareto tail
	idx = x>thr      # TODO: reintegrate in operator
	r[idx] = (a*k**a)/x[idx]**(a+1)	
	return r

def piecewise(x, data):
	"""
	 piecewise function can be used to define a pdf from tabulated data 
	 https://github.com/noahwaterfieldprice/python_primer/blob/master/ch_7/PiecewiseConstant2.py#L24
	 NB: hitandrun doesn't need the vectorized function
	"""	
	if not isinstance(x, (float, int)): raise TypeError
	for i in range(len(data) - 1):
		if data[i][1] <= x < data[i + 1][1] or x == data[-1][1]:
			return data[i][0]	
	return 0 # this should be removed


def get_piecewise_pareto(rescale_factor=None):
	"""
	1d function
	"""
		# get histogram
	thr,perc, b, yav,total_income,nb_household=income_tabulation()	
	if rescale_factor==None:
		xmax  = np.max(thr)+1000				
	else:
		if isinstance(rescale_factor,(int,float)):
			thr *= np.array(rescale_factor) # r*list(...) raises an error
			xmax  = np.max(thr)+rescale_factor*1000
		else: raise TypeError('rescale_factor must be int or float')	
	h,width= histogram_from_tabulation(thr,perc,xmax)	
	# prepare piecewise function from histogram
	eps = 1E-20
	data = np.vstack((h,thr)).T
	data=np.vstack((np.array([eps,0]), data, np.array([np.nan,xmax])))
	# fit pareto params 
	j=-2
	a,k=fit_pareto_tail(thr,perc,j)	
	def pdf(x):						
		return cython_har.piecewise_pareto(x,data,thr[j], k , a)
	return pdf

def get_piecewise_pareto_with_parameters(onedim=True,idx=None, rescale_factor=None):
	"""
	nD function
	return a ready-to-use (parametrized) cython_har.piecewise_pareto function
	TODO: 
	* change the interface of get_piecewise_pareto_with_parameters. only one param: idx
	"""
	raise DeprecationWarning('is it still in use ?')
	
	# get histogram
	thr,perc, b, yav,total_income,nb_household=income_tabulation()	
	if rescale_factor==None:
		xmax  = np.max(thr)+1000				
	else:
		if isinstance(rescale_factor,(int,float)):
			thr *= np.array(rescale_factor) # r*list(...) raises an error
			xmax  = np.max(thr)+rescale_factor*1000
		else: raise TypeError('rescale_factor must be int or float')	
	h,width= histogram_from_tabulation(thr,perc,xmax)	
	# prepare piecewise function from histogram
	eps = 1E-20
	data = np.vstack((h,thr)).T
	data=np.vstack((np.array([eps,0]), data, np.array([np.nan,xmax])))
	# fit pareto params 
	j=-2
	a,k=fit_pareto_tail(thr,perc,j)	
	if(onedim):
		def pdf(x):			
			# only 1 variable is pdf constrained		
			return cython_har.piecewise_pareto(x[0][0],data,thr[j], k , a)
	else:
		if(idx==None):		
			def pdf(x):			
				# ALL variables are pdf constrained
				return np.prod(cython_har.piecewise_pareto_vec(x[0],data,thr[j], k , a))		
		else:
			def pdf(x):			
				# variables specified by idx are pdf constrained
				return np.prod(cython_har.piecewise_pareto_vec(x[0][idx],data,thr[j], k , a))					
	return pdf, xmax , yav, total_income


def test_piecewise():
	data= [(0.4, 1), (0.2, 1.5), (0.1, 3)]
	xmax=4
	data = data + [(None, xmax)]
	n=200
	x = np.linspace(0,5,n)
	y = np.zeros(n)
	for i in range(x.shape[0]):
		y[i] = piecewise(x[i],data)
	plt.plot(x,y)	
	plt.show()

def test_piecewise_histogram():
	# get histogram
	thr,perc, b, yav,total_income,nb_household=income_tabulation()	
	xmax  = np.max(thr)+1000
	h,width= histogram_from_tabulation(thr,perc,xmax)
	# prepare for piecewise
	data = np.vstack((h,thr)).T
	data=np.vstack((data,np.array([np.nan,xmax])))
	def pdf(x):
		return piecewise(x,data)
	# call piecewise
	n=1000
	xtest = np.linspace(0,xmax+100,n)
	y = np.zeros(n)
	for i in range(xtest.shape[0]):
		y[i] = pdf(xtest[i])
	plt.plot(xtest,y)	
	plt.show()

def test_piecewise_pareto_vec():
	# get histogram
	thr,perc, b, yav,total_income,nb_household=income_tabulation()	
	xmax  = np.max(thr)+5000
	h,width= histogram_from_tabulation(thr,perc,xmax)
	# prepare for piecewise	
	eps = 1E-20
	data = np.vstack((h,thr)).T
	data=np.vstack((np.array([eps,0]), data, np.array([np.nan,xmax])))
	# fit pareto params 
	j=-2
	a,k=fit_pareto_tail(thr,perc,j)	
	def pdf(x):
		return piecewise_pareto(x,data,thr[j], k , a)
	def pdf_vec(x):
		return piecewise_pareto_vec(x,data,thr[j], k , a)	
	# call piecewise
	n=1000
	xtest = np.linspace(0,xmax+100,n)
	y = np.zeros(n)
	for i in range(xtest.shape[0]):
		y[i] = pdf(xtest[i]) 
	assert(np.allclose(pdf_vec(xtest),y))	

def test_piecewise_pareto_histogram():
	# get histogram
	thr,perc, b, yav,total_income,nb_household=income_tabulation()	
	xmax  = np.max(thr)+5000
	h,width= histogram_from_tabulation(thr,perc,xmax)
	# prepare for piecewise
	data = np.vstack((h,thr)).T
	data=np.vstack((data,np.array([np.nan,xmax])))	
	# fit pareto params 
	j=-2
	a,k=fit_pareto_tail(thr,perc,j)	
	def pdf(x):
		return piecewise_pareto(x,data,thr[j], k , a)
	# call piecewise
	n=1000
	xtest = np.linspace(0,xmax+100,n)
	y = np.zeros(n)
	for i in range(xtest.shape[0]):
		y[i] = pdf(xtest[i])
	plt.plot(xtest,y)	
	plt.show()



def test_histogram_from_tabulation():
	thr,perc, b, yav,total_income,nb_household=income_tabulation()	
	ymax  = np.max(thr)+1000
	h,width= histogram_from_tabulation(thr,perc,ymax)
	plt.subplot(3,1,1)
	plt.bar(left=thr,height=perc, width=width, alpha=0.5) 
	plt.title('perc')
	plt.subplot(3,1,2)
	plt.bar(left=thr,height=np.cumsum(h), width=width, alpha=0.5)
	plt.title('cumsum')
	plt.subplot(3,1,3)
	plt.bar(left=thr,height=h, width=width, alpha=0.5)
	plt.title('hist')
	plt.show()
	
if __name__ == "__main__":
	#test_F_from_pc()	
	#test_histogram_from_tabulation()
	#test_piecewise()
	#test_piecewise_pareto_vec()
	#test_piecewise_histogram()
	#test_piecewise_pareto_histogram()
	#test_share_of_revenue_from_pdf()
	#test_share_of_revenue_from_data()        # OK
	#test_share_of_revenue_from_quad_pdf_pareto()     # OK
	#test_share_of_revenue_from_quad_pdf_piecewise_pareto()
	#test_wealth_income_WID_FR()
	fit_widget_pareto_WID()
	#fit_widget_MtoWBs()
	#fit_widget_chemical_potential()
	#test_get_WID_FR()
	##test_equal_mean_rescaled_WID_FR()
	#test_cdf_from_tabulation_linear()
	#test_icdf_from_tabulation_linear()
	#test_share_of_revenue_from_data_logtransform()
	#test_quad_xfx_from_data()


"""
def pdf_pareto(x,k,a):
	raise DeprecationWarning('see scipy.stats')
	if(x>k): 
		return (a*k**a)/x**(a+1)
	else:
		return 0

def F_from_pareto_curve():
	""
	compute F(y) or f(y) as a function of the pareto curve b(p)
	the method is similar to Fournier eq 3.5
	solve equadiff
	""
	import sympy
	from sympy import *
	y,C1= symbols('y,C1')
	F, b = symbols('F b', cls=Function)
	diffeq = Eq( (1-F(y))*( b(y) + y* b(y).diff(y)) - y*F(y).diff(y)*( b(y) -1) ,0 )
	#diffeq = Eq(f(x).diff(x, x) - 2*f(x).diff(x) + f(x), sin(x))
	eq0=simplify(dsolve(diffeq, F(y)))
	eq1 = eq0.subs(C1,1)
	#FF = Lambda(y, eq1 )

def pc(pp,bb):
	""
	pareto curve
	adapted directly from [Fournier 15], C.1.2.3
	
	Inputs The following inputs have to be specified to the Matlab function pc.m.
	A vector thr of incomes corresponding to the thresholds in the tax tabulation.
	A vector pp of percentiles (in r0, 1s) corresponding to the thresholds in the tax tabulation.
	A vector bb of (inverted) Pareto coefficients corresponding to the thresholds in the tax
	tabulation.
	The average income of the taxpaying population y_av.
	A table P of points in r0, 1s of nodes.
	Outputs
	A vector B of approximative values taken by the Pareto curve b at the points of P, that is,
	B=b(P).
	
	https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.PchipInterpolator.html
	""
	# interpolation	
	interp_b = PchipInterpolator(pp, bb, axis=0, extrapolate=False) #None	
	# extrapolation: [Fournier 15] B.3
	# compose: https://fr.mathworks.com/help/symbolic/compose.html
	return interp_b

def F_from_pc(interp_b, y, theta_min, quad_limit):
	""
	https://docs.scipy.org/doc/scipy/reference/tutorial/integrate.html
	""
	b= interp_b
	interp_b_der = interp_b.derivative()
	def f(t):
		# run F_from_pareto_curve to get the symbolic result
		return interp_b_der(t)/( interp_b(t) -1 ) + \
				interp_b(t)  / (t* ( interp_b(t) -  1))
	ny = y.shape[0]
	Fy = np.zeros(ny)
			
	for i in range(ny):
		Fy[i] = 1+quad(f, theta_min, y[i], limit=quad_limit)[0]	
	return Fy  

def f_from_F(y, Fy):
	F_interp = PchipInterpolator(y, Fy, axis=0, extrapolate=False)
	return F_interp.derivative()

"""
