Numerical issues
======

Share of wealth from data
------

*   numerical integration
*   rank


Condensation: Pareto when b is close to 1
------

*   theory says: (see Evans et al.) no condensation when Mtot/nw  = rv.mean
*   observation: condensation in certain experimental settings; Mtot/nw  = rv.mean
*   observation: slow convergence: 
*   observation: marginal empirical mean (of random samples computed by har_cd_native)
	differs from target mean.
	
	nosetests har_nonuniform_test.py:TestClass_har_nonuniform.test_har_cd_empirical_mean
	
    depends on har_cd acceleration (see Filiasi et al.), depends on b, whether the full array x is used or only a marginal x[:,0], on thinning. 
   

Condensation: lognormal when sigma is high (>0.5)
------    

*    test_lognorm: alpha = 1.5; sigma = 1.8 ; lb=0; Mtot = 15; thin= 10 ; accel=1
     *    bumps that vanish, and come back again
     *    notice that alpha>1
     *    solution: increase thin to 50, to "forget" the bumps


References
------
*    Filiasi et al, http://arxiv.org/abs/1309.7795v2 , appendix "monte carlo".
*    Evans, et al.
