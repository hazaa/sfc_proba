# -*- coding: utf-8 -*- #

#  har_wrap.py
#  
#  Copyright 2016 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
CHANGELOG: 
* 2016/12/29: added conversion : b = np.array(b, dtype="float64") and  lb,ub in create_constr , 
	and also for x0 in get_samples.
"""

"""
http://rpy.sourceforge.net/rpy2/doc-dev/html/introduction.html
https://rpy2.bitbucket.io/
"""

import numpy as np 
import scipy.sparse
from rpy2.robjects import r
from rpy2.robjects.numpy2ri import numpy2ri
from rpy2.robjects.packages import importr, data
from rpy2.rinterface import RRuntimeError

try:
	importr('ineq')
except:	
	pass
importr('hitandrun')

def create_constr(A,b,lb,ub):
	"""
	create a set of equality and inequality constraints for the R package hitandrun
	http://github.com/gertvv/hitandrun
	"""
	if not(A.ndim==2):
		raise ValueError('matrix should be 2-dim')
	if scipy.sparse.issparse(A):
		A=A.todense()
	A = np.array(A, dtype="float64") # <- convert to double precision numeric since R doesn't have unsigned ints
	b = np.array(b, dtype="float64") 
	lb = np.array(lb, dtype="float64")
	ub = np.array(ub, dtype="float64")
	neq,nflow=A.shape	
	A = numpy2ri(A)    #http://rpy2.readthedocs.io/en/version_2.8.x/numpy.html
	r.assign("A", A)
	r.assign("neq",neq); 
	r.assign("nflow",nflow)
	r.assign("rhs_eq", numpy2ri(b))
	r.assign("rhs_low", numpy2ri(lb))
	r.assign("rhs_up", numpy2ri(ub))
	constr=r("""
		list(constr=rbind(				
					A,     # eq				
					-diag(nflow), # lower bound
					diag(nflow) # upper bound
					), 
				dir=c(rep("=", neq), rep('<=', 2*nflow )),
				rhs=c( rhs_eq, # eq
					  rhs_low, 
					  rhs_up
					 )
				)
	""")
	#print constr
	return constr
	
def interior_point(constr):
	"""
	wrapper to the function har.run in the R package hitandrun
	with just one sample.
	(nb: har.init doesn't work for all types of constraints)
	"""	
	r.assign("constr",constr)	
	r("""state<-har.init(constr)""")	
	samples= r("result <- har.run(state, n.samples=1)$samples") # get just 1 sample
	samples=np.array(samples)
	return samples.flatten()

#def get_samples(constr,n,**kwargs):
def get_samples(*args,**kwargs):
	"""
	wrapper to the function har.run in the R package hitandrun
	"""
	constr,n = args[0],args[1]
	r.assign("n.samples",n)
	r.assign("constr",constr)	
	if kwargs.has_key('x0'):
		x0 = np.array(kwargs['x0'], dtype="float64")		
		x0 = numpy2ri(x0)		
		r.assign("x0",x0)
		r("""state<-har.init(constr,x0=x0)""")
	else:		
		r("""state<-har.init(constr)""")	
	samples= r("result <- har.run(state, n.samples=n.samples)$samples")
	samples=np.array(samples)
	return samples

def check_constr(x,A,b,lb,ub):
	"""
	check that constraints are respected
	"""
	nr,nc=A.shape
	tol=1.e-9
	if (lb>=ub).any() :
			raise ValueError('lb>=ub')
	#print A,x,np.dot(A,x)-b
	return np.all(np.dot(A,x)-b<tol) & np.all(x>=lb) & np.all(x<=ub)

def example(n):
	if n==0:
		A = np.array([[1.,1.,0.]])
		b = np.array([1.])
		lb =np.array([0.,0.,0.])
		ub =np.array([1.,1.,1.])
	if n==1:
		A = np.array([[1.,1.,0.],[0.,1.,1.]])
		b = np.array([1.,1.])
		lb =np.array([0.,0.,0.])
		ub =np.array([1.,1.,1.])
	if n==2:
		A = np.array([[1.,1.]])
		b = np.array([1.])
		lb =np.array([0.,0.])
		ub =np.array([1.,1.])		
	if n==3:
		n=20
		A = np.ones(n).reshape((1,n))
		b = np.array([1.])
		lb =np.zeros(n)
		ub =np.ones(n)			
	return A,b,lb,ub

def test_interior_point():
	"""
	"""
	A,b,lb,ub = example(0)
	constr = create_constr(A,b,lb,ub)
	x=interior_point(constr)
	print "x0=",x
	print "constraints checked ?:",check_constr(x,A,b,lb,ub)

def test_har_sample():
	A,b,lb,ub = example(2)
	constr = create_constr(A,b,lb,ub)
	x=interior_point(constr)
	samples = get_samples(constr, 1000)

if __name__ == "__main__":	
	test_1()
