#cython: boundscheck=False, wraparound=False, nonecheck=False

"""
see notebooks/cython_root_ficm.py for some performance tests using timeit

http://cython.readthedocs.io/en/latest
>cython ficm_cython.pyx -a
>gcc -O3 -march=native ficm_cython.c -shared -fPIC `python-config --cflags --libs` -o ficm_cython.so
"""


import numpy as np
cimport numpy as np

cpdef _func_cython(double u, np.ndarray[double, ndim=1] x, np.ndarray[double, ndim=1] y,int dim_x,int dim_y, int directed, double L):
	""" The individual probabilities are:
		p_ij = z xi yj / ( 1+ z xi yj)
		The equation that must be solved is :	
		\sum_i \sum_j p_ij = L
	:param u: candidate solution of the equation	
	:type u: float
	:param x: fitnesses
	:type x: numpy.array
	:param y: fitnesses
	:type y: numpy.array
	:param dim_x: size of array x
	:type dim_x: integer
	:param dim_y: size of array y
	:type dim_y: integer
	:param directed: 1 for a directed network
	:type directed: integer
	:param L: number of links
	:type L: float or integer
	:returns: objective function
	"""
	cdef double r
	cdef double xi
	cdef double yj
	cdef int i 
	cdef int j
	
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
				if (directed==True) and (i==j):
						pass
				else:
					xi=x[i]
					yj=y[j]		
					r+=  xi*yj/ (1.+u* xi*yj)
	return r*u-L

	
cpdef _func_dij_cython(double u, np.ndarray[double, ndim=2] dij, int dim_x,int dim_y, int directed, double L):
	"""  individual probabilities are:
		p_ij = z dij/ ( 1+ z dij)
		The equation to solve is :	
		\sum_i \sum_j p_ij = L	
	
	efficient indexing: http://cython.readthedocs.io/en/latest/src/tutorial/numpy.html#efficient-indexing
		" So if v for instance isn’t typed, then the lookup f[v, w] isn’t optimized. "
		
	:param u: candidate solution of the equation
	:type u: float
	:param dij:  dyadic fitnesses
	:type dij: ndarray, ndim=2
	:param dim_x: shape[0] of array dij
	:type dim_x: integer
	:param dim_y: shape[1] array dij
	:type dim_y: integer
	:param directed: 1 for a directed network
	:type directed: integer
	:param L: number of links
	:type L: float or integer
	:returns: objective function
	"""
	cdef double r
	cdef int i 
	cdef int j
	cdef double d
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
				if (directed==True) and (i==j):
						pass
				else:	
					d = dij[i][j]
					r+=  d/ (1.+u*d)	
	return r*u-L

cpdef _jac_cython(double u, np.ndarray[double, ndim=1] x, np.ndarray[double, ndim=1] y,int dim_x,int dim_y, int directed, double L):
	"""Jacobian of _func corresponds to :
		p_ij = z x_i y_j / ( 1+ z x_i y_j)
	:param u: Jacobian of the candidate solution of the equation
	:type u: float	
	:rtype: float
	:returns: jacobian
	"""
	cdef double r
	cdef double xi
	cdef double yj
	cdef int i 
	cdef int j	
	cdef np.ndarray rr = np.zeros(1,dtype=np.double)
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
			if (directed==True) and (i==j):
					pass
			else:		
				r+=  x[i]*y[j]/ (1.+u* x[i]*y[j] )**2											
	rr[0]=r			
	return rr	
	
cpdef _jac_dij_cython(double u, np.ndarray[double, ndim=2] dij, int dim_x,int dim_y, int directed, double L):
	"""Jacobian of _func corresponds to :
		p_ij = z dij / ( 1+ z dij)
	:param u: Jacobian of the candidate solution of the equation
	:type u: float
	:returns: jacobian
	:rtype: float
	"""
	cdef double r
	cdef double d
	cdef int i 
	cdef int j	
	cdef np.ndarray rr = np.zeros(1,dtype=np.double)
	r=0.
	for i in range(dim_x):
		for j in range(dim_y):
			if (directed==True) and (i==j):
					pass
			else:
				d = dij[i][j]
				r+= d	/ (1.+u*d)**2				
	rr[0]=r			
	return rr				
	
	
