# -*- coding: utf-8 -*-
"""
Created on Aug 20 2018

Module:
    compressed_sensing - compressed_sensing for sfc-like under determined systems
						 Sparse solutions, but not non-negative 
						 (see EM_topology_flow.py for non-negative ).
							
    
Author:
    Aurélien Hazan, after 
    
Description:
    Implementation of ...
    
Usage:
    Be ``strn_in,strn_out`` two 1-dimensional NumPy arrays,. 
    Import the module and initialize the ... ::
        >>> import 
        >>> cs = ...
    To create ::
        >>> cs.do_smthg()

References:
.. [Saracco2015] `F. Saracco, R. Di Clemente, A. Gabrielli, T. Squartini,
    Randomizing bipartite networks: the case of the World Trade Web,
    Scientific Reports 5, 10595 (2015)
    <http://www.nature.com/articles/srep10595>`_   
   
    
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from transmat import CSP_sfc_fullconnected_noMLK
from sklearn.linear_model import Lasso
from sklearn.preprocessing import scale

from scipy.sparse import csc_matrix

# -------------------------------------------------------------------
# VAMP by GAMPTeam https://github.com/GAMPTeam/vampyre/
import os
import sys
#vp_path = os.path.abspath('../../')
vp_path = os.path.abspath('/home/aurelien/local/git/vampyre')
if not vp_path in sys.path:
	sys.path.append(vp_path)
import vampyre as vp

swamp_path = os.path.abspath('/home/aurelien/local/git/SwAMP-Demo/python')
if not swamp_path in sys.path:
	sys.path.append(swamp_path)
import amp

def test_vampyre_positive_prior():
	"""
	the prior is Gauss-Bernoulli with strictly positive mean
	https://github.com/GAMPTeam/vampyre/blob/master/demos/sparse/sparse_lin_inverse_amp.ipynb
	"""

	# Parameters
	nz0 = 1000   # number of components of z0
	nz1 = 500    # number of measurements z1

	# Compute the shapes
	zshape0 = (nz0,)   # Shape of z0 matrix
	zshape1 = (nz1,)   # Shape of z1 matrix = shape of y matrix
	Ashape = (nz1,nz0)   # Shape of A matrix
	
	prob_on = 0.1      # fraction of components that are *on*
	z0_mean_on = 5     # mean for the on components !!!!!!!!!!!!!!! POSITIVE
	z0_var_on = 0.5      # variance for the on components
	snr = 30           # SNR in dB	

	# Generate the random input 
	z0_on = np.random.normal(z0_mean_on, np.sqrt(z0_var_on), zshape0)
	u = np.random.uniform(0, 1, zshape0) < prob_on
	z0 = z0_on*u
	ind = np.array(range(nz0))
	plt.plot(ind,z0)

	#
	A = np.random.normal(0, 1/np.sqrt(nz0), Ashape)
	z1 = A.dot(z0)
	# add noise
	zvar1 = np.mean(np.abs(z1)**2)
	wvar = zvar1*np.power(10, -0.1*snr)
	w = np.random.normal(0,np.sqrt(wvar), zshape1)
	y = z1 + w
	# vamp solver
	est0_off = vp.estim.DiscreteEst(0,1,zshape0)
	est0_on   = vp.estim.GaussEst(z0_mean_on, z0_var_on,zshape0)
	est_list = [est0_off, est0_on]
	pz0 = np.array([1-prob_on, prob_on])
	est0 = vp.estim.MixEst(est_list, w=pz0, name='Input')
	Aop = vp.trans.MatrixLT(A,zshape0)
	est1  = vp.estim.GaussEst(y,wvar,zshape1,name='Output')
	# run
	nit = 20  # number of iterations
	solver = vp.solver.Gamp(est0,est1,Aop,hist_list=['z0', 'zvar0'],nit=nit)
	solver.summary()
	solver.solve()
	# plot
	zhat0 = solver.z0
	ind = np.array(range(nz0))
	plt.plot(ind,z0)
	plt.plot(ind,zhat0)
	plt.legend(['True', 'Estimate'])


def make_CS_data(nr,nc,**kwargs):
	"""
	"""
	if kwargs['model']=='Fsparse':
		# Parameters
		nz0 = nc  # number of components of z0
		nz1 = nr   # number of measurements z1
		frac_nonzeros_A =[]
		if kwargs.has_key('frac_nonzeros_A'):
			frac_nonzeros_A = kwargs['frac_nonzeros_A']
		else:
			frac_nonzeros_A = 0.07
		# Compute the shapes
		zshape0 = (nz0,)   # Shape of z0 matrix
		zshape1 = (nz1,)   # Shape of z1 matrix = shape of y matrix
		Ashape = (nz1,nz0)   # Shape of A matrix
		
		prob_on = 0.01      # fraction of components that are *on*
		z0_mean_on = 5     # mean for the on components !!!!!!!!!!!!!!! POSITIVE
		z0_var_on = 0.5      # variance for the on components
		snr = 20           # SNR in dB	

		# Generate the random input 
		z0_on = np.random.normal(z0_mean_on, np.sqrt(z0_var_on), zshape0)
		u = np.random.uniform(0, 1, zshape0) < prob_on
		z0 = z0_on*u
		ind = np.array(range(nz0))
		plt.plot(ind,z0)

		# generate A
		#A = np.random.normal(0, 1/np.sqrt(nz0), Ashape)
		A = sample_Fsparse(Ashape[0], Ashape[1], frac_nonzeros_A)
		z1 = A.dot(z0)
		# add noise
		zvar1 = np.mean(np.abs(z1)**2)
		wvar = zvar1*np.power(10, -0.1*snr)
		w = np.random.normal(0,np.sqrt(wvar), zshape1)
		y = z1 + w
	else:
		raise ValueError('unknown dataset')	
		
	return A,y,z0,z0_mean_on, z0_var_on, prob_on,wvar

def test_vampyre_Fsparse():
	"""
	the measurement operator is sparse
	"""
	A,y,z0,z0_mean_on, z0_var_on, prob_on,wvar= make_CS_data('Fsparse') 
	zshape1,zshape0=A.shape

	#y = z1 # NONOISE
	# vamp solver
	est0_off = vp.estim.DiscreteEst(0,1,zshape0)
	est0_on   = vp.estim.GaussEst(z0_mean_on, z0_var_on,zshape0)
	est_list = [est0_off, est0_on]
	pz0 = np.array([1-prob_on, prob_on])
	est0 = vp.estim.MixEst(est_list, w=pz0, name='Input')
	Aop = vp.trans.MatrixLT(A,zshape0)
	est1  = vp.estim.GaussEst(y,wvar,zshape1,name='Output')
	# run
	nit = 50  # number of iterations
	solver = vp.solver.Gamp(est0,est1,Aop,hist_list=['z0', 'zvar0'],nit=nit)
	solver.summary()
	solver.solve()
	# plot
	zhat0 = solver.z0
	ind = np.array(range(nz0))
	plt.plot(ind,z0)
	plt.plot(ind,zhat0)
	plt.legend(['True', 'Estimate'])
	err = np.mean((A.dot(zhat0) - y) ** 2)
	
def test_vampyre_sfc():	
	nb,nf,nh = 3,10,50
	A,y=CSP_sfc_fullconnected_noMLK(nb,nf,nh)	
	A=np.array(A.todense().astype('float'))
	nz1,nz0 = A.shape
	A = 1/np.sqrt(nz0) * A
	#
	zshape0 = (nz0,)   # Shape of z0 matrix
	zshape1 = (nz1,)   # Shape of z1 matrix = shape of y matrix
	Ashape = (nz1,nz0)   # Shape of A matrix
	
	prob_on = 0.01      # fraction of components that are *on*
	
	z0_mean_on = 0.1     # mean for the on components !!!!!!!!!!!!!!! POSITIVE
	z0_var_on = 0.01      # variance for the on components
	wvar = 0.001 # ??????????????,
	"""
	snr = 100           # SNR in dB		
	# Generate the random input 
	z0_on = np.random.normal(z0_mean_on, np.sqrt(z0_var_on), zshape0)
	u = np.random.uniform(0, 1, zshape0) < prob_on
	z0 = z0_on*u
	ind = np.array(range(nz0))
	plt.plot(ind,z0)

	#
	#A = np.random.normal(0, 1/np.sqrt(nz0), Ashape)
	frac_nonzeros_F = 0.07
	A = sample_Fsparse(Ashape[0], Ashape[1], frac_nonzeros_F)
	z1 = A.dot(z0)
	# add noise
	zvar1 = np.mean(np.abs(z1)**2)
	wvar = zvar1*np.power(10, -0.1*snr) """
	w = np.random.normal(0,np.sqrt(wvar), zshape1)
	y = y+ w
	# lasso
	# http://www.science.smith.edu/~jcrouser/SDS293/labs/2016/lab10/Lab%2010%20-%20Ridge%20Regression%20and%20the%20Lasso%20in%20Python.pdf
	# p.251-255 of “Introduction to Statistical Learning with Applications in R”
	lasso = Lasso(max_iter=10000, normalize=True)
	coefs = []
	alphas = 10**np.linspace(10,-2,10)*0.5
	for a in alphas:
		lasso.set_params(alpha=a)
		lasso.fit(scale(A), y)
		coefs.append(lasso.coef_)
		
	ax = plt.gca()
	ax.plot(alphas*2, coefs)
	ax.set_xscale('log')
	plt.axis('tight')
	plt.xlabel('alpha')
	plt.ylabel('weights')
	
	#y = z1 # NONOISE
	# vamp solver
	est0_off = vp.estim.DiscreteEst(0,1,zshape0)
	est0_on   = vp.estim.GaussEst(z0_mean_on, z0_var_on,zshape0)
	est_list = [est0_off, est0_on]
	pz0 = np.array([1-prob_on, prob_on])
	est0 = vp.estim.MixEst(est_list, w=pz0, name='Input')
	Aop = vp.trans.MatrixLT(A,zshape0)
	est1  = vp.estim.GaussEst(y,wvar,zshape1,name='Output')
	# run
	nit = 1  # number of iterations
	solver = vp.solver.Gamp(est0,est1,Aop,hist_list=['z0', 'zvar0'],nit=nit)
	solver.summary()
	solver.solve()
	# plot
	zhat0 = solver.z0
	ind = np.array(range(nz0))	
	plt.plot(ind,zhat0)
	plt.legend(['True', 'Estimate'])
	err = np.mean((A.dot(zhat0) - y) ** 2)

# -------------------------------------------------------------------
# swamp by Sphinx team	
# https://github.com/eric-tramel/SwAMP-Demo
	

def test_SwAMP():
	nb,nf,nh = 3,30,100
	F,y=CSP_sfc_fullconnected_noMLK(nb,nf,nh)	
	F=np.array(F.todense().astype('float'))
	F=F/float(F.shape[0])
	y=y/float(F.shape[0])
	# Instance's parameters
	#n = 1024
	#alpha = 0.72
	rho, m_pr, v_pr = 0.05, 1.0, 0.1
	delta = 1e-8
	#print r' * Parameters: N = %d, \rho = %.2f, \alpha = %.2f, \Delta = %.2g, \gamma = %d' % (n, rho, alpha, delta, gamma)
	prior_prmts = [rho, m_pr, v_pr]
	# Pre-processing
	"""
	gamma = 20
	m = ceil(alpha * n)
	k = ceil(prior_prmts[0] * n)
	
	# Gen. instance
	x, F, y = sample_instance(n, m, k, delta, gamma)
	"""
	# Algorithm's parameters
	t_max = 200

	# Run algorithm
	#t = time.time()
	x=0
	alpha = F.shape[0]/float(F.shape[1])
	a_sw, c_sw, mse_sw, diff_sw = amp.run_swamp(y, F, x, 1.0, prior_prmts, t_max)
	err = np.mean((F.dot(a_sw) - y) ** 2)/np.mean( a_sw** 2) *100.
	plt.plot(range(F.shape[1]), a_sw)
	plt.plot(c_sw)
# -------------------------------------------------------------------
# AMP by Sphinx team
# https://github.com/sphinxteam/AMP_tutorial

def sample_instance(size_x, frac_nonzeros, rows_to_columns, var_noise):
    """Samples F from P(F) and {x, y} from P(x, y | F)"""    
    # Some pre-processing
    size_nonzeros = int(np.ceil(frac_nonzeros * size_x))
    size_y = int(np.ceil(rows_to_columns * size_x))
    
    # Sample x from P_0(x)
    x0 = np.zeros(size_x)
    nonzeros = np.random.choice(size_x, size_nonzeros, replace=False)
    x0[nonzeros] = np.random.randn(size_nonzeros)
    
    # Generate F and y = Fx + noise
    F = np.random.randn(size_y, size_x) / np.sqrt(size_x)
    noise = np.sqrt(var_noise) * np.random.randn(size_y)
    
    y =F.dot(x0) + noise
    return x0, F, y

def sample_Fsparse(size_y, size_x, frac_nonzeros_F, var_noise=1.):
    """Samples F from P(F) and {x, y} from P(x, y | F)
    
    TODO: replace by: 
		sparse.rand(m, n[, density, format, dtype, ]) Generate a sparse matrix of the given shape and density with uniformly distributed values.
		sparse.random(m...
    """    
    # Some pre-processing
    size_nonzeros_F = int(np.ceil(frac_nonzeros_F * size_x*size_y))
    
    # Sample x from P_0(x)
    nonzeros_F = np.random.choice(size_x*size_y, size_nonzeros_F, replace=False)
    # Generate F and y = Fx + noise
    #F = np.zeros(size_y* size_x)
    #F[nonzeros_F] = np.sqrt(var_noise)*np.random.randn(size_nonzeros_F)
	#F=F.reshape((size_y, size_x))
    data =  np.sqrt(var_noise)*np.random.randn(size_nonzeros_F)
    row = np.random.randint(0,size_y,size_nonzeros_F)
    col = np.random.randint(0,size_x,size_nonzeros_F)
    F=csc_matrix((data, (row, col)), shape=(size_y, size_x), dtype=np.float)	
    return F

def sample_instance_Fsparse(size_x, frac_nonzeros, frac_nonzeros_F,rows_to_columns, var_noise):
    """Samples F from P(F) and {x, y} from P(x, y | F)"""    
    # Some pre-processing
    size_y = int(np.ceil(rows_to_columns * size_x))
    size_nonzeros = int(np.ceil(frac_nonzeros * size_x))
    size_nonzeros_F = int(np.ceil(frac_nonzeros_F * size_x*size_y))
    
    # Sample x from P_0(x)
    x0 = np.zeros(size_x)
    nonzeros = np.random.choice(size_x, size_nonzeros, replace=False)
    nonzeros_F = np.random.choice(size_x*size_y, size_nonzeros_F, replace=False)
    x0[nonzeros] = np.random.randn(size_nonzeros)    
    
    # Generate F and y = Fx + noise
    #F = np.random.randn(size_y, size_x) / np.sqrt(size_x)
    F = np.zeros(size_y* size_x)
    F[nonzeros_F] = np.random.randn(size_nonzeros_F)
    F=F.reshape((size_y, size_x))
    
    noise = np.sqrt(var_noise) * np.random.randn(size_y)
    
    y =F.dot(x0) + noise
    return x0, F, y


def iterate_amp(F, y, var_noise, frac_nonzeros,
                x0=None, max_iter=100, tol=1e-7, verbose=1):
    """Iterates AMP to solve y = Fx, w/ x Bernoulli-Gaussian"""
    
    # Some pre-processing
    size_y, size_x = F.shape
    sqrF = F * F
    
    # Initialize variables
    A = np.ones(size_x)
    B = np.zeros(size_x)
    a = np.zeros(size_x)
    c = np.ones(size_x)
    v = np.ones(size_y)
    w = np.copy(y)
    
    mses = np.zeros(max_iter)
    for t in range(max_iter):
        # Store old v and compute new v and w
        v_old = np.copy(v)
        v = sqrF.dot(c)
        w = F.dot(a) - v * ((y - w) / (var_noise + v_old))
        
        # Iterate A and B and compute new mean and variance
        a_old = np.copy(a)
        A = sqrF.T.dot(1 / (var_noise + v))
        B = F.T.dot((y - w) / (var_noise + v)) + A * a
        a, c = prior(A, B, frac_nonzeros)
        
        # Compute metrics
        diff = np.mean(np.abs(a - a_old))
        mses[t] = np.mean((a - x0) ** 2) if x0 is not None else 0
        err = np.mean((F.dot(a) - y) ** 2)
        # Print iteration status on screen
        if verbose:
            print("t = %d, diff = %g; mse = %g; err=%g" % (t, diff, mses[t],err))
        
        # Check for convergence
        if diff < tol:
            break
            
    return mses[:t],a
    
def prior(A, B, rho):
    """Compute f and f' for Bernoulli-Gaussian prior"""
    
    # let x = s * t, with s ~ Bernoulli(p) and t ~ N(0, 1); then m = E(t), v = Var(t), p = E(s)
    m = B / (1 + A)
    v = 1 / (1 + A)
    p = rho / (rho + (1 - rho) * np.sqrt(1 + A) * np.exp(-.5 * m ** 2 / v))
    
    # compute E(s * t) and Var(s * t) from E(t), Var(t) and E(s)
    a = p * m
    c = p * v + p * (1 - p) * m ** 2
    return a, c    
    
    
def test():    
	plt.rcParams["figure.figsize"] = (8, 8)
	plt.rcParams["font.size"] = 14
	np.random.seed(42)
	   
	"""
	x, F, y = sample_instance(2000, 0.2, 0.5, 1e-5)
	mses_amp,a = iterate_amp(F, y, var_noise=1e-5, frac_nonzeros=0.2, x0=x);
	"""
	
	size_x, frac_nonzeros, frac_nonzeros_F,rows_to_columns, var_noise=2000, 0.02,0.02, 0.1, 1e-5
	x, F, y = sample_instance_Fsparse(size_x, frac_nonzeros, frac_nonzeros_F,rows_to_columns, var_noise)
	mses_amp,a = iterate_amp(F, y, var_noise=1e-5, frac_nonzeros=0.2, x0=x);
	"""
	size_x, frac_nonzeros, frac_nonzeros_F,rows_to_columns, var_noise=20000, 0.1,0.2, 0.001, 1e-8
	x, F, y = sample_instance_Fsparse(size_x, frac_nonzeros, frac_nonzeros_F,rows_to_columns, var_noise)
	mses_amp,a = iterate_amp(F, y, var_noise=1e-5, frac_nonzeros=0.2, x0=x);
	"""    
	#plt.stem(x)   
	plt.subplot(2,2,1) 
	plt.stem(a)  
	plt.subplot(2,2,2) 
	plt.hist(a,50)
	plt.subplot(2,2,3) 
	plt.plot(mses_amp)
	plt.show() 
	
if __name__ == "__main__":
	test_vampyre_Fsparse()
