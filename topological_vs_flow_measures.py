# -*- coding:utf-8
#  topological_vs_flow_measures.py
#  
#  Copyright 2018 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
TODO: get rid of PerformanceWarning 'your performance may suffer as PyTables will pickle ...'
https://stackoverflow.com/questions/14355151/how-to-make-pandas-hdfstore-put-operation-faster/14370190#14370190

nb,nf,nw are kept fixed in order to simplify the structure of the DataFrame.

refs:
	pandas: http://pandas.pydata.org/pandas-docs/stable/

defs

* [H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to th
e household j"
* [H17] "B_{nf,nf}[i,j]= 1 if the firm j is selling capital goods t
o the firm i."
	

"""

import warnings
import numpy as np
#from EP_network import *
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pandas as pd
import json
import networkx as nx
from networkx.algorithms import bipartite
from network import from_numpy_matrix_bipartite
from scipy.stats import gaussian_kde
import statsmodels.api as sm

label_sector_agg = ['B-E', 'F', 'G-I', 'J', 'K', 'L', 'M_N', 'O-Q', 'R-U']
marker = ["o","v","^","1","D","s","p","+","*"]	#colors https://matplotlib.org/tutorials/colors/colors.html#sphx-glr-tutorials-colors-colors-py
color = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'purple', 'brown']

fs = 20 # fontsize
plt.rcParams['font.size']=20
plt.rcParams['xtick.labelsize']='large'
plt.rcParams['ytick.labelsize']='large'
######################
## utils
	

def test_independence(x,y):
	"""
	test for statistical independence between two multivariate samples
	
	https://arxiv.org/abs/1610.04782
	https://github.com/wittawatj/fsic-test
	copied from https://github.com/wittawatj/fsic-test/blob/master/ipynb/demo_nfsic.ipynb
	"""
	import fsic.util as util
	import fsic.data as data
	import fsic.kernel as kernel
	import fsic.indtest as it
	import fsic.glo as glo
	import matplotlib.pyplot as plt
	import numpy as np
	import scipy.stats as stats
	import scipy
	import theano
	# reshape	
	if x.ndim==1: x.reshape((x.shape[0],1))
	if y.ndim==1: y.reshape((y.shape[0],1))	
	# create ps, train and test
	pdata=data.PairedData(x,y)
	tr, te = pdata.split_tr_te(tr_proportion=0.5)
	# J is the number of test locations
	J = 1
	# Significance level of the test
	alpha = 0.01
	# There are many options for the optimization. 
	# Almost all of them have default values. 
	# Here, we will list a few to give you a sense of what you can control.
	op = {
		'n_test_locs':J,  # number of test locations
		'max_iter':200, # maximum number of gradient ascent iterations
		'V_step':1, # Step size for the test locations of X
		'W_step':1, # Step size for the test locations of Y
		'gwidthx_step':1, # Step size for the Gaussian width of X
		'gwidthy_step':1, # Step size for the Gaussian width of Y
		'tol_fun':1e-4, # Stop if the objective function does not increase more than this
#		'seed':None # random seed
	}
	# Do the optimization with the options in op.
	op_V, op_W, op_gwx, op_gwy, info = it.GaussNFSIC.optimize_locs_widths(tr, alpha, **op )
	# construct an NFSIC test.
	nfsic_opt = it.GaussNFSIC(op_gwx, op_gwy, op_V, op_W, alpha)
	# Perform the independence test on the testing data te.
	# return a dictionary of testing results
	r = nfsic_opt.perform_test(te)
	return r

#######
# plot functions
def plot_kde_by_sector(transtype,xtitle,it=0,
							bw_method='scott'):	
	"""
	"""
	min_ = np.min(transtype[it,:])
	max_ = np.max(transtype[it,:])
	xx = np.linspace(min_,max_, 1000)
	for s in sectors:
		idx = s==firm_to_sector[it,:]
		transtype_by_sector = transtype[it,idx]
		if(transtype_by_sector.size>1):
			kde = gaussian_kde(transtype_by_sector,bw_method=bw_method)# "silverman", 'scott',0.1	
			pdf_xx = kde.evaluate(xx) 
			plt.plot(xx,pdf_xx)
	plt.ylabel("kernel pdf")
	plt.xlabel(xtitle)	

def scatter_plot_by_sector(E,firm_to_sector,niter,
							title,loc='lower right',isec=None):
	if isec==None: I=enumerate(label_sector_agg)
	else:
		I=[(isec,label_sector_agg[isec])]
	for i,l_s in I :		
		m = marker[i]
		idx = np.where(i==firm_to_sector[0,:])[0]
		plt.scatter( i*np.ones(idx.shape[0]*niter ),   E[:,idx].flat,
					color= color[i],#sector_color,
					#s = 1E6 * s_Is.flatten(), 	alpha=0.5,
					marker=m,
					label = l_s			
					)	
		plt.annotate(l_s,xy=(i, 0.5),fontsize=fs,va="center", ha="center")			
	plt.xlabel("sector",fontsize=fs)
	plt.ylabel(title,fontsize=fs)	
	ax = plt.gca()	
	ax.set_xticklabels([])	
	plt.legend(loc=loc)

def hist_by_sector(E,title,firm_to_sector,loc='lower right',isec=None,n_bins=50,
				histtype='step',fill=False,lim=None,
				**kwargs):
	#https://matplotlib.org/gallery/statistics/histogram_multihist.html#sphx-glr-gallery-statistics-histogram-multihist-py
	if isec==None: 
		I=enumerate(label_sector_agg)
		c=color
	else:
		I=[(isec,label_sector_agg[isec])]
		c=color[isec]
	x_multi=[]	
	for i,l_s in I :		
		#m = marker[i]
		idx = np.where(i==firm_to_sector[0,:])[0]
		if lim==None:
			x_multi.append(E[:,idx].flatten())
		else:
			dummy = E[:,idx].flatten()	
			idx= (dummy>=lim[0])*(dummy<=lim[1])
			x_multi.append(dummy[idx])				
	plt.hist(x_multi,n_bins,histtype=histtype, normed=True, 
			fill=fill,color=c,**kwargs)
		

def scatter_plot_XY_by_sector(X,Y,firm_to_sector,xlabel,ylabel,isec=None,
							marker=marker,label_sector_agg=label_sector_agg,
							color=color):	
	"""
	for firm data only
	"""
	if not X.shape==Y.shape: 
		raise ValueError('array dimension mismatch')
	if isec==None: I=enumerate(label_sector_agg)
	else:
		I=[(isec,label_sector_agg[isec])]
	for i,l_s in I:		
		m = marker[i]
		idx = np.where(i==firm_to_sector[0,:])[0]
		plt.scatter( X[:,idx].flatten(),  Y[:,idx].flatten(),
					color= color[i],#sector_color,
					#s = 1E6 * s_Is.flatten(), 	alpha=0.5,
					marker=m,
					label = l_s			
					)	
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)	
	plt.legend()

def MetabolicEP_plot(i,mu,s,av,va,lb,ub, samplePts=None,ndots=300):
	# DUPLICATED:!!! (from test_optgpsampler)
	# copied from MetabolicEP.ipynb
	#x = l<mu[ii]<u?1/np.sqrt(2*np.pi*sig[ii]):0
	rv=norm(loc=mu[i],scale=np.sqrt(s[i]))
	x=0
	if(lb[i]<mu[i] and mu[i]<ub[i]): x= 1./np.sqrt(2.*np.pi*s[i])				
	m=max( rv.pdf(lb[i]), rv.pdf(ub[i]),x )
	v=np.sqrt(-2.*s[i]*np.log(np.sqrt(2*np.pi*s[i])*1e-3*m));
	l1,l2=max(lb[i],mu[i]-v),min(ub[i],mu[i]+v)	
	xvals = np.linspace(l1,l2,ndots)
				
	"""xvals=np.linspace(lb[i],ub[i],ndots)"""
	a, b = (lb[i] - mu[i]) / s[i], (ub[i] - mu[i]) /s[i]
	y=truncnorm.pdf(xvals,a,b,loc=mu[i],scale=np.sqrt(s[i]) )
	if not samplePts==None:
		if isinstance(samplePts,pd.DataFrame):
			plt.hist(samplePts.iloc[:,i])
		else:	
			plt.hist(samplePts[:,i])
	else:		
		plt.plot(xvals,y)

def plot_network(A,B,av,va,idx=0,plot_neighbors=False,plot_widget=False):
	""" get solution matrix: transmat * av, transmat*std
	
	input:
	------
  	A: array, shape=[nf,nh]
	[H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
    
    B:array, shape=[nf,nf]
    [H17] "B_{nf,nf}[i,j]= 1 if the firm j is selling capital goods to the firm i."
    
    idx: int
    index of agent
	"""
	pass
	# A:
	# B:investment
	# see plot_network

###############################################################
###############################################################
# READ DATASET and stack over iterations
# 'r': [0.03],'alpha0':[0.1] , 'alpha1':[0.75],'alpha2':[0.01],'delta' :[0.1],'k':[6], 'M':[10.] ,'niter':2 }
# other accessors:
#	df.xs(0,level="niter").mu
#	df2.iloc[0]
#	np.asarray(df2.mu)
	
def read_dataset_sfc(path_fname_out='data/topo_vs_flow_sfc.h5',
					groupname='/sfc_block_bayesian'):
	"""
	read data produced by network_weights.py:make_dataset_sfc
	
	Example:
	%matplotlib
	r=read_dataset_sfc()
	r=read_dataset_sfc(groupname='/sfc_rndunif_bayesian')
	r=read_dataset_sfc(groupname='/sfc_rndunif_nnls')

	"""

	store = pd.HDFStore(path_fname_out)
	#p_fix=store[groupname+'/config/'+'p_fix']
	p_fix=store[groupname+'/p_fix']
	p_fix =json.loads( p_fix[0].decode())
	#p_var=store[groupname+'/config/'+'p_var']
	p_var=store[groupname+'/p_var']
	p_var =json.loads( p_var[0].decode())
	nb, nf,nh = p_fix['nb'],p_fix['nf'],p_fix['nh']
	df = store[groupname+'/data']
	
	# choose a slice corresponding to a specific set of parameters
	#df2 = df.loc[ ('2010', 'CZ' , 0.03, 0.1, 0.75, 0.01, 0.1, 6, 10,  slice(0,1) ) ]
	df2 = df.loc[ ('2010', 'CZ' , slice(0,5) ) ]
	mu    = np.vstack(df2.mu)
	#sigma = np.vstack(df2.sigma)
	# A
	cons_hh_firm_degree_in = np.vstack(df2.cons_hh_firm_degree_in)
	cons_hh_firm_degree_out = np.vstack(df2.cons_hh_firm_degree_out)
	cons_hh_firm_AND_in  = np.vstack(df2.cons_hh_firm_AND_in)	
	cons_hh_firm_AND_out = 	np.vstack(df2.cons_hh_firm_AND_out)
	# B
	try:
		invest_degree_in  = np.vstack(df2.invest_degree_in)
		invest_degree_out = np.vstack(df2.invest_degree_out)
		invest_AND_in = np.vstack(df2.invest_AND_in)
		invest_AND_out = np.vstack(df2.invest_AND_out)
	except:
		print 'invest_** absent in this dataset'
	
	firm_to_sector = np.vstack(df2.firm_to_sector)
	sectors = np.unique(firm_to_sector)
	nsec = sectors.shape[0]
	#label_short,label_long = get_bmw_labels(nb,nf,nh)
	store.close()
	niter = mu.shape[0]
	
	# markers and colors
	L = np.asarray(label_sector_agg,dtype="S");
	M=np.asarray(marker,dtype="S"); 

	r={}
	r['mu']=mu; r['niter']=niter; r['nb'],r['nf'],r['nh']=nb,nf,nh
	r['cons_hh_firm_degree_out']=cons_hh_firm_degree_out
	r['cons_hh_firm_degree_in']=cons_hh_firm_degree_in	
	r['invest_degree_in']=invest_degree_in
	r['invest_degree_out']=invest_degree_out
	r['invest_AND_in']=invest_AND_in
	r['invest_AND_out']=invest_AND_out
	r['cons_hh_firm_AND_in']=cons_hh_firm_AND_in
	r['cons_hh_firm_AND_out']=cons_hh_firm_AND_out
	r['firm_to_sector']=firm_to_sector
	r['sectors']=sectors 
	r['nsec']=nsec
	r['L']=L
	r['M']=M
	return r

def read_dataset_bmw(path_fname_out,groupname):
	"""

	how to use it in jupyter:
	%matplotlib
	from topological_vs_flow_measures import *
	%load -r 486-538 topological_vs_flow_measures.py 
	%load -s scatter_plot_XY_by_sector,hist_by_sector topological_vs_flow_measures.py
	
	path_fname_out= 'data/topo_vs_flow_bmw.h5' #
	#groupname='/small_investfirm' # '/small'
	groupname='/small_investfirm_L_cons_hh_20'
	groupname='/rnd_fitness_small_investfirm_L_cons_hh_20'
	read_dataset(path_fname_out,groupname)
	"""
	raise NotImplementedError
	warnings.warn('ONLY BLOCK NETWORK DONE SO FAR!!!!!!!!!!')
	
	store = pd.HDFStore(path_fname_out)
	p_fix=store[groupname+'/config/'+'p_fix']
	p_fix =json.loads( p_fix[0].decode())
	p_var=store[groupname+'/config/'+'p_var']
	p_var =json.loads( p_var[0].decode())
	nb, nf,nw = p_fix['nb'],p_fix['nf'],p_fix['nw']
	df = store[groupname+'/data']
	
	# choose a slice corresponding to a specific set of parameters
	#df2 = df.loc[ ('2010', 'CZ' , 0.03, 0.1, 0.75, 0.01, 0.1, 6, 10,  slice(0,1) ) ]
	df2 = df.loc[ ('2010', 'CZ' , 0.03, 0.1, 0.75, 0.01, 0.1, 6, 10,  slice(0,10) ) ]
	mu    = np.vstack(df2.mu)
	sigma = np.vstack(df2.sigma)
	av    = np.vstack(df2.av)
	va = np.vstack(df2.va)
	ub    = np.vstack(df2.ub)
	lb    = np.zeros((ub.shape[0],ub.shape[1]))
	# A
	cons_hh_firm_degree_in = np.vstack(df2.cons_hh_firm_degree_in)
	cons_hh_firm_degree_out = np.vstack(df2.cons_hh_firm_degree_out)
	cons_hh_firm_AND_in  = np.vstack(df2.cons_hh_firm_AND_in)	
	cons_hh_firm_AND_out = 	np.vstack(df2.cons_hh_firm_AND_out)
	# B
	try:
		invest_degree_in  = np.vstack(df2.invest_degree_in)
		invest_degree_out = np.vstack(df2.invest_degree_out)
		invest_AND_in = np.vstack(df2.invest_AND_in)
		invest_AND_out = np.vstack(df2.invest_AND_out)
	except:
		print 'invest_** absent in this dataset'
	
	firm_to_sector = np.vstack(df2.firm_to_sector)
	sectors = np.unique(firm_to_sector)
	nsec = sectors.shape[0]
	label_short,label_long = get_bmw_labels(nb,nf,nw)
	store.close()
	niter = mu.shape[0]
	
	# markers and colors
	L = np.asarray(label_sector_agg,dtype="S");
	# markers https://matplotlib.org/api/markers_api.html#module-matplotlib.markers
	#marker = ["o","v","^","1","D","s","p","+","*"]
	#colors https://matplotlib.org/tutorials/colors/colors.html#sphx-glr-tutorials-colors-colors-py
	#color = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'purple', 'brown', 'tab:pink']
	#color = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'purple', 'brown']
	M=np.asarray(marker,dtype="S"); 
	
	fs = 20 # fontsize
	plt.rcParams['font.size']=20
	plt.rcParams['xtick.labelsize']='large'
	plt.rcParams['ytick.labelsize']='large'

def get_sum_iter(mu,niter,label,nb,nf,nh):	
	"""
	"""
	if label=="Cd":
		idx_mu_start=0; idx_mu_end=nh*nf; 
		r0=nf ; r1=nh; rout = r1
		axis=0
	elif label=="Cs":	
		idx_mu_start=0; idx_mu_end=nh*nf; 
		r0=nf ; r1=nh ; rout=r0
		axis=1	
	elif label=="Is":	
		"""
		Id = mu[nh*nf:nh*nf+nf**2]
		Id = Id.reshape((nf,nf))
		av_Is=Id.sum(axis=1)
		av_Id=Id.sum(axis=0)	
		"""
		idx_mu_start=nh*nf; idx_mu_end=nh*nf+nf**2; 
		r0= nf; r1= nf; rout= nf
		axis=1		
	elif label=="Id":	
		idx_mu_start=nh*nf; idx_mu_end=nh*nf+nf**2; 
		r0= nf; r1= nf; rout= nf
		axis=0			
	else:
		raise ValueError('unrecognized label')		
	result = np.zeros((niter,rout))	
	for i in range(niter):
		x = mu[i,idx_mu_start:idx_mu_end]
		x=x.reshape((r0,r1))
		x=x.sum(axis=axis)
		result[i,:]=x
	return result	

def plot_budget(r,it=0):
	"""
	Example: 
	%matplotlib
	r=read_dataset_sfc(groupname='/sfc_rndunif_bayesian')
	plot_budget(r,it=0)
	
	"""
	mu_post=r['mu'][it,:]; niter=r['niter']; nb,nf,nh=r['nb'],r['nf'],r['nh']
	cons_hh_firm_degree_out=r['cons_hh_firm_degree_out']
	cons_hh_firm_degree_in=r['cons_hh_firm_degree_in']
	firm_to_sector=r['firm_to_sector']
	sectors =r['sectors']
	nsec=r['nsec']
	L=r['L']
	M=['M']
	#-budget hh
	if True:
		nr = 4; nc=2
		plt.subplot(nr,nc,1)
		Cd = mu_post[0:nh*nf]
		plt.plot(Cd); plt.ylabel('Cd');		
		plt.subplot(nr,nc,2)
		Cd=Cd.reshape((nf,nh))
		plt.matshow(Cd,cmap="binary",fignum=False, aspect="auto")
		plt.ylabel('firms');
		plt.subplot(nr,nc,3)
		WBd = mu_post[nh*nf+nf**2:nh*nf+nf**2+nf*nh]
		plt.plot(WBd); plt.ylabel('WBd');			
		plt.subplot(nr,nc,4)
		WBd=WBd.reshape((nf,nh))
		plt.matshow(WBd,cmap="binary",fignum=False, aspect="auto")
		plt.ylabel('firms');
		plt.subplot(nr,nc,5)
		IDd = mu_post[nf*nh+nf**2+nf*nh+nf*nb:]
		plt.plot(IDd); plt.ylabel('IDd');		
		plt.subplot(nr,nc,6)
		IDd=IDd.reshape((nb,nh))
		plt.matshow(IDd,cmap="binary",fignum=False, aspect="auto")
		plt.ylabel('banks');
		plt.xlabel('hh')
		#plt.colorbar(orientation="horizontal")
		plt.subplot(nr,nc,7)
		residual = -Cd.sum(axis=0) + WBd.sum(axis=0)+IDd.sum(axis=0)
		plt.plot(range(nh),np.array(residual).flatten());	plt.ylabel('residual');
		plt.xlabel('hh')
		plt.subplot(nr,nc,8)
		x = np.vstack((WBd.sum(axis=0), IDd.sum(axis=0),Cd.sum(axis=0)  ))
		plt.matshow(x,cmap="binary",fignum=False, aspect="auto")
		plt.ylabel('budget')
		plt.colorbar(orientation="horizontal")
		ax=plt.gca()
		ax.set_yticklabels(['','WBs(+)','IDs(+)','Cd(-)'])
		#-budget firms
		plt.figure()
		Cs=Cd.sum(axis=1)
		WBd_sum=WBd.sum(axis=1)
		Id = mu_post[nh*nf:nh*nf+nf**2]
		Is = Id.reshape((nf,nf))
		Is=Is.sum(axis=1)
		ILd=mu_post[nf*nh+nf**2 + nf*nh: nf*nh+nf**2 + nf*nh+nf*nb]
		ILd=ILd.reshape((nb,nf))
		ILd_sum=ILd.sum(axis=0)
		residual = Cs.flatten() -WBd_sum.flatten() + Is.flatten() - np.array(ILd_sum).flatten()
		nplot=3
		plt.subplot(nplot,1,1)
		plt.plot(range(nf),np.array(residual).flatten())
		plt.ylabel('residual');
		plt.xlabel('firms')
		plt.subplot(nplot,1,2)
		plt.plot(range(nf**2),Id.reshape(nf**2,1) )
		plt.ylabel('Id');
		plt.subplot(nplot,1,3)
		x = np.vstack((Cs.flatten() , Is.flatten(),WBd_sum.flatten(), np.array(ILd_sum).flatten() ))
		plt.matshow(x,cmap="binary",fignum=False, aspect="auto")
		plt.ylabel('budget')
		ax=plt.gca()
		ax.set_yticklabels(['','Cs(+)','Is(+)','WBd(-)','ILd(-)'])
		plt.xlabel('firm')
		plt.colorbar(orientation="horizontal")
		plt.show()
		# printable version
	if True :
		plt.figure()
		#nplot=2
		#plt.subplot(nplot,1,1)
		x = np.vstack((WBd.sum(axis=0), IDd.sum(axis=0),Cd.sum(axis=0)  ))
		plt.matshow(x,cmap="binary",fignum=False, aspect="auto")
		plt.xlabel('index of households')
		plt.colorbar(orientation="horizontal")
		ax=plt.gca()
		ax.set_yticklabels(['','WBs(+)','IDs(+)','Cd(-)'])
		plt.figure()
		#nplot=2
		#plt.subplot(nplot,1,2)
		x = np.vstack((Cs.flatten() , Is.flatten(),WBd_sum.flatten(), np.array(ILd_sum).flatten() ))
		plt.matshow(x,cmap="binary",fignum=False, aspect="auto")
		plt.ylabel('budget')
		ax=plt.gca()
		ax.set_yticklabels(['','Cs(+)','Is(+)','WBd(-)','ILd(-)'])
		plt.xlabel('index of firms')
		plt.colorbar(orientation="horizontal")
	
def plot_dataset_Cd_Cs(r,plot_all=False):	
	"""
	Cd= household demand, Cs=consumption supply by firms
	select specific flows: Cd  (firm / hh)
	extract from network.py:
	 * [H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
	 * [H18?] exponential graph model: bipartite undirected
	     get houshold consumption demand
	 
	Example: 
	plot_dataset_Cd_Cs(r)
	"""
	mu=r['mu']; niter=r['niter']; nb,nf,nh=r['nb'],r['nf'],r['nh']
	cons_hh_firm_degree_out=r['cons_hh_firm_degree_out']
	cons_hh_firm_degree_in=r['cons_hh_firm_degree_in']
	firm_to_sector=r['firm_to_sector']
	sectors =r['sectors']
	nsec=r['nsec']
	L=r['L']
	M=['M']
	cons_hh_firm_AND_in=r['cons_hh_firm_AND_in']
	cons_hh_firm_AND_out=r['cons_hh_firm_AND_out']
	
	av_Cd = get_sum_iter(mu,niter,"Cd",nb,nf,nh)
	av_Cs = get_sum_iter(mu,niter,"Cs",nb,nf,nh)	
	# --	
	# SYNTHESE A LA SEABORN (joinplot): av_Cs/degree
	gs = gridspec.GridSpec(2, 2,
	                       width_ratios=[3, 1], height_ratios=[1,3])
	plt.subplot(gs[0])
	plt.hist(cons_hh_firm_degree_out.flatten(),bins=30);
	ax=plt.gca(); ax.set_xticklabels([]);ax.set_yticklabels([])
	min_= np.min(cons_hh_firm_degree_out)-0.5
	max_=np.max(cons_hh_firm_degree_out)+10
	ax.set_xlim((min_,max_))
	# --	
	plt.subplot(gs[2])	
	scatter_plot_XY_by_sector(cons_hh_firm_degree_out, av_Cs,
		firm_to_sector,
		"out degree of firms","average Cs")
	ax=plt.gca();ax.set_xlim((min_,max_))
	ax.set_ylim((np.min(av_Cs)-0.001,np.max(av_Cs)))	
	#
	# REGRESSION by sector
	"""
	X=cons_hh_firm_degree_out ; Y=av_Cs
	l=[]
	for i,l_s in enumerate(label_sector_agg):
		#m = marker[i]
		idx = np.where(i==firm_to_sector[0,:])[0]
		l.append((np.mean(X[:,idx]), np.mean(Y[:,idx]) ))
	l=np.array(l)	
	model = sm.OLS(l[:,1],l[:,0])
	res = model.fit()
	print(res.summary())	
	plt.plot( l[:,0] , res.fittedvalues, 'r--', label="OLS")
	#scatter_plot_XY_by_sector(X,Y,"out degree","average Cs" )
	#plt.xlim([0,350])
	"""
	# --	
	plt.subplot(gs[3])	
	plt.hist(av_Cs.flatten(),bins=30,orientation=u'horizontal');
	ax=plt.gca();ax.set_ylim((np.min(av_Cs)-0.001,np.max(av_Cs)))
	ax.set_xticklabels([]);ax.set_yticklabels([])	
	plt.subplots_adjust(wspace=0.001, hspace=0.01)
	
	# SYNTHESE A LA SEABORN (joinplot): av_Cd/degree
	plt.figure()
	gs = gridspec.GridSpec(2, 2, width_ratios=[3, 1], height_ratios=[1,3])
	plt.subplot(gs[0])
	plt.hist(cons_hh_firm_degree_in.flatten(),bins=50,);
	ax=plt.gca(); ax.set_xticklabels([]);ax.set_yticklabels([])
	min_= np.min(cons_hh_firm_degree_in)-0.5
	max_=np.max(cons_hh_firm_degree_in)+1
	ax.set_xlim((min_,max_))
	plt.subplot(gs[2])	
	plt.scatter( cons_hh_firm_degree_in.flat, av_Cd.flat)
	plt.xlabel("in degree of households")
	plt.ylabel("average Cd")
	#NOT ADAPTED!!:scatter_plot_XY_by_sector(cons_hh_firm_degree_out, av_Cd,
	#							"in degree of households","average Cd")
	ax=plt.gca();ax.set_xlim((min_,max_))
	ymin=np.min(av_Cd)-0.001
	ymax=np.max(av_Cd)
	ax.set_ylim((ymin,ymax))								
	plt.subplot(gs[3])	
	plt.hist(av_Cd.flatten(),bins=100,orientation=u'horizontal');
	ax=plt.gca();	ax.set_ylim((ymin,ymax))
	ax.set_xticklabels([]);ax.set_yticklabels([])	
	plt.subplots_adjust(wspace=0.001, hspace=0.01)

	if plot_all:
		# plot FLATTENED (==merge all iterations)	
		nr=2
		nc=2
		plt.subplot(nr,nc,1)
		plt.hist(av_Cd.flatten());
		plt.xlabel("Cd of households")
		plt.subplot(nr,nc,2)
		plt.hist(cons_hh_firm_AND_in.flatten());
		plt.xlabel("AND of households")
		plt.subplot(nr,nc,3)
		plt.scatter( cons_hh_firm_degree_in.flat, av_Cd.flat)
		plt.xlabel("degree of households")
		plt.ylabel("Mean Cd flow of households")		
		plt.subplot(nr,nc,4)
		plt.scatter( cons_hh_firm_AND_in.flat, av_Cd.flat)
		plt.xlabel("AND of households")
		plt.ylabel("Mean Cd flow of households")
		plt.show()
		# plot BY ITERATION
		# plot BY SECTOR
		# WARNING: the following works for Cd data, but returns meaningless results
		#scatter_plot_XY_by_sector(cons_hh_firm_AND_in,av_Cd,"AND_in","av_Cd")
		# => plot by sector is harder, because hh are connected to many firms
		#    that belong to different sectors
		"""
		# any relation between std and mu ? ????????????????????????
		# plot s_Cs = f(mu_Cs)
		it = 0
		plt.plot(mu_Cs[it,:], s_Cs[it,:])
		plt.show()
		plt.xlabel("s_Cs")
		plt.ylabel("mu_Cs")
		# plot FLATTENED (==merge all iterations)
		nr=5
		nc=2
		plt.subplot(nr,nc,1)
		plt.hist(av_Cs.flatten(),bins=30);
		plt.xlabel("av Cs")
		plt.subplot(nr,nc,2)
		plt.hist(av_Cd.flatten(),bins=30);
		plt.xlabel("av Cd")
		plt.subplot(nr,nc,3)	
		plt.hist(cons_hh_firm_degree_out.flatten());
		plt.xlabel("degree out")
		plt.subplot(nr,nc,4)
		plt.scatter( cons_hh_firm_degree_out.flat, av_Cd.flat)
		plt.xlabel("degree out")
		plt.ylabel("Mean Cd flow of hh")		
		plt.subplot(nr,nc,5)
		plt.hist(cons_hh_firm_degree_in.flatten());
		plt.xlabel("degree in")		
		plt.subplot(nr,nc,6)
		plt.scatter( cons_hh_firm_degree_in.flat, av_Cs.flat)
		plt.xlabel("degree in ")
		plt.ylabel("Mean Cs flow of firms")		
		plt.subplot(nr,nc,7)
		plt.hist(cons_hh_firm_AND_out.flatten()); 
		plt.xlabel("AND out of firms")
		plt.subplot(nr,nc,8)
		plt.hist(cons_hh_firm_AND_in.flatten()); 
		plt.xlabel("AND in of firms")
		plt.subplot(nr,nc,9)
		plt.scatter( cons_hh_firm_AND_out.flat, av_Cs.flat)
		plt.xlabel("AND out of households")
		plt.ylabel("Mean Cs flow of firms")
		plt.subplot(nr,nc,10)
		plt.scatter( cons_hh_firm_AND_in.flat, av_Cd.flat)
		plt.xlabel("AND in of households")
		plt.ylabel("Mean Cd flow of firms")
		plt.subplots_adjust(wspace=0.5, hspace=0.4)
		plt.show()
		# plot BY ITERATION
		#https://matplotlib.org/gallery/lines_bars_and_markers/scatter_demo2.html#sphx-glr-gallery-lines-bars-and-markers-scatter-demo2-py
		nr=2
		nc=2
		niter = mu_Cs.shape[0]
		dim = mu_Cs.shape[1]
		plt.subplot(nr,nc,1)
		for i in range(niter):
			plt.hist(mu_Cs[i,:]);
		plt.xlabel("Cs of firms")
		plt.subplot(nr,nc,2)
		for i in range(niter):
			plt.hist(cons_hh_firm_AND_out[i,:]); 
		plt.xlabel("AND of firms")
		plt.subplot(nr,nc,3)
		plt.scatter( cons_hh_firm_degree_in.flat, mu_Cs.flat,
					c= np.repeat(np.linspace(0,1,niter),dim), 
					s = 1E6 * s_Cs.flatten(), 	alpha=0.5)
					#color= np.repeat(['b','r'],dim))			
		plt.xlabel("degree of firms")
		plt.ylabel("Mean Cs flow of firms")	
		plt.subplot(nr,nc,4)
		plt.scatter( cons_hh_firm_AND_out.flat, mu_Cs.flat,
					c= np.repeat(np.linspace(0,1,niter),dim),
					s = 1E6 * s_Cs.flatten(), 	alpha=0.5
						 )
		plt.xlabel("AND of households")
		plt.ylabel("Mean Cs flow of firms")
		plt.show()"""
		###############
		# plot BY SECTOR
		nr=2
		nc=2
		fs=20
		# markers and labels
		sector_label = np.tile(L[firm_to_sector[0,:]],niter)
		markers=np.tile(M[firm_to_sector[0,:]],niter)	
		# mu_Cs with sector label
		plt.scatter(firm_to_sector.flat, av_Cs.flat)
		for l_s,i in zip(label_sector_agg, np.unique(firm_to_sector)):
			plt.annotate(l_s,xy=(i, 1.1* max(av_Cs.flat)),
						fontsize=fs,va="center", ha="center")
		ax = plt.gca()	
		ax.set_xticklabels([])	
		plt.ylabel('$E[C_s]$',fontsize=fs)
		plt.xlabel('sector',fontsize=fs)
		"""
		# calculer moy(moyennes), 
		# degree, mu_Cs
		plt.subplot(nr,nc,1)
		it=0
		title= 'AND of households'
		plot_kde_by_sector(cons_hh_firm_AND_out,title,it=0,bw_method='scott')
		plt.subplot(nr,nc,2)
		it=0
		title= 'Mean Cs flow of firms'
		plot_kde_by_sector(av_Cs,title,it=0,bw_method='scott')
		# with markers	+ colors
		scatter_plot_XY_by_sector(cons_hh_firm_degree_in, av_Cs, firm_to_sector,
									"degree of firms","Mean Cs flow of firms" )
		# AND, mu_Cs
		scatter_plot_XY_by_sector(cons_hh_firm_AND_out,av_Cs, firm_to_sector,
									"AND of households",
									"Mean Cs flow of firms"	)						
		##########
		# SYNTHESE A LA SEABORN: av/AND
			#https://matplotlib.org/api/_as_gen/matplotlib.gridspec.GridSpec.html#examples-using-matplotlib-gridspec-gridspec	
		#plt.subplot(nr,nc,1)
		#hist_by_sector(invest_degree_out,"out-degree of firms",isec=None,
		#				stacked=True,fill=False)
		hist_by_sector(invest_degree_out,"out-degree of firms",isec=None,
						histtype='bar',n_bins=10,fill=True)		
		ax=plt.gca(); ax.set_xticklabels([]);ax.set_yticklabels([])
		ax.set_xlim((np.min(invest_degree_out)-0.5,np.max(invest_degree_out)))			
		plt.subplot(gs[2])
		#plt.subplot(nr,nc,3)	
		scatter_plot_XY_by_sector(invest_degree_out,av_Is,"out-degree","average Is")	
		plt.plot(invest_degree_out.flatten(), res.fittedvalues, 'r--', label="OLS")
		ax=plt.gca();ax.set_xlim((np.min(invest_degree_out)-0.5,np.max(invest_degree_out)))
		ax.set_ylim((np.min(av_Is)-0.001,np.max(av_Is)))
		plt.subplot(gs[3])
		#plt.subplot(nr,nc,4)
		#hist_by_sector(av_Is,"av_Is",isec=None,orientation=u'horizontal',fill=False)
		hist_by_sector(av_Is,"av_Is",isec=None,orientation=u'horizontal',
			histtype='bar',n_bins=10,fill=True)
		ax=plt.gca();ax.set_ylim((np.min(av_Is)-0.001,np.max(av_Is)))
		ax.set_xticklabels([]);ax.set_yticklabels([])	
		plt.subplots_adjust(wspace=0.001, hspace=0.01)"""
	
def check_consistency_intermediate_consumption_firm():
	"""
	compare aggregated intermediate consumption of firm
	with empirical IO table used to build the network
	(for a given iteration)
	"""
	# aggregation
	# get IO table
	# comparison
	pass
def check_consistency_final_consumption_hh():
	"""
	"""
	# aggregation
	# get IO table
	# comparison
	pass
		
	
	
def plot_dataset_Is(r,plot_all=False):		
	"""	
	Example:
	%matplotlib
	r=read_dataset_sfc(groupname='/sfc_rndunif_bayesian')
	plot_dataset_Is(r)
	"""		
	######################################################
	# Is,Id,K  (firm / firm)
	# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# recalculer table IO empirique
	# comparer à IO
	#    get firm consumption supply
	#    NB: investment  Is  -Id
	mu=r['mu']; niter=r['niter']; nb,nf,nh=r['nb'],r['nf'],r['nh']
	cons_hh_firm_degree_out=r['cons_hh_firm_degree_out']
	cons_hh_firm_degree_in=r['cons_hh_firm_degree_in']
	firm_to_sector=r['firm_to_sector']
	sectors =r['sectors']
	nsec=r['nsec']
	L=r['L']
	M=['M']	
	invest_degree_in=r['invest_degree_in']
	invest_degree_out=r['invest_degree_out']
	invest_AND_in=r['invest_AND_in']
	invest_AND_out=r['invest_AND_out']
	
	av_Is=get_sum_iter(mu,niter,"Is",nb,nf,nh)
	av_Id=get_sum_iter(mu,niter,"Id",nb,nf,nh)

	# SYNTHESE SEABORN (à la jointplot): av_Is/out-degree
	#https://matplotlib.org/api/_as_gen/matplotlib.gridspec.GridSpec.html#examples-using-matplotlib-gridspec-gridspec
	gs = gridspec.GridSpec(2, 2,
	                       width_ratios=[3, 1], height_ratios=[1,3])
	plt.subplot(gs[0])
	#plt.subplot(nr,nc,1)
	#hist_by_sector(invest_degree_out,"out-degree of firms",isec=None,
	#				stacked=True,fill=False)
	hist_by_sector(invest_degree_out,"out-degree of firms",
					firm_to_sector,isec=None,
					histtype='bar',n_bins=10,fill=True)		
	ax=plt.gca(); ax.set_xticklabels([]);ax.set_yticklabels([])
	ax.set_xlim((np.min(invest_degree_out)-0.5,np.max(invest_degree_out)))			
	# scatter+regression
	plt.subplot(gs[2])	
	scatter_plot_XY_by_sector(invest_degree_out,av_Is,firm_to_sector,
							"out-degree","average Is")	
	#plt.plot(invest_degree_out[idx_lt4].flatten(), res_lt4.fittedvalues, 'r--', label="OLS")
	#plt.plot(invest_degree_out[idx_gt4].flatten(), res_gt4.fittedvalues, 'r--')
	ax=plt.gca();ax.set_xlim((np.min(invest_degree_out)-0.5,np.max(invest_degree_out)))
	ymin=np.min(av_Is)-0.001;ymax=np.max(av_Is)
	ax.set_ylim((ymin,ymax))
	plt.subplot(gs[3])
	#plt.subplot(nr,nc,4)
	#hist_by_sector(av_Is,"av_Is",isec=None,orientation=u'horizontal',fill=False)
	hist_by_sector(av_Is,"av_Is",firm_to_sector,isec=None,orientation=u'horizontal',
		histtype='bar',n_bins=10,fill=True,lim=[ymin,ymax])
	ax=plt.gca();ax.set_ylim((ymin,ymax))
	ax.set_xticklabels([]);ax.set_yticklabels([])	
	plt.subplots_adjust(wspace=0.001, hspace=0.01)
	
	if plot_all	:
		"""l = "K"
		idx=np.where(np.array(label_long)==l)[0]
		mu_K = mu[:,idx]
		s_K = sigma[:,idx]
		av_K = av[:,idx]
		va_K = va[:,idx]
		idx_bounds=np.where(np.array(p_fix['label_short'])==l)[0]
		lb_K = lb[:,idx_bounds]
		ub_K = ub[:,idx_bounds]	
		# plot all pdfs
		i=0
		mu=mu_K[0,:]
		s=s_K[0,:]
		av=av_K[0,:]
		va=va_K[0,:]
		lb=lb_K[0,:][0]*np.ones(nf)
		ub=ub_K[0,:][0]*np.ones(nf)
		f=plt.figure()
		for i in range(10):
			MetabolicEP_plot(i,mu,s,av,va,lb,ub, samplePts=None,ndots=300)
		plt.xlabel("K")
		plt.ylabel("pdf(K)")	
		# plot FLATTENED (==merge all iterations)	
		# plot BY ITERATION
		
		# plot BY SECTOR
		nr=2
		nc=2
		# any relation between std and mu ? ????????????????????????
		it = 0
		plt.scatter(av_Is[it,:], s_Is[it,:])
		plt.show()
		plt.ylabel("s_Is")
		plt.xlabel("mu_Is")
		"""
		# histograms
		plt.hist(av_Is.flatten(),30)
		hist_by_sector(av_Is,"av_Is",loc='lower right',isec=None)
		# mu_Is/av_Is by sector
		scatter_plot_by_sector(av_Is,firm_to_sector,niter,"av_Is of firms")
		#scatter_plot_by_sector(av_K,firm_to_sector,"av_K firms",loc='upper right')
		"""
		# AND by sector ???????????
		# kde: mu, AND by sector
		plt.subplot(nr,nc,1)
		it=0
		title= 'AND of firms'
		plot_kde_by_sector(invest_AND_out,title,it=0,bw_method='scott')	
		plt.subplot(nr,nc,2)
		title= 'Mean Is flow of firms'
		plot_kde_by_sector(mu_Is,title,it=0,bw_method='scott')	
		it=0
		"""
		# in-degree vs mu
		plt.subplot(nr,nc,3)
		scatter_plot_by_sector(invest_degree_out,firm_to_sector,
								"out-degree of firms")
																				
		scatter_plot_XY_by_sector(invest_degree_in,av_Is,firm_to_sector,"in-degree of firms",
									"av_Is flow of firms")
		# out-degree vs mu
		scatter_plot_XY_by_sector(invest_degree_out,av_Is,firm_to_sector,"out-degree of firms",
									"av_Is flow of firms")	

		plt.subplot(nr,nc,4)
		# AND vs mu	
		scatter_plot_XY_by_sector(invest_AND_out,av_Is,firm_to_sector,"AND_out of firms",
									"av_Is flow of firms")										 
		# mu_K vs mu_Is
		#scatter_plot_XY_by_sector(av_K,av_Is,"av_K flow of firms",	"av_Is flow of firms")								

										
		# independence tests							
		r1=test_independence(av_K,av_Is)
		r2=test_independence(np.asarray(invest_degree_out,dtype=float),av_Is)

def plot_dcGM_conservation_error():
	"""
	Cd= household demand, Cs=consumption supply by firms
	select specific flows: Cd  (firm / hh)
	extract from network.py:
	 * [H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
	 * [H18?] exponential graph model: bipartite undirected
	     get houshold consumption demand
	 
	Example: 
	plot_dataset_Cd_Cs(r)
	"""
	mu=r['mu']; niter=r['niter']; nb,nf,nh=r['nb'],r['nf'],r['nh']
	cons_hh_firm_degree_out=r['cons_hh_firm_degree_out']
	cons_hh_firm_degree_in=r['cons_hh_firm_degree_in']
	firm_to_sector=r['firm_to_sector']
	sectors =r['sectors']
	nsec=r['nsec']
	L=r['L']
	M=['M']

	av_Cd = get_sum_iter(mu,niter,"Cd",nb,nf,nh)
	av_Cs = get_sum_iter(mu,niter,"Cs",nb,nf,nh)
	"""					
	# compute dcGM for a specific subnetwork 
	# replace w_sub <- w_sub_dcgm in x
	# compute Ax-b, analyse error.
	"""
	

def plot_dataset_WB():		
	######################################################
	# select specific flows: wage bill  (hh/firm)
	"""
	WBd = mu_post[nh*nf+nf**2:nh*nf+nf**2+nf*nh]
	WBd=WBd.reshape((nf,nh))
	WBd.sum(axis=0)
	"""
	pass
	
def plot_dataset_IL():		
	######################################################
	# select specific flows: IL interest on loans  (bank/firm)
	pass

def plot_dataset_ID():		
	######################################################
	# select specific flows: ID interest on deposit  (bank/hh)
	"""
	IDd=IDd.reshape((nb,nh))
	IDd.sum(axis=0)	
	"""
	pass
