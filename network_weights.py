# -*- coding: utf-8 -*-
"""
Created on jun 2018

Module:
    network_weights - create datasets with sfc random network
						and solutions to linear systems
							
    
Author:
    Aurélien Hazan
    
Description:
    Implementation of ...
    
Usage:
    Be ``strn_in,strn_out`` two 1-dimensional NumPy arrays,. 
    Import the module and initialize the ... ::
        >>> import 
        >>> cs = ...
    To create ::
        >>> cs.do_smthg()

References:
.. [Saracco2015] `F. Saracco, R. Di Clemente, A. Gabrielli, T. Squartini,
    Randomizing bipartite networks: the case of the World Trade Web,
    Scientific Reports 5, 10595 (2015)
    <http://www.nature.com/articles/srep10595>`_   
   
    
"""

"""
	TODO: include har, EP, maxent.
"""
import os.path
import json
from scipy.sparse import diags
from scipy.sparse.linalg import inv
from network import *
from transmat import get_bmw_labels
import networkx as nx
#import altginv
import nls_solvers

fs = 20 # fontsize
plt.rcParams['font.size']=20
plt.rcParams['xtick.labelsize']='large'
plt.rcParams['ytick.labelsize']='large'

#------------------------------------------------------------------
def average_neighbor_degree_from_matrix(M):
	"""
	get ANN from numpy matrix
	the results of average neighbor degree can be sorted in multiple ways:
	
	sorted by DEGREE: https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.assortativity.average_degree_connectivity.html?highlight=nearest%20neighbor#networkx-algorithms-assortativity-average-degree-connectivity
	>$ANND_in_out=nx.average_degree_connectivity(g, source='in', target='out')
	sorted by NODE: https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.assortativity.average_neighbor_degree.html#networkx.algorithms.assortativity.average_neighbor_degree
	"""
	g,X,Y = from_numpy_matrix_bipartite(M)
	AND_x = nx.average_neighbor_degree(g, nodes=range(M.shape[0]))				
	AND_x= np.asarray(AND_x.items())
	AND_x= AND_x[ np.array(AND_x[:,0],dtype=int) ,1]  # sort
	AND_y = nx.average_neighbor_degree(g, nodes=range(M.shape[1]))				
	AND_y= np.asarray(AND_y.items())
	AND_y= AND_y[ np.array(AND_y[:,0],dtype=int) ,1] # sort
	return AND_x,AND_y	
#------------------------------------------------------------------
# SOLVE WEIGHTS: BAYESIAN

def solve_weights_bayesian(A,b,mu_prior,sig_prior):
	"""
	ref: Waldrip,Niven, Entropy 2017, 19, 58; doi:10.3390/e19020058
		eq. (33)
	
	mean: <X> = m + \Sigma A^T ( A \Sigma A^T)^{-1} (b-Am)
	
	A.shape = (m,n) with m=nb+nf+nh << n 
	Computing the posterior requires to invert an sparse (m,m) matrix.
	
	parameters:
	-----------
	A: array, shape=(m,n)
	matrix of the linear system of equations Ax=b  that represent the constraint.
		
	b: array, shape=(m,)
	rhs of linear system
	
	mu_prior: array, shape=(n,)
	prior mean.
	
	sig_prior: array, shape=(n,)
	prior diagonal covariance.
	
	output:
	-------
	mean_post:
	posterior mean
	
	cov_post:
	posterior covariance
	
	see:
	----
	sparse linalg: https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html#module-scipy.sparse.linalg
	inv sparse: https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.inv.html#scipy.sparse.linalg.inv
	sparse matrix-vector-product: https://docs.scipy.org/doc/scipy/reference/sparse.html#matrix-vector-product
	diags: https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.diags.html#scipy.sparse.diags
	"""
	if not sig_prior.ndim==1:
		raise ValueError("sig_prior.ndim!=1")
	if not mu_prior.ndim==1:
		raise ValueError("mu_prior.ndim!=1")	
	A=A.astype(float)
	b=b.astype(float)
	nr,nc = A.shape
	# convert to sparse matrix
	sig_prior_diag = diags(sig_prior)	
	Am=A.dot(mu_prior)
	b_minus_Am = b - Am.reshape(b.shape[0],1)
	if not b_minus_Am.shape[1]==1: raise ValueError("b_minus_Am.shape[1]!=1")
	sig_A_T= sig_prior_diag.dot(A.transpose())
	Inv = inv(A.dot(sig_A_T)) 
	mean_post = sig_A_T.dot( Inv )
	if not mean_post.shape==(nc,nr):raise ValueError("mean_post.shape!=(nc,nr)")
	mean_post = mean_post.dot(b_minus_Am) + mu_prior.reshape((nc,1))
	#cov
	cov_post = 0
	return mean_post,cov_post
	
def solve_weights_bayesian_sparse(A,b,threshold=None):
	"""
	sparse pseudoinverse: see altginv.py
	"""
	# compute sparse pseudoinverse of A
	B = altginv.altginv2(A,threshold=threshold)
	# return spinv(A)*b
	return B.dot(b)	
	
def solve_weights_bayesian_simple(A,b,mu_prior):
	"""
	!!DEPRECATED: if Sigma = sigma*eye(n,n), use least square solution !!
	!!https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.lsqr.html#scipy.sparse.linalg.lsqr
	!!scipy.sparse.linalg.lsqr(A, b)
	
	
	ref: Waldrip,Niven, Entropy 2017, 19, 58; doi:10.3390/e19020058
		eq. (33)
	if Sigma = sigma*eye(n,n)
	mean: <X> = m + A^T ( A  A^T)^{-1} (b-Am)
	
	A.shape = (m,n) with m=nb+nf+nh << n 
	Computing the posterior requires to invert an sparse (m,m) matrix.
	
	parameters:
	-----------
	A: array, shape=(m,n)
	matrix of the linear system of equations Ax=b  that represent the constraint.
		
	b: array, shape=(m,)
	rhs of linear system
	
	mu_prior: array, shape=(n,)
	prior mean.
	
	output:
	-------
	mean_post:
	posterior mean
	
	cov_post:
	posterior covariance
	
	see:
	----
	sparse linalg: https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html#module-scipy.sparse.linalg
	inv sparse: https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.inv.html#scipy.sparse.linalg.inv
	sparse matrix-vector-product: https://docs.scipy.org/doc/scipy/reference/sparse.html#matrix-vector-product
	diags: https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.diags.html#scipy.sparse.diags
	
	if not mu_prior.ndim==1:
		raise ValueError("mu_prior.ndim!=1")	
	A=A.astype(float)
	b=b.astype(float)
	nr,nc = A.shape
	# convert to sparse matrix
	Am=A.dot(mu_prior)
	b_minus_Am = b - Am.reshape(b.shape[0],1)
	if not b_minus_Am.shape[1]==1: raise ValueError("b_minus_Am.shape[1]!=1")
	Inv = inv(A.dot(A.transpose())) 
	mean_post = A.transpose().dot( Inv )
	if not mean_post.shape==(nc,nr):raise ValueError("mean_post.shape!=(nc,nr)")
	mean_post = mean_post.dot(b_minus_Am) + mu_prior.reshape((nc,1))
	#cov
	cov_post = 0
	return mean_post,cov_post
	"""	

def checksum_solution(mean_post,model="CSP_sfc_FiCM_noMLK"):
	"""
	CSP_sfc_FiCM_noMLK:
	mean_post = [Cd,Id,WBd,ILd,IDd]
	"""
	if model=="CSP_sfc_FiCM_noMLK":
		pass
	else:
		raise ValueError('model not supported')	
	
def test_bayesian_plot_weighted_network_sfc_FiCM_noMLK(): 
	"""
	plot weights
	
	economic model = sfc
	network model=block
	method to solve weight = bayesian
	"""	
	# get network
	r = example_init_network_block(model="sfc_FiCM_noMLK")
	# params
	p=r['p']
	nb=p['network']['nb']; nf=p['network']['nf']; nh=p['network']['nh']
	A,B,C,D,E=r['A'],r['B'],r['C'],r['D'],r['E']
	F,g,lb,ub,mu_prior=r['F'],r['g'],r['lb'],r['ub'],r['prior']
	nc = F.shape[1]
	mu_prior = 10.*np.asarray(mu_prior.todense()).flatten()
	#sig_prior = 0.1*self.mu_prior
	# bayesian solution for weights 
	mu_post,cov_post = solve_weights_bayesian_simple(F,g,mu_prior)
	r['mu_prior'] = mu_prior
	r['mu_post']= mean_post
	plot_weighted_network_sfc_FiCM_noMLK(r)
	
		
def plot_weighted_network_sfc_FiCM_noMLK(r):
	"""
	economic model = sfc
	network model= * 
	method to solve weight = *
	"""
	# get params
	p=r['p']
	nb=p['network']['nb']; nf=p['network']['nf']; nh=p['network']['nh']
	mu_prior= r['mu_prior'] 
	mu_post= r['mu_post']
	F,g=r['F'],r['g']
	# residual
	res = F.dot(mu_post)	-g	
	plt.plot(mu_post)
	plt.xlabel('posterior')
	plt.ylabel('residual')
	#-budget hh
	nr = 4; nc=2
	plt.subplot(nr,nc,1)
	Cd = mu_post[0:nh*nf]
	plt.plot(Cd); plt.ylabel('Cd');		
	plt.subplot(nr,nc,2)
	Cd=Cd.reshape((nf,nh))
	plt.matshow(Cd,cmap="binary",fignum=False, aspect="auto")
	plt.ylabel('firms');
	plt.subplot(nr,nc,3)
	WBd = mu_post[nh*nf+nf**2:nh*nf+nf**2+nf*nh]
	plt.plot(WBd); plt.ylabel('WBd');			
	plt.subplot(nr,nc,4)
	WBd=WBd.reshape((nf,nh))
	plt.matshow(WBd,cmap="binary",fignum=False, aspect="auto")
	plt.ylabel('firms');
	plt.subplot(nr,nc,5)
	IDd = mu_post[nf*nh+nf**2+nf*nh+nf*nb:]
	plt.plot(IDd); plt.ylabel('IDd');		
	plt.subplot(nr,nc,6)
	IDd=IDd.reshape((nb,nh))
	plt.matshow(IDd,cmap="binary",fignum=False, aspect="auto")
	plt.ylabel('banks');
	plt.xlabel('hh')
	#plt.colorbar(orientation="horizontal")
	plt.subplot(nr,nc,7)
	residual = -Cd.sum(axis=0) + WBd.sum(axis=0)+IDd.sum(axis=0)
	plt.plot(range(nh),np.array(residual).flatten());	plt.ylabel('residual');
	plt.xlabel('hh')
	plt.subplot(nr,nc,8)
	x = np.vstack((WBd.sum(axis=0), IDd.sum(axis=0),Cd.sum(axis=0)  ))
	plt.matshow(x,cmap="binary",fignum=False, aspect="auto")
	plt.ylabel('budget')
	plt.colorbar(orientation="horizontal")
	ax=plt.gca()
	ax.set_yticklabels(['','WBs(+)','IDs(+)','Cd(-)'])
	plt.show()
	#-budget firms
	Cs=Cd.sum(axis=1)
	WBd_sum=WBd.sum(axis=1)
	Id = mu_post[nh*nf:nh*nf+nf**2]
	Is = Id.reshape((nf,nf))
	Is=Is.sum(axis=1)
	ILd=mu_post[nf*nh+nf**2 + nf*nh: nf*nh+nf**2 + nf*nh+nf*nb]
	ILd=ILd.reshape((nb,nf))
	ILd_sum=ILd.sum(axis=0)
	residual = Cs.flatten() -WBd_sum.flatten() + Is.flatten() - np.array(ILd_sum).flatten()
	nplot=2
	plt.subplot(nplot,1,1)
	plt.plot(range(nf),np.array(residual).flatten())
	plt.ylabel('residual');
	plt.xlabel('firms')
	plt.subplot(nplot,1,2)
	x = np.vstack((Cs.flatten() , Is.flatten(),WBd_sum.flatten(), np.array(ILd_sum).flatten() ))
	plt.matshow(x,cmap="binary",fignum=False, aspect="auto")
	plt.ylabel('budget')
	ax=plt.gca()
	ax.set_yticklabels(['','Cs(+)','Is(+)','WBd(-)','ILd(-)'])
	plt.xlabel('firm')
	plt.colorbar(orientation="horizontal")
	plt.show()
	#-budget banks
		
	# holo/bokeh: 
	#   get balance sheet per agent type with slider
	#   hh, firms: barplot consumptions 
	# plot weighted network

# -----------------------------------------------------
# CREATE DATASET 

def make_dataset_sfc_block_bayesian(path_fname_in = 'data/eurostat_network.h5',
									path_fname_out = 'data/topo_vs_flow_sfc.h5'):
	groupname='/sfc_block_bayesian'
	nb=3;nf=50;nh=500 
	p_fix = {'fname_eurostat_in':path_fname_in,
			'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,  
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 			
			'algo_weights':'bayesian', 'proxy_invest_conso':True,
			'network_model':'block',
			'sfc_model':"noMLK_alpha0"	}
	p_var  = {'year':['2010'], 'geo':['CZ'], 'niter':10,
			'alpha0':.5	}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset_sfc(path_fname_out,groupname,p)

def make_dataset_sfc_rndunif_bayesian(path_fname_in = 'data/eurostat_network.h5',
									path_fname_out = 'data/topo_vs_flow_sfc.h5'):
	groupname='/sfc_rndunif_bayesian'
	nb=3;nf=50;nh=500 
	p_fix = {'fname_eurostat_in':path_fname_in,
			'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,  
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 
			'nlink_per_hh_wage':1,			
			'algo_weights':'bayesian',#{'bayesian','sparse'}
			'proxy_invest_conso':True,
			'network_model':'rndfitness_unif',
			'sfc_model':"noMLK_alpha0"}
	p_var  = {'year':['2010'], 'geo':['CZ'], 'niter':5,
			 'alpha0':10.}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset_sfc(path_fname_out,groupname,p)
	
	
def make_dataset_sfc_rndunif_sparse(path_fname_in = 'data/eurostat_network.h5',
									path_fname_out = 'data/topo_vs_flow_sfc.h5'):
	groupname='/sfc_rndunif_sparse'
	nb=3;nf=50;nh=150 
	p_fix = {'fname_eurostat_in':path_fname_in,
			'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,  
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 
			'nlink_per_hh_wage':1,			
			'algo_weights':'sparse',#{'bayesian','sparse'}
			'algo_sparse_threshold':0.001,
			'proxy_invest_conso':True,
			'network_model':'rndfitness_unif',
			'sfc_model':"noMLK_alpha0"}
	p_var  = {'year':['2010'], 'geo':['CZ'], 'niter':2,
			 'alpha0':10.}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset_sfc(path_fname_out,groupname,p)	
	
def make_dataset_sfc_rndunif_leastnorm_pos(path_fname_in = 'data/eurostat_network.h5',
									path_fname_out = 'data/topo_vs_flow_sfc.h5'):
	groupname='/sfc_rndunif_leastnorm_pos'
	nb=3;nf=50;nh=500 	
	p_fix = {'fname_eurostat_in':path_fname_in,
			'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,#!!!!!instead of 2, to avoid sparse investment
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 
			'nlink_per_hh_wage':1,			
			'algo_weights':'leastnorm_pos',#{'bayesian','sparse','leastnorm_pos'}
			'algo_weights_param':1, #norm
			'algo_weights_sparsity':None,#0.5
			'proxy_invest_conso':True,
			'network_model':'rndfitness_unif',
			'sfc_model':"noMLK_alpha0"}
	p_var  = {'year':['2010'], 'geo':['CZ'], 'niter':5,
			 'alpha0':0.1}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset_sfc(path_fname_out,groupname,p)		
	
def make_dataset_sfc_rndunif_nnls(path_fname_in = 'data/eurostat_network.h5',
									path_fname_out = 'data/topo_vs_flow_sfc.h5'):
	groupname='/sfc_rndunif_nnls'
	nb=3;nf=100;nh=1000 	
	p_fix = {'fname_eurostat_in':path_fname_in,
			'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,#!!!!!instead of 2, to avoid sparse investment
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 
			'nlink_per_hh_wage':1,			
			'algo_weights':'nnls',#{'nnls','bayesian','sparse','leastnorm_pos'}
			#'algo_weights_param':None, 
			'proxy_invest_conso':True,
			'network_model':'rndfitness_unif',
			'sfc_model':"noMLK_alpha0"
			#'sfc_model':"noMLK"
			}
	p_var  = {'year':['2010'], 'geo':['CZ'], 'niter':10,
			 'alpha0':1.}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset_sfc(path_fname_out,groupname,p)		
	

def make_dataset_bmw_rndfitness_bayesian():	
	"""
	economic model = bmw
	network model = rndfitness
	
	"""
	raise NotImplementedError	
	path_fname_out = 'data/topo_vs_flow_bmw.h5'
	groupname='/rnd_fitness_small_investfirm_L_cons_hh_20'
	maxiterEP = 1000
	nb=3;nf=50;nh=300 

	p_fix = {'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,  
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 
			'nlink_wage': 1,# rename:'nlink_per_hh_wage'
			'algo_weights':'bayesian', 'proxy_invest_conso':True,
			'network_model':'rndfitness','fitness_param':1.5}
	p_var  = {'year':['2010'], 'geo':['CZ'], 
		'r': [0.03],'alpha0':[0.1] , 'alpha1':[0.75],'alpha2':[0.01],
		'delta' :[0.1],'k':[6], 'M':[10.] ,'niter':5}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset(path_fname_out,groupname,p)

	
def make_dataset_bmw_block_bayesian():
	raise NotImplementedError
	path_fname_out = 'data/topo_vs_flow_bmw.h5'
	groupname='/small_investfirm_L_cons_hh_20'
	#groupname='/small_investfirm'
	maxiterEP = 1000#!
	nb=3;nf=50;nh=300 

	p_fix = {'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,  
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 			
			'algo_weights':'bayesian', 'proxy_invest_conso':True,
			'network_model':'block','fitness_param':1.5}
	p_var  = {'year':['2010'], 'geo':['CZ'], 
		'r': [0.03],'alpha0':[0.1] , 'alpha1':[0.75],'alpha2':[0.01],
		'delta' :[0.1],'k':[6], 'M':[10.] ,'niter':5}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset(path_fname_out,groupname,p)

def make_dataset_bmw_block():
	raise NotImplementedError
	path_fname_out = 'data/topo_vs_flow_bmw.h5'
	groupname='/small_investfirm_L_cons_hh_20'
	#groupname='/small_investfirm'
	maxiterEP = 1000#!
	nb=3;nf=50;nh=300 

	p_fix = {'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,  
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20, 			
			'algo_weights':'bayesian', 'proxy_invest_conso':True,
			'network_model':'block','fitness_param':1.5}
	p_var  = {'year':['2010'], 'geo':['CZ'], 
		'r': [0.03],'alpha0':[0.1] , 'alpha1':[0.75],'alpha2':[0.01],
		'delta' :[0.1],'k':[6], 'M':[10.] ,'niter':5}		
	p={'fix':p_fix, 'var':p_var}	
	make_dataset(path_fname_out,groupname,p)


	
def make_dataset_bmw(path_fname_out,groupname,p):
	"""
	economic model = bmw
	
	NB: rnd_fitness init can fail => must repeat
	"""	
	# constant params
	repeat = 10
	p_fix=p['fix']
	p_var=p['var']
	maxiterEP = p_fix['maxiterEP']
	nb=p_fix['nb']
	nf=p_fix['nf']
	nh=p_fix['nh']
				
	L_invest= int(p_fix['nlink_per_firm_invest'] *nf)
	L_cons_firm=int(p_fix['nlink_per_firm_cons']*nf)
	L_cons_hh = int(p_fix['nlink_per_hh_cons'] *nh)
	L_wage = None
	proxy_invest_conso = p_fix['proxy_invest_conso']
	label_short,label_long = get_bmw_labels(nb,nf,nh)
	nflow_short=len(label_short)
	nflow_long=len(label_long)	
	p_fix['label_short']=label_short
	p_fix['label_long']=label_long
	fitness_param = None
	if p_fix['network_model']=='rndfitness':
		fitness_param=p_fix['fitness_param']
		L_wage = int(p_fix['nlink_wage'] *nh)

	# create index and dataframe 
	iterables = [p_var['year'],p_var['geo'],  p_var['r'],
				p_var['alpha0'],p_var['alpha1'] ,p_var['alpha2'],
				p_var['delta'],p_var['k'],p_var['M'], range(p_var['niter'])]
	idx=pd.MultiIndex.from_product(iterables,
							names=['year','geo','r','alpha0','alpha1','alpha2',
							'delta','k','M','niter'])
	df = pd.DataFrame({
					   'mu': [np.zeros(nflow_long) for i in range(len(idx))],
					   'sigma': [np.zeros(nflow_long) for i in range(len(idx))],
					   'av': [np.zeros(nflow_long) for i in range(len(idx))],
					   'va': [np.zeros(nflow_long) for i in range(len(idx))],
					   'ub': [np.zeros(nflow_short) for i in range(len(idx))],
					   'cons_hh_firm_degree_in':  [np.zeros(nh) for i in range(len(idx))],
					   'cons_hh_firm_degree_out':  [np.zeros(nf) for i in range(len(idx))],
					   'cons_hh_firm_AND_in':  [np.zeros(nh) for i in range(len(idx))],
					   'cons_hh_firm_AND_out':  [np.zeros(nf) for i in range(len(idx))],				   
					   'invest_degree_in':  [np.zeros(nf) for i in range(len(idx))],
					   'invest_degree_out':  [np.zeros(nf) for i in range(len(idx))],
					   'invest_AND_out':  [np.zeros(nf) for i in range(len(idx))],
					   'invest_AND_in':  [np.zeros(nf) for i in range(len(idx))],				    
					   'firm_to_sector': [np.zeros(nf) for i in range(len(idx))]
					   },index=idx)
	
	# check file already exists
	if(os.path.exists(path_fname_out)): 	# ALSO CHECK GROUP EXISTS
		#raise IOError('file already exists')
		store = pd.HDFStore(path_fname_out)
		try:
			store.select(groupname+'/dat')
			ans = input("overwrite?")
			if ans=='y': store.remove(groupname+'/dat')
			else:
				store.close()
				raise IOError('file already exists')
		except KeyError:
			pass	
		store.close()
	
	# store config
	p_fix_str = pd.Series(json.dumps(p_fix))
	p_fix_str.to_hdf(path_fname_out,groupname+'/config/'+'p_fix')
	p_var_str = pd.Series(json.dumps(p_var))
	p_var_str.to_hdf(path_fname_out,groupname+'/config/'+'p_var')
	
	# loop through index	
	record_F_done=0				
	for (year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it) in df.index:	
		p  = {'year':year, 'geo':geo, 'nb':nb, 'nf':nf, 'nh':nh,'r': r,
			'alpha0': alpha0, 'alpha1':alpha1,'alpha2':alpha2,
			'delta' : delta,'k':k, 'M':M ,'niter': 0}
		init_network_done = 0	
		# setup	block
		#e = init_EP_network_block(year,geo,nb,nf,nh,L_invest,L_cons_firm,L_cons_hh)
		e = EP_network(p)
		e.init()
		if p_fix['network_model']=='block':
			e.init_block_network(year,geo,nb,nf,nh,L_invest,L_cons_firm,L_cons_hh,proxy_invest_conso)		
			init_network_done=1
		elif p_fix['network_model']=='rndfitness':
			for i in range(repeat):
				try:
					e.init_rndfitness_network(year,geo,nb,nf,nh,L_invest,L_cons_firm,L_cons_hh,L_wage,fitness_param,proxy_invest_conso)
					init_network_done=1
				except julia.core.JuliaError:
					print "!!!!!PyError!!!!! i=%d"%(i)
				if init_network_done: break			
		else:
			raise ValueError('network_model not recognized')	
		# sample a full network
		if init_network_done:
			mu,s,av,va,ub_long,A,B,C,D,E,FF,F,g,firm_to_sector = e.run_network(nb,nf,nh,r,alpha0,alpha1,alpha2,delta,k,M,maxiter=maxiterEP)
		else: 
			raise ValueError('network could not be initialized')	
		# Assortativity vs flow		
		AND_out_A,AND_in_A = average_neighbor_degree_from_matrix(A)
		AND_out_B,AND_in_B = average_neighbor_degree_from_matrix(B)
		"""
		# Degree vs flow
		# firms: nb of buyers (among firms AND hh); sales (invest); sales (custom)
		# firms: nb of banks; amount of interest
		# hh: nb of suppliers, total amount of consumption
		# banks: nb of customers (firms AND hh), total amount of loans per category
		"""
		# to df	
		#self.df.to_hdf(self.path_fname_out,groupname)
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].mu = mu
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].sigma = s
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].av = av
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].va = va
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].ub = e._get_short_ub(ub_long)
		# A: household consumption
		# [H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].cons_hh_firm_degree_in = A.sum(axis=0) 
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].cons_hh_firm_degree_out = A.sum(axis=1) 
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].cons_hh_firm_AND_in = AND_in_A
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].cons_hh_firm_AND_out = AND_out_A
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].firm_to_sector=firm_to_sector 
		# B: firm investments
		if 	p_fix['proxy_invest_conso']:
			# see network.py:init_network_block
			#               :init_consumption_firm_network_block "dij[i,j] : float, increases with the propensity of firms of sector j buying intermediate goods from firms of sector i "
			df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_degree_in = B.sum(axis=0) # check definition above.
			df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_degree_out = B.sum(axis=1)
		else:	
			# [H17] "B_{nf,nf}[i,j]= 1 if the firm j is selling capital goods to the firm i."
			df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_degree_in = B.sum(axis=1) 
			df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_degree_out = B.sum(axis=0)
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_AND_in = AND_in_B
		df.loc[(year,geo,r,alpha0,alpha1,alpha2,delta,k,M,it)].invest_AND_out = AND_out_B
		# to hdf
		df.to_hdf(path_fname_out,groupname+'/data')
		if not record_F_done:
			df_F = pd.DataFrame(F)
			df_F.to_hdf(path_fname_out,groupname+'/F')
			record_F_done=1
		# sauvegarder les fitness une fois au moins !!!!!!!
		# !!!!!!!!!!!!!!!!!!!!!

def make_dataset_sfc(path_fname_out,groupname,p):
	"""
	economic model = sfc bare model (no stocks MLK, no behavior)
	network model= * 
	method to solve weights = *
	
	NB: rnd_fitness init can fail => must repeat
	
	TODO: 
	sparse storage !!!!!!
			pandas + sparse => not adapted: http://pandas.pydata.org/pandas-docs/stable/sparse.html?highlight=sparse#interaction-with-scipy-sparse
			 scipy.sparse.save_npz https://stackoverflow.com/questions/8955448/save-load-scipy-sparse-csr-matrix-in-portable-data-format
			 lsqr: https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.lsqr.html#scipy.sparse.linalg.lsqr

	label_short
	df_fix
	
	"""	
	# constant params
	repeat = 20 # if network init fails
	p_fix=p['fix']
	p_var=p['var']
	fname_eurostat_in = p_fix['fname_eurostat_in']
	algo_weights= p_fix['algo_weights'] #{'bayesian'}
	try:
		algo_weights_param= p_fix['algo_weights_param']
	except: pass	
	try:
		algo_sparse_threshold = p_fix['algo_sparse_threshold']					
	except: pass
	network_model = p_fix['network_model']#{'block','rndfitness_unif','rndfitness_pareto'}
	sfc_model=p_fix['sfc_model']#{'noMLK','noMLK_alpha0'}
	nb=p_fix['nb']
	nf=p_fix['nf']
	nh=p_fix['nh']
	
	L_invest= int(p_fix['nlink_per_firm_invest'] *nf)
	L_cons_firm=int(p_fix['nlink_per_firm_cons']*nf)
	L_cons_hh = int(p_fix['nlink_per_hh_cons'] *nh)
	L_wage = None
	proxy_invest_conso = p_fix['proxy_invest_conso']

	nflow_long=nf*nh + nf**2 + nf*nh + nb*nf+ nb*nh
	p_fix['label_short']=['Cd','Id','WBd','ILd','IDd'] # get this from transmat

	fitness_param = None
	if p_fix['network_model']=='rndfitness_pareto':
		fitness_param=p_fix['fitness_param']
	if p_fix['network_model'][0:10]=='rndfitness':	
		L_wage = int(p_fix['nlink_per_hh_wage']*nh)	
	# p_var
	alpha0	= p_var['alpha0']		 # !!!!!!!!!!!!CHANGE THIS

	# create index and dataframe 
	iterables = [p_var['year'],p_var['geo'], range(p_var['niter'])]
	idx=pd.MultiIndex.from_product(iterables,
							names=['year','geo','niter'])
	df = pd.DataFrame({
					   'mu': [np.zeros(nflow_long) for i in range(len(idx))],
					  # 'sigma': [np.zeros(nflow_long) for i in range(len(idx))],
					   'cons_hh_firm_degree_in':  [np.zeros(nh) for i in range(len(idx))],
					   'cons_hh_firm_degree_out':  [np.zeros(nf) for i in range(len(idx))],
					   'cons_hh_firm_AND_in':  [np.zeros(nh) for i in range(len(idx))],
					   'cons_hh_firm_AND_out':  [np.zeros(nf) for i in range(len(idx))],				   
					   'invest_degree_in':  [np.zeros(nf) for i in range(len(idx))],
					   'invest_degree_out':  [np.zeros(nf) for i in range(len(idx))],
					   'invest_AND_out':  [np.zeros(nf) for i in range(len(idx))],
					   'invest_AND_in':  [np.zeros(nf) for i in range(len(idx))],				    
					   'firm_to_sector': [np.zeros(nf) for i in range(len(idx))]
					   },index=idx)
	# from transmat.py:CSP_sfc_FiCM_noMLK()			   
	nc_= nflow_long
	nr_=nb+nf+nh
	"""	   TODO!
	df_fix = pd.DataFrame({
						'mu_prior': scipy.sparse.lil_matrix((nc_,1),dtype='int8') ,
					   'sigma_prior': scipy.sparse.lil_matrix((nc_,1),dtype='int8'),
					   'F': scipy.sparse.lil_matrix((nr_,nc_),dtype='int8'),
					   'fitness': np.zeros(nf)
					   },index=idx)				   
	"""				   
	
	# check file already exists
	"""
	if(os.path.exists(path_fname_out)): 	# ALSO CHECK GROUP EXISTS
		#raise IOError('file already exists')
		store = pd.HDFStore(path_fname_out)
		try:
			store.select(groupname+'/dat')
			ans = input("overwrite?")
			if ans=='y': store.remove(groupname+'/dat')
			else:
				store.close()
				raise IOError('file already exists')
		except KeyError:
			pass	
		store.close()
	"""
	# store config
	p_fix_str = pd.Series(json.dumps(p_fix))
	#p_fix_str.to_hdf(path_fname_out,groupname+'/config/'+'p_fix')
	p_fix_str.to_hdf(path_fname_out,groupname+'/p_fix')
	p_var_str = pd.Series(json.dumps(p_var))
	#p_var_str.to_hdf(path_fname_out,groupname+'/config/'+'p_var')
	p_var_str.to_hdf(path_fname_out,groupname+'/p_var')
	
	# loop through index	
	record_oneshot_done=0				
	for (year,geo,it) in df.index:	
		p  = {'year':year, 'geo':geo, 'nb':nb, 'nf':nf, 'nh':nh,
				'niter': 0}
		init_network_done = 0	
		# init network
		if network_model=='block':
			n=init_network_block(year=year,geo=geo,nb=nb,nf=nf,nh=nh,
								fname=fname_eurostat_in,L_invest=L_invest, 
								L_cons_firm=L_cons_firm , 
								L_cons_hh = L_cons_hh,
								proxy_invest_conso=proxy_invest_conso)			
			init_network_done=1		
		elif network_model=='rndfitness_unif':	
			for i in range(repeat):
				fitness = np.random.rand(nf)
				try:						
					n=init_network_rndfitness(year=year,geo=geo,nb=nb,nf=nf,nh=nh,
							fname=fname_eurostat_in,
							L_invest=L_invest, L_cons_firm= L_cons_firm,
							L_cons_hh = L_cons_hh,
							L_wage= L_wage,
							fitness=fitness,proxy_invest_conso=proxy_invest_conso)
					init_network_done=1
				except :
					print "error during init net"
				if init_network_done: break					
		else:
			raise ValueError('network_model not recognized')	
		# sample
		A,B,C,D,E,FF = n.sample_full_network(nb,nf,nh)
		F,g,lb,ub,mu_prior=0,0,0,0,0
		if sfc_model=="noMLK":
			F,g,lb,ub,mu_prior = transmat.CSP_sfc_FiCM_noMLK(nb,nf,nh,A,B,C,D,E)
		elif sfc_model=="noMLK_alpha0":	
			F,g,lb,ub,mu_prior = transmat.CSP_sfc_FiCM_noMLK_alpha0(nb,nf,nh,A,B,C,D,E,
										alpha0=alpha0)	
		else: 	
			raise ValueError('unknown sfc_model')							
		mu_prior = 1*np.asarray(mu_prior.todense()).flatten()		
		# solve weights
		if init_network_done:
			if algo_weights=='bayesian':				
				#mu_post,cov_post = solve_weights_bayesian_simple(F,g,mu_prior)
				if(isinstance(g,np.ndarray)):
					mu_post = scipy.sparse.linalg.lsqr(F, g)[0]	
				else:	
					mu_post = scipy.sparse.linalg.lsqr(F, g.todense())[0]
				#print "mu_post",mu_post
			elif algo_weights=="sparse": 
				mu_post = solve_weights_bayesian_sparse(F,g,
										threshold=algo_sparse_threshold)
			elif algo_weights=='leastnorm_pos':
				mu_post = altginv.solve_leastnorm_pos(F,g,
						norm=algo_weights_param,sparsity=p_fix['algo_weights_sparsity'])										
			elif algo_weights=='nnls':					
				#gg = scipy.sparse.csr_matrix(g) #g.reshape((1,g.shape[0]))
				#print "F.shape, gg.shape=", F.shape, gg.shape
				#mu_post, _ = nls_solvers.nls_activeset(scipy.sparse.csr_matrix(F),gg)
				nnls = nls_solvers.LbfgsNNLS()
				nnls.fit(F,g)
				mu_post= nnls.coef_
			else:
				raise ValueError('unknown algo_weights')
		else: 
			raise ValueError('network init not done')	
		# Assortativity vs flow		
		AND_out_A,AND_in_A = average_neighbor_degree_from_matrix(A)
		AND_out_B,AND_in_B = average_neighbor_degree_from_matrix(B)
		"""
		# Degree vs flow
		# firms: nb of buyers (among firms AND hh); sales (invest); sales (custom)
		# firms: nb of banks; amount of interest
		# hh: nb of suppliers, total amount of consumption
		# banks: nb of customers (firms AND hh), total amount of loans per category
		"""
		# to df	
		#self.df.to_hdf(self.path_fname_out,groupname)
		df.loc[(year,geo,it)].mu = np.array(mu_post).flatten()
		#df.loc[(year,geo,it)].sigma = sigma
		df.loc[(year,geo,it)].ub = ub #e._get_short_ub(ub_long)
		# A: household consumption
		# [H17] p.5 "A_{nf,nh}[i,j]= 1 if the firm i is selling consumption goods to the household j"
		df.loc[(year,geo,it)].cons_hh_firm_degree_in = A.sum(axis=0) 
		df.loc[(year,geo,it)].cons_hh_firm_degree_out = A.sum(axis=1) 
		df.loc[(year,geo,it)].cons_hh_firm_AND_in = AND_in_A
		df.loc[(year,geo,it)].cons_hh_firm_AND_out = AND_out_A
		df.loc[(year,geo,it)].firm_to_sector=n.result_firm_to_sector 
		# B: firm investments
		if 	p_fix['proxy_invest_conso']:
			# see network.py:init_network_block
			#               :init_consumption_firm_network_block "dij[i,j] : float, increases with the propensity of firms of sector j buying intermediate goods from firms of sector i "
			df.loc[(year,geo,it)].invest_degree_in = B.sum(axis=0) # check definition above.
			df.loc[(year,geo,it)].invest_degree_out = B.sum(axis=1)
		else:	
			# [H17] "B_{nf,nf}[i,j]= 1 if the firm j is selling capital goods to the firm i."
			df.loc[(year,geo,it)].invest_degree_in = B.sum(axis=1) 
			df.loc[(year,geo,it)].invest_degree_out = B.sum(axis=0)
		df.loc[(year,geo,it)].invest_AND_in = AND_in_B
		df.loc[(year,geo,it)].invest_AND_out = AND_out_B
		# to hdf
		df.to_hdf(path_fname_out,groupname+'/data')
		if not record_oneshot_done:
			"""df_fix.F = F
			df_fix.mu_prior = mu_prior
			df_fix.sigma_prior = sigma_prior
			df_fix.fitness= ?
			df_fix.to_hdf(path_fname_out,groupname+'/data_fix')"""
			record_oneshot_done=1
		

def average_error_dcGM_nnls_bayes(niter=100,outfile = 'data/avg_error_dcGM.npz'):
	"""
	iterate error comparison done in make_dataset_error_dcGM_nnls_bayes()
	
	usage:
	------
	average_error_dcGM_nnls_bayes(outfile =outfile)
	
	npzfile = np.load(outfile)
	err_list = npzfile['err_list']
	neg_list = npzfile['neg_list']
	runtime_error = npzfile['runtime_error']
	idx = runtime_error==0
	err_list[idx,:].mean(axis=0)
	neg_list[idx,:].mean(axis=0)
	
	Refs:
	https://docs.scipy.org/doc/numpy/reference/generated/numpy.savez.html#numpy.savez
	
	"""
	label_dcgm =  'WBd_WBs'
	err_list=[]; neg_list=[]; runtime_error=[]
	for i in range(niter):
		try:
			e, n=make_dataset_error_dcGM_nnls_bayes(label_dcgm)
			err_list.append(e)
			neg_list.append(n)
			runtime_error.append(0)
		except RuntimeError:
			runtime_error.append(1)
	err_list = np.array(err_list)	
	neg_list = np.array(neg_list)
	runtime_error = np.array(runtime_error)
	np.savez(outfile, err_list=err_list, neg_list=neg_list,
					runtime_error=runtime_error)

def make_dataset_error_dcGM_nnls_bayes(	label_dcgm =  'WBd_WBs',
								path_fname_in = 'data/eurostat_network.h5',
								path_fname_out = 'data/topo_vs_flow_sfc.h5',
								plot=False):
	"""
	compare error for 3 network reconstruction ensembles:
	-dcGM  (degree-corrected gravity model)
	-nnls  (nonlinear least-square)
	-bayes (bayesian)
	
	Refs:
	
	"""
	# create ficm
	#groupname='/sfc_rndunif_nnls'
	nb=3;nf=100;nh=1000 
	p_fix = {'fname_eurostat_in':path_fname_in,
			'nb':nb, 'nf':nf, 'nh':nh, 'nlink_per_firm_cons': 2,#!!!!!instead of 2, to avoid sparse investment
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 5, 
			'nlink_per_hh_wage':1,			
			'algo_weights':'nnls',#{'nnls','bayesian','sparse','leastnorm_pos'}
			#'algo_weights_param':None, 
			'proxy_invest_conso':True,
			'network_model':'rndfitness_unif',
			'sfc_model':"noMLK_alpha0"
			#'sfc_model':"noMLK"
			}
	p_var  = {'year':['2010'], 'geo':['CZ'], 'niter':10,
			 'alpha0':1.}		
	p={'fix':p_fix, 'var':p_var}	
	nb=p_fix['nb']
	nf=p_fix['nf']
	nh=p_fix['nh']
	
	L_invest= int(p_fix['nlink_per_firm_invest'] *nf)
	L_cons_firm=int(p_fix['nlink_per_firm_cons']*nf)
	L_cons_hh = int(p_fix['nlink_per_hh_cons'] *nh)
	L_wage = None
	if p_fix['network_model'][0:10]=='rndfitness':	
		L_wage = int(p_fix['nlink_per_hh_wage']*nh)
	proxy_invest_conso = p_fix['proxy_invest_conso']
	
	fitness = np.random.rand(nf)
	year = p_var['year'][0]
	geo = p_var['geo'][0]
	n=init_network_rndfitness(year=year,geo=geo,nb=nb,nf=nf,nh=nh,
							fname=path_fname_in,
							L_invest=L_invest, L_cons_firm= L_cons_firm,
							L_cons_hh = L_cons_hh,
							L_wage= L_wage,
							fitness=fitness,proxy_invest_conso=proxy_invest_conso)
	# sample
	sfc_model=p_fix['sfc_model']
	alpha0	= p_var['alpha0']
	A,B,C,D,E,FF = n.sample_full_network(nb,nf,nh)
	F,g,lb,ub,mu_prior=0,0,0,0,0
	if sfc_model=="noMLK":
		F,g,lb,ub,mu_prior = transmat.CSP_sfc_FiCM_noMLK(nb,nf,nh,A,B,C,D,E)
	elif sfc_model=="noMLK_alpha0":	
		F,g,lb,ub,mu_prior = transmat.CSP_sfc_FiCM_noMLK_alpha0(nb,nf,nh,A,B,C,D,E,
									alpha0=alpha0)							
	# ------------------								
	# solve for weights 
	# nnls
	nnls = nls_solvers.LbfgsNNLS()
	nnls.fit(F,g)
	mu_nnls= nnls.coef_
	# bayesian (with Sigma=eye, equivalent to lsqr)
	mu_lsqr = scipy.sparse.linalg.lsqr(F, g)[0]
	# -----
	# 
	# compute dcGM for a specific subnetwork
	# Cd/cs
	# (copied from topological_vs_flow_measures.py:get_sum_iter)
	#label_dcgm = 'Id_Is'
	ficm_net =[]
	aij=[]
	Wij=[]
	mu = mu_nnls
	if(label_dcgm =='Cd_Cs'):
		# COMMENT on result: mu_ficm is constant for all hh. 
		# WHY ? for model "noMLK_alpha0", Cd = cste
		#       (for model "noMLK" only a trivial solution is found)
		idx_mu_start=0; idx_mu_end=nh*nf; r0=nf ; r1=nh; axis=0
		x=mu[idx_mu_start:idx_mu_end]
		aij=A
		Wij_nnls=np.zeros((nf,nh))
		for i in range(nf):
			Wij_nnls[i,:]=x[i*nh:(i+1)*nh]
		ficm_net = n.ficm_consumption_hh	
	elif (label_dcgm =='Id_Is'):
		# COMMENT on result: nnls investment network is too sparse
		# 						to make comparisons
		# Wij[i,j] = (1/u + Wi[i]*Wj[j])*aij[i,j]/W 
		# u = ficm_net.sol.x[0]
		idx_mu_start=nh*nf; idx_mu_end=nh*nf+nf**2; r0= nf; r1= nf; axis=0
		ficm_net = n.ficm_investment_firm
		aij = B		
	elif (label_dcgm =='WBd_WBs'):
		"""
		"""	
		idx_mu_start=nh*nf+nf**2 ; idx_mu_end = nh*nf+nf**2+nf*nh; axis=0
		Wij_nnls=mu[idx_mu_start:idx_mu_end].reshape((nf,nh))
		ficm_net = n.ficm_wage 
		aij = C
	# aggregate nnls weights 
	Wj= Wij_nnls.sum(axis=axis)
	axis=1
	Wi=Wij_nnls.sum(axis=axis)
	W= np.sum(Wi) # should be equal to sum(Wj)
	#pij =ficm_net.get_biadjacency_matrix(ficm_net._pij)
	Wij_ficm=ficm_net.get_wij_degree_corrected(Wi,Wj,W,aij) 
	# ---
	# consistency checks
	# forbidden situation: aij=0 et wij!=0 
	if np.any(np.bitwise_and(aij==0, Wij_nnls>0)):
		raise ValueError('aij==0 => Wij_nnls=0')
	if np.any(np.bitwise_and(aij==0, Wij_ficm>0)):
		raise ValueError('aij==0 => Wij_ficm=0')	
	# sparsity should be close
	if not np.isclose( np.sum(1*(Wij_ficm>0)), np.sum(1*(Wij_nnls>0)) ):
		warnings.warn('sparsity mismatch')
	# ---	
	# plot: compare weights before rescaling
	if(plot):
		plt.scatter(Wij_nnls[aij==1],Wij_ficm[aij==1])	
		plt.subplot(2,1,1)
		plt.matshow(Wij_nnls,aspect="auto",fignum=False)
		plt.subplot(2,1,2)
		plt.matshow(Wij_ficm,aspect="auto",fignum=False)
	# ---
	# replace w_sub <- w_sub_dcgm in x
	mu_dcgm = mu_nnls.copy()
	dim = np.product(Wij_ficm.shape)
	mu_dcgm[idx_mu_start:idx_mu_end] = Wij_ficm.reshape(1,dim)
	# rescale weights to get the same sum as nnls solution
	mu_dcgm[idx_mu_start:idx_mu_end] = mu_dcgm[idx_mu_start:idx_mu_end] *W / np.sum(mu_dcgm[idx_mu_start:idx_mu_end])
	#   sum of weights
	#print 'sum(mu)=',np.sum(mu[idx_mu_start:idx_mu_end]), \
	#	'sum(mu_ficm)=',np.sum(mu_ficm[idx_mu_start:idx_mu_end])
	# -------------------
	# compare degree/strength plot in dcGM and nnls
	# useless: the marginals <wi>,<wj> are the same
	
	# -------------------
	# Residual
	# compute F_{ficm}x-g, analyse error, compare to Fx-g.
	#   error
	nplot = 3
	err_list = []
	neg_list = []
	for i,(mu,t) in enumerate(zip([mu_nnls,mu_dcgm,mu_lsqr],
									['mu_nnls','mu_dcgm_'+label_dcgm,'mu_lsqr'])):
		res = F.dot(mu)-g
		err_rel = np.sum(np.abs(res))/np.sum(np.abs(mu))*100
		neg=np.sum( 1*mu<0)/float(np.sum(mu!=0))*100
		err_list.append(err_rel)
		neg_list.append(neg)
		print "##########################"
		print "err_rel_"+t+"=",err_rel, " %"
		print  "%f pct negative coeffs (among non-zero coeffs)"%(neg)
		if(plot):
			plt.subplot(nplot,1,i+1)
			plt.ylabel()
			plt.plot(res)
	return 	err_list, neg_list
	
