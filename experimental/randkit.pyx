"""

BUILD:
	$python setup.py build_ext --inplace

USAGE:
	import randkit
	r=randkit.RandomKit()
	r.random() #long integer
	r.randn()
SEE:
https://groups.google.com/forum/#!topic/cython-users/9UGMi_b3tVo

"""

cdef extern from "randomkit.h":
	ctypedef struct rk_state: 
		pass
	double rk_double(rk_state *state);
	double rk_gauss(rk_state *state)
	unsigned long rk_random(rk_state * state)
	unsigned long rk_interval(unsigned long max, rk_state *state)
	void rk_seed(unsigned long seed, rk_state * state)

from libc cimport stdlib

import os

cdef class RandomKit:
	""" Simple randomkit wrapper """
	cdef rk_state *state

	#cdef __cinit__(RandomKit self):
	def __cinit__(RandomKit self):	
		self.state = <rk_state*> stdlib.malloc(sizeof(rk_state))
		if (self.state == NULL): raise MemoryError
    
    #cdef __init__(RandomKit self):    
	def __init__(RandomKit self):
		cdef unsigned long *seedptr
		cdef object seed = os.urandom(sizeof(unsigned long))
		seedptr = <unsigned long*>(<void*>(<char*> seed))
		rk_seed(seedptr[0], self.state)

	def __dealloc__(RandomKit self):
		if self.state:
			stdlib.free(self.state)

	cpdef unsigned long random(RandomKit self):
		"""
		Returns a random long between 0 and LONG_MAX inclusive
		"""
		return rk_random(self.state)
	
	cpdef double randn(RandomKit self):
		"""
		return a random gaussian deviate with variance unity and zero mean.
		"""
		return rk_gauss(self.state)	

	cpdef double uniform(RandomKit self):
		"""
		Returns a random double between 0.0 and 1.0, 1.0 excluded.
		"""
		return rk_double(self.state)
		
	cpdef unsigned long interval(RandomKit self, int max):	
		"""
		Returns a random unsigned long between 0 and max inclusive.
		"""
		return rk_interval(max, self.state)
		
"""
import randkit
import matplotlib.pylab as plt
r=randkit.RandomKit()

x=[r.randn() for i in range(1000) ]
plt.hist(x); plt.show()

x=[r.uniform() for i in range(10000) ]
plt.hist(x); plt.show()

x=[r.interval(10) for i in range(100000) ]
plt.hist(x,30); plt.show()

"""

"""
OTHER APPROACH:

def foobar():
    cdef rk_state state
    cdef unsigned long *seedptr, random_value
    cdef object seed = os.urandom(sizeof(unsigned long))
    seedptr = <unsigned long*>(<void*>(<char*> seed))
    rk_seed(seedptr[0], &state)
    random_value = rk_random(&state)
"""

