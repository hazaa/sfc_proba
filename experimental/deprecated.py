		
def sample_cube_1b1fnW_pareto(nsamp, scale):
	"""
	for test purpose only, cube constraint instead of BMW
	(scaling is not possible with this function)
	"""
	dim=10
	pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False)
	C,b,F,g,x0=example_CSP_problems("cube",n=dim)
	b= x_max*b # rescale the inequality constraint
	x0 = x_max* x0
	h=har(C,b,F,g,x0,uniform=False)	
	h.init()
	t=time.time()
	x=h.sample(nsamp, pdf,native=True)
	# visual inspection
	f=fig_with_function_name("\nt="+str(t)+ ' sec')		
	h.plot_metrics(x)
	return x,1,1,dim


def sample_BMW_1b1fnW_pareto_piecewise(nsamp, scale):
	"""
	get samples for the bmw problem 1b1fnW 
	pdf constraint on all incomes (pareto_piecewise)
	"""
	raise NotImplementedError #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	# get CSP 
	nb,nf,nw=1,1,9
	C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_119')
	dim=nw-1
	if (nsamp<=dim**4): 
		warnings.warn('not enough samples', Warning)	
	# rescale pdf rather than CSP problem (using income average, and not Mtot)
	thr,perc, bb, yav, total_income,nb_household = income_tabulation() # yav is needed before calling get_..
	scale = scale* WBs_avg / yav
	# constrain the pdf of ALL INCOMES (which indices are given by idx_WBs)
	pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False,idx = idx_WBs,
																		rescale_factor=scale)				
	# hit and run sampling
	h=har(C,b,F,g,x0,uniform=False,dim=dim)		
	h.init()
	t=time.time()
	x=h.sample(nsamp, pdf,native=True)
	t = time.time()	-t		
	# visual inspection
	f=fig_with_function_name("\nt="+str(t)+ ' sec')		
	h.plot_metrics(x)
	return x,nb,nf,nw



def test_pareto():
	"""
	"""
	
	raise DeprecationWarning()
		
	print "warning: scale is not used !!!!!"
	
	fname = 'store.h5'	
	groupname = "bmw_11N/pareto"		
	scale= [1, 1.1] 
	nsamp = 10000	
	nb,nf,nw=1,1,9	
	
	# Mtot is necessary
	C,b,F,g,x0,Mtot,WBs_avg,idx_WBs = example_CSP_problems('bmw_11'+str(nw))

	# prepare pdf
	b= 	float(Mtot) / float(Mtot - nw)
	rv = pareto(b)	
	# check:
	# b must be > 1 so that E[x] exists.
	if b<=1: raise ValueError('b is invalid, must be >1')
	# Mtot/nw = E(X), critical regime
	if( abs( rv.mean() - Mtot/nw)>0.001 ): raise ValueError('b is invalid: Mtot/nw != E(X)') 		
	
	# sample from the single site weigth function 
	def func_weight_sample(nsamp,scale):
		# prepare pdf
		# pareto.pdf(x, b) = b / x**(b+1) for x >= 1, b > 0.
		# E(x) = b/(b-1) => b = Mtot / (Mtot-nw)
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto

		# sample reduced model		
		bmw = bmw_sym_1F1BnW(nw)
		bmw.init()	
		x = bmw.get_har_samples(nsamp,pdf=rv.pdf,uniform=False,native=False, algo="CD")	
		return x
		
	# single-site marginal: see paper
	def func_marginal(x,scale):		
		b= 	float(Mtot) / float(Mtot - nw)
		rv = pareto(b, scale=scale)	
		return rv.pdf(x)
	
	# record configuration	
	label,label_long = get_bmw_labels(nb,nf,nw)
	idx_first_WBs = label_long.index('WBs')
	idx_first_M = label_long.index('M')
	conf={'nw':nw,'nb':nb, 'nf':nf, 'seed':123,	'model':'bmw', 
			'model_params':{'r': 0.03,'alpha0':0.1 , 'alpha1':0.75,'alpha2':0.01,'delta' :0.1 },
		  'labels':label_long,
		  'idx_first_WBs':idx_first_WBs,'idx_first_M':idx_first_M	}
	# main function	  
	compute_shares_bmw(fname, groupname, conf, func_weight_sample, func_marginal, nsamp, scale)



	
def compute_shares_bmw_DEPREC(fname, groupname, conf, func_weight_sample, func_marginal, nsamp,param):
	"""
	compute share of total income for the three percentile groups [0-50%],[50-90%],[90-100%]
	bmw model
	"""
	raise DeprecationWarning()
	
	# open h5 store	
	store = pd.HDFStore(fname)
	#-------
	# init
	print "har sampling..."	
	shares_inc = np.zeros((3,len(param) ))
	shares_wea = np.zeros((3,len(param) ))
	idx_first_WBs = conf['idx_first_WBs']
	idx_first_M   = conf['idx_first_M']
	nw 			  = conf['nw']
	# from hit-and-run data: compute shares of revenue & wealth 
	for j,s in enumerate(param):
		x = func_weight_sample(nsamp,s)
		# store only first sample set	
		if(j==0):
			#df = pd.DataFrame(x, columns=label_long) # BUG	
			df = pd.DataFrame(x) 
			store[groupname+'/samples'] = df			
				
		shares_inc[0,j]=share_of_revenue_from_data(x[:,idx_first_WBs:idx_first_WBs+nw].flatten(),
												percentiles[0] )
		shares_inc[1,j]= share_of_revenue_from_data(x[:,idx_first_WBs:idx_first_WBs+nw].flatten(),
												percentiles[1] ) - shares_inc[0,j]
		shares_inc[2,j]= 1- (shares_inc[0,j]+shares_inc[1,j]) 
		# shares of wealth				
		shares_wea[0,j]=share_of_revenue_from_data(x[:,idx_first_M:idx_first_M+nw].flatten(),
												percentiles[0] )
		shares_wea[1,j]= share_of_revenue_from_data(x[:,idx_first_M:idx_first_M+nw].flatten(),
												percentiles[1] ) - shares_wea[0,j]
		shares_wea[2,j]= 1- (shares_wea[0,j]+shares_wea[1,j]) 			
	# store	income
	col_label = ['sample_income'+str(s) for s in param]	
	store[groupname+'/shares/sample_income']=pd.DataFrame(shares_inc,	columns=col_label)	
	# store	wealth
	col_label = ['sample_wealth'+str(s) for s in param]	
	store[groupname+'/shares/sample_wealth']=pd.DataFrame(shares_wea,	columns=col_label)
	# store config
	config = pd.Series(json.dumps(conf))
	store[groupname+'/config']= config		
	
	#-------
	# from numerical integration of marginal pdf : shares of income
	# Different values of scale (this can correspond to growth)		
	shares = share_income_from_pdf_analytic_marginal(param)
	#print "shares=",np.array(shares)
	col_label = ['pdf_wealth'+str(s) for s in param]
	store[groupname+'/shares/pdf_wealth']=pd.DataFrame(np.array(shares).T, columns=col_label)		
	#-------					
	# modèle NfNbNw:
	# repeat
	groupname = "bmw_NNN"
	#----------															
	store.close()

# --------------------------------------------------
# FROM gpareto.py

def test_share_of_revenue_from_quad_pdf_DEPREC():
	"""
	DEPRECATED
	
	using quadrature
	"""
	raise DeprecationWarning('')
	
	k=1; a=2
	def pdf(x):
		return pdf_pareto(x,k,a)	
	x0 = 3 #lower bound of revenue
	share =  share_of_revenue_from_pdf(pdf,x0,1000)
	share_theor = (x0/k)**(1-a)
	print share, share_theor
