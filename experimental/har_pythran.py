import numpy as np
from numba import jit
from random import uniform

#pythran export bounds(float list list, float list,float list,float list )
def bounds(A,b,d, y):
		"""
		see hitanrdun/bounds.c
		"""
		a = (b - np.dot(A,y)).flatten()	
		c = np.dot(A,d).flatten()	 		
		nrow = a.shape[0]
		l0=-1E20
		l1=1E20
		for i in range(nrow):
			if (c[i] < 0.0): 
				t = a[i] / c[i]
				if (t > l0): 
					l0 = t				
			elif (c[i] > 0.0) :
				t = a[i] / c[i]
				if ( t < l1) :
					l1 = t
		return l0,l1					

def pdf_pareto(x):
	return 1.
	

def pdf_normal(x):
	return 1.	
	

def pdf_uniform(x):
	return 1.	

#pythran export hitandrun(float[:,:], float[:,:],float[:,:], int,int, bool, float[:,:],float[:,:])
def hitandrun(A,b,y0,nsamp,pdf_id,uniform,basis,translation):
		#
		#sample the problem Ay<=b_new
		#(without caring for the equality)
			
		# GetRNGstate(); ???????
		dim_y = A.shape[1]
		samples = np.zeros((nsamp,dim_y))
		y=y0
		y_new=0
		reject_rate = 0
		
		for i in range(nsamp):
			# generate random direction d
			d = np.random.normal(size=dim_y).reshape((dim_y,1))						
			# calculate bounds l
			l0,l1 = bounds(A,b,d, y) 			
			#v = l0 + np.random.uniform() * (l1 - l0)			
			
			v = l0 + uniform(0,1) * (l1 - l0)			
			"""						
			if uniform:	
				y = v*d + y								
				samples[i,:]=y.flatten()					
			else:				
				#r = np.random.uniform()
				r = np.random.random()
				
				x= np.dot(basis, y).T+ translation.T				
				y_new = v*d + y							
				x_new= np.dot(basis, y_new).T+ translation.T
				
				if(pdf_id==0):
					p_x = pdf_uniform( x)
					p_xnew = pdf_uniform( x_new )
				if(pdf_id==1):	
					p_x = pdf_normal( x)
					p_xnew = pdf_normal( x_new )
				if(pdf_id==2):		
					p_x = pdf_pareto( x)
					p_xnew = pdf_pareto( x_new )
				
				if(p_x>0):
					if(r < np.min([1., p_xnew/p_x])):
						samples[i,:]=y_new.flatten()
						y=y_new																		
				else:	
					reject_rate += 1	
					if(i==2):
						samples[i,:]=samples[i-1,:]
					else:	
						samples[i,:]=y.flatten()		
		"""							
		reject_rate = float(reject_rate)/float(nsamp)			
		return samples, reject_rate


def test_inequality_pareto():
		"""
		test inequality only with pareto distrib
		"""
		n=2; nsamp = 1000
		
		unif=False
		basis = np.eye(n)
		translation = np.zeros(n).reshape((n,1))
		debug=True
		
		pdf_id= 0  
		n=2; nsamp = 500
		A = np.vstack((-np.eye(n), np.eye(n)))
		b=10*np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])	
		x0 = 1./n*np.ones(n).reshape([n,1])
		
		#x,reject_rate = hitandrun(A,b,x0,nsamp,pdf_id,unif,basis,translation)
		#print reject_rate,x
		

