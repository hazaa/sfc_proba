# -*- coding: utf-8
import numpy as np
from numba import jit
from transmat import example_CSP_problems

"""
ERRORS:

* exceptions pas supportés:
** erreur: constant inference not possible for $const164.2 % $164.5
** solution: commenté "raise ValueError( "

* nopython=False:
** erreur: LoweringError: p_x
			File "har.py", line 71
** solution : ?

* nopython=True:
** erreur: unsupported call signature
	[1] During: resolving callee type: Function(np.random.normal)
** solution: aucune
** rem: pourtant np.random.normal est supporté depuis numba 0.21, et on utilise 0.30

"""

@jit(nopython=False,cache=True)
def bounds(A,b,d, y):
		"""
		see hitanrdun/bounds.c
		"""
		a = b - np.dot(A,y)
		c = np.dot(A,d) 		
		nrow = a.shape[0]
		l0=-np.inf
		l1=np.inf		
		for i in range(nrow):
			if (c[i] < 0.0): 
				t = a[i] / c[i]
				if (t > l0): 
					l0 = t				
			elif (c[i] > 0.0) :
				t = a[i] / c[i]
				if ( t < l1) :
					l1 = t
		return l0,l1					

@jit(nopython=False,cache=True)
def pdf_pareto(x):
	return 1.
	
@jit(nopython=False,cache=True)	
def pdf_normal(x):
	return 1.	
	
@jit(nopython=False,cache=True)	
def pdf_uniform(x):
	return 1.	


@jit(nopython=False,cache=True)
def hitandrun(A,b,y0,nsamp,pdf_id,uniform,basis,translation):
		#
		#sample the problem Ay<=b_new
		#(without caring for the equality)
			
		# GetRNGstate(); ???????
		dim_y = A.shape[1]
		samples = np.zeros((nsamp,dim_y))
		y=y0
		y_new=0
		reject_rate = 0
		
		for i in range(nsamp):			
			# generate random direction d
			d = np.random.normal(size=dim_y).reshape((dim_y,1))									
			# calculate bounds l
			l0,l1 = bounds(A,b,d, y) 	
			"""					
			if (not(np.isfinite(l0)))|(not(np.isfinite(l1))): 
				raise ValueError("Bounding function gave NA bounds [%f, %f]"%(l0,l1));			
			if (l0 == l1) :
				raise ValueError("Bounding function gave empty interval");
			"""	
			#v = l0 + np.random.uniform() * (l1 - l0)			
			v = l0 + np.random.random() * (l1 - l0)					
			if uniform:	
				y = v*d + y								
				samples[i,:]=y.flatten()					
			else:				
				#r = np.random.uniform()
				r = np.random.random()				
				x= np.dot(basis, y).T+ translation.T				
				
				y_new = v*d + y										
				x_new= np.dot(basis, y_new).T+ translation.T
				
				if(pdf_id==0):
					p_x = pdf_uniform( x)
					p_xnew = pdf_uniform( x_new )					
				if(pdf_id==1):	
					p_x = pdf_normal( x)
					p_xnew = pdf_normal( x_new )					
				if(pdf_id==2):		
					p_x = pdf_pareto( x)
					p_xnew = pdf_pareto( x_new )				
				if(p_x>0):
					if(r < np.min([1., p_xnew/p_x])):
						samples[i,:]=y_new.flatten()
						y=y_new 				
				else:	
					reject_rate += 1						
					if(i==2):
						samples[i,:]=samples[i-1,:]
					else:	
						samples[i,:]=y.flatten()		
									
		reject_rate = float(reject_rate)/float(nsamp)		
			
		return samples, reject_rate

# this is taken adapted from har_nonuniform_test.py
def test_inequality_pareto():
		"""
		test inequality only with pareto distrib
		"""
		n=2; nsamp = 1000
		#
		uniform=False
		basis = np.eye(n)
		translation = np.zeros(n).reshape((n,1))
		debug=True
		# no init!!
		pdf_id= 0  
		n=2; nsamp = 500
		A = np.vstack((-np.eye(n), np.eye(n)))
		b=10*np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])	
		x0 = 1./n*np.ones(n).reshape([n,1])
		# sample only the ineq: Ay<=b
		x,reject_rate = hitandrun(A,b,x0,nsamp,pdf_id,uniform,basis,translation)
		print reject_rate,x
def tests():
		test_inequality_pareto()

tests()
