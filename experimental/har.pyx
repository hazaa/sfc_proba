#cython: boundscheck=False, wraparound=False, nonecheck=False
import numpy as np
cimport numpy as np 
from libc.math cimport pow

from random import uniform, seed, sample

from cpython cimport array  # REMOVE THAT
import array    # REMOVE THAT

from libc.stdlib cimport rand, RAND_MAX # REMOVE THAT

cdef extern from "npy_math.h":
	bint npy_isnan(double x)

import randkit
import interpolate
#cimport randkit #http://docs.cython.org/en/latest/src/tutorial/pxd_files.html
"""
http://cython.readthedocs.io/en/latest
>cython har.pyx -a
>gcc -O3 -march=native har.c -shared -fPIC `python-config --cflags --libs` -o har.so
"""

cdef cbounds(np.ndarray[double, ndim=2] A, np.ndarray[double, ndim=1] b, 
		np.ndarray[double, ndim=1, negative_indices=False, mode='c'] d,
		np.ndarray[double, ndim=1] y):
		"""
		see hitanrdun/bounds.c
		NaN: https://github.com/astropy/astropy/pull/186
		"""
		cdef int i,j,nrow = A.shape[0], ncol = A.shape[1]
		cdef double tmp=0
		cdef np.ndarray[double, ndim=1]  a = np.zeros(nrow)
		cdef np.ndarray[double, ndim=1]  c = np.zeros(nrow)
		
		# a = b - np.dot(A,y)
		for i in range(nrow):	
			tmp=0.
			for j in range(ncol):
				tmp += A[i,j]*y[j]
			a[i] = b[i] - tmp		
		#c = np.dot(A,d)	
		for i in range(nrow):
			tmp=0.
			for j in range(ncol):
				tmp += A[i,j]*d[j]
			c[i] = tmp
		#compute bounds l0=-1E20 l1=1E20	
		cdef double t,l0 = np.nan, l1 = np.nan		
		for i in range(nrow):
			if (c[i] < 0.0): 
				t = a[i] / c[i]
				if ( npy_isnan(l0) or t > l0): 
					l0 = t				
			elif (c[i] > 0.0) :
				t = a[i] / c[i]
				if ( npy_isnan(l1) or t < l1) :
					l1 = t
		return l0,l1

def bounds(A,b,d, y):
		"""
		see hitanrdun/bounds.c
		"""
		a = (b - np.dot(A,y)).flatten()	
		c = np.dot(A,d).flatten()			
		nrow = a.shape[0]
		l0=-1E20
		l1=1E20
		for i in range(nrow):
			if (c[i] < 0.0): 
				t = a[i] / c[i]
				if (t > l0): 
					l0 = t				
			elif (c[i] > 0.0) :
				t = a[i] / c[i]
				if ( t < l1) :
					l1 = t
		return l0,l1					

def pdf_pareto(x):
	# see scipy/stats/_continuous_distns.py
	c=1.	
	return pow(1+c*x, -1.0-1.0/c)

cpdef pdf_pareto_cython(double x,double a,double k):
	# pareto tail
	cdef double r
	if(x>k): 
		r= a*pow(k,a)/pow(x,a+1.)
	else:
		r=0.	
	return r

cpdef pdf_genpareto_cython(double x,double c):
	# see scipy/stats/_continuous_distns.py	
	return pow(1+c*x, -1.0-1.0/c)
	
def pdf_pareto_all(x):			
	c=1.								
	#return np.prod(pow(1+c*x, -1.0-1.0/c))
	return np.prod(np.power(1+c*x, -1.0-1.0/c))

def pdf_pareto_one(x):			
	c=1.					
	# ! note the following, because x is 2d array
	return pow(1+c*x[0][0], -1.0-1.0/c)

def piecewise(x, data):
	"""
	piecewise function can be used to define a pdf from tabulated data 
	https://github.com/noahwaterfieldprice/python_primer/blob/master/ch_7/PiecewiseConstant2.py#L24
	NB: hitandrun doesn't need the vectorized function
	"""
	for i in range(len(data) - 1):
		if data[i][1] <= x < data[i + 1][1] or x == data[-1][1]:
			return data[i][0]
	return 0
	
cpdef piecewise_pareto(double x, np.ndarray[double, ndim=2] data, 
					double thr, double k , double a):
	"""
	 piecewise function can be used to define a pdf from tabulated data 
	 and pareto for the tail
	 https://github.com/noahwaterfieldprice/python_primer/blob/master/ch_7/PiecewiseConstant2.py#L24
	 NB: hitandrun doesn't need the vectorized function
	"""	
	# pareto tail
	if(x>thr): return (a*k**a)/x**(a+1)
	# bulk
	for i in range(data.shape[0] - 1):
		if data[i][1] <= x < data[i + 1][1] or x == data[data.shape[0]-1][1]:
			return data[i][0]	
	return 0
		
cpdef piecewise_pareto_vec(np.ndarray[double, ndim=1] x, np.ndarray[double, ndim=2] data,
						double thr, double k , double a):
	"""
	vectorized version
	adapted from https://github.com/noahwaterfieldprice/python_primer/blob/master/ch_7/PiecewiseConstant2.py#L24
	operator must be replaced (https://docs.python.org/2/library/operator.html) by:
	http://cython.readthedocs.io/en/latest/src/userguide/special_methods.html?highlight=operator
	"""
	r = np.zeros(len(x))
	#if idx.size==x.size:
	#	return r
	# bulk
	for i in xrange(data.shape[0] - 1):		
		#cond = operator.and_(data[i][1] <= x, x < data[i + 1][1])
		#cond = __and__(data[i][1] <= x, x < data[i + 1][1])
		cond = (data[i][1] <= x).__and__( x < data[i + 1][1])
		#cond = operator.or_(cond, x == data[-1][1])
		#cond = cond.__or__(x == data[-1][1])
		cond = cond.__or__(x == data[data.shape[0]-1][1])
		r[cond] = data[i][0]
	# pareto tail
	idx = x>thr      # TODO: reintegrate in operator
	r[idx] = (a*k**a)/x[idx]**(a+1)	
	return r


#def hitandrun(A,b,y0,nsamp,pdf_id,uniform,basis,translation):
#def hitandrun(A,b,y0,nsamp,pdf,uniform,basis,translation):
cpdef hitandrun(np.ndarray[double, ndim=2] A, np.ndarray[double, ndim=2] b,
				np.ndarray[double, ndim=2] y0, int nsamp, pdf, bint uniform, 
				np.ndarray[double, ndim=2] basis, np.ndarray[double, ndim=2] translation):
		#
		#sample the problem Ay<=b_new
		#(without caring for the equality)
			
		# GetRNGstate(); ???????
		dim_y = A.shape[1]
		samples = np.zeros((nsamp,dim_y))
		y=y0
		y_new=0
		reject_rate = 0
		
		for i in range(nsamp):
			# generate random direction d
			d = np.random.normal(size=dim_y).reshape((dim_y,1))						
			# calculate bounds l
			l0,l1 = bounds(A,b,d, y) 			
			v = l0 + np.random.uniform() * (l1 - l0)						
			#v = l0 + uniform(0,1) * (l1 - l0)			# error: bool is not callable
									
			if uniform:	
				y = v*d + y								
				samples[i,:]=y.flatten()					
			else:				
				r = np.random.uniform()
				#r = np.random.random()
				
				x= np.dot(basis, y).T+ translation.T				
				y_new = v*d + y							
				x_new= np.dot(basis, y_new).T+ translation.T
	
				p_x = pdf( x)
				p_xnew = pdf( x_new )
				if(p_x>0):
					if(r < np.min([1., p_xnew/p_x])):
						samples[i,:]=y_new.flatten()
						y=y_new																					
					else:	
						reject_rate += 1	
						if(i==2):
							samples[i,:]=samples[i-1,:]
						else:	
							samples[i,:]=y.flatten()		
		"""	
		"""						
		reject_rate = float(reject_rate)/float(nsamp)			
		return samples, reject_rate

#ctypedef float (*cfptr)(float)
#ctypedef object (*pyfunc)(float x) #https://stackoverflow.com/questions/18348083/passing-cython-function-to-cython-function

cpdef har_cd_native(np.ndarray[double, ndim=1] x0, int dim, double M,int nsamp, f, seed_, int thin, int accel, int log):
	"""
	x0,dim,M,nsamp,f,seed=None,thin
	thinning was added
	
	array: http://docs.cython.org/en/latest/src/tutorial/array.html
	
	efficient indexing: http://cython.readthedocs.io/en/latest/src/tutorial/numpy.html#efficient-indexing
		" So if v for instance isn’t typed, then the lookup f[v, w] isn’t optimized. "
	"""
	if not seed==None:
		seed(seed_)
	cdef x_arr = array.array('d', [0.]*(dim*nsamp) ) 	
	cdef x_cur = array.array('d', [0.]*dim)		
	cdef int k=0
	cdef int n=1
	cdef int it = 0
	cdef int reject =0
	cdef float eps,xA,xB,xA_new,xB_new, m,r
	cdef bint cond
	cdef int j,A,B
	# main loop
	while(it<10**10 and n<nsamp):	
		# copy x0 to x_cur
		if(it==0):
			for j in range(dim):
				x_cur[j] = x0[j]
				x_arr[j] = x0[j]
		# https://docs.python.org/2/library/random.html#random.sample
		A,B=sample(xrange(dim), 2)
			# if accel==1
		# propose a move x[A] -> x[A] + eps ; x[B] -> x[B] -eps
		xA = x_cur[A]; xB = x_cur[B]
		m = min(M-xA,xB )
		r = rand()
		eps = r/<double>RAND_MAX * m #eps = uniform( 0, m ) 					
		xA_new= xA + eps ; xB_new= xB - eps
		# accept or reject	
		r = rand()/<double>RAND_MAX #uniform(0.,1.)		
		if(log==1):
			cond = np.log(r) < f(xA_new)-f(xA ) +f(xB_new)-f(xB)
		else:
			cond = r < (f(xA_new)/f(xA ) *f(xB_new)/f(xB))
		if ( cond ):	
			# accept: copy xA_new and xB_new
			x_cur[A]=xA_new
			x_cur[B]=xB_new
			k += 1 
			# copy to x_arr
			if( k%thin ==0):
				for j in range(dim):				
					x_arr[ n*dim + j ] = x_cur[j]
				n += 1	
		else:
			reject +=1			
		it += 1			
	
	# checksum
	x_arr = np.frombuffer(x_arr[0:nsamp*dim],dtype=float)
	x_arr=np.reshape(x_arr,(nsamp,dim))
	s=np.sum(x_arr,1)
	if(not( np.allclose(s, M*np.ones(nsamp) ) )): 
		print "s=",s
		raise ValueError('incorrect checksum')
	return x_arr, float(reject)/float(it)			

cpdef har_cd_native_inplace(np.ndarray[double, ndim=1] x0, int dim, double M,int nsamp, f, seed_, int thin, int accel, int log,double p1,double p2):
	"""
	x0,dim,M,nsamp,f,seed=None,thin
	thinning was added
	
	array: http://docs.cython.org/en/latest/src/tutorial/array.html
	
	efficient indexing: http://cython.readthedocs.io/en/latest/src/tutorial/numpy.html#efficient-indexing
		" So if v for instance isn’t typed, then the lookup f[v, w] isn’t optimized. "
	"""
	if not seed==None:
		seed(seed_)
	#cdef x_arr = array.array('d', [0.]*(dim*nsamp) ) 	
	cdef x_cur = array.array('d', [0.]*dim)		
	cdef int k=0
	cdef int n=1
	cdef int it = 0
	cdef int reject =0
	cdef float eps,xA,xB,xA_new,xB_new, m,r
	cdef bint cond
	cdef int j,A,B
	# main loop
	while(it<10**10 and n<nsamp):	
		# copy x0 to x_cur
		if(it==0):
			for j in range(dim):
				x_cur[j] = x0[j]
				#x_arr[j] = x0[j]
		# https://docs.python.org/2/library/random.html#random.sample
		A,B=sample(xrange(dim), 2)
			# if accel==1
		# propose a move x[A] -> x[A] + eps ; x[B] -> x[B] -eps
		xA = x_cur[A]; xB = x_cur[B]
		m = min(M-xA,xB )
		r = rand()
		eps = r/<double>RAND_MAX * m #eps = uniform( 0, m ) 					
		xA_new= xA + eps ; xB_new= xB - eps
		# accept or reject	
		r = rand()/<double>RAND_MAX #uniform(0.,1.)		
		if(log==1):
			cond = np.log(r) < f(xA_new,p1,p2)-f(xA,p1,p2 ) +f(xB_new,p1,p2)-f(xB,p1,p2)
		else:
			cond = r < (f(xA_new,p1,p2)/f(xA,p1,p2 ) *f(xB_new,p1,p2)/f(xB,p1,p2))
		if ( cond ):	
			# accept: copy xA_new and xB_new
			x_cur[A]=xA_new
			x_cur[B]=xB_new
			k += 1 
			# copy to x_arr
			if( k%thin ==0):
				n += 1	
		else:
			reject +=1			
		it += 1			
	
	# checksum
	s=np.sum(x_cur)
	if(not( np.isclose(s, M ) )): 
		print "s=",s
		raise ValueError('incorrect checksum')
	return x_cur, float(reject)/float(it)			

def test_stdlib_rand():
	"""
	random number generation using stdlib 
	see https://hplgit.github.io/teamods/MC_cython/main_MC_cython.html
	"""
	cdef int a,k,i
	cdef float x
	
	# int 0 to a
	a=6
	print 
	kk=[]
	for i in range(2000):		
		#k =  1 + int(rand()/(RAND_MAX*6.0))  # FAILS  
		k =   int(6.0 * <float>rand()/<float>RAND_MAX)
		# int((rand()/RAND_MAX)*(<float>a +1.)  )
		kk.append(k)
	l=[0,1,2,3,4,5,6];
	h,b=np.histogram(kk,bins=l+[7])
	print h,b
	
	print "====== UNIFORM ========"
	for i in range(20):
		r = rand()/<float>RAND_MAX
		print "r=",r
		
def har_hd(np.ndarray[double, ndim=2] A, np.ndarray[double, ndim=1] b,
				np.ndarray[double, ndim=1] y0, int nsamp, pdf,  
				np.ndarray[double, ndim=2] basis, np.ndarray[double, ndim=1] translation,
				int seed, int thin, bint log):
	"""
	sample the problem Ay<=b_new
	(without caring for the equality)		
	
	using randomkit
	lastmod: 2017-07-04
	"""				
	cdef int dim = A.shape[1]	
	cdef int dim_x = translation.shape[0]
	cdef int nr_basis = basis.shape[0], nc_basis = basis.shape[1]	
	cdef np.ndarray[double, ndim=2, negative_indices=False, mode='c'] samples= np.zeros((nsamp,dim))	
	cdef np.ndarray[double, ndim=1, negative_indices=False, mode='c'] d	= np.zeros(dim)	
	cdef np.ndarray[double, ndim=1, negative_indices=False, mode='c'] y_cur	= np.zeros(dim)	
	cdef np.ndarray[double, ndim=1, negative_indices=False, mode='c'] y_new	= np.zeros(dim)	
	cdef np.ndarray[double, ndim=1, negative_indices=False, mode='c'] x_cur	= np.zeros(dim_x)	
	cdef np.ndarray[double, ndim=1, negative_indices=False, mode='c'] x_new	= np.zeros(dim_x)		
	cdef double v=0,l0=0,l1=0, p_x_cur=0, p_x_new=0, tmp=0
	cdef bint cond
	cdef int reject=0
	cdef int k=0
	cdef int n=1
	cdef int it = 0
	r=randkit.RandomKit() #SEED !?????????,

	# main loop
	while(it<10**10 and n<nsamp):
		# generate random direction d. fast enough when dim is high ?
		#d = np.random.normal(size=dim).reshape((dim,1))	
		d = np.random.normal(size=dim)
		# copy y0 to y_cur
		if(it==0):
			for j in range(dim):
				y_cur[j] = y0[j]
				samples[0,j] = y0[j]
		# calculate bounds l
		l0,l1 = cbounds(A,b,d, y_cur) 			
		v = l0 + r.uniform() * (l1 - l0)
		# update x_cur				
		#x_cur= np.dot(basis, y_cur).T+ translation.T				
		for i in range(nr_basis):
			tmp=0.
			for j in range(nc_basis):
				tmp += basis[i,j]*y_cur[j]
			x_cur[i] = tmp + translation[i]			
		# update y	
		#y_new = v*d + y_cur							
		for j in range(dim):
			y_new[j] = v*d[j] + y_cur[j]
		# update x_new
		#x_new= np.dot(basis, y_new).T+ translation.T
		for i in range(nr_basis):
			tmp=0.
			for j in range(nc_basis):
				tmp += basis[i,j]*y_new[j]
			x_new[i] = tmp	+ translation[i]		
		# probabilities
		p_x_cur = pdf( x_cur)
		p_x_new = pdf( x_new )
		if(log==True):
			cond = np.log(r.uniform()) < np.min([0., p_x_new-p_x_cur])
		else:
			#if(p_x_cur==0): 
			#	print x_cur
			#	raise ValueError
			cond = r.uniform() < np.min([1., p_x_new/p_x_cur])
		# accept/reject	
		if(cond):
			for j in range(dim):			
				y_cur[j]=y_new[j]																								
			k += 1 
			# copy to x_arr
			if( k%thin ==0):
				for j in range(dim):									
					samples[ n, j ] = y_cur[j]
				n += 1	
		else:
			reject +=1			
		it += 1		
	# checksum ?	
	return samples, float(reject)/float(it)		

def har_cd_np(np.ndarray[double, ndim=1] x0, int dim, double lb,double M,int nsamp, f, seed_, int thin, int accel, int log):
	"""
	x0,dim,M,nsamp,f,seed=None,thin
	thinning was added
	moved from array.array to np array
	"""
	if not seed==None:
		seed(seed_)				
	cdef np.ndarray[np.float_t, ndim=2, negative_indices=False, mode='c'] x_arr= np.zeros((nsamp,dim))	
	cdef np.ndarray[np.float_t, ndim=1, negative_indices=False, mode='c'] x_cur	= np.zeros(dim)	
	#x_arr= np.zeros((nsamp,dim))	
	#x_cur	= np.zeros(dim)
	#cdef np.ndarray[np.float_t, ndim=2] x_arr= np.zeros((dim,nsamp))	
	#cdef np.ndarray[np.float_t, ndim=1] x_cur	= np.zeros(dim)
	cdef int A,B,C
	cdef int k=0
	cdef int n=1
	cdef int it = 0
	cdef int reject =0
	cdef float eps,xA,xB,xA_new,xB_new, m,r
	cdef bint cond
	# check args
	if(np.any(x0>M) or np.any(x0<lb)): raise ValueError('x0 not in [m,M]')
	# main loop
	while(it<10**10 and n<nsamp):	
		# copy x0 to x_cur
		if(it==0):
			for j in range(dim):
				x_cur[j] = x0[j]
				x_arr[0,j] = x0[j]
		# https://docs.python.org/2/library/random.html#random.sample
		if accel==1:
			B = np.argmax(x_cur)
			A = int((<float>dim-1.)* <float>rand()/<float>RAND_MAX  )
			if A==B: A=dim-1 # sample without replacement, recoding dim-1 by B
			if(<float>rand()/<float>RAND_MAX < 0.5):
				C=A; A=B ; B = C  # filiasi "either xa of xb"
		else:	
			A,B=sample(xrange(dim), 2) # without replacement
			#A = int(rand()/(RAND_MAX*<float>dim)) 
			#B = int(rand()/(RAND_MAX*(<double>dim-1.) ) )
			#if(B==A):
			#	B=dim-1 # sample without replacement, recoding dim-1 by A
		# propose a move x[A] -> x[A] + eps ; x[B] -> x[B] -eps
		xA = x_cur[A]; xB = x_cur[B]
		m = min(M-xA,xB-lb )		
		eps = m * <float>rand()/<float>RAND_MAX  #eps = uniform( 0, m ) 					
		xA_new= xA + eps ; xB_new= xB - eps
		# accept or reject	
		r = <float>rand()/<float>RAND_MAX #uniform(0.,1.)	
		if(log==1):
			cond = np.log(r) < f(xA_new)-f(xA ) +f(xB_new)-f(xB)
		else:
			cond = r < (f(xA_new)/f(xA ) *f(xB_new)/f(xB))				
		if ( cond ):	
			# accept: copy xA_new and xB_new
			x_cur[A]=xA_new
			x_cur[B]=xB_new
			k += 1 
			# copy to x_arr
			if( k%thin ==0):
				for j in range(dim):				
					#x_arr[ n*dim + j ] = x_cur[j]
					x_arr[ n, j ] = x_cur[j]
				n += 1	
		else:
			reject +=1			
		it += 1			
	
	# checksum	
	s=np.sum(x_arr,1)
	if(not( np.allclose(s, M*np.ones(nsamp),rtol=1e-02, atol=1e-03 ) )): 
		print "s=",s
		raise ValueError('incorrect checksum')
	return x_arr, float(reject)/float(it)			

def test_inequality_pareto():
		"""
		test inequality only with pareto distrib
		"""
		n=2; nsamp = 1000
		
		unif=False
		basis = np.eye(n)
		translation = np.zeros(n).reshape((n,1))
		debug=True
		
		pdf_id= 0  
		n=2; nsamp = 500
		A = np.vstack((-np.eye(n), np.eye(n)))
		b=10*np.hstack((np.zeros(n),np.ones(n))).reshape([2*n,1])	
		x0 = 1./n*np.ones(n).reshape([n,1])
		
		x,reject_rate = hitandrun(A,b,x0,nsamp,pdf_id,unif,basis,translation)
		print reject_rate,x
		
def pdf_horner_prod(np.ndarray[double, ndim=1, negative_indices=False] c, 
			 np.ndarray[double, ndim=1, negative_indices=False] x, double scale,double cutoff):
			# WARNING: the factor 1/x is included in this function	 
			if np.any(x/scale<cutoff): 
				return 1e-10
			else:
				return np.prod( interpolate.C_Horner_vec(c, np.log10(x/scale)))

def pdf_horner(np.ndarray[double, ndim=1, negative_indices=False] c, double x, double scale,double cutoff):
			if np.any(x/scale<cutoff): 
				return 1e-10
			else:
				return interpolate.C_Horner(c, np.log10(x/scale))
