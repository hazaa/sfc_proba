#cython: boundscheck=False, wraparound=False, nonecheck=False

import numpy as np
cimport numpy as np
cimport cython

#ctypedef np.float_t DTYPE_t

"""
>cython interpolate.pyx -a
>gcc -O3 -march=native interpolate.c -shared -fPIC `python-config --cflags --libs` -o interpolate.so

copied from:
https://stackoverflow.com/questions/28250401/problems-in-implementing-horners-method-in-python
"""

#def C_Horner(np.ndarray[double, ndim=1, negative_indices=False] c, DTYPE_t x):
def C_Horner(np.ndarray[double, ndim=1, negative_indices=False] c, double x):
    #cdef DTYPE_t p=0
	cdef double p=0
	for i in reversed(c):
		p = p * x + i
	return p 

def C_Horner_vec(np.ndarray[double, ndim=1, negative_indices=False] c, np.ndarray[double, ndim=1, negative_indices=False] x):
	cdef j=0
	cdef int dim = x.shape[0]
	#cdef DTYPE_t p=0    
	cdef np.ndarray[double, ndim=1]  p = np.zeros(dim)

	for k in range(dim):		
		for i in reversed(c):
			p[k] = p[k] * x[k] + i
	return p	
