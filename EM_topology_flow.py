# -*- coding: utf-8 -*-
"""
Created on oct 1st 2018

Module:
    EM_topology_flow -  EM algorithm to fit both the network topology
						  and the continuous weights.
						 The algorithms proposed by [Rao2018] compute
						 non-negative and sparse solutions. 
    
Author:
    Aurélien Hazan, after 
    
Description:
    Implementation of ...
    
Usage:
    Be ``strn_in,strn_out`` two 1-dimensional NumPy arrays,. 
    Import the module and initialize the ... ::
        >>> import 
        >>> cs = ...
    To create ::
        >>> cs.do_smthg()

References:
.. [Rao2018] `Nalci,Fedorov,Al-Shoukairi,Liu, Rao,
    Rectified Gaussian Scale Mixtures and the Sparse
	Non-Negative Least Squares Problem,
    <http://arxiv.org/abs/1601.06207>`_ 

scipy.sparse: http://docs.scipy.org/doc/scipy/reference/sparse.html
inv sparse: https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.inv.html#scipy.sparse.linalg.inv
diags: https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.diags.html#scipy.sparse.diags
sparse linalg: https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html#module-scipy.sparse.linalg
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from transmat import CSP_sfc_fullconnected_noMLK,CSP_sfc_FiCM_noMLK,CSP_sfc_FiCM_noMLK_alpha0
#from scipy.sparse import diags,spdiags
import scipy.sparse
import scipy.sparse.linalg
#from scipy.sparse import csc_matrix
#from scipy.sparse.linalg import inv
from sklearn.utils.extmath import safe_sparse_dot
from scipy.special import erfc

from CS_topology_flow import make_CS_data
from topological_measures import prepare_network_rndfitness

def Sigma(Phi,sigma,gamma):
	"""
	[Rao2018]
	Σ = Γ − ΓΦ^T(σ^2 I + ΦΓΦ^T)^{-1} ΦΓ     eq.(24)
	!TODO: optimize unnecessary operations below!
	"""
	N,M = Phi.shape
	if not scipy.sparse.issparse(Phi):
		raise ValueError('Phi must be sparse')
	gamma_diag = scipy.sparse.diags(gamma)
	PhiGamma = Phi.dot( gamma_diag )
	PhiGammaPhiT=PhiGamma.dot( Phi.transpose() )
	try:
		Inv = scipy.sparse.linalg.inv( sigma**2 *scipy.sparse.eye(N) +PhiGammaPhiT)
	except RuntimeError:
		raise RuntimeError("Sigma: matrix inversion failed")
	dum = Inv.dot(PhiGamma)
	GammaPhiT = gamma_diag.dot( Phi.transpose() )
	dum = GammaPhiT.dot(dum)
	return 	gamma_diag- dum
	
def muSigma(Phi,sigma,gamma,y):
	"""
	[Rao2018]
	μ = ΓΦ^T (σ^2 I + ΦΓΦ^T)^{−1} y			eq.(23)
	Σ = Γ − ΓΦ^T(σ^2 I + ΦΓΦ^T)^{-1} ΦΓ     eq.(24)
	
	!TODO: optimize unnecessary operations below!
	
	input:
	------
	Phi: sparse array, shape=(N,M)
	main matrix in y=Phi x + noise
	
	sigma: float
	noise standard deviation
	
	gamma: float
	parameter of the bayesian sparse model
	
	y: float
	lhs in system of equation y=Phi x + noise
	
	output:
	-------
	mu: array, shape=(M,)
	parameter of the posterior of the rectified gaussian
	
	Sigma:array, shape=(M,M)
	parameter of the posterior of the rectified gaussian
	
	"""
	N,M = Phi.shape
	if not gamma.size==M:
		raise ValueError('gamma has invalid size')
	if not scipy.sparse.issparse(Phi):
		raise ValueError('Phi must be sparse')
		
	gamma_diag = scipy.sparse.diags(gamma)
	PhiGamma = Phi.dot( gamma_diag )
	PhiGammaPhiT=PhiGamma.dot( Phi.transpose() )
	try:
		Inv = scipy.sparse.linalg.inv( sigma**2 *scipy.sparse.eye(N) +PhiGammaPhiT)
	except RuntimeError:
		raise RuntimeError("Sigma: matrix inversion failed")

	GammaPhiTInv = gamma_diag.dot( Phi.transpose() ).dot(Inv)
	mu = safe_sparse_dot(GammaPhiTInv,y)
	Sigma = gamma_diag - GammaPhiTInv.dot(PhiGamma)
	return 	mu, Sigma
	
def av_RG_product_posterior(mu,diag_Sigma):
	"""
	<x>,<x2> : see equations (47,48) in [Rao2018]
	"""
	if not mu.shape == diag_Sigma.shape:
		raise ValueError('shape mismatch')
	exp_ = np.exp( - mu**2/(2.*diag_Sigma))
	erfc_ = erfc( - mu /np.sqrt(2.*diag_Sigma))
	exp_over_erf = exp_ / erfc_
	av_x  = mu + np.sqrt(2*diag_Sigma/np.pi)* exp_over_erf
	av_x2 =	mu**2 + diag_Sigma + mu * np.sqrt(diag_Sigma/np.pi) *exp_over_erf
	return av_x,av_x2	
	
def EM_DA(sigma, gamma0,Phi,y,tol=1e-4,maxit=1000):	
	"""
	Expectation Maximization (EM) algorithm to solve y=Phi x + noise
	with diagonal approximation (DA) as in [Rao2018]

	while not converged:
		compute μ ,	Σ 		
		compute <x>,<x2>
		compute gamma			
	return <x>,<x2>,μ,Σ,gamma	
	"""	
	eps = 1E10; err=100
	it=0
	gamma=gamma0*np.ones(Phi.shape[1])
	err_vec=np.zeros(maxit)
	while (eps>tol) and (it<maxit) and (err>1.0):
		# E-step
		mu,Sigma = muSigma(Phi,sigma,gamma,y)
		diag_Sigma=  Sigma.diagonal()
		av_x,av_x2 = av_RG_product_posterior(mu,diag_Sigma)
		# M-step
		gamma = av_x2
		# assess convergence
		if(it>0):
			eps = np.mean(np.abs(av_x_old-av_x ) )+np.mean(np.abs(av_x2_old-av_x2 ) )
		err = np.mean((Phi.dot(av_x) - y) ** 2)/np.mean( av_x** 2) *100.		
		print "it=%d, eps=%f, err=%f"%(it,eps,err)
		av_x_old=av_x
		av_x2_old=av_x2
		err_vec[it]=err
		it+=1
	return av_x,av_x2,mu,diag_Sigma,err_vec
	
def test_av_RG_product_posterior():
	"""
	!!TODO!!
	"""
	av_RG_product_posterior(mu,diag_Sigma)
	

	
def test_EM_DA_sfc(model='sfc_FiCM_noMLK_A_unknown'):
	"""
	algo=EM_DA
	data=CSP_sfc_fullconnected_noMLK
	"""
	sigma= 1e-2
	gamma0=1.0
	nb,nf,nh = 3,10,50
	nb,nf,nh = 3,20,1000
	A,y=[],[]
	if model=='sfc_fullconnected_noMLK':		
		A,y=CSP_sfc_fullconnected_noMLK(nb,nf,nh)
	elif model=='sfc_FiCM_noMLK':
		p={'network':{'year':'2010','geo':'CZ','nb':nb,'nf':nf,'nh':nh,
					  'nlink_per_firm_cons': 2,  
					'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20,#3 is better for plotting
					'nlink_per_hh_wage':1},
				'mc':{'niter':10}}
		n,p=prepare_network_rndfitness(p=p,fitness_type='uniform')
		A_net,B,C,D,E,FF = n.sample_full_network(nb,nf,nh)
		A,y,lb,ub,mu_prior = CSP_sfc_FiCM_noMLK(nb,nf,nh,A_net,B,C,D,E)
	elif model=='sfc_FiCM_noMLK_A_unknown':	
		# here A_net is unknown
		# and we're interested in the inferred connectivity
		p={'network':{'year':'2010','geo':'CZ','nb':nb,'nf':nf,'nh':nh,
			  'nlink_per_firm_cons': 2,  
			'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20,#3 is better for plotting
			'nlink_per_hh_wage':1},
		'mc':{'niter':10}}
		n,p=prepare_network_rndfitness(p=p,fitness_type='uniform')
		A_net,B,C,D,E,FF = n.sample_full_network(nb,nf,nh)
		A,y,lb,ub,mu_prior = CSP_sfc_FiCM_noMLK(nb,nf,nh,None,B,C,D,E)	
	elif model=='sfc_FiCM_noMLK_alpha0':
		alpha0=0.1		
		A_net,B,C,D,E,FF = n.sample_full_network(nb,nf,nh)
		A,y,lb,ub,mu_prior = CSP_sfc_FiCM_noMLK_alpha0(nb,nf,nh,A_net,B,C,D,E,
										alpha0=alpha0)
	else:
		raise ValueError('unknown model')	
	tol=1e-5;maxit=3000
	av_x,av_x2,mu,diag_Sigma,err = EM_DA(sigma, gamma0,A,y,tol,maxit)
	print "A.shape=",A.shape
	plt.subplot(2,1,1)
	plt.plot(av_x)
	plt.ylabel('x')
	plt.subplot(2,1,2)
	plt.plot(err)
	plt.ylabel('err=f(it)')
	plt.show()
	
def test_EM_DA_Asparse():
	"""
	test EM_DA algorithm with sparse matrix A
	"""
	nr = 100
	nc = 500
	A,y,z0,z0_mean_on, z0_var_on, prob_on,wvar= make_CS_data(nr,nc,model='Fsparse',frac_nonzeros_A=0.5) 	
	print "A.shape=",A.shape
	tol=1e-5;maxit=100
	sigma= np.sqrt(wvar)
	gamma0=1.0
	av_x,av_x2,mu,diag_Sigma,err = EM_DA(sigma, gamma0,A,y,tol,maxit)
	# plot
	nplot = 3
	plt.subplot(nplot,1,1)
	plt.plot(err)
	plt.ylabel('err=y-Ax')
	plt.subplot(nplot,1,2)
	plt.plot(z0)
	plt.ylabel('x true')
	plt.subplot(nplot,1,3)
	plt.plot(av_x)
	plt.plot(av_x-np.sqrt(av_x2), color='r')
	plt.plot(av_x+np.sqrt(av_x2), color='r')
	plt.ylabel('x approx')
	plt.show()
def test_diagonal_dominance_sfc(model='sfc_fullconnected_noMLK'):
	"""
	check for diagonal dominance in Σ
	"""
	#nb,nf,nh = 3,10,50
	nb,nf,nh = 3,20,100
	A,y=[],[]
	if model=='sfc_fullconnected_noMLK':
		A,y=CSP_sfc_fullconnected_noMLK(nb,nf,nh)	
	else:
		raise ValueError('unknown model')	
	nr,nc = A.shape
	#A=np.array(A.todense().astype('float'))
	#nz1,nz0 = A.shape
	# compute Sigma
	gamma_v = np.logspace(-3,1, 3)
	sigma_v = np.logspace(-6,0, 3)
	
	for sigma in sigma_v:
		for gamma in gamma_v:
			success=1
			try:
				mu,S = muSigma(A,sigma,gamma*np.ones(nc), np.random.rand(nr))
				#S=Sigma(A,sigma,gamma*np.ones(nc))				
			except RuntimeError:
				success=0
				print "Sigma failed"	
			# get mean of diagonal terms
			if(success):
				mean_abs_diag = S.diagonal()
				mean_abs_diag = np.mean(np.abs(mean_abs_diag))
				mean_abs_all = (np.abs(S)).mean()				
				#print "sigma=%f,gamma=%f, mean_abs_diag=%f, mean_abs_all=%f"%(sigma,gamma,mean_abs_diag,mean_abs_all)
				print "sigma=%f,gamma=%f, mean_abs_all/mean_abs_diag=%f"%(sigma,gamma,mean_abs_all/mean_abs_diag)
			else:
				print "sigma=%f,gamma=%f, mean_abs_all/mean_abs_diag FAILED"%(sigma,gamma)
	
	
	

if __name__ == "__main__":
	
	#test_diagonal_dominance_sfc()
	test_EM_DA_sfc()
	#test_EM_DA_Asparse()
