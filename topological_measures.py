# -*- coding:utf-8
#  topological_measures.py
#  
#  Copyright 2018 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""

Refs:
	[Atalay11]: ?????????
	[Caldarelli et al. 02] 10.1103/PhysRevLett.89.258702
	[Garlaschelli et al. 04] 

see also:
http://networkx.github.io
https://pypi.python.org/pypi/pynverse
https://github.com/nkoub/multinetx

powerlaw fit: 
 https://pypi.python.org/pypi/powerlaw
 http://nbviewer.jupyter.org/github/jeffalstott/powerlaw/blob/master/manuscript/Manuscript_Code.ipynb
 http://www.josephshaheen.com/fitting-power-law-comparing-degree-distribution-synthetic-network-collected-one/2506
 https://github.com/jothemachine/fitting-a-powerlaw/blob/master/Network_Fitting.ipynb


TODO:
 all networks !!
 centrality: https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.bipartite.centrality.degree_centrality.html#networkx.algorithms.bipartite.centrality.degree_centrality
"""

import numpy as np
import pandas as pd
import networkx as nx
from networkx.algorithms import bipartite
import matplotlib.pyplot as plt
from network import *
import multinetx as mx
import sys
sys.path.append('extern')
from comp_enum import comp_enum 
from comp_next import comp_next
import powerlaw	
from scipy import interpolate
from scipy.stats import gaussian_kde
import matplotlib.lines as lines

marker = ["o","v","^","1","D","s","p","+","*"]
label_sector_agg = ['B-E', 'F', 'G-I', 'J', 'K', 'L', 'M_N', 'O-Q', 'R-U']
color = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'purple', 'brown']

def test_multinetx_3layers():
	#https://github.com/nkoub/multinetx
	N1 = 10; N2=30; N3=100; N=N1+N2+N3
	g1 = mx.erdos_renyi_graph(N1,0.07,seed=218)
	g2 = mx.erdos_renyi_graph(N2,0.07,seed=211)
	g3 = mx.erdos_renyi_graph(N3,0.07,seed=208)
	#Define the type of interconnection between the layers
	#https://github.com/nkoub/multinetx#define-the-type-of-interconnection-between-the-layers-1
	#adj_block = mx.lil_matrix(np.zeros((N*4,N*4)))
	adj_block = mx.lil_matrix(np.zeros((N,N)))
	adj_block[0  :  N1 ,   N1:N1+N2] = np.random.poisson(0.005,size=(N1,N2)) #np.identity(N)   # L_12
	adj_block[0  :  N1 , N1+N2:N1+N2+N3] = np.random.poisson(0.005,size=(N1,N3))   # L_13
	#adj_block[0  :  N , 3*N:4*N] = np.random.poisson(0.006,size=(N,N))   # L_34
	#adj_block[3*N:4*N , 2*N:3*N] = np.random.poisson(0.008,size=(N,N))   # L_14
	adj_block += adj_block.T
	adj_block[adj_block>1] = 1
	#Create an instance of the MultilayerGraph class
	mg = mx.MultilayerGraph_AH(list_of_layers=[g1,g2,g3],  #,g1],
							inter_adjacency_matrix=adj_block)
	mg.set_edges_weights(inter_layer_edges_weight=5)
	mg.set_intra_edges_weights(layer=0,weight=1)
	mg.set_intra_edges_weights(layer=1,weight=2)
	mg.set_intra_edges_weights(layer=2,weight=3)
	#mg.set_intra_edges_weights(layer=3,weight=4)
	#Plot the adjacency matrix and the multiplex networks
	pos = mx.get_position(mg,mx.fruchterman_reingold_layout(mg.get_layer(0)),
					  layer_vertical_shift=.3,
					  layer_horizontal_shift=0.9,
					  proj_angle=.2)
	fig = plt.figure(figsize=(15,5))
	ax1 = fig.add_subplot(111)			  
	mx.draw_networkx(mg,pos=pos,ax=ax1,node_size=50,with_labels=False,
				 edge_color=[mg[a][b]['weight'] for a,b in mg.edges()],
				 edge_cmap=plt.cm.jet_r)
	plt.show()

def test_multinetx():
	#https://github.com/nkoub/multinetx
	N = 50
	g1 = mx.erdos_renyi_graph(N,0.07,seed=218)
	g2 = mx.erdos_renyi_graph(N,0.07,seed=211)
	g3 = mx.erdos_renyi_graph(N,0.07,seed=208)
	#Define the type of interconnection between the layers
	#https://github.com/nkoub/multinetx#define-the-type-of-interconnection-between-the-layers-1
	adj_block = mx.lil_matrix(np.zeros((N*4,N*4)))
	adj_block[0  :  N ,   N:2*N] = np.identity(N)   # L_12
	adj_block[0  :  N , 2*N:3*N] = np.random.poisson(0.005,size=(N,N))   # L_13
	adj_block[0  :  N , 3*N:4*N] = np.random.poisson(0.006,size=(N,N))   # L_34
	adj_block[3*N:4*N , 2*N:3*N] = np.random.poisson(0.008,size=(N,N))   # L_14
	adj_block += adj_block.T
	adj_block[adj_block>1] = 1
	#Create an instance of the MultilayerGraph class
	mg = mx.MultilayerGraph(list_of_layers=[g1,g2,g3,g1],
							inter_adjacency_matrix=adj_block)
	mg.set_edges_weights(inter_layer_edges_weight=5)
	mg.set_intra_edges_weights(layer=0,weight=1)
	mg.set_intra_edges_weights(layer=1,weight=2)
	mg.set_intra_edges_weights(layer=2,weight=3)
	mg.set_intra_edges_weights(layer=3,weight=4)
	#Plot the adjacency matrix and the multiplex networks
	pos = mx.get_position(mg,mx.fruchterman_reingold_layout(mg.get_layer(0)),
					  layer_vertical_shift=.3,
					  layer_horizontal_shift=0.9,
					  proj_angle=.2)
	fig = plt.figure(figsize=(15,5))
	ax1 = fig.add_subplot(111)			  
	mx.draw_networkx(mg,pos=pos,ax=ax1,node_size=50,with_labels=False,
				 edge_color=[mg[a][b]['weight'] for a,b in mg.edges()],
				 edge_cmap=plt.cm.jet_r)
	plt.show()


def test_employment_network_rndfitness_consistency_1():
	"""
	check consistency of employment network: sector employment must
	agree with empirical data used to fit the network
	
	loop on quenched variable
	"""	
	unit='MIO_EUR';
	year='2010';geo='CZ';nb=3;nf=100;nw=500;
	fname='data/eurostat_network.h5'
	L_invest=3*nf
	L_cons_firm=2*nf
	L_cons_hh = 4*nw
	L_wage=nw
	# pareto.pdf(x, b) = b / x**(b+1) for x >= 1, b > 0.
    # https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
	b=1.2
	rv = pareto(b)
	# load pre-processed data in h5 file
	n=NaioNetwork()
	n.get_nama_from_h5(fname)	
	n.aggregate_dot_products(year,geo,fname_in=fname)
	n.aggregate_supplyuse(year,geo,unit='MIO_EUR',fname_in=fname)	
	n.get_bd_from_h5(fname, 'bd_9ac_l_form_r2')	
	n.aggregate_bd(year,geo,fname_in=fname)
	# init
	# loop on quenched variable
	niter = 50
	niter_effective=0
	employment_per_sector_ficm_stack = np.array([])
	n_sector = len(n.sectors_aggregate.keys()) # MOVE TO NaioNetwork() init

	nfirm_per_sector_ficm= np.zeros(n_sector)
	for k in range(niter):
		#fitness=np.random.rand(nf)
		fitness = rv.rvs(nf)
		try:	
			n.init_employment_network_rndfitness(nw,nf,L_wage,fitness) # this init first		
			init_done=1
			niter_effective +=1
		except ValueError:
			init_done=0
		if init_done:	
			pij =n.ficm_wage.get_biadjacency_matrix(n.ficm_wage._pij)
			#pij=n.ficm_wage.mat
			employment_per_firm_average = nw * pij[:,0]
			# sector employment sum, from ficm
			employment_per_sector_ficm= np.zeros(n_sector)
			for i,s in enumerate(n.label_sector):			
				i_firm = np.where( s == n.result_firm_to_sector )[0]
				employment_per_sector_ficm[i] = np.sum(employment_per_firm_average[i_firm])		
				nfirm_per_sector_ficm[i]=i_firm.shape[0] # this is repeated without necessity
			if(employment_per_sector_ficm_stack.size==0):	
				# this test is preferred to k==0 because exception can occur	
				employment_per_sector_ficm_stack=employment_per_sector_ficm
			else:	
				employment_per_sector_ficm_stack = np.hstack((employment_per_sector_ficm_stack,employment_per_sector_ficm))	
	employment_per_sector_ficm_mean=employment_per_sector_ficm_stack.copy()
	employment_per_sector_ficm_mean = employment_per_sector_ficm_mean.reshape((niter_effective,n_sector))
	employment_per_sector_ficm_mean= np.mean(employment_per_sector_ficm_mean,axis=0)
	# sector employment sum, from eurostat	
	pct_nb_employ = n.result_aggregate_bd.loc[sorted(n.sectors_aggregate.keys()),'PCT_NB_EMPLOY']
	pct_nb_firm = n.result_aggregate_bd.loc[sorted(n.sectors_aggregate.keys()),'PCT_NB_FIRM']
	employment_per_sector_empirical = nw*np.asarray(pct_nb_employ)		
	nb_firm_per_sector_empirical = nf * np.asarray(pct_nb_firm)
	# plot nb firm/sector ; nb employ/sector
	x=range(n_sector)
	plt.subplot(211)
	plt.plot(x,employment_per_sector_empirical,'x',label='nb job/sector empirical')
	plt.plot(x,nfirm_per_sector_ficm,label='nb firm/sector ficm')
	plt.plot(x,nb_firm_per_sector_empirical, label='nb firm/sector empirical')
	plt.legend()
	plt.xlim([min(x)-0.5,max(x)+0.5])
	plt.subplot(212)
	plt.plot(np.tile(x,niter_effective),employment_per_sector_ficm_stack,
			'o', label="employment per sector ficm")
	plt.plot(x,employment_per_sector_empirical,'x',markersize=15,
			label="employment per sector empirical"	)
	plt.plot(x,employment_per_sector_ficm_mean,'^',markersize=10,
			label="employment per sector ficm mean")
	plt.xlim([min(x)-0.5,max(x)+0.5])
	plt.legend()
	# check sector demography is respected

def test_employment_network_rndfitness_consistency_2():
	"""
	check consistency of employment network: sector employment must
	agree with empirical data used to fit the network
	
	loop on quenched variable + network sampling
	"""	
	pass

"""
def test_sample_fitness_pareto_hd():
	# sample
	#groups= np.hstack((np.zeros(5),np.ones(10),2*np.ones(20) ))
	#sums= np.array([1,1,1])	
	groups= np.hstack((np.zeros(10),np.ones(20),2*np.ones(30),
						3*np.ones(10),4*np.ones(20),5*np.ones(30),
						6*np.ones(10),7*np.ones(20),8*np.ones(30)	))
	means= np.array([[0,1,2,3,4,5,6,7,8],np.tile([0.1, 0.05, 0.033],3)])
	
	sums= np.array([1,1,1,1,1,1,1,1,1])			
	x=sample_fitness_pareto_hd(groups, sums,p=1.,debug=False)
	
	plt.scatter(groups,x)
	plt.scatter(means[0,:],means[1,:], c= 'r')
	plt.xlabel("group")
	plt.ylabel("x")

def test_sample_fitness_pareto_cd():
	# sample
	#groups= np.hstack((np.zeros(5),np.ones(10),2*np.ones(20) ))
	#sums= np.array([1,1,1])	
	groups= np.hstack((np.zeros(10),np.ones(20),2*np.ones(30),
						3*np.ones(10),4*np.ones(20),5*np.ones(30),
						6*np.ones(10),7*np.ones(20),8*np.ones(30)	))
	sums= np.array([1,1,1,1,1,1,1,1,1])	
	x=sample_fitness_pareto_cd(groups, sums,debug=True)
	#####################
	# GROUP BY GROUP
	g,g_idx = np.unique(groups,	return_index=True )
	for g_uniq in g:		
		powerlaw.plot_ccdf( x[groups == g_uniq], color ='b')
	powerlaw.plot_ccdf( x, color='r', linestyle='--',)	
	plt.title("CCDF of Degree distribution")
	plt.ylabel("P(X > x)")
	plt.xlabel("x Distribution")
	plt.show()
	#####################
	# ALL TOGETHER
	# cdf
	powerlaw.plot_cdf(x, color='r')
	plt.title("Cumulative Distribution Function")
	plt.ylabel("F(x)")
	plt.xlabel("x Distribution")
	plt.show()
	# ccdf
	powerlaw.plot_ccdf(x, color ='b')
	plt.title("CCDF of Degree distribution")
	plt.ylabel("P(X > x)")
	plt.xlabel("x Distribution")
	plt.show()
	# fit
	res = powerlaw.Fit(x,discrete=False)
	print("Max Likelihood Estimate of Alpha: %5.3f" % (res.power_law.alpha)) 
	cutoffString_gen = '{:,.2f}'.format(res.power_law.xmin)  # I'm just formatting the output
	print("Cutoff value, xmin:  "+cutoffString_gen)
	#R, p = results.distribution_compare('power_law', 'lognormal')
	# fit:plot
	fig4 = res.plot_ccdf(linewidth=1.5)
	res.power_law.plot_ccdf(ax=fig4, color='r', linestyle='--', label='PL Fit')
	res.lognormal.plot_ccdf(ax=fig4, color='g', linestyle='--', label='Lognormal Fit') 
	res.exponential.plot_ccdf(ax=fig4, color='y', linestyle='--', label='Exponential Fit') 
	res.truncated_power_law.plot_ccdf(ax=fig4, color='m', linestyle='--', label='truncated_power_law')
	plt.title("Comparison of Power Law with Other Fits")
	plt.legend(loc="lower left")
	plt.xlabel("Distribution (log)")
	plt.show()
"""
######################################################################
## intermediate consumption of firms

# helper function
def get_aij(pij):
	rnd = np.random.rand(pij.shape[0]*pij.shape[1] ).reshape((pij.shape[0], pij.shape[1]))
	return np.array(rnd < pij , dtype=np.int64)

def degree_empirical_block_1sector():
	"""
	viz 1 sector only (not all sector
	"""
	raise NotImplementedError		
	# ONE SECTOR ONLY												
	# get theoretical degree for one firm in a given sector
	nsec = sectors.shape[0]	
	i0 = 10
	nk_steps = 10 		
	kmin = 0	
	pi0_by_sec = pij_investment_firm[i0,unique_indices.tolist()] 
	nfmax_by_sec  = np.asarray(n.result_pct_nb_firm * nf,int) 
	k_vec = np.asarray(np.linspace(kmin, pi0_by_sec.size, nk_steps) , int)
	d_th = degree_theor_block(nsec,k_vec,pi0_by_sec,nfmax_by_sec)
	print k_vec,d_th
	# get empirical degree
	nsamp = p['mc']['niter']
	d_sample =np.zeros(nsamp,dtype=int)
	for i in range(nsamp):																																																																																																																																																																					
		rnd = np.random.rand(pij_investment_firm.shape[1] )
		mat = np.array(rnd < pij_investment_firm[i0,:] , dtype=np.int64)	
		d_sample[i] = mat.sum()
	plt.hist(d_sample, normed=True)		
	plt.plot(k_vec,d_th)
	plt.show()
	
def assortativity_theor(pcp,dc,up,w=None):
	"""
	compute the theoretical assortativity (Average Nearest Neighbor)
	see Saracco et al arXiv:1503.05098 , eq.(34-35)
	
	parameters:
	----------
	pcp: array, shape=(C,P)
	the edge probability matrix
	
	dc: array, shape=(C,)
	the degree array along the first axis
	
	up: array, shape=(P,)
	the degree array along the second axis
	
	"""
	if not pcp.ndim==2: raise ValueError('pij must be 2-dimensional')
	if not dc.ndim==1: raise ValueError('dc must be 1-dimensional')
	if not up.ndim==1: raise ValueError('up must be 1-dimensional')	
	C,P = pcp.shape
	ucnn = np.zeros(C)
	dpnn = np.zeros(P)
	if w==None:
		for c in range(C):
				ucnn[c]= 1./dc[c] * np.sum(pcp[c,:] *(up - pcp[c,:] +1.) )			
		for p in range(P):
				dpnn[p]= 1./up[p] * np.sum(pcp[:,p] *(dc - pcp[:,p] +1.) )				
	else:
		if C==P:					
			for c in range(C):
					ucnn[c]= 1./dc[c] * np.sum(w*(pcp[c,:] *(up - pcp[c,:] +1.)) )			
			for p in range(P):
					dpnn[p]= 1./up[p] * np.sum(w*(pcp[:,p] *(dc - pcp[:,p] +1.) ))				
		else:
			raise ValueError('nonsquare pij not supported')
		
	return ucnn,dpnn

def degree_theor_block(nsec,k_vec,pi_by_sec,nmax_by_sec):
	"""
	compute the probability P(degree=k) for each k in k_vec
	in the case of block models.
	in reduced dimension (an enumeration is necessary)
	
	params:
	--------
	k_vec: int array, shape=(nk,)
	the degrees 
	
	pi_by_sec: float array, shape=(nsec,)
	the probabilities that an edge exists between an agent of sector i and an agent of sector s in [0,nsec]
	
	nmax_by_sec: int array, shape=(nsec,)
	the maximum number of agents per sector
	
	returns:
	---------
	pk: float array, shape=(nk,)
	the probabilities for each k in k_vec
	
	see:
	http://people.sc.fsu.edu/~jburkardt/py_src/subset/ 
	comp_next.py,comp_enum.py, i4_choose.py	
	"""
	from scipy.stats import binom
	pk=np.zeros(k_vec.size)
	nbcompmax = 1e5
	for ik,k in enumerate(k_vec):
		nbcomp = comp_enum( k, nsec )
		if(nbcomp<nbcompmax): 
			#exact
			p_sum=0.
			a = np.zeros(nsec)
			more = False
			h = 0
			t = 0
			while(True):
				a, more, h, t = comp_next ( k, nsec, a, more, h, t )
				#for i in range ( 0, k ):  print ( '%2d  ' % ( a[i] ) ),
				p_prod = 1.
				for i in range(0,nsec):
					p_prod *= binom.pmf(a[i], nmax_by_sec[i], pi_by_sec[i])
				p_sum += p_prod	
				if ( not more ):
					break
		else:	
			#approx
			raise NotImplementedError()
		pk[ik]=p_sum	
	
	return pk
	
	
def measure_sample_mono_DiGraph(aij,verb=0):
	"""
	adjacency matrix of monopartite directed graph
	"""
	# convert to DiGraph
	#https://networkx.github.io/documentation/stable/reference/generated/networkx.convert_matrix.from_numpy_matrix.html?highlight=from_numpy#networkx.convert_matrix.from_numpy_matrix	
	g = nx.from_numpy_matrix(aij,parallel_edges=False,create_using=nx.DiGraph())
	# compute degree
	# https://networkx.github.io/documentation/stable/reference/classes/generated/networkx.DiGraph.degree.html#networkx.DiGraph.degree
	#d = g.degree().values()	
	deg_in = g.in_degree().values()		
	deg_out = g.out_degree().values()		
	# assortativity:  average-degree-connectivity
	#https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.assortativity.average_degree_connectivity.html?highlight=nearest%20neighbor#networkx-algorithms-assortativity-average-degree-connectivity
	ANND_in_out=nx.average_degree_connectivity(g, source='in', target='out')
	ANND_out_in=nx.average_degree_connectivity(g, source='out', target='in')
	# assortativity: correlation_coefficient
	r_out_in = nx.degree_pearson_correlation_coefficient(g, x='out', y='in') 
	r_in_out = nx.degree_pearson_correlation_coefficient(g, x='in', y='out') 
	# print
	if(verb>0):
		print "deg_in=",deg_in, " deg_out=",deg_out
		print "r_out_in=",r_out_in," r_in_out=",r_in_out
		print ANND_in_out
		print ANND_out_in
	return deg_in,deg_out,r_out_in,r_in_out, ANND_in_out, ANND_out_in

	
def measure_sample_bip_Graph(aij,verb=0):
	"""
	"""
	# convert to Graph
	g,X,Y = from_numpy_matrix_bipartite(aij)	
	# degree
	deg_0,deg_1=bipartite.degrees(g,nodes=Y)	
	# 'ANNU','ANND'
	#https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.assortativity.average_degree_connectivity.html?highlight=nearest%20neighbor#networkx-algorithms-assortativity-average-degree-connectivity
	# !!!!!!!!!!  average_neighbor_degree=> sorted by NODE, not degree
	# only for selected nodes
	ann_0 = nx.average_neighbor_degree(g, nodes=X)
	ann_1 = nx.average_neighbor_degree(g, nodes=Y)
	if(verb>0):
		print deg_0,deg_1
		print ann_0,ann_1
	return deg_0,deg_1,ann_0,ann_1

	
def prepare_network_block():
	plt.rcParams['font.size']=20
	plt.rcParams['xtick.labelsize']='large'
	plt.rcParams['ytick.labelsize']='large'
	# prepare network
	n=NaioNetwork()
	p={'network':{'year':'2010','geo':'CZ','nb':3,'nf':100,'nh':1000,
				  'nlink_per_firm_cons': 2,  
				'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20},#3 is better for plotting
			'mc':{'niter':10}}
	nb = p['network']['nb']
	nf = p['network']['nf']
	nh = p['network']['nh']
	year = p['network']['year']
	geo = p['network']['geo']
	#L_invest = int(np.floor(p['network']['nlink_per_firm'] * nf)) # [Atalay11]		
	L_invest= int(p['network']['nlink_per_firm_invest'] *nf)
	L_cons_firm=int(p['network']['nlink_per_firm_cons']*nf)
	L_cons_hh = int(p['network']['nlink_per_hh_cons'] *nh)
	n=init_network_block(year=year,geo=geo,nb=nb,nf=nf,nh=nh,fname='data/eurostat_network.h5',L_invest=L_invest, L_cons_firm=L_cons_firm , L_cons_hh = L_cons_hh	)
	"""
	p['fitness'] = {'seed':None, 'thin': 1, 'accel':1, 'log':1,
				'f':cython_har.pdf_pareto_cython,
				'f_mean': 0,
				'f_nsamp': lambda x:10*x**2,
				'param1':1.1 , 'param2':0.1	}
	p['fitness']['f_mean'] = p['fitness']['param2']* p['fitness']['param1']/(p['fitness']['param1']-1.)  # b*xlim/b-1
	n=init_network_rndfitness(year='2008',geo='CZ',nb=nb,nf=nf,nw=nw,fname='data/eurostat_network.h5',fitness = p['fitness'],L_invest=L_invest)#, L_cons_firm=L_cons_firm , L_cons_hh = L_cons_hh, 	
	n=init_network_pareto(year='2008',geo='CZ',nb=nb,nf=nf,nw=nw,fname='data/eurostat_network.h5',b_pareto=b_pareto,nsamp=1000)#L_invest=L_invest, L_cons_firm=L_cons_firm , L_cons_hh = L_cons_hh, 	
	"""
	return n,p

def prepare_network_rndfitness(p=None,fitness_type='uniform'):
	"""
	Example:
	n,p = prepare_network_rndfitness()
	"""
	plt.rcParams['font.size']=20
	plt.rcParams['xtick.labelsize']='large'
	plt.rcParams['ytick.labelsize']='large'
	# prepare network
	n=NaioNetwork()	
	if p==None:
		p={'network':{'year':'2010','geo':'CZ','nb':3,'nf':100,'nh':1000,
					  'nlink_per_firm_cons': 2,  
					'nlink_per_firm_invest':2 , 'nlink_per_hh_cons': 20,#3 is better for plotting
					'nlink_per_hh_wage':1},
				'mc':{'niter':10}}
	nb = p['network']['nb']
	nf = p['network']['nf']
	nh = p['network']['nh']
	year = p['network']['year']
	geo = p['network']['geo']
	#L_invest = int(np.floor(p['network']['nlink_per_firm'] * nf)) # [Atalay11]		
	L_invest= int(p['network']['nlink_per_firm_invest'] *nf)
	L_cons_firm=int(p['network']['nlink_per_firm_cons']*nf)
	L_cons_hh = int(p['network']['nlink_per_hh_cons'] *nh)
	L_wage = int(p['network']['nlink_per_hh_wage']*nh)
	# fitness
	fitness=0
	if fitness_type=='uniform':
		fitness = np.random.rand(nf)
	elif fitness_type=='pareto':
		# pareto.pdf(x, b) = b / x**(b+1) for x >= 1, b > 0.
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto    		
		b=1.0
		rv = pareto(b)
		fitness = rv.rvs(nf)
	else:
		raise ValueError('unrecognized fitness type')	
	n=init_network_rndfitness(year=year,geo=geo,nb=nb,nf=nf,nh=nh,
							fname='data/eurostat_network.h5',
							L_invest=L_invest, L_cons_firm= L_cons_firm,
							L_cons_hh = L_cons_hh,
							L_wage= L_wage,
							fitness=fitness,proxy_invest_conso=True)							
	return n,p

def plot_network_bipartite(aij,firm_to_sector,label_sector,scale=0.2):
	"""
	https://networkx.github.io/documentation/stable/tutorial.html#drawing-graphs
	"""
	result_firm_to_sector = firm_to_sector
	sectors = np.unique(result_firm_to_sector)
	nsec = sectors.shape[0]
	n_node_firms = aij.shape[0]
	n_node_hh = aij.shape[1]	
	# get graph
	G,X,Y=from_numpy_matrix_bipartite(aij) 

	# position of center nodes for firms
	pos_center = {}
	theta = 1./float(nsec)
	for i in range(nsec):
		sec_  = sectors[i]
		#position of centers
		pos_center[sec_]= (0,i*theta , label_sector[sec_])		

	# position of nodes	
	# firms
	pos_nodes={}
	for i in range(n_node_firms):
		sec_ = result_firm_to_sector[i]
		posx_center,posy_center,label = pos_center[sec_]	
		posx,posy = posx_center+np.random.rand()*theta*scale ,posy_center+np.random.rand()*theta*scale
		pos_nodes[i]=(posx,posy)
	# hh	
	posx=1.0
	for i in range(n_node_hh):		
		j = i+ n_node_firms
		posy = i*1./float(n_node_hh)
		pos_nodes[j]=(posx,posy)
	# draw	
	nx.draw(G,pos_nodes,node_color='white', cmap="gray")
	ax = plt.gca()
	# add text 
	# https://stackoverflow.com/questions/49139968/create-input-arrows-in-networkx
	for i in range(nsec):
		sec_  = sectors[i]						
		posx_center,posy_center,label=pos_center[sec_]
		posy = (-0.1,posy_center)
		ax.annotate(label_sector[sec_], posy) 
	#plt.axis('off')
	#plt.savefig("graph.pdf")
	plt.show()	
	
def plot_network_unipartite(aij,firm_to_sector,label_sector,a=1.2):
	"""
	see figures_mageco2017.py
	"""
	result_firm_to_sector = firm_to_sector
	sectors = np.unique(result_firm_to_sector)
	nsec = sectors.shape[0]
	# params
	padding = dict(x=(-1.2, 1.2), y=(-1.2, 1.2))
	scale = 0.1
	
	# position of center nodes
	pos_center = {}
	for i in range(nsec):
		sec_  = sectors[i]
		#position of centers
		theta = i*2.*np.pi/float(nsec)
		pos_center[sec_]= (np.cos(theta),np.sin(theta), label_sector[sec_])	
	# positions of nodes	
	n_node = aij.shape[0]	
	pos_nodes={}
	for i in range(n_node):
		sec_ = result_firm_to_sector[i]
		posx_center,posy_center,label = pos_center[sec_]	
		posx,posy = posx_center+np.random.rand()*scale ,posy_center+np.random.rand()*scale
		pos_nodes[i]=(posx,posy)
	
	# edges
	DG = nx.DiGraph()
	for i in range(aij.shape[0]):
		idx = np.argwhere(aij[i,:])
		s= idx.size
		if s>0:
			#print zip(i*np.ones(s,dtype=int),idx.ravel())
			DG.add_edges_from(zip(i*np.ones(s,dtype=int),idx.ravel()))
		else:
			DG.add_node(i)
		# add attributes	
		sec_  = result_firm_to_sector[i]
		DG.node[i]['sector'] = label_sector[sec_]
	
	# draw	
	nx.draw(DG,pos_nodes,node_color='white', cmap="gray")
	# add text 
	#https://matplotlib.org/api/_as_gen/matplotlib.pyplot.annotate.html#matplotlib.pyplot.annotate
	for i in range(nsec):
		sec_  = sectors[i]		
		#position of centers
		theta = i*2.*np.pi/float(nsec)
		pos = (a*np.cos(theta),a*np.sin(theta))
		plt.annotate(label_sector[sec_], pos) 		
	#plt.savefig("graph.pdf")
	plt.show()	


def topo_consumption_household_block(n,p,nsamp):
	"""
	"""
	f = n.ficm_consumption_hh 
	fs=plt.rcParams['font.size']
	nf = p['network']['nf']	
	pij =f.get_biadjacency_matrix(f._pij)
	sectors,unique_indices = np.unique(n.result_firm_to_sector,
										return_index=True )
	label_sector = sorted(n.sectors_aggregate.keys())
	# get sectors 
	firm_to_sector= n.result_firm_to_sector	
	nsec = sectors.shape[0]
	pij_red = pij[np.ix_(unique_indices, unique_indices+1)]	
	#################							
	# plot pij matrix
	# visual check: unique_indices correspond to pij blocks
	# text: https://matplotlib.org/api/text_api.html#module-matplotlib.text
	fig=plt.figure()
	#plt.imshow(pij,interpolation="none",cmap='binary'); 
	plt.plot(pij[:,0])
	y=0.
	for i,(l_s,nf_s) in enumerate(zip(label_sector,nf*n.result_pct_nb_firm)):
		print i,l_s,nf_s
		y+=nf_s/2.		
		#print nf_s,xy		
		#xy = (310, y); plt.annotate(l_s, xy, xycoords=xycoords)	
		#plt.text(-40, y, l_s, va='center', ha='center',fontsize=s)
		plt.text(y, pij_red[i,0]+0.05, l_s, va='center', ha='center',fontsize=fs)
		y+=nf_s/2.
	plt.xlabel('firms',fontsize=fs);
	plt.ylabel('$p_{ij}$',fontsize=fs)
	
	#################	
	#plot network	
	aij=get_aij(pij)	
	plot_network_bipartite(aij,firm_to_sector,label_sector)
	
	#################							
	# knn
	# plot 1 sample
	deg_0,deg_1,ann_0,ann_1 = measure_sample_bip_Graph(aij,verb=0)
	plt.scatter(deg_0.values(),ann_0.values())
	plt.xlabel('k'); plt.ylabel('$k^{nn}$')
	plt.scatter(deg_1.values(),ann_1.values())
	plt.xlabel('k'); plt.xlabel('$k^{nn}$')
	# get nsamp
	deg_0_all = []
	deg_1_all = []
	ann_0_all = []
	ann_1_all = []
	for i in range(nsamp):	
		aij = get_aij()
		deg_0,deg_1,ann_0,ann_1 = measure_sample_bip_Graph(aij,verb=0)		
		deg_0_all += deg_0.values()
		deg_1_all += deg_1.values()
		ann_0_all += ann_0.values()
		ann_1_all += ann_1.values()
	# plot nsamp
	# TODO: ajouter baseline knn=E[k]=20
	# TODO: distinguer les entreprises par secteur
	plt.scatter(deg_0_all,ann_0_all)
	y=p['network']['nlink_per_hh_cons']
	plt.plot([min(deg_0_all),max(deg_0_all)],[y,y],'r--')
	plt.xlabel('k'); plt.ylabel('$k^{nn}$')
	plt.scatter(deg_1_all,ann_1_all)
	plt.plot([y,y],[min(ann_1_all),max(ann_1_all)],'r--')
	plt.xlabel('k'); plt.ylabel('$k^{nn}$')

def topo_wage_rndfitness(n,p,nsamp):
	f = n.ficm_wage
	pij =f.get_biadjacency_matrix(f._pij)
	sectors,unique_indices = np.unique(n.result_firm_to_sector,
										return_index=True )
	label_sector = sorted(n.sectors_aggregate.keys())
	# get sectors 
	firm_to_sector= n.result_firm_to_sector	
	#################	
	#plot network	
	aij=get_aij(pij)
	n_job_per_hh = aij.sum(axis=0)
	plot_network_bipartite(aij,firm_to_sector,label_sector)
	s = "wage network, min=%d max=%d"%(min(n_job_per_hh), max(n_job_per_hh))
	plt.text( 0.1,1,s)


def topo_consumption_firm_rndfitness(n,p,nsamp):
	"""
	Example:
	n,p=prepare_network_rndfitness()
	topo_consumption_firm_rndfitness(n,p,10)
	"""
	f = n.ficm_consumption_firm 
	#f = n.ficm_investment_firm
	fs=plt.rcParams['font.size']
	nf = p['network']['nf']	
	pij =f.get_biadjacency_matrix(f._pij_dij)
	sectors,unique_indices = np.unique(n.result_firm_to_sector,
										return_index=True )
	label_sector = sorted(n.sectors_aggregate.keys())
	# get sectors 
	firm_to_sector= n.result_firm_to_sector	
	nsec = sectors.shape[0]
	#################								
	# plot pij matrix
	# visual check: unique_indices correspond to pij blocks
	# text: https://matplotlib.org/api/text_api.html#module-matplotlib.text
	# annotate: https://matplotlib.org/api/_as_gen/matplotlib.pyplot.annotate.html#matplotlib-pyplot-annotate
	#plt.imshow(pij,interpolation="none",cmap='binary'); 
	plt.matshow(pij,cmap='binary'); 
	plt.colorbar()
	# text: sectors
	s=20
	xycoords = "data"
	y=0.
	y_start=0
	margin=1
	ax = plt.gca()	
	for (l_s,nf_s) in zip(label_sector,nf*n.result_pct_nb_firm):
		y+=nf_s/2.		
		#print nf_s,xy		
		#xy = (310, y); plt.annotate(l_s, xy, xycoords=xycoords)	
		plt.text(-40, y, l_s, va='center', ha='center',fontsize=s)
		plt.text(y, -20, l_s, va='center', ha='center',fontsize=s)
		y+=nf_s/2.
		# lines TO VISUALIZE SECTORS 
		line = lines.Line2D([1, 1], 
							[y_start+margin, y_start+nf_s-margin],
                    lw=2, color='black', axes=ax)
		y_start += nf_s            
		ax.add_line(line)
	
	ax.set_xticklabels([])
	ax.set_yticklabels([])	
	#################	
	# plot the graph	
	aij=get_aij(pij)
	plot_network_unipartite(aij,n.result_firm_to_sector.copy(),label_sector)		

	#####################################
	# get empirical degree of all sectors 
	from topological_vs_flow_measures import scatter_plot_XY_by_sector
	in_degree = []
	out_degree= []
	
	for i in range(nsamp):	
		aij=get_aij(pij)
		o_deg = aij.sum(axis=1).tolist() # i buys from j
		i_deg = aij.sum(axis=0).tolist()  
		out_degree += o_deg
		in_degree += i_deg
	# imposed fitnesses			
	xi_firm= pij.sum(axis=1)  #f.dij.sum(axis=1) 
	yj_firm= pij.sum(axis=0)  #f.dij.sum(axis=0) 
	sigma_x = np.sqrt( np.sum( (pij*(1-pij)**2) , axis=1))
	sigma_y = np.sqrt( np.sum( (pij*(1-pij)**2) , axis=0))
	# rename
	o_deg_last = o_deg
	i_deg_last = i_deg
	o_deg= out_degree 
	i_deg= in_degree
	#plot kout
	plt.figure()
	plt.scatter(np.tile(xi_firm,nsamp), o_deg)
	xlabel,ylabel='$E[k_{out}]$','$k_{out}$'
	X=np.tile(xi_firm,nsamp).reshape((nsamp,nf))
	Y=np.asarray(o_deg).reshape((nsamp,nf))
	Z = np.tile(firm_to_sector,nsamp).reshape((nsamp,nf))
	scatter_plot_XY_by_sector(X,Y,Z,xlabel,ylabel)
	plt.plot(xi_firm,xi_firm,'k--')
	#plot kin
	plt.figure()
	plt.scatter(np.tile(yj_firm,nsamp), i_deg)
	xlabel,ylabel='$E[k_{in}]$','$k_{in}$'
	X=np.tile(yj_firm,nsamp).reshape((nsamp,nf))
	Y=np.asarray(i_deg).reshape((nsamp,nf))
	Z = np.tile(firm_to_sector,nsamp).reshape((nsamp,nf))
	scatter_plot_XY_by_sector(X,Y,Z,xlabel,ylabel)
	plt.plot(yj_firm,yj_firm,'k--')
	#################	
	# knn
	plt.figure()
	assortativity_compare(pij)

	
	
def topo_consumption_firm_block(n,p,nsamp):
	f = n.ficm_consumption_firm 
	fs=plt.rcParams['font.size']
	nf = p['network']['nf']	
	pij =f.get_biadjacency_matrix(f._pij_dij)
	sectors,unique_indices = np.unique(n.result_firm_to_sector,
										return_index=True )
	label_sector = sorted(n.sectors_aggregate.keys())
	# get sectors 
	firm_to_sector= n.result_firm_to_sector	
	nsec = sectors.shape[0]
	#################								
	# plot pij matrix
	# visual check: unique_indices correspond to pij blocks
	# text: https://matplotlib.org/api/text_api.html#module-matplotlib.text
	# annotate: https://matplotlib.org/api/_as_gen/matplotlib.pyplot.annotate.html#matplotlib-pyplot-annotate
	fig=plt.figure()
	#plt.imshow(pij,interpolation="none",cmap='binary'); 
	plt.matshow(pij,cmap='binary'); 
	plt.colorbar()
	# text: sectors
	s=20
	xycoords = "data"
	y=0.
	for (l_s,nf_s) in zip(label_sector,nf*n.result_pct_nb_firm):
		y+=nf_s/2.		
		#print nf_s,xy		
		#xy = (310, y); plt.annotate(l_s, xy, xycoords=xycoords)	
		plt.text(-40, y, l_s, va='center', ha='center',fontsize=s)
		plt.text(y, -20, l_s, va='center', ha='center',fontsize=s)
		y+=nf_s/2.
	ax = plt.gca()	
	ax.set_xticklabels([])
	ax.set_yticklabels([])	
	# plot the graph
	rnd = np.random.rand(pij.shape[0]*pij.shape[1] ).reshape((pij.shape[0], pij.shape[1]))
	aij = np.array(rnd < pij , dtype=np.int64)
	plot_network_unipartite(aij,n.result_firm_to_sector.copy(),label_sector)		
		
	################
	# symmetry of matrix
	def text_sectors_red(s=20):
		# ?plt.txt: "The default transform specifies that text is in data coords"
		for i,l_s in enumerate(label_sector):
			print i,l_s
			plt.text(-1, i, l_s, va='center', ha='center',fontsize=s)		
			plt.text(i,-1, l_s, va='center', ha='center',fontsize=s)		
		ax = plt.gca()	
		ax.set_yticklabels([])	
		ax.set_xticklabels([])	
	def text_sign_red():
		# https://stackoverflow.com/questions/20998083/show-the-values-in-the-grid-using-matplotlib
		s=20
		isec = range(nsec)
		for i in isec:
			for j in isec:
				if(i>j):
					print i,j,pij_red[i,j]-pij_red[j,i]
					if(pij_red[i,j]>pij_red[j,i]):
						plt.text(j,i, '+', va='center', ha='center', fontsize=s,
							bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))		
						#plt.text(i,j, "%d,%d"%(i,j),fontsize=8)
					else:
						plt.text(j,i, '-', va='center', ha='center',fontsize=s,
						bbox=dict(boxstyle='round', facecolor='white', edgecolor='0.3'))				
						#plt.text(i,j, '-')				
	pij_red = pij[np.ix_(unique_indices, unique_indices+1)]	
	#plt.imshow(pij_red,interpolation="none",cmap="binary",origin='upper');		
	plt.matshow(pij_red,cmap="binary");		
	plt.colorbar() 	
	text_sectors_red()
	#plt.imshow(np.abs(np.tril(pij_red-pij_red.transpose())),interpolation="none",cmap="binary",origin='upper');			
	plt.matshow(np.abs(np.tril(pij_red-pij_red.transpose())),cmap="binary");			
	text_sign_red()
	plt.colorbar() 	
	text_sectors_red()	


	#####################################
	# kin kout: get empirical degree of all sectors 
	#nsamp = 10# p['mc']['niter']
	in_degree = []
	out_degree= []
	
	for i in range(nsamp):	
		aij=get_aij(pij)
		o_deg = aij.sum(axis=1).tolist() # i buys from j
		i_deg = aij.sum(axis=0).tolist()  
		out_degree += o_deg
		in_degree += i_deg
	# imposed fitnesses			
	xi_firm= pij.sum(axis=1)  #f.dij.sum(axis=1) 
	yj_firm= pij.sum(axis=0)  #f.dij.sum(axis=0) 
	sigma_x = np.sqrt( np.sum( (pij*(1-pij)**2) , axis=1))
	sigma_y = np.sqrt( np.sum( (pij*(1-pij)**2) , axis=0))
	# rename
	o_deg_last = o_deg
	i_deg_last = i_deg
	o_deg= out_degree 
	i_deg= in_degree
	# advanced indexing: https://docs.scipy.org/doc/numpy/reference/arrays.indexing.html#integer-array-indexing	
	pij_red = pij[np.ix_(unique_indices, unique_indices+1)]	
	k_in=pij.sum(axis=0)
	k_out=pij.sum(axis=1)	
	k_in_red= k_in[unique_indices] # must be weighted by firm population
	k_out_red=k_out[unique_indices]
	############
	# k_out=f(fitness x) , E[k_out]_theor, +/- sigma
	plt.plot(xi_firm,xi_firm,'r-')
	plt.scatter(np.tile(xi_firm,nsamp), o_deg)
	#error bar: https://matplotlib.org/examples/statistics/errorbar_demo_features.html
	#fig, (ax0, ax1) = plt.subplots(nrows=2, sharex=True)
	fig, ax0 = plt.subplots(nrows=1, sharex=True)
	plt.scatter(np.tile(xi_firm,nsamp), o_deg, color='k')
	ax0.errorbar(xi_firm, xi_firm, yerr=sigma_x , 
				color='k',
				fmt='-o')
	#ax0.set_title('variable, symmetric error')
	plt.xlabel('$E[k_{out}]$',fontsize=fs);
	plt.ylabel('$k_{out}$',fontsize=fs)
	fig.subplots_adjust(bottom=0.15)#, left=0.2	
	# annotate sectors
	s=15; y=-2
	for i,(l_s,xi) in enumerate(zip(label_sector,k_out_red)):		
		print i,l_s,xi,y	
		plt.annotate(l_s,xy=(xi, 0), arrowprops=dict(arrowstyle='->'),
				xytext=(xi, y),fontsize=s,va="center", ha="center")
		if y==-2: y=-4
		elif y==-4: y=-2		
	
	fig, ax0 = plt.subplots(nrows=1, sharex=True)			
	plt.scatter(np.tile(yj_firm,nsamp), i_deg,color='k')
	ax0.errorbar(yj_firm, yj_firm, yerr=sigma_y , fmt='-o',color='k')	
	#plt.xlabel('$\sum_i p_{ij}$',fontsize=fs);
	plt.xlabel('$E[k_{in}]$',fontsize=fs);
	plt.ylabel('$k_{in}$',fontsize=fs)
	fig.subplots_adjust(bottom=0.15)#, left=0.2
		# annotate sectors
	s=15; y=-2
	for i,(l_s,xi) in enumerate(zip(label_sector,k_in_red)):		
		print i,l_s,xi,y	
		plt.annotate(l_s,xy=(xi, 0), arrowprops=dict(arrowstyle='->'),
				xytext=(xi, y),fontsize=s,va="center", ha="center")
		if y==-2: y=-4
		elif y==-4: y=-2
	######
	# sankey diagram 
	# https://www.sciencedirect.com/science/article/pii/S0921344917301167?via%3Dihub
	# https://github.com/ricklupton/floweaver
	# https://hub.mybinder.org/user/ricklupton-floweaver-d63hhsz4/notebooks/docs/tutorials/quickstart.ipynb	
	#####
	# get assortativity coefficient (theor)
	# get assortativity coefficient (empirical)
	assortativity_compare(pij)	
	# by sector
	# => see topo_vs_flow_...	
	
	####
	# Decomposing assortativity
		# pop_i*pop_j*IO_ij  Q: qui domine ?				
	# REDUCED ANN_out(s_I) = f(k_out) 
	# extract reduced pij	
	# k_*_red must be weighted by firm population									
	plt.imshow(pij_red, interpolation="none"); plt.colorbar() 
	
	# comutes reduced ANN
	ucnn,dpnn= assortativity_theor(pij,k_out,k_in)
	ucnn_red,dpnn_red = assortativity_theor(pij_red,k_out_red,
											k_in_red)#,
											#w=nf*n.result_pct_nb_firm)
	def test():
		#ucnn[c]= 1./dc[c] * np.sum(pcp[c,:] *(up - pcp[c,:] +1.) )			
		#dpnn[p]= 1./up[p] * np.sum(pcp[:,p] *(dc - pcp[:,p] +1.) )													
		#assortativity_theor(pcp,dc,up,w=None)
		c=0
		cc = unique_indices[c]+1
		dummy = pij[cc,:] *(k_in - pij[cc,:] +1.) 		
		dummy[unique_indices+1]
		w=nf*n.result_pct_nb_firm
		dummy_red = (pij_red[c,:] *(k_in_red - pij_red[c,:] +1.) )
		print dummy[unique_indices+1], dummy_red
		print np.sum(dummy[unique_indices+1]) , np.sum(dummy_red)
		dummy_red = w * dummy_red
	# !!!!!!! NB: les valeurs sont quasimment constantes
	# weighted by population !!!!!!!! FOR K_in, k_out !!!!!!!!!!
	# => effet de la  pondération par la population ??
	#ucnn_red_pop = ucnn_red * n.result_pct_nb_firm
	#dpnn_red_pop = dpnn_red * n.result_pct_nb_firm		
	# plot and !compare to full matrix!	
	idx_kin= np.argsort(k_in)
	idx_kout= np.argsort(k_out)
	
	idx_kin_red= np.argsort(k_in_red)
	idx_kout_red= np.argsort(k_out_red)
	plt.subplot(2,1,1)
	plt.plot(k_in_red[idx_kin_red],
			0.5*dpnn_red[idx_kin_red],color='k', linestyle='-.' )	
	plt.plot(k_in[idx_kin], dpnn[idx_kin],color='k', linestyle='-' )	
	plt.xlabel('$k_{in,red}$',fontsize=fs); 
	plt.ylabel('$<k_{out,red}^{nn} ?? > $',fontsize=fs)			
	plt.subplot(2,1,2)
	plt.plot(k_out_red[idx_kout_red],
			0.5*ucnn_red[idx_kout_red],color='k', linestyle='-.' )
	plt.plot(k_out[idx_kout], ucnn[idx_kout],color='k', linestyle='-' )		
	plt.xlabel('$k_{out,red}$',fontsize=fs); 
	plt.ylabel('$<k_{in,red}^{nn} ??> $',fontsize=fs)	
	"""
	PAS DE POND2RATION PAR LA POPULATION POUR ANN!!!	
	plt.subplot(2,1,1)
	plt.plot(k_in_red[idx_kin_red],
			dpnn_red_pop[idx_kin_red],color='k', linestyle='--' )
	plt.xlabel('$k_{in,red}$',fontsize=fs); 
	plt.ylabel('$<k_{out,red}^{nn} ?? > $',fontsize=fs)			
	plt.subplot(2,1,2)
	plt.plot(k_out_red[idx_kout_red],
			ucnn_red_pop[idx_kout_red],color='k', linestyle='--' )
	plt.xlabel('$k_{out,red}$',fontsize=fs); 
	plt.ylabel('$<k_{in,red}^{nn} ??> $',fontsize=fs)			
	"""
	# dominant terms ?		
	"""# plot
	plt.subplot(2,1,1)
	plt.scatter(ANND_in_out.keys(),ANND_in_out.values(),color='k')	
	plt.plot(k_in[idx_kin], dpnn[idx_kin],color='k', linestyle='--' )
	#plt.plot( range(nf),dpnn, color='k', linestyle='--')
	plt.xlabel('$k_{in}$',fontsize=fs); 
	plt.ylabel('$k_{out}^{nn},  <k_{out}^{nn}> $',fontsize=fs)
	xmax = max(ANND_in_out.keys())
	ymax = max(ANND_in_out.values())
	plt.xlim([0,xmax])
	plt.ylim([0,1.1*ymax])"""
	
	
	###
	# vertex centrality of matrices
	# Q: quel lien théorique entre centrality et ANN ?
	#https://networkx.github.io/documentation/stable/reference/algorithms/generated/networkx.algorithms.centrality.current_flow_betweenness_centrality.html?highlight=random%20walk
	from networkx.algorithms.centrality import current_flow_betweenness_centrality
	mat = np.asarray(n.result_io_indus_indus_agg)
	g = nx.from_numpy_matrix(mat,parallel_edges=False)#,create_using=nx.DiGraph())
	warnings.warn('undirected graphs!')
	r = current_flow_betweenness_centrality(g)
	plt.scatter(r.keys(),r.values())
	
def topo_investment_firm_rndfitness(n,p):	
	""" Function doc 
	cf topo_consumption_firm_rndfitness
	
	"""
	pass
	"""#nb = p['network']['nb']
	nf = p['network']['nf']
	#nw = p['network']['nw']
	#L_invest = int(np.floor(p['network']['nlink_per_firm'] * nf)) 	
		
	####################################
	# investment_firm: get pij
	f = n.ficm_investment_firm 	
	pij =f.get_biadjacency_matrix(f._pij)
	aij=get_aij(pij)
	sectors,unique_indices = np.unique(n.result_firm_to_sector,
												return_index=True )
	label_sector = n.sectors_aggregate.keys()											
	# plot the graph
	plot_network_unipartite(aij,n.result_firm_to_sector.copy(),label_sector)
	assortativity_compare(pij)"""
	
def topo_investment_firm_block(n,p):
	""" Function doc """
	#nb = p['network']['nb']
	nf = p['network']['nf']
	#nw = p['network']['nw']
	#L_invest = int(np.floor(p['network']['nlink_per_firm'] * nf)) 	
		
	####################################
	# investment_firm: get pij
	f = n.ficm_investment_firm 	
	
	pij =f.get_biadjacency_matrix(f._pij)
	# get reduced pij 					
	sectors,unique_indices = np.unique(n.result_firm_to_sector,
												return_index=True )
	# plot the graph
	rnd = np.random.rand(pij.shape[0]*pij.shape[1] ).reshape((pij.shape[0], pij.shape[1]))
	aij = np.array(rnd < pij , dtype=np.int64)
	label_sector = n.sectors_aggregate.keys()
	plot_network_unipartite(aij,n.result_firm_to_sector.copy(),label_sector)		
												
	#get empirical degree of 1 sector
	#degree_empirical_block_1sector()	
	
	#####################################
	# get empirical degree of all sectors 
	nsamp = 10# p['mc']['niter']
	in_degree = []
	out_degree= []
	
	for i in range(nsamp):	
		aij=get_aij(pij)	
		o_deg = aij.sum(axis=1).tolist() # i buys from j
		i_deg = aij.sum(axis=0).tolist()  
		out_degree += o_deg
		in_degree += i_deg
	# imposed fitnesses			
	xi_firm= f.strn_in 
	yj_firm= f.strn_out 
	# rename
	o_deg= out_degree 
	i_deg= in_degree
	# get sectors 
	firm_to_sector= n.result_firm_to_sector
	sectors = np.unique(firm_to_sector)
	nsec = sectors.shape[0]
	############
	# check differentiation by sector
	
	
	############
	# k_in=f(fitness x), empirical
	colors = np.asarray(np.tile(firm_to_sector,nsamp)/float(nsec), dtype='U' ) #unicode str
	plt.scatter(np.tile(yj_firm,nsamp), i_deg,
				c=colors
				)
	colors = np.asarray(firm_to_sector/float(nsec), dtype='U' ) #unicode str
	plt.scatter(yj_firm, aij.sum(axis=0).tolist(),
				c=colors
				)
	plt.scatter(xi_firm, aij.sum(axis=1).tolist(),
				c=colors
				)			
	plt.show()
	# k_in = f(fitness y), theoretical
	# when: pdf(x) = pareto(x0=1, b=1.0)	 
	# see: calculus in article
	delta = f.sol.x[0]
	y = np.linspace(min(yj_firm), max(yj_firm), 100)
	x0=p['fitness']['param2']
	# ATTENTION: the expression for k_in_pareto depends on the choice of
	# parameters b=1 which is not really respected (indeed for b=1, E(x) doesn'exist)
	k_in_pareto_1_0 = float(nf) * x0* delta * y*np.log((1.+delta*x0*y)/x0)	
	plt.scatter(np.tile(yj_firm,nsamp), i_deg)#plt.scatter(yj_firm, i_deg)
	ax = plt.gca()
	ax.plot(y,k_in_pareto_1_0/float(nf))
	#ax.set_xscale("log")
    #ax.set_yscale("log")
    
	# k_in: reciprocal function F^{-1}
	# https://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html#spline-interpolation-in-1-d-procedural-interpolate-splxxx
	#https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.UnivariateSpline.html#scipy.interpolate.UnivariateSpline
	# NB: x and y have been inverted, to fit the reciprocal function
	Finv = interpolate.UnivariateSpline(k_in_pareto_1_0/float(nf), y) 
	#Finv.set_smoothing_factor(0.5)
	Finv_diff = Finv.derivative(1)
	rho_x = lambda u:  p['fitness']['f'](u, p['fitness']['param1'],p['fitness']['param2'])
	k_new = np.linspace( min(i_deg), max(i_deg),100)	
	#P_k_in = lambda kk : rho_x( Finv(kk) ) * Finv_diff(kk) # NON !!!!!!!!!!!!!
		
	############
	# k_out=f(fitness x) , empirical
	plt.scatter(np.tile(xi_firm,nsamp), o_deg)   #plt.scatter(xi_firm, o_deg)
	#gaussian_kde: https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.gaussian_kde.html#scipy-stats-gaussian-kde
	kde_y = gaussian_kde(yj_firm,bw_method='scott')# "silverman", 'scott',0.1
	y = np.linspace(min(yj_firm), max(yj_firm), 100)
	pdf_y = kde_y.evaluate(y) 
	plt.plot(y,pdf_y)	
	plt.hist(yj_firm,30,normed=True)	
	int_ymin_ymax = kde_y.integrate_box_1d(min(yj_firm), max(yj_firm))
	# k_out=f(fitness x) , theoretical
	delta = f.sol.x[0]
	x_new = np.linspace(min(xi_firm), max(xi_firm), 100)	
	# ATTENTION: the expression for k_in_pareto depends on the choice of
	fun=lambda x: np.sum( pdf_y*y /(1.+delta*x*y)) / int_ymin_ymax
	fun_x_new = np.array([fun(u) for u in x_new])
	k_out_theor = float(nf) * delta * x_new *fun_x_new
	#
	plt.scatter(np.tile(xi_firm,nsamp), o_deg) #plt.scatter(xi_firm, o_deg)
	ax = plt.gca()
	ax.plot(x_new, k_out_theor/float(nf))
	# k_out: reciprocal function F^{-1}
	Finv_out = interpolate.UnivariateSpline(k_out_theor/float(nf), x_new) 
	Finv_out_diff = Finv_out.derivative(1)
	P_k_out = lambda kk : kde_y.evaluate( Finv_out(kk) ) * Finv_out_diff(kk)
	#k_new = np.linspace( min(k_in_pareto_1_0), max(k_in_pareto_1_0),100)
	k_out_new = np.linspace( min(o_deg), max(o_deg),100)	
	P_k_out_new = [rho_x(kk) for kk in k_out_new]
	
	# PLOT: k_out->P(k_out)
	x_new = np.linspace(min(xi_firm), max(xi_firm), 100)	
	nplot=4
	plt.subplot(nplot,1,1)
	plt.scatter(np.tile(xi_firm,nsamp), o_deg)#plt.scatter(xi_firm, o_deg)
	ax = plt.gca()
	ax.plot(x_new,k_out_theor/float(nf))
	plt.xlabel('x')	; plt.ylabel('out-degree')
	plt.subplot(nplot,1,2)
	plt.plot(k_out_theor/float(nf), x_new, 'ro', ms=5)
	plt.plot(k_out_new, [Finv_out(kk) for kk in k_out_new]);
	plt.xlabel('k_out_new')	; plt.ylabel('Finv_out')
	plt.subplot(nplot,1,3)
	plt.plot(k_out_new, [Finv_out_diff(kk) for kk in k_out_new]);
	plt.xlabel('k_out_new')	; plt.ylabel('Finv_out_diff')
	plt.subplot(nplot,1,4)
	#figCDF = powerlaw.plot_cdf(i_deg, color='b'); 	figCDF.plot(k_new,P_k_new,color='r')
	plt.hist(o_deg,50,normed=True,log=True);
	plt.plot(k_out_new,P_k_out_new,color='r')	
	plt.xlabel('k_out_new')	; plt.ylabel('freq')
	
	# PLOT: k_in -> P_k_in
	nplot=4
	plt.subplot(nplot,1,1)
	plt.scatter(np.tile(yj_firm,nsamp), i_deg)#	plt.scatter(yj_firm, i_deg)
	ax = plt.gca()
	ax.plot(y,k_in_pareto_1_0/float(nf))
	plt.xlabel('y')	; plt.xlabel('in-degree')
	plt.subplot(nplot,1,2)
	plt.plot(k_in_pareto_1_0/float(nf), y, 'ro', ms=5)
	plt.plot(k_new, [Finv(kk) for kk in k_new]);
	plt.xlabel('k_new')	; plt.ylabel('Finv')
	plt.subplot(nplot,1,3)
	plt.plot(k_new, [Finv_diff(kk) for kk in k_new]);
	plt.xlabel('k_new')	; plt.ylabel('Finv_diff')
	plt.subplot(nplot,1,4)	
	k_new = np.linspace( min(i_deg), max(i_deg),100)
	# NO plt.hist(i_deg,normed=True);plt.plot(k_new,P_k_new,color='r')
	#g = lambda kk : rho_x( Finv(kk) ) * Finv_diff(kk) !!!!!!!NO
	g = lambda kk : kde_y.evaluate( Finv(kk) ) * Finv_diff(kk) 
	P_k_in_new = [g(kk) for kk in k_new]
	plt.hist(i_deg,normed=True);plt.plot(k_new,P_k_in_new,color='r')
	
	# save figs
	fs=plt.rcParams['font.size']
	plt.scatter(np.tile(xi_firm,nsamp), o_deg,color='k')#plt.scatter(xi_firm, o_deg,color='k')
	ax = plt.gca()
	ax.plot(x_new,k_out_theor/float(nf),color='k')
	plt.xlabel('$x$',fontsize=fs)	; plt.ylabel('$k_{out}$',fontsize=fs)
	#
	plt.hist(o_deg,50,normed=True,color='k',log=True,fill=False);
	plt.plot(k_out_new,P_k_out_new,color='k')	
	#plt.xlim([0,100])
	plt.xlabel('$k_{out}$',fontsize=fs)	; plt.ylabel('$P(k_{out})$',fontsize=fs)
	#
	plt.scatter(np.tile(yj_firm,nsamp), i_deg,color='k')#plt.scatter(yj_firm, i_deg,color='k')
	ax = plt.gca()
	ax.plot(y,k_in_pareto_1_0/float(nf),color='k')
	plt.xlabel('$y$',fontsize=fs)	; plt.ylabel('$k_{in}$',fontsize=fs)
	#
	plt.hist(i_deg,normed=True,color='k',log=False,fill=False);
	plt.plot(k_new,P_k_in_new,color='k')
	#plt.semilogy(k_new,P_k_in_new,color='k')
	plt.xlabel('$k_{in}$',fontsize=fs)	; plt.ylabel('$P(k_{in})$',fontsize=fs)
	#################
	# P(kout): powerlaw fit; theoretical ; empirical
	res = powerlaw.Fit(o_deg,discrete=True)
	fig4 = res.plot_ccdf(linewidth=1.5)
	res.power_law.plot_ccdf(ax=fig4, color='r', linestyle='--', label='PL Fit')
	res.lognormal.plot_ccdf(ax=fig4, color='g', linestyle='--', label='Lognormal Fit') 
	res.exponential.plot_ccdf(ax=fig4, color='y', linestyle='--', label='Exponential Fit') 
	res.truncated_power_law.plot_ccdf(ax=fig4, color='m', linestyle='--', label='truncated_power_law')
	plt.title("Comparison of Power Law with Other Fits")
	plt.legend(loc="lower left")
	plt.xlabel("Distribution (log)")
	plt.show()

	assortativity_compare(pij)

def assortativity_compare(pij):
	#####
	# get assortativity coefficient (theor)
	# get assortativity coefficient (empirical)
	fs = 20#plt.rcParams['font.size']
	nsamp=10	
	ANND_in_out_keys=[];ANND_in_out_values=[]
	ANND_out_in_keys=[];ANND_out_in_values=[]
	for i in range(nsamp):	
		rnd = np.random.rand(pij.shape[0]*pij.shape[1] ).reshape((pij.shape[0], pij.shape[1]))
		aij = np.array(rnd < pij , dtype=np.int64)	
		r=measure_sample_mono_DiGraph(aij,verb=0)
		deg_in,deg_out,r_out_in,r_in_out, ANND_in_out, ANND_out_in = r		
		ANND_in_out_keys += ANND_in_out.keys()		
		ANND_in_out_values += ANND_in_out.values() 
		ANND_out_in_keys += ANND_out_in.keys()
		ANND_out_in_values += ANND_out_in.values() 
	ucnn,dpnn = assortativity_theor(pij,pij.sum(axis=1),pij.sum(axis=0))
	
	# plot
	k_in=pij.sum(axis=0)
	k_out=pij.sum(axis=1)
	idx_kin= np.argsort(k_in)
	idx_kout= np.argsort(k_out)
	#plt.subplot(2,1,1)
	plt.figure()	
	plt.scatter(ANND_in_out_keys,ANND_in_out_values,color='k')	
	plt.plot(k_in[idx_kin], dpnn[idx_kin],color='k', linestyle='--' )
	#plt.plot( range(nf),dpnn, color='k', linestyle='--')
	plt.xlabel('$k_{in}, E[k_{in}]$',fontsize=fs); 
	plt.ylabel('$k_{out}^{nn},  E[k_{out}^{nn}] $',fontsize=fs)
	xmax = max(ANND_in_out.keys())
	ymax = max(ANND_in_out.values())
	plt.xlim([0,xmax])
	plt.ylim([0,1.1*ymax])
	plt.figure()
	#plt.subplot(2,1,2)
	plt.scatter(ANND_out_in_keys,ANND_out_in_values,color='k')
	plt.plot(k_out[idx_kout], ucnn[idx_kout],color='k', linestyle='--' )
	#plt.plot( range(nf),ucnn, color='k', linestyle='--' )
	plt.xlabel('$k_{out}, E[k_{out}]$',fontsize=fs); 
	plt.ylabel('$k_{in}^{nn}, E[k_{in}^{nn}]$',fontsize=fs)
	xmax = max(ANND_out_in.keys())
	ymax = max(ANND_out_in.values())
	plt.xlim([-1,xmax])
	plt.ylim([0,1.1*ymax])
	

"""
def weighted_IO(pct_firm_by_sec,sector_labels,IO):		
	#weight IO by population in each sector		
	mat = IO.copy()
	for s_agg_key_sell in sector_labels:
		for s_agg_key_buy in sector_labels:
			mat[s_agg_key_sell][s_agg_key_buy] = IO[s_agg_key_sell][s_agg_key_buy]/(pct_firm_by_sec[s_agg_key_sell]*pct_firm_by_sec[s_agg_key_buy])
	return mat	
	
pct_firm_by_sec = n.result_aggregate_bd.loc[sorted(n.sectors_aggregate.keys()),'PCT_NB_FIRM']
wIO = weighted_IO(pct_firm_by_sec,
					sorted(n.sectors_aggregate.keys()),
					n.result_io_indus_indus_agg)
"""	


if __name__ == "__main__":
	#n,p=prepare_network_block()
	n,p=prepare_network_rndfitness()
	topo_investment_firm_block(n,p)
	topo_consumption_household_block(n,p)
