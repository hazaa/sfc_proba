# -*- coding: utf-8
#  utils.py
#  
#  Copyright 2017 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

import numpy as np
import sys
import matplotlib.pyplot as plt
import json
import pandas as pd


def save_h5(x,config,fname,groupname):
	"""
	
	Example:
	---------
	from transmat import get_bmw_labels
	import datetime
	
	label,label_long = get_bmw_labels(nb,nf,nw)
	idx_first_WBs = label_long.index('WBs')
	idx_first_M = label_long.index('M')
	config = { 
			'label': callingfuncname,
			'model_params':{'model':'bmw','nw':nw,'nb':nb, 'nf':nf, 
						   'params': pnum,
						   'labels':label_long, 'idx_first_WBs':idx_first_WBs,
						   'idx_first_M':idx_first_M },
		    'sampler_params':{'seed':123, 'uniform':False,'native':False, 'algo': 'HD',
								'nsamp':nsamp, 'thin':thin},
			'experiment_params': {},
			datetime = str(datetime.datetime.today())								   
		  	}
	
	fname= '/data/samples.h5'
	groupname = '/bmw_11N/transmat_test/lognorm'	
	save_h5(x,config,fname,groupname)
	"""	
	nb,nf,nw=1,1,config['model_params']['nw']
	nsamp = 1000
	
	# open h5 store	
	store = pd.HDFStore(fname)
	# store data
	df = pd.DataFrame(x) 
	label = config['label']
	store[groupname+'/samples/'+label] = df	
	# store config
	conf_str = pd.Series(json.dumps(config))
	store[groupname+'/config/'+label]= conf_str
	store.close()

def check_constr(C,b,F,g,x, tol=1E-5):
	"""
	
	"""				
	# check equality
	delta= np.abs(np.dot(F,x)-g)	
	if(np.any(delta>tol)):
		print "delta Fx-g=",delta
		raise ValueError('equ Fx-g=0 is not respected')
	# check inequality	
	delta= np.dot(C, x)-b
	if(np.any(delta>0+tol)):
		print "delta Cx-b=",delta
		print "idx=",np.where(delta>0+tol)[0]
		raise ValueError('inequ Cx-b<=0 is not respected')	
	

def check_constr_vec(C,b,F,g,x):
	"""
	vectorized version
	copied from har_nonuniform.py
	"""
	#raise NotImplementedError
	nsamp = x.shape[0]		
	tol = 1E-3
	# check equality
	delta= np.abs(np.dot(F,x.T)-np.repeat(g,nsamp, axis=1))	
	if(np.any(delta>tol)):
		raise ValueError('equ Fx-g=0 is not respected')
	# check inequality	
	delta= np.dot( C, x.T)-np.repeat(b,nsamp, axis=1)
	if(np.any(delta>0+tol)):
		raise ValueError('inequ Cx-b<=0 is not respected')
	
def fig_with_function_name(comment=None):
	"""
	introspect function name, and return figure with the corresponding title
	"""
	#this_function_name = sys._getframe().f_code.co_name
	calling_function_name = sys._getframe(1).f_code.co_name
	f=plt.figure()
	#f.suptitle(this_function_name)
	if(comment==None):
		title= calling_function_name
	else:	
		title= calling_function_name + comment
	f.suptitle(title)	
	return f
