# -*- coding: utf-8 -*- #
#  bp_beta.py
#  
#  Copyright 2016 aurelien <aurelien@aurelien-HP-EliteBook-840-G1>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
CHANGELOG:
2016-12-13: corrected _change_coord and _process_msg
	        moved example_CSP_problems from here to transmat.py
"""

"""
REMARKS:
* sample_dataset(2) gives poor results. The lower bound is not coherent.
  This sould have been corrected by the correction in p.22 [CossioMulet16]
  [max(A_{a->i},a_i), min(B_{a->i},b_i)]

[CossioMulet16]: arXiv 1602.08412
"""

"""
TODO:
* Q: comment gerer les plantages ? e.g; effacement controle des fichiers
* quand aucune contrainte sur une var, pas de message correspondant => l'indiquer ?
* implémenter transmat_...
* make a clean c++/python wrapper for bpB
* normalization of messages
* test case:
   assert(s'il n'y a pas de contrainte sur une var, elle ne donne pas de msg)
   avec x0 donné ou x0 calculé par solver
   compare different msg for the same var
"""
import sys
sys.path.append('../hitandrun')     # REMOVE THIS !!!!!!!
import os
import scipy
import numpy as np 
import har_wrap
import transmat
import matplotlib.pyplot as plt

def write_mat_to_file(A,lb,ub, fname):
	"""
	write matrices to file. use the same naming convention as bpB
	
	Parameters:
	-----------
	fname: string
	full path except the suffix. example: '/home/alice/mymatrixfile'
	
	A: ndarray, shape= (m,n)
	the matrix that encodes equality constraints Ax=b
	
	b: ndarray, shape= (m,)
	
	ub: 	ndarray, shape= (m,)
	upper bounds
	
	lb: 	ndarray, shape= (m,)
	lower bounds	
	"""
	fmt='%5.5f'
	scipy.savetxt(fname+'.sto', A, fmt )
	scipy.savetxt(fname+'.ub',ub, fmt)
	scipy.savetxt(fname+'.lb',lb, fmt)
	

class bp_beta():
	def __init__(self):	
		self.fname     = []
		self.path_bp_bin=[]
		self.init_done = False
		self.run_done  = False
		self.x0		   = []
		self.solver_underdetermined = ""
		self.msg_raw=[]
		self.msg_uniq=[]
		self.msg_translated=[]
		self.gamma_bounded = lambda x,A,B,alpha,beta: np.power(x-A,alpha) *np.power(B-x,beta) 
	def _process_msg(self):
		"""
		"""
		# keep only one message per variable
		# (variables are encoded in the second column of *.msg)
		idx_var_uniq = np.unique(self.msg_raw[:,1],
								 return_index=True)[1]	
		self.msg_uniq = self.msg_raw[idx_var_uniq, 1:]
		# remove x0 (but self.msg_uniq does not have the same dimension
		# as x0)
		# msg_uniq = [ [idx_var , A, B , alpha, beta], [...]]		
		msg_translated= self.msg_uniq
		#print "self.msg_uniq=",self.msg_uniq
		for i in range( msg_translated.shape[0]):
			idx_var = int(msg_translated[i,0])
			#print "idx_var=", idx_var, type(idx_var), self.x0[idx_var]
			msg_translated[i,1] += self.x0[idx_var] #A<- A - x0[i]
			msg_translated[i,2] += self.x0[idx_var] #B<- B - x0[i]
		return msg_translated	
	def _change_coord(self,A,b,lb,ub,x0):
		"""
		change coordinate
		"""
		#lb_translate,ub_translate = lb + x0, ub+x0  # HERE !!!!!!!!!!
		lb_translate,ub_translate = lb - x0, ub-x0  
		return lb_translate,ub_translate
		
	def _find_particular_solution(self,A,b,lb,ub,solver):
		"""
		"""
		if(solver=='slacklp'):
			constr = har_wrap.create_constr(A,b,lb,ub)
			x0=har_wrap.interior_point(constr)						
		else:			
			raise ValueError('unknown solver')
		return x0	
		
	def init_from_file(self,**kwargs):	
		"""
		keyword parameters:
		----		
		fname: string
		path+prefix of the temporary files
			
		path_bp_bin: string
		path to bpB binary
		"""
		# read params
		self.fname = kwargs['fname']
		self.path_bp_bin=kwargs['path_bp_bin']				
		self.init_done=True			
			
	def init(self, A,b,lb,ub,**kwargs):			
		"""
		in:
		---
		
		keyword parameters:
		----		
		fname: string
		path+prefix of the temporary files
		
		solver: string
		name of the solver for the underdetermined system
		
		path_bp_bin: string
		path to bpB binary
		"""
		# read params
		self.fname = kwargs['fname']
		self.solver_underdetermined = kwargs['solver']
		self.path_bp_bin=kwargs['path_bp_bin']
		# check params
		#   lb<ub
		if np.any(lb>ub):
			raise ValueError('lb>ub')	
		# change coordinates
		print "A=",A, "b=",b, " lb=",lb, " ub=",ub
		if (np.all(b==0)):
			lb_translate=lb
			ub_translate=ub
		else:
			# x0 is given 
			if(kwargs.has_key('x0')):
				self.x0=kwargs['x0']			
			# find a particular solution x0 	
			else:		
				print "Finding a particular solution..."	
				self.x0 = self._find_particular_solution(A,b,lb,ub,
														self.solver_underdetermined)					
			# check that x0 is such that A.x0=b												
			if not(har_wrap.check_constr(self.x0,A,b,lb,ub)):
					raise ValueError('invalid interior point')	
			lb_translate,ub_translate = self._change_coord(A,b,lb,ub,self.x0)		
		# write matrix
		write_mat_to_file(A,lb_translate,ub_translate, self.fname)
		self.init_done=True	
			
	def run(self):
		if not(self.init_done):
			raise RuntimeError('init() must be called before run()')
		# call bpB			
		cmd = self.path_bp_bin+" --path "+self.fname
		print "os call: ",cmd
		try:
			o=os.system(cmd)
			print "os.system: o=",o
		except e: #OSError:				
			print "e=",e
			raise e
			#raise OSError('file not found')
		# read output files
		self.msg_raw = np.loadtxt(self.fname+'.msg')
		# process messages		
		self.msg_translated=self._process_msg()		
		#  correct for x0 (in A_i,B_i) ??
		self.run_done = True
	def get_msg(self,n):
		return self.msg_raw
	def get_marginal(self,n):			
		pass
	def get_entropy(self):
		pass
	def plot(self):
		"""
		"""
		if not(self.run_done):
			raise RuntimeError('run() must be called before plot()')
		nx = 100		
		for i in range(self.msg_translated.shape[0]):
			varlabel,A,B,alpha,beta = self.msg_translated[i,:]
			x  = np.linspace(A,B,nx)
			fx = self.gamma_bounded(x,A,B,alpha,beta)
			# noramlization !!!!! should not be done here
			fx /= np.sum(fx)			
			plt.plot(x,fx, label=str(int(varlabel)))
		plt.legend()
		plt.show()

def sample_dataset(k):
	if(k==0):
		A = np.array([[1.,0.,-1.]])
		b = np.array([[0.]])
		lb = np.array([0.,0.,0.])
		ub = np.array([1.,1.,1.])
		x0 = []
	elif(k==1):
		A = np.array([[1.,0.,-1.]])
		b = np.array([[0.]])
		lb = np.array([0.,0.,0.])
		ub = np.array([1.,1.,2.])	
		x0 = []
	elif(k==2):
		"""
		conserved quantity M shared among n agents
		(boltzmann-gibbs distribution is expected)
		"""
		n = 10
		M = 1.
		A = np.ones(n).reshape((1,n))	
		b = np.array([[M]])
		lb = np.zeros(n)
		ub = M*np.ones(n)  # no agent can take more than the sum
		x0 = M/float(n)*np.ones(n)
	elif(k==3):	
		"""
		"""
		pass
	return A,b,lb,ub,x0

def test_iJR904_from_file():
	"""
	supposing test_iJR904_from_mat was run already
	"""
	fname='/home/aurelien/local/bin/bp_beta/C/network_test_files/Ec_iJR904'
	path_bp_bin='/home/aurelien/local/bin/bp_beta/C/convex_bp/build/bpB'	
	bpB=bp_beta()	
	bpB.init_from_file(fname=fname,path_bp_bin=path_bp_bin)
	bpB.run()
	print "msg_raw=",bpB.msg_raw		
	print "x0=",bpB.x0		
	print  "msg_translated=",bpB.msg_translated
	bpB.plot()

def test_iJR904_from_mat():
	"""
	needs first to change coords (find interior point...)
	"""
	# test_Metabolic.jl:
	#met=MetabolicEP.ReadMatrix("/home/aurelien/local/git/Metabolic-EP/Ec_iJR904.mat")
	fname='/home/aurelien/local/git/Metabolic-EP/Ec_iJR904.mat'
	#fname='/home/aurelien/local/git/Metabolic-EP/iYO844.mat'
	# julia
	if True:
		import julia
		j=julia.Julia()
		j._call("""
		include("/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl");
		using MetabolicEP
		using PyCall
		using COBRA
		met=MetabolicEP.ReadMatrix("%s")
		"""%(fname))
		A=j.eval(u'met.S');
		A=np.asarray(A)
		b=j.eval(u'met.b');
		#hack
		b=np.zeros(A.shape[0])
		lb=j.eval(u'met.lb');
		ub=j.eval(u'met.ub')+1.; # because for some i, ub[i]=lb[i]
		np.all(lb<=ub)
	
	# use COBRAPY ??????
	if False:
		import scipy.io
		scipy.io.whosmat(fname)
		mat = scipy.io.loadmat(fname)
		A = mat['Ec_iJR904'][0][0][2]
		b = mat['Ec_iJR904'][0][0][3]
		lb = mat['Ec_iJR904'][0][0][6] #???????
		ub = mat['Ec_iJR904'][0][0][5]
	
	x0=[]
	bpB=bp_beta()	
	#fname='/home/aurelien/local/owncloud/boulot/recherche/phystat/health_eco_abm/popdyn/external/bp_beta/C/network_test_files/test'
	#path_bp_bin='/home/aurelien/local/owncloud/boulot/recherche/phystat/health_eco_abm/popdyn/external/bp_beta/C/convex_bp/build/bpB'
	fname='/home/aurelien/local/bin/bp_beta/C/network_test_files/Ec_iJR904'
	path_bp_bin='/home/aurelien/local/bin/bp_beta/C/convex_bp/build/bpB'
	if (x0==[]):
		bpB.init(A,b,lb,ub, fname=fname,
				solver='slacklp', path_bp_bin=path_bp_bin)
	else:			
		bpB.init(A,b,lb,ub, fname=fname,
				solver='slacklp', path_bp_bin=path_bp_bin, x0=x0)
	bpB.run()
	print "msg_raw=",bpB.msg_raw		
	print "x0=",bpB.x0		
	print  "msg_translated=",bpB.msg_translated
	bpB.plot()


def test_rbc_from_file():
	"""
	red blood cell
	fails for unknown reason
	"""
	raise NotImplementedError('error in matrix size')
	
	#fname='/home/aurelien/local/owncloud/boulot/recherche/phystat/health_eco_abm/popdyn/external/bp_beta/C/network_test_files/test'
	#path_bp_bin='/home/aurelien/local/owncloud/boulot/recherche/phystat/health_eco_abm/popdyn/external/bp_beta/C/convex_bp/build/bpB'
	fname='/home/aurelien/local/bin/bp_beta/C/network_test_files/rbc'
	path_bp_bin='/home/aurelien/local/bin/bp_beta/C/convex_bp/build/bpB'	
	bpB=bp_beta()	
	bpB.init_from_file(fname=fname,path_bp_bin=path_bp_bin)
	bpB.run()
	print "msg_raw=",bpB.msg_raw		
	print "x0=",bpB.x0		
	print  "msg_translated=",bpB.msg_translated
	bpB.plot()

def test_rbc():
	"""
	red blood cell
	"""
	raise NotImplementedError('error in matrix size')
	
	A=np.loadtxt("/home/aurelien/local/bin/bp_beta/C/network_test_files/rbc_transpose.sto")
	lb=np.loadtxt("/home/aurelien/local/bin/bp_beta/C/network_test_files/rbc_transpose.lb")
	m= A.shape[0]
	lb=lb.reshape((m,1))
	ub=np.loadtxt("/home/aurelien/local/bin/bp_beta/C/network_test_files/rbc_transpose.ub")
	ub=ub.reshape((m,1))
	b = np.zeros((m,1))
	
	x0=[]
	bpB=bp_beta()	
	#fname='/home/aurelien/local/owncloud/boulot/recherche/phystat/health_eco_abm/popdyn/external/bp_beta/C/network_test_files/test'
	#path_bp_bin='/home/aurelien/local/owncloud/boulot/recherche/phystat/health_eco_abm/popdyn/external/bp_beta/C/convex_bp/build/bpB'
	fname='/home/aurelien/local/bin/bp_beta/C/network_test_files/test'
	path_bp_bin='/home/aurelien/local/bin/bp_beta/C/convex_bp/build/bpB'
	if (x0==[]):
		bpB.init(A,b,lb,ub, fname=fname,
				solver='slacklp', path_bp_bin=path_bp_bin)
	else:			
		bpB.init(A,b,lb,ub, fname=fname,
				solver='slacklp', path_bp_bin=path_bp_bin, x0=x0)
	bpB.run()
	print "msg_raw=",bpB.msg_raw		
	print "x0=",bpB.x0		
	print  "msg_translated=",bpB.msg_translated
	bpB.plot()

def test_gibbs():
	"""
	this fails for some reason
	"""	
	A,b,lb,ub,x0=transmat.example_CSP_problems("gibbs",n=100)	
	bpB=bp_beta()	
	#fname='/home/aurelien/local/owncloud/boulot/recherche/phystat/health_eco_abm/popdyn/external/bp_beta/C/network_test_files/test'
	#path_bp_bin='/home/aurelien/local/owncloud/boulot/recherche/phystat/health_eco_abm/popdyn/external/bp_beta/C/convex_bp/build/bpB'
	fname='/home/aurelien/local/bin/bp_beta/C/network_test_files/test'
	path_bp_bin='/home/aurelien/local/bin/bp_beta/C/convex_bp/build/bpB'
	if (x0==[]):
		bpB.init(A,b,lb,ub, fname=fname,
				solver='slacklp', path_bp_bin=path_bp_bin)
	else:			
		bpB.init(A,b,lb,ub, fname=fname,
				solver='slacklp', path_bp_bin=path_bp_bin, x0=x0)
	bpB.run()
	print "msg_raw=",bpB.msg_raw		
	print "x0=",bpB.x0		
	print  "msg_translated=",bpB.msg_translated
	bpB.plot()

if __name__ == "__main__":	
	#test_gibbs()
	#test_rbc()
	test_iJR904_from_mat()
