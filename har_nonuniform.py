# -*- coding: utf-8
#  har_nonuniform.py
#  
#  Copyright 2017 Aurélien Hazan <>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

"""
non uniform hit and run sampler for continuous constraint satisfaction problem (CSP)
of the form Cw<=b  ; Fw = g

reference:
tervonen13
tervonen14
zabinsky,smith "Hit-and-Run Methods" in Encyclopedia of Operations Research
and Management Science, 3rd ed., 2013

QR decomposition:
https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.qr.html#numpy.linalg.qr
Pseudo-inverse:
https://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.pinv.html#numpy.linalg.pinv
kde:
https://docs.scipy.org/doc/scipy/reference/tutorial/stats.html#univariate-estimation

"""

"""
TODO:

URGENT:
* without equ constraint -> test only non uniform _hitandrun 
* pdf constraint-> comparer pdf_target/pdf_mesures;  plusieurs f (unif, exp, power?, )
* pdf qui pondère le revenu ou le patrimoine
* CSP= aggregated ; 	disaggre
* comparer har_wrap et har_nonuniform	

MOYEN TERME:
* compare samplers: pymc, pythran, numba
Q: which is compatible with np, np.random, np.linalg ?????????,
		# with pymc: must transform MCMC.Hitandrun into bounding box rejection
		# (bounding box is easy)
		# 
		# Q: howto to sample Ax<=b in pythran/numba/
		# decorators: http://intermediate-and-advanced-software-carpentry.readthedocs.io/en/latest/idiomatic-python-2.html?highlight=args#function-decoratorscython ?
* GetRNGstate()	
"""

import array  # for testing purpose, remove when har_cd_cstsum moves to cpython

import numpy as np
import random as rng
import matplotlib.pylab as plt
import math
import time
#from transmat import example_CSP_problems    # avoid if possible, keep the 2 separated
#!transmat is called by init
from scipy.stats import pareto, genpareto
from scipy import stats
import warnings

try:
	import har as cython_har
except ImportError:
	print 'har is missing, running pure python'

try:	
	from har import har_cd_native	
except ImportError:
	print 'har_cd_native is missing'
	

def pdf_pareto_all(x):			
	c=1.
	rv = genpareto(c)						
	return np.prod(rv.pdf( x)	)
def pdf_pareto_one(x):			
	c=1.
	rv = genpareto(c)						
	return rv.pdf( x.flat[0])	
def pdf_normal_all(x):
	# all gaussian
	mu=0
	sig=1
	return np.exp( - np.sum( ((x-mu)/sig)**2))		
def pdf_normal_one(x):
	# one gaussian
	mu=1
	sig=1
	return np.exp( - ((x.flat[0]-mu)/sig)**2)		
def pdf_uniform(x):
	return 1.


class har():
	"""
	sample (uniform & non-uniform) the CSP: 
	Cw<=b  ; Fw = g
	
	Hypersperical direction (!= coordinate direction)
	
	Transposed from the R code by Tervonen et al. https://github.com/gertvv/hitandrun
	"""
	def __init__(self,C,b,F,g,x0=None,dim=None):
		self.C=C
		self.b=b
		self.F=F
		self.g=g
		self.A=[]
		self.b_new = []
		self.F_pinv = []
		self.basis=[]
		self.dim=dim	
		self.translation=[]	
		self.transform= []
		self.transform_inv= []		
		#self.uniform = uniform 
		self.x0 = x0
		self.y0 = []	
		#self.relative_err=relative_err
		self.tol =  5E-2		
		self.tol_rel =  1E-2		
		self.reject_rate=0	
		self.nbins=50		
		self.debug=False
	def _check_args(self):		
		# check that: 
		# x0,b,g are column matrices 
		# all vars have up and low constr (else, _bounds will complain)
		if isinstance(self.F,np.matrixlib.defmatrix.matrix): raise TypeError('F has unsopported type np.matrixlib.defmatrix.matrix')
		if not isinstance(self.F,np.ndarray): raise TypeError('F must be np.ndarray, %s given'%str(type(F)) )
		if( self.x0 != None ):
			if not isinstance(self.x0,np.ndarray): raise TypeError('x0 must be np.ndarray, %s given'%str(type(self.x0)) )
			if( self.x0.shape[0]<self.x0.shape[1]): raise ValueError('x0 must be column vector')
		if not isinstance(self.b,np.ndarray): raise TypeError('b must be np.ndarray, %s given'%str(type(self.b)) )
		if( self.b.shape[0]<self.b.shape[1]): raise ValueError('b must be column vector')
		if not isinstance(self.g,np.ndarray): raise TypeError('g must be np.ndarray, %s given'%str(type(self.g)) )
		if( self.g.shape[0]<self.g.shape[1]): raise ValueError('g must be column vector')		
	def init(self):
		self._check_args()
		self._createBasis()		
		# transform the constraint 
		self.A =   np.dot(self.C , self.basis )
		self.b_new = self.b - np.dot(self.C , self.translation)
		# transform x0
		# hitandrun.R: har.init
		# D^T D y0 = y0 = D^T (x0 - F_pinv * g)
		if( self.x0 != None ):
			self.y0 = np.dot( self.basis.T , self.x0 - self.translation) 
		else:
			print "trying get_x0_linprog..."
			from transmat import bmw_sym_1F1BnW
			bmw = bmw_sym_1F1BnW(10)
			self.y0 = bmw.get_x0_linprog(self.A,self.b_new).reshape((self.A.shape[1],1))			 
				
	def _createBasis(self):	
		# create basis and translation
		# see R/hitandrun/transformSimplex:solution.basis()
		self.F_pinv = np.linalg.pinv(self.F)				
		nr=self.F_pinv.shape[0]
		IminusFpinvF = np.eye(nr) - np.dot(self.F_pinv, self.F) 		
		q,r = np.linalg.qr( IminusFpinvF  )		
		# dimension of the basis 
		if isinstance(self.dim,(int)) and self.dim>0:
			d=self.dim
		else:	
			d = np.linalg.matrix_rank(IminusFpinvF)		
		# create change of basis
		# basis=qr.Q(the.qr)[, 1:the.qr$rank, drop=FALSE],				
		# check that we have enough columns of the basis, that are solution of 
		# Fw=g
		idx_sorted = np.flipud(np.argsort(np.abs(np.diag(r))))
		self.basis = q[:, idx_sorted[0:d]]		
		err_abs=np.zeros(self.basis.shape[1])		
		for j in range(self.basis.shape[1]):
			delta = np.dot(self.F, self.basis[:,j].reshape(( self.basis.shape[0] ,1)) ) 			
			err_abs[j] =np.linalg.norm(delta)			
		
		if( np.any(err_abs	> self.tol) ): 
			plt.plot(err_abs); plt.show()
			raise ValueError('basis vectors dont respect Fw=0 ')			
			
		# create translation
		self.translation= np.dot( self.F_pinv, self.g)		
		
	def _bounds(self,A,b,d, y):
		"""
		see hitanrdun/bounds.c
		"""		
		a = (b - np.dot(A,y)).flatten()
		c = np.dot(A,d).flatten()
		#print "A=",A, " b=",b, " d=",d, " y=", y
		#print " c=",c, " d=",d, " a=",a , "  a / c=",a/c
		nrow = a.shape[0]
		l0=-np.inf
		l1=np.inf
		# TODO: vectorize this:
		for i in range(nrow):
			if (c[i] < 0.0): 
				t = a[i] / c[i]
				if (t > l0): 
					l0 = t				
			elif (c[i] > 0.0) :
				t = a[i] / c[i]
				if ( t < l1) :
					l1 = t
		return l0,l1					

	def	_hitandrun(self, A,b,y0,nsamp,pdf,uniform,basis,translation):		
		"""
		sample the problem Ay<=b_new
		(without caring for the equality)
		"""		
		# GetRNGstate(); ???????
		dim_y = A.shape[1]
		samples = np.zeros((nsamp,dim_y))
		y=y0
		y_new=0
		reject_rate = 0
		for i in range(nsamp):
			# generate random direction d
			d = np.random.normal(size=dim_y).reshape((dim_y,1))			
			# calculate bounds l
			l0,l1 = self._bounds(A,b,d, y) 
			if (not(np.isfinite(l0)))|(not(np.isfinite(l1))): 
				raise ValueError("Bounding function gave NA bounds [%f, %f]"%(l0,l1));			
			if (l0 == l1) :
				raise ValueError("Bounding function gave empty interval");			
			# main step	
			v = l0 + np.random.uniform() * (l1 - l0)			
			if uniform:	
				y = v*d + y								
				samples[i,:]=y.flatten()					
			else:
				# rejection, if nonuniform
				r = np.random.uniform()
				# change coordinate				
				x= np.dot(basis, y).T+ translation.T				
				#print " l0=",l0, " l1=",l1, " y=",y, " v=",v, " d=",d, " v*d=",v*d
				y_new = v*d + y							
				x_new= np.dot(basis, y_new).T+ translation.T
				# compute probabilities
				p_x = pdf( x)
				p_xnew = pdf( x_new )
				# reject		
				if(p_x>0):
					if(r < np.min([1., p_xnew/p_x])):
						samples[i,:]=y_new.flatten()
						y=y_new																		
				else:	
					reject_rate += 1	
					if(i==2):
						samples[i,:]=samples[i-1,:]
					else:	
						samples[i,:]=y.flatten()		
				if(self.debug):			
					print "unif=",uniform," i=",i, " d=",d, " (l0,l1)=(%f,%f)"%(l0,l1), " v=",v, " r=",r, "x0=",x, " p(x)=",p_x, " x_new=",x_new, "p(x_new)=", p_xnew , ' min=', np.min([1., p_xnew/p_x])
						
		reject_rate = float(reject_rate)/float(nsamp)			
		return samples,reject_rate

	
	def sample(self,nsamp, pdf=None, native=False, uniform=True,thin=10,seed=0,log=False):
		"""
        sample.R: checkPolytope
         constr = list(constr = constr$constr[ , 1:(n - 1), drop=FALSE],
                      rhs = constr$rhs - constr$constr[ , n, drop=TRUE],
        """             
		n = self.A.shape[1]
		y0_new = self.y0
		A_new=self.A
		b_new= self.b_new
		# sample
		if native==False:
			samples,reject_rate=self._hitandrun(A_new,b_new, y0_new,nsamp,pdf,
												uniform,self.basis,self.translation)						
		elif native==True:
			samples,reject_rate=cython_har.hitandrun(A_new,b_new, y0_new,nsamp,pdf,
												uniform,self.basis,self.translation)		
		elif native=='har_hd':	
			samples,reject_rate=cython_har.har_hd( A_new, b_new.flatten(), 
												y0_new.flatten(),
												nsamp, pdf,  
				self.basis, self.translation.flatten(), seed, thin, log)
			
		self.reject_rate=reject_rate										
		# check that constraint is respected
		delta= np.dot(A_new, samples.T)-np.repeat(b_new,nsamp, axis=1)	
		if(np.any(delta>self.tol)):	
			idx = np.where(delta>self.tol)[0]
			print "idx[0:100]=",idx[0:100]
			print "delta[idx[0:100]]=",delta[idx[0:100]]			
			samples = np.delete(samples, idx, 0)
			#raise ValueError('inequ Ay-b_new<=0 is not respected')	
			pct = float(idx.shape[0])/float(nsamp) * 100.
			warnings.warn('inequ Ay-b_new<=0 is not respected by %f pct of samples'%(pct), Warning)
		# x = Fpinv*g + D*samples
		return np.dot(self.basis,samples.T).T+ self.translation.T , reject_rate
			
	
	def check_constr(self,x):		
		"""
		TODO: use utils.check_constr_vec instead ??
		"""		
		nsamp = x.shape[0]		
		tol = self.tol
		# check equality
		delta= np.abs(np.dot(self.F,x.T)-np.repeat(self.g,nsamp, axis=1))	
		if(np.any(delta>tol)):
			raise ValueError('equ Fx-g=0 is not respected')
		# check inequality	
		delta= np.dot(self.C, x.T)-np.repeat(self.b,nsamp, axis=1)
		if(np.any(delta>0+tol)):
			raise ValueError('inequ Cx-b<=0 is not respected')
		
	def plot_metrics(self,x,pdf=None, log=True,msg=None):		
		"""
		acor...
		"""		
		dim = x.shape[1]
		M  = np.sum(x[-1,:])
		fs=18
		# histo 1ere composante
		fig = plt.figure('metrics',figsize=(16,8))
		nr,nc=2,3		
		ax=plt.subplot(nr,nc,1)	
		if(pdf==None):
			count,bins,patches=plt.hist(x[:,0],self.nbins)			
		else:			
			#count,bins,patches=plt.hist(x[:,0],self.nbins, normed = True, log=log)				
			count,bins,patches=plt.hist(x.flatten(),self.nbins, normed = True, log=log)	
			xx=np.linspace(0,M,1000);		
			plt.plot(xx  , pdf(xx) )
			plt.xlabel('x',fontsize=fs); plt.ylabel('Frequency',fontsize=fs)										
			#plt.semilogy(xx  , pdf(xx) )					
		# ajouter pdf
		# kde autres composantes			
		plt.subplot(nr,nc,2)	
		for j in range(x.shape[1]):
			try:
				kernel = stats.gaussian_kde(x[:,j])
				y = kernel(bins)
				plt.plot(bins,y)
			except:
				pass	
		#plt.hist(x[:,1],self.nbins)
		# 
		plt.subplot(nr,nc,3)	
		plt.acorr(x[:,0], detrend=plt.mlab.detrend_mean)
		plt.acorr(x[:,1], detrend=plt.mlab.detrend_mean)
		plt.subplot(nr,nc,4)
		if(x.shape[1]==2):			
			plt.scatter(x[:,0],x[:,1])	
		else:
			n=x.shape[0]
			#plt.plot(range(n), x[:,0],range(n), x[:,1])				
			plt.plot(x)
		plt.subplot(nr,nc,5)	
		plt.plot(IPR(x))				
		plt.subplot(nr,nc,6)	
		plt.plot( np.sum(x, 1) )	
		plt.ylabel('sum')		
		tit = "dim=%d Mtot= %1.1f Mtot/nw=%1.2f mean(x)=%1.2f  reject=%1.1f"%(dim,M,M/float(dim),np.mean(x),self.reject_rate)
		if not msg ==None:
			tit += msg
		ax.set_title(tit)		
		plt.show()
	
def test_har_cd_cstsum():
	"""
	TODO:
	checksum
	acor
	phase diagram IPR (inverse participation rate)
	"""
	dim= 10		 
	nsamp= 5000
	
	# choose pdf, make sure that E(X) = M/dim 
	# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
	# pareto.pdf(x, b) = b / x**(b+1)
	b=2. # NB: b must be > 1 so that E[x] exists.
	rv = pareto(b)
	M = dim * rv.mean()
	x0= M * np.ones(dim)/dim
	# sample
	x,reject_rate = har_cd_cstsum(x0,dim,M,nsamp, rv.pdf)

	# plot
	print "M=",M," mean=",rv.mean()," reject_rate=",reject_rate	
	plt.hist(x[:,0], bins=30, normed = True)
	xx=np.linspace(0,M,100); plt.plot(xx  , rv.pdf(xx) )
	plt.show()
	
	
	
def har_cd_cstsum(x0,dim,M,nsamp,f,seed=None):
	"""
	har sampling of the constant sum problem $\sum M_i=M$ with the coordinate direction (CD) algorithm.
	
	NB: in order to ease porting to cython: use the following libs:
	  random https://docs.python.org/2/library/random.html
	  math https://docs.python.org/2/library/math.html
	  array http://cython.readthedocs.io/en/latest/src/tutorial/array.html	
			
	input:
	-----
	x0: array-like, shape=(dim,)
	initial state
	
	dim:int
	dimension of x0
	
	M: double
	constant sum constraint $\sum M_i=M$
	
	nsamp: int
	number of samples
	
	f: function 
	target pdf for non-uniform sampling
	
	output:
	------		
	k : int
	number of generated samples  (k<= nsamp)
	
	x: array.array,  shape(k*dim, )
	generated samples
	
	reject_rate: float
	rejection rate
									
	see:
	-----
	Zabinsky "Hit-and-run methods" in encyclopedia of operations research.
	Filiasi http://arxiv.org/abs/1309.7795v2, appendix
	"""	
	if not seed==None:
		rng.seed(seed)
	x = array.array('d', [0.]*(dim*nsamp) ) #???????
	#array.resize(x, dim*nsamp)
	x_tmp = array.array('d', [0.]*dim)#???????
	#array.resize(x_tmp, dim)
	reject=0
	k=0
	for i in range(nsamp):		
		# copy x to tmp
		if(k>0):
			for j in range(dim):
				x_tmp[j] = x[ (k-1)*dim + j ]
		else:
			for j in range(dim):
				x_tmp[j] = x0[j]
		# choose A,B
		idx= range(dim); 
		rng.shuffle(idx) #in place
		A = idx[0] 
		B = idx[1]
		# propose a move x[A] -> x[A] + eps ; x[B] -> x[B] -eps
		xA = x_tmp[A]; xB = x_tmp[B]
		eps = rng.uniform( 0, min(M-xA,xB ) ) 			
		xA_new= xA + eps ; xB_new= xB - eps
		#if ( xA_new<0 or xA_new>M or xB_new<0 or xB_new>M): raise ValueError('move not allowed') #DEBUG
		# accept/reject		
		r = rng.uniform(0,1)		
		if ( r < (f(xA_new)/f(xA ) *f(xB_new)/f(xB)) ):	
			# accept: copy x to tmp
			for j in range(dim):				
				x[ k*dim + j ] = x_tmp[j] 									
			x[ k*dim + A ] = xA_new
			x[ k*dim + B ] = xB_new
			k+=1
		else:
			reject += 1 		
	# checksum
	x = np.frombuffer(x[0:k*dim],dtype=float)
	x=np.reshape(x,(k,dim))
	s=np.sum(x,1)
	if(not( np.allclose(s, M*np.ones(k) ) )): raise ValueError('incorrect checksum')
				
	return x, float(reject)/nsamp	
		
def	thin_function(typ='default'):
	"""
	see tervonen 13
	"""
	if(typ=='default'): 
		return lambda n: int(np.ceil(np.log(n + 1.)/4. * n**3))	
	else:
		raise ValueError('unknown function type') 

def IPR(x):
	"""
	Inverse Participation Rate
	see Filiasi et al
	"""	
	nr,dim=x.shape
	s = np.sum(x,axis=1)
	ipr = np.sum( (x/np.outer(s,np.ones(dim)))**2,axis=1)
	return ipr

def sample_rnd_fitness_cd(groups, sums,fitness)	:
	"""
	sample a vector from a given distribution, such that groups of 
	variables have prescribed sums.
	
	# NB: b must be > 1 so that E[x] exists. 
	
	parameters:
	------------
	g: array, shape=(n,)
	g[i] is the group of individual i
	
	sums: array, shape=(m,)
	sums[j] is the value of the sum of all of the j-th group in g
	
	fitness: dict
		fitness['seed']: int or None
			seed of random number generator	
		fitness['thin']: int
			thinning factor
		fitness['accel']: bool
			turn acceleration on
		fitness['log']: bool
			use log probabilities
		fitness['f'] : function
			pdf of the sampled variable
		fitness['f_mean']: float
			mean of the pdf
		f_nsamp =  fitness['f_nsamp'] : function
			return the number of samples depending on the dimension of the sample
		fitness['param1']: float
			parameter of the sampled pdf
		fitness['param2']: float
			parameter of the sampled pdf
	returns:
	--------
	x: array, shape=(n,)	
	sampled vector
	"""
	# check params
	g,g_idx = np.unique(groups,	return_index=True )
	if g.size != sums.size: raise ValueError('shape mismatch')
	if np.any(sums<=0): raise ValueError('all sums must be >0')
	if np.any(np.diff(groups)<0):raise ValueError('values in groups must be increasing')
	# from har_nonuniform_test.py: 		
	n = groups.size
	g_idx_ext = np.hstack((g_idx,np.array([n])))
	x=[]	
	# read fitness param	
	seed=fitness['seed']
	thin= fitness['thin'] 	
	accel= fitness['accel']
	log= fitness['log']
	f=  fitness['f']
	f_mean=  fitness['f_mean']
	f_nsamp =  fitness['f_nsamp']
	param1=  fitness['param1']
	param2= fitness['param2']
	for i in range(g_idx_ext.size-1) :	
		s = sums[i] 
		n_ingroup =  g_idx_ext[i+1] - g_idx_ext[i] 		
		nsamp=f_nsamp(n_ingroup)#30*n_ingroup**2 heuristic
		# get samples
		# choose pdf, make sure that E(X) = M/dim 
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		# pareto.pdf(x, b) = b / x**(b+1)	
		dim= n_ingroup	
		M = dim*f_mean #s 
		x0= M * np.ones(dim)/dim						
		x_tmp,reject_rate = cython_har.har_cd_native_inplace(x0, dim, M,nsamp, f, seed, thin,accel,log, param1,param2)
		# rescale so that new sum is s
		x_tmp= np.array(x_tmp) * s /M 									
		# collect
		if(i==0):
			x=x_tmp 
		else:
			x=np.hstack((x, x_tmp ))				
	return x	

def sample_fitness_pareto_cd(groups, sums,b=2.,debug=False,nsamp=None):
	"""
	# NB: b must be > 1 so that E[x] exists. 	
	"""
	# check params
	g,g_idx = np.unique(groups,	return_index=True )
	if g.size != sums.size: raise ValueError('shape mismatch')
	if np.any(sums<=0): raise ValueError('all sums must be >0')
	if np.any(np.diff(groups)<0):raise ValueError('values in groups must be increasing')
	# from har_nonuniform_test.py: 		
	n = groups.size
	g_idx_ext = np.hstack((g_idx,np.array([n])))
	
	rv = pareto(b)
	seed=None
	thin= 1	
	x=[]
	accel=1
	log=1
	for i in range(g_idx_ext.size-1) :	
		s = sums[i] 
		n_ingroup =  g_idx_ext[i+1] - g_idx_ext[i] 
		if nsamp==None:
			nsamp=30*n_ingroup**2 #heuristic
		# get samples
		# choose pdf, make sure that E(X) = M/dim 
		# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.pareto.html#scipy.stats.pareto
		# pareto.pdf(x, b) = b / x**(b+1)	
		dim= n_ingroup	
		M = s# SHOULD BE : dim * rv.mean()
		x0= M * np.ones(dim)/dim				
		f= rv.pdf
		x_tmp,reject_rate = har_cd_native(x0, dim, M,nsamp, f, seed, thin,
											accel, log)
		# collect
		if(i==0):
			x=x_tmp[-1:].flatten()
		else:
			x=np.hstack((x, x_tmp[-1:].flatten() ))	
	return x
	
def sample_fitness_pareto_bygroup(groups, sums,p=1.,debug=False):
	"""
	sample a vector of n fitnesses from a pareto pdf.
	Fitnesses belong to groups. 
	sampling is done !group by group!.
	
	Variable i belongs to group i, the total sum of the  is sums[i]
	All elements in sums must be >0
	
	this function is used by topological_measures.py
	
	parameters:
	--------------
	groups: array, shape=(n,)
	variable i belongs to the group identified by groups[i]. groups is an increasing sequence
	
	sums: array, shape =(ngroup,)
	the sum of variables in group j is sums[j]
	
	p: float
	parameter of the pareto pdf
	
	outputs:
	---------------
	x: array, shape=(n,)
	fitnesses
	"""
	raise NotImplementedError('fails for unknown reason')
	# check params
	g,g_idx = np.unique(groups,	return_index=True )
	if g.size != sums.size: raise ValueError('shape mismatch')
	if np.any(sums<=0): raise ValueError('all sums must be >0')
	if np.any(np.diff(groups)<0):raise ValueError('values in groups must be increasing')
	# compute C,b,F,g,x0
	n = groups.size	
	g_idx_ext = np.hstack((g_idx,np.array([n])))
	pdf = cython_har.pdf_pareto_all	
	x=[]
	for i in range(g_idx_ext.size-1) :
		s = sums[i] 
		n_ingroup =  g_idx_ext[i+1] - g_idx_ext[i] 
		nsamp=30*n_ingroup**2 #heuristic
		ub = s*np.ones(n_ingroup) 
		F= np.ones((1,n_ingroup))
		x0 = 1./float(n_ingroup)*np.ones(n_ingroup).reshape([n_ingroup,1])
		C = np.vstack((-np.eye(n_ingroup), np.eye(n_ingroup)))
		b=np.hstack((np.zeros(n_ingroup),ub)).reshape([2*n_ingroup,1])		
		# get samples
		h=har(C,b,F,s * np.ones(n_ingroup).reshape([n_ingroup,1]),x0)	
		h.init()
		x_tmp, reject_rate = h.sample(nsamp, pdf,native=True)
		if(i==0):
			x=x_tmp[-1:].flatten()
		else:
			x=np.hstack((x, x_tmp[-1:].flatten() ))	
	return x
	
	
def sample_fitness_pareto_hd(groups, sums,p=1.,debug=False):
	"""
	sample a vector of n fitnesses from a pareto pdf.
	Fitnesses belong to groups. 
	sampling is done simultaneously for all variables(not group by group).
	
	Variable i belongs to group i, the total sum of the  is sums[i]
	All elements in sums must be >0
	
	this function is used by topological_measures.py
	
	parameters:
	--------------
	groups: array, shape=(n,)
	variable i belongs to the group identified by groups[i]. groups is an increasing sequence
	
	sums: array, shape =(ngroup,)
	the sum of variables in group j is sums[j]
	
	p: float
	parameter of the pareto pdf
	
	outputs:
	---------------
	x: array, shape=(n,)
	fitnesses
	"""
	# check params
	g,g_idx = np.unique(groups,	return_index=True )
	if g.size != sums.size: raise ValueError('shape mismatch')
	if np.any(sums<=0): raise ValueError('all sums must be >0')
	if np.any(np.diff(groups)<0):raise ValueError('values in groups must be increasing')
	# from har_nonuniform_test.py: 
	n = groups.size
	nsamp=10 #heuristic
	# pdf,x_max,yav,total_income = get_piecewise_pareto_with_parameters(onedim=False)
	# compute C,b,F,g,x0	
	g_idx_ext = np.hstack((g_idx,np.array([n])))
	ub = np.zeros(n) 
	F= np.zeros((sums.size,n))
	x0 = np.zeros(n).reshape([n,1])
	for i in range(g_idx_ext.size-1) :
		ub[g_idx_ext[i]:g_idx_ext[i+1]] = sums[i]
		F[i,g_idx_ext[i]:g_idx_ext[i+1] ] = 1
		x0[g_idx_ext[i]:g_idx_ext[i+1]] = sums[i] / (g_idx_ext[i+1] - g_idx_ext[i] )
	C = np.vstack((-np.eye(n), np.eye(n)))
	b=np.hstack((np.zeros(n),ub)).reshape([2*n,1])	
	# get samples
	pdf = cython_har.pdf_pareto_all	
	h=har(C,b,F,sums.reshape([sums.size,1]),x0)	
	h.init()
	t=time.time()
	#x, reject_rate = h.sample(nsamp, pdf,native=True)
	x, reject_rate = h.sample(nsamp, pdf,native='har_hd')
	t = time.time()	-t		
	if debug: h.plot_metrics(x)
	return x[-1:].flatten()
		
if __name__ == "__main__":
	#C,b,F,g=example(1)  => check that error is raised	
	"""
	C,b,F,g,x0=example(3)
	h=har(C,b,F,g,x0)	
	h.init()
	nsamp = 3000
	x=h.sample(nsamp)
	
	print "x=",x
	h.check_constr(x)
	h.plot_metrics(x)
	#print x
	"""	
	test_har_cd_cstsum()	
	pass
	"""
	def some_pdf(x,alpha):
		return np.exp( - np.sum( 1./0.01*(x-0.5)**2))
	c=1.
	rv = genpareto(c, loc=0.1)		
	def some_pdf(x):
		#print x,rv.pdf(x[0]), type(rv.pdf(x[0]))
		return rv.pdf( x.flat[0])	
		
	C,b,F,g,x0=example_CSP_problems(3)
	h=har(C,b,F,g,x0,uniform=False)	
	h.init()
	nsamp = 3000
	alpha = 0
	x=h.sample(nsamp, some_pdf)	
	h.plot_metrics(x)
	print "rejection rate=",h.reject_rate
	"""

