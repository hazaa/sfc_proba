# -*- coding: utf-8 -*-
# copied from https://gist.github.com/vene/7224672
from time import time
import sys
#from sklearn.utils.validation import check_arrays  doesn't work
from sklearn.utils.extmath import safe_sparse_dot
from scipy.sparse import csr_matrix, issparse
from pandas import DataFrame
from scipy.optimize import nnls
import numpy as np
import scipy
from scipy.optimize import fmin_l_bfgs_b

from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.utils.extmath import safe_sparse_dot

"""
https://en.wikipedia.org/wiki/Non-negative_least_squares

nls+sparse
https://gist.github.com/mblondel/4421380
https://gist.github.com/artemyk/5002777

notebook (not sparse)
https://gist.github.com/vene/7224672

scipy nnls:
https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.nnls.html

nmf Non-negative matrix factorization
https://sklearn.org/modules/decomposition.html#nmf

misc:
elemental: http://libelemental.org/documentation


"""
class LbfgsNNLS_L1(BaseEstimator, RegressorMixin):
	#copied from https://gist.github.com/vene/4429796
	# with L1 regularization
	def __init__(self, tol=1e-3, callback=None, alpha=1.0):
		self.tol = tol
		self.callback = callback
		self.alpha = alpha
		
	def fit(self, X, y):
		n_features = X.shape[1]

		def f(w, *args):
			return (np.sum((safe_sparse_dot(X, w) - y) ** 2) +
					self.alpha * np.sum(w))

		def fprime(w, *args):
				if self.callback is not None:
					self.coef_ = w
					self.callback(self)
				return self.alpha + 2 * safe_sparse_dot(X.T,
									safe_sparse_dot(X, w) - y)

		coef0 = np.zeros(n_features, dtype=np.float64)
		w, f, d = fmin_l_bfgs_b(f, x0=coef0, fprime=fprime, pgtol=self.tol,
								bounds=[(0, None)] * n_features)
		self.coef_ = w

		return self

	def n_nonzero(self, percentage=False):
		nz = np.sum(self.coef_ != 0)

		if percentage:
			nz /= float(self.coef_.shape[0])
			
		return nz
	
	def predict(self, X):
		return safe_sparse_dot(X, self.coef_)


class LbfgsNNLS(BaseEstimator, RegressorMixin):
	"""
	copied from :
	https://gist.github.com/mblondel/4421380
	https://gist.github.com/artemyk/5002777
	NB: no L1 regularization
	"""
	def __init__(self, tol=1e-3, callback=None):
		self.tol = tol
		self.callback = callback
		self.fval= None #Value of func at the minimum.
	def fit(self, X, y):
		n_features = X.shape[1]
		def f(w, *args):
			return np.sum(np.power((safe_sparse_dot(X, w) - y), 2))
	
		def fprime(w, *args):
			if self.callback is not None:
				self.coef_ = w
				self.callback(self)
			return 2 * np.ravel(safe_sparse_dot(X.T, (safe_sparse_dot(X, w) - y).T))
	
		coef0 = np.zeros(n_features, dtype=np.float64)
		w, f, d = fmin_l_bfgs_b(f, x0=coef0, fprime=fprime, pgtol=self.tol,
								bounds=[(0, None)] * n_features)
		self.fval = f						
		self.coef_ = w
	
		return self
	
	def n_nonzero(self, percentage=False):
		nz = np.sum(self.coef_ != 0)
	
		if percentage:
			nz /= float(self.coef_.shape[0])
	
		return nz
	
	def predict(self, X):
		return safe_sparse_dot(X, self.coef_)


def nls_activeset(X, Y, W_init=None):
    """Non-negative least squares by the active set method
    
    solves min ||Y - XW||_F, no regularization
    
    Parameters
    ----------
    X, Y : array-like
        The known parts of the problem.

    W_init : array-like,
        Preallocated memory to store results in. Its contents are not used.
    
    Returns
    -------
    
    W : array-like
        The solution.
    
    residual : float,
        The value of the target function at the objective.
    """
    #X, Y = check_arrays(X, Y, check_ccontiguous=True, sparse_format='csr')
    n_samples, n_features = X.shape
    n_samples, n_targets = Y.shape
    residual = 0
    if not W_init or W_init.shape != (n_features, n_targets):
        W = np.empty((n_features, n_targets))
    for k in xrange(n_targets):
        this_Y = Y[:, k]
        # densify the column vector if sparse
        if hasattr(this_Y, 'toarray'):
            this_Y = this_Y.toarray().squeeze()
        W[:, k], this_res = nnls(X, this_Y)
        residual += this_res ** 2
    return W, np.sqrt(residual)


from scipy.optimize.lbfgsb import fmin_l_bfgs_b

# Authors: Mathieu Blondel, Vlad Niculae
def nls_lbfgs_b(X, Y, W_init=None, l1_reg=0, l2_reg=0, max_iter=5000, tol=1e-3, callback=None):
    """Non-negative least squares solver using L-BFGS-B.
        
    Solves for W in
    min 0.5 ||Y - XW||^2_F + + l1_reg * sum(W) + 0.5 * l2_reg * ||W||^2_F
    
    """
   # X, Y = check_arrays(X, Y, sparse_format='csr')
    n_samples, n_features = X.shape
    n_targets = Y.shape[1]
    G = safe_sparse_dot(X.T, X)
    Xy = safe_sparse_dot(X.T, Y)

    def f(w, *args):
        W = w.reshape((n_features, n_targets))
        diff = (safe_sparse_dot(X, W) - Y)
        diff = diff.A if hasattr(diff, 'A') else diff
        res = 0.5 * np.sum(diff ** 2)
        if l2_reg:
            res += 0.5 * l2_reg * np.sum(W ** 2)
        if l1_reg:
            res += l1_reg * np.sum(W)
        return res

    def fprime(w, *args):
        W = w.reshape((n_features, n_targets))
        grad = (np.dot(G, W) - Xy).ravel()
        if l2_reg:
            grad += l2_reg * w
        if l1_reg:
            grad += l1_reg
        return grad

    if W_init is None:
        W = np.zeros((n_features * n_targets,), dtype=np.float64)
    else:
        W = W_init.ravel().copy()
    W, residual, d = fmin_l_bfgs_b(
                f, x0=W, fprime=fprime, pgtol=tol,
                bounds=[(0, None)] * n_features * n_targets,
                maxiter=max_iter,
                callback=callback)
    
    # testing reveals that sometimes, very small negative values occur
    W[W < 0] = 0
    
    if l1_reg:
        residual -= l1_reg * np.sum(W)
    if l2_reg:
        residual -= 0.5 * l2_reg * np.sum(W ** 2)
    residual = np.sqrt(2 * residual)
    if d['warnflag'] > 0:
        print("L-BFGS-B failed to converge")
    
    return W.reshape((n_features, n_targets)), residual

# Author: Vlad Niculae <vlad@vene.ro>
#         Lars Buitinck <L.J.Buitinck@uva.nl>
# Author: Chih-Jen Lin, National Taiwan University (original projected gradient
#     NMF implementation)
# Author: Anthony Di Franco (original Python and NumPy port)
# License: BSD 3 clause


def nls_projgrad(X, Y, W_init=None, l1_reg=0, l2_reg=0, tol=1e-3, max_iter=5000,
                 sigma=0.01, beta=0.1, callback=None):
    """Non-negative least square solver

    Solves a non-negative least squares subproblem using the
    projected gradient descent algorithm.
    min 0.5 * || XW - Y ||^2_F + l1_reg * sum(W) + 0.5 * l2_reg * * ||W||^2_F

    Parameters
    ----------
    Y, X : array-like
        Constant matrices.

    W_init : array-like
        Initial guess for the solution.

    l1_reg, l2_reg : float,
        Regularization factors

    tol : float
        Tolerance of the stopping condition.

    max_iter : int
        Maximum number of iterations before timing out.

    sigma : float
        Constant used in the sufficient decrease condition checked by the line
        search.  Smaller values lead to a looser sufficient decrease condition,
        thus reducing the time taken by the line search, but potentially
        increasing the number of iterations of the projected gradient
        procedure. 0.01 is a commonly used value in the optimization
        literature.

    beta : float
        Factor by which the step size is decreased (resp. increased) until
        (resp. as long as) the sufficient decrease condition is satisfied.
        Larger values allow to find a better step size but lead to longer line
        search. 0.1 is a commonly used value in the optimization literature.

    Returns
    -------
    W : array-like
        Solution to the non-negative least squares problem.

    grad : array-like
        The gradient.
    
    n_iter : int
        The number of iterations done by the algorithm.

    Reference
    ---------

    C.-J. Lin. Projected gradient methods
    for non-negative matrix factorization. Neural
    Computation, 19(2007), 2756-2779.
    http://www.csie.ntu.edu.tw/~cjlin/nmf/

    """
    X, Y = check_arrays(X, Y, sparse_format='csr')
    n_samples, n_features = X.shape
    n_targets = Y.shape[1]
    XY = safe_sparse_dot(X.T, Y)
    G = np.dot(X.T, X)

    if W_init is None:
        W = np.zeros((n_features, n_targets), dtype=np.float64)
    else:
        W = W_init.copy()
    init_grad_norm = safe_fro(np.dot(G, W) - XY + l2_reg * W + l1_reg)
    # values justified in the paper
    alpha = 1
    for n_iter in range(1, max_iter + 1):
        grad = np.dot(G, W) - XY  # X'(XW - Y) using precomputation
        if l2_reg:
            grad += l2_reg * W
        if l1_reg:
            grad += l1_reg

        # The following multiplication with a boolean array is more than twice
        # as fast as indexing into grad.
        proj_grad_norm = np.linalg.norm(grad * np.logical_or(grad < 0, W > 0))
        if proj_grad_norm / init_grad_norm < tol:
            break

        W_prev = W

        for inner_iter in range(20):
            # Gradient step.
            W_next = W - alpha * grad
            # Projection step.
            W_next *= W_next > 0
            d = W_next - W
            gradd = np.dot(grad.ravel(), d.ravel())
            dGd = np.dot(np.dot((G + l2_reg), d).ravel(), d.ravel())
            suff_decr = (1 - sigma) * gradd + 0.5 * dGd < 0
            if inner_iter == 0:
                decr_alpha = not suff_decr

            if decr_alpha:
                if suff_decr:
                    W = W_next
                    break
                else:
                    alpha *= beta
            elif not suff_decr or (W_prev == W_next).all():
                W = W_prev
                break
            else:
                alpha /= beta
                W_prev = W_next
        if callback:
            callback(W)

    if n_iter == max_iter:
        print("PG failed to converge")
    residual = safe_fro(np.dot(X, W) - Y)
    return W, residual
