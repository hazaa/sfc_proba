# -*- coding: utf-8 -*-
"""
compare various ways to solve the CSP {Ax=b, lb<=x<=ub}
  * optGpSampler (from cobrapy, and original implementation)
  * MetabolicEP https://github.com/anna-pa-m/Metabolic-EP/
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.stats import truncnorm,norm
import os
from os.path import join

# helper functions
def scaleBounds(lb,ub):
	
	scaleFactor = max(abs(lb.min()), ub.max()) / 1000
	
	if scaleFactor > 1:
		lb = lb / scaleFactor
		ub = ub / scaleFactor
	else:
		scaleFactor = 1
		
	return lb,ub,scaleFactor

def MetabolicEP_run(fname):
	
	import julia
	j=julia.Julia()
	j._call(u""" 
		using COBRA
		include("/home/aurelien/local/git/Metabolic-EP/src/MetabolicEP.jl");
		 using MetabolicEP
		using Distributions, Gadfly
		met=MetabolicEP.ReadMatrix("%s")
		 r=MetabolicEP.metabolicEP(met.S,met.b,met.lb,met.ub, beta=1e10)
	"""%(fname))
	mu=j.eval(u'r.μ')
	s=j.eval(u'r.σ')
	av=j.eval(u'r.av')
	va=j.eval(u'r.va')
	lb=j.eval(u'met.lb')
	ub=j.eval(u'met.ub')
	return mu,s,av,va,lb,ub

def MetabolicEP_plot(i,mu,s,av,va,lb,ub, samplePts=None,ndots=300):
	# copied from MetabolicEP.ipynb
	#x = l<mu[ii]<u?1/np.sqrt(2*np.pi*sig[ii]):0
	rv=norm(loc=mu[i],scale=np.sqrt(s[i]))
	x=0
	if(lb[i]<mu[i] and mu[i]<ub[i]): x= 1./np.sqrt(2.*np.pi*s[i])				
	m=max( rv.pdf(lb[i]), rv.pdf(ub[i]),x )
	v=np.sqrt(-2.*s[i]*np.log(np.sqrt(2*np.pi*s[i])*1e-3*m));
	l1,l2=max(lb[i],mu[i]-v),min(ub[i],mu[i]+v)	
	xvals = np.linspace(l1,l2,ndots)
				
	"""xvals=np.linspace(lb[i],ub[i],ndots)"""
	a, b = (lb[i] - mu[i]) / s[i], (ub[i] - mu[i]) /s[i]
	y=truncnorm.pdf(xvals,a,b,loc=mu[i],scale=np.sqrt(s[i]) )
	if not samplePts==None:
		if isinstance(samplePts,pd.DataFrame):
			plt.hist(samplePts.iloc[:,i])
		else:	
			plt.hist(samplePts[:,i])
	else:		
		plt.plot(xvals,y)
	
#################################################	
#################################################	
# cobrapy implementation of optGpSampler
"""
* https://github.com/opencobra/cobrapy/blob/290535a53169c719208daa3eea14a9acb8d46b58/documentation_builder/sampling.ipynb
* problem inhomogenous:
https://github.com/opencobra/cobrapy/issues/558
* docker
https://github.com/dmccloskey/optGpSampler/blob/master/Dockerfile
"""

from cobra.test import create_test_model
from cobra.flux_analysis import sample
from cobra.flux_analysis.sampling import OptGPSampler
import cobra.test

model = create_test_model("textbook")
s = sample(model, 100)
s.head()

optgp = OptGPSampler(model, processes=4)
s2 = optgp.sample(100)
optgp.validate(s2)

# load matlab example
# https://github.com/opencobra/cobrapy/blob/290535a53169c719208daa3eea14a9acb8d46b58/documentation_builder/io.ipynb
#data_dir = cobra.test.data_dir
#cobra.io.load_matlab_model(join(data_dir, "mini.mat"), variable_name="mini_textbook")
fname = join("/home/aurelien/local/git/Metabolic-EP/","Ec_iJR904.mat")
m=cobra.io.load_matlab_model(fname, variable_name="Ec_iJR904")
optgp = OptGPSampler(m, processes=4)
s2 = optgp.sample(100)
optgp.validate(s2)

###############################"
# comparaison with MetabolicEP 
fname="/home/aurelien/local/git/Metabolic-EP/Ec_iJR904.mat"
mu,s,av,va,lb,ub=MetabolicEP_run(fname)
# plot
i=10 
MetabolicEP_plot(i,mu,s,av,va,lb,ub,s2)
	
#################################################	
# the same with another non-metabolic CSP 
# (BMW model)
"""
* sampling.py
cf https://github.com/opencobra/cobrapy/blob/devel/cobra/flux_analysis/sampling.py
  Problem = namedtuple("Problem", ["equalities", "b", "inequalities", "bounds",
                      "variable_fixed", "variable_bounds", "nullspace",
"homogeneous"])

  def HRSampler(object):
	def __init__()
		...
		self.problem = self.__build_problem()
		...
    def __build_problem(self):
        ""Build the matrix representation of the sampling problem.""
        # Set up the mathematical problem
        prob = constraint_matrices(self.model, zero_tol=feasibility_tol)
        # check if there any non-zero equality constraints
		equalities = prob.equalities  

* cobra.util.array.py: constraint_matrices		

    Returns
    -------
    collections.namedtuple
        A named tuple consisting of 6 matrices and 2 vectors:
        - "equalities" is a matrix S such that S*vars = b. It includes a row
          for each constraint and one column for each variable.
        - "b" the right side of the equality equation such that S*vars = b.
        - "inequalities" is a matrix M such that lb <= M*vars <= ub.
          It contains a row for each inequality and as many columns as
          variables.
        - "bounds" is a compound matrix [lb ub] containing the lower and
          upper bounds for the inequality constraints in M.
        - "variable_fixed" is a boolean vector indicating whether the variable
          at that index is fixed (lower bound == upper_bound) and
          is thus bounded by an equality constraint.
        - "variable_bounds" is a compound matrix [lb ub] containing the
          lower and upper bounds for all variables.
    
"""
#create
optgp.problem.equalities
optgp.problem.inequalities
optgp.problem.homogeneous=False
optgp.problem.nullspace
optgp.problem.b
optgp.problem.bounds=np.array([])
optgp.problem.variable_fixed
optgp.problem.variable_bounds
optgp.problem.index #?
optgp.problem.count #?


len(m.metabolites) #761
len(m.reactions) #1075
optgp.problem.equalities.shape #(762, 2150)

#################################################
#################################################
# test original optGpSampler
# http://cs.ru.nl/~wmegchel/optGpSampler/#install-python.xhtml

#export OPTGPSAMPLER_LIBS_DIR=/home/aurelien/local/bin/optGpSampler-1.1/optGpSampler
#export LD_LIBRARY_PATH={$LD_LIBRARY_PATH}:/home/aurelien/local/bin/optGpSampler-1.1
###########################
# metabolic model
import sys
sys.path.append('extern')
import optGpSampler.optGpSamplerPython as osp
import optGpSampler.optReduceModelPython as orm
from optGpSampler.cbModel import CbModel
import optGpSampler.test as t 

t.testReduceModel('glpk')
t.testSampleModel('glpk')

###########################
# under the form A,b,lb,ub;  with "p"-files 
# copied from cbModelSampler.py:
# load
m = CbModel.load('models','Ecoli_MFA_reduced.p')
# params	
warmupPts= np.ndarray(shape=(0,0),dtype='d') 
nSamples = 10000;
nSteps = 50;
solverName = "glpk";
nThreads = 2; 
verbose= 1
# rescale bounds
lb_resc,ub_resc,scaleFactor = scaleBounds(m.lower_bounds,m.upper_bounds)
[warmupPts, samplePts] =  osp.optGpSamplerPython(m.S.todense(), lb_resc,
												ub_resc, m.objective_coefficients,m.b,
												warmupPts, nSamples, nSteps, solverName, nThreads, verbose)
# rescale back to original bounds
samplePts = samplePts * scaleFactor		
# plot
plt.hist(samplePts[:,0])
									
###########################
# under the form A,b,lb,ub; with mat models
# load model
import cobra.test
from cobra.util import constraint_matrices
import os
from os.path import join
fname = join("/home/aurelien/local/git/Metabolic-EP/","Ec_iJR904.mat")
model=cobra.io.load_matlab_model(fname, variable_name="Ec_iJR904")
feasibility_tol = 1e-6
prob = constraint_matrices(model, zero_tol=feasibility_tol)
S = prob.equalities
b = prob.b
var_bounds = np.atleast_2d(prob.variable_bounds)
objective_coefficients = np.zeros(S.shape[1])
lb_resc,ub_resc,scaleFactor = scaleBounds(var_bounds[:,0],var_bounds[:,1])
# params	
warmupPts= np.ndarray(shape=(0,0),dtype='d') 
nSamples = 10#0;
nSteps = 1#25;
solverName = "glpk";
nThreads = 2; 
verbose= 1
# sample
[warmupPts, samplePts] =  osp.optGpSamplerPython(S, lb_resc,
												ub_resc,objective_coefficients,
												b,
												warmupPts, nSamples, nSteps, solverName, nThreads, verbose)
# rescale back to original bounds
samplePts = samplePts * scaleFactor		

###############################"
# comparaison with MetabolicEP 
mu,s,av,va,lb,ub=MetabolicEP_run(fname)
# plot
i=10 
MetabolicEP_plot(i,mu,s,av,va,lb,ub,samplePts)

###########################
# model= bmw
import transmat
import network		
# model params
fname="/home/aurelien/local/git/sfc_proba/data/eurostat_network.h5"		
nb,nf,nw=3,30,100
p_bmw  = {'r': 0.03,'alpha0':0.1 , 'alpha1':0.75,
	'alpha2':0.01,'delta' :0.1,'k':6, 'M':10.}	
Mtot =p_bmw['M']
L_invest= 2 *nf
L_cons_firm=3*nf
L_cons_hh = 4 *nw
# create matrices
A,B,C,D,E,FF=network.get_network_matrix_block(year='2010',geo='CZ',
											nb=nb,nf=nf,nw=nw,fname=fname,
											L_invest=L_invest , L_cons_firm=L_cons_firm , L_cons_hh = L_cons_hh)		
S,b,lb,ub = transmat.CSP_bmw_FiCM(nb,nf,nw,A,B,C,D,E, p_bmw, M=Mtot)
# sampler params	
warmupPts= np.ndarray(shape=(0,0),dtype='d') 
nSamples = 100#0;
nSteps = 2#25;
solverName = "glpk";
nThreads = 2; 
verbose= 1
objective_coefficients = np.zeros(S.shape[1])
#reduce
metabolites= np.asarray(range(S.shape[0]),dtype="S").tolist()
reactions= np.asarray(range(S.shape[1]),dtype="S").tolist()
tolerance= 1e-6
[S_r, lb_r, ub_r, c, b_r, mets, rxns, fixedRxnIds, fixedFluxes] = orm.reduceModelPython(S.todense(), lb, 
            ub, objective_coefficients, b.flatten(), metabolites, reactions, tolerance, solverName, verbose)
  
# sample
lb_resc,ub_resc,scaleFactor = scaleBounds(lb,ub)
[warmupPts, samplePts] =  osp.optGpSamplerPython(S.todense(), lb_resc,
												ub_resc,objective_coefficients,
												b.flatten(),
												warmupPts, nSamples, nSteps, solverName, nThreads, verbose)
# rescale back to original bounds
samplePts = samplePts * scaleFactor		

